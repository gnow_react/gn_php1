//localhost
//faceBookAppId=304641059719468;

//Live
faceBookAppId=577459282421311;

//Live
//faceBookAppId=384930311662517;
//faceBookAppId=1757853841102145;

function FBLogin() {
//	refreshLoginHeader();
}

function initFB() {
	if(typeof FB != 'undefined' && FB != null) {
		FB.init({
			appId: faceBookAppId,
			status: true, // check login status
			cookie: true, // enable cookies to allow the server to access the session
			xfbml: true, // parse XFBML
			oauth: true
		});
	}else{return;}
}

/*-----------------------------facebook login code -----------------------------*/
$(document).ready(function(){
	 initFB();		
	//refreshLoginHeader();
	$('.facebookLoginWindow').unbind("click").click(function(e){
			//(".fbmessage").removeClass("errorMsg").html("");
			e.preventDefault();
		//if($("#fbChk").is(":checked")){
			fbConnect();	
		//}else{
		//	$(".fbmessage").addClass("errorMsg").html("You must agree to our Terms and Conditions")
		//}
	});
	$('.facebookLoginWindowCampaign').unbind("click").click(function(e){
		e.preventDefault();
		fbConnectCampaign();	
	});
	$('.googleLoginWindow').unbind("click").click(function(e){
	//	$(".fbmessage").removeClass("errorMsg").html("");
		e.preventDefault();
	//	if($("#fbChk").is(":checked")){
			//var _gPlusCallBack=$(this).attr("rel").split(",")[0];
			//var _clientId=$(this).attr("rel").split(",")[1];
			gPlusConnect();			
		//}else{
		//	$(".fbmessage").addClass("errorMsg").html("You must agree to our Terms and Conditions")
	//	}
	});
	$('.googleLoginWindowCampaign').unbind("click").click(function(e){
			e.preventDefault();
				gPlusConnectCampaign();			
	});
});

function fbLogout(){
	$.cookie("doautologin", null, { path: '/' });
	$.cookie("doautologin", false, { path: '/' });
	window.location.href="/control/logout";
}

function refreshLoginHeader() {
    $.ajax({url: '/control/loginheader', cache: false, 
    	success: function(data) {
    		$('#loginHeader').html(data);
    		//doFacebookLogin();    		
    	}
    });
    
	jQuery.ajax({url: '/control/cartheader',
	    cache:false,
		success: function(data) {
			var timer2 = setInterval(function() {
        			if(document.getElementById("cartheader") != null) {
        			    document.getElementById("cartheader").innerHTML = data;
        				clearInterval(timer2);
        			}
        		}, 500);
	}});    	
}

function reloadPage() {
	/*
	$('.servermsgsuccess').remove();
	if(window.location.href.indexOf("ssoActivatio") != -1)
		window.location.href="http://gift.shop.indiatimes.com";
	else
		window.location.reload();
		*/
	//refreshLoginHeader();		
}

loginAttemptCount = 0;

function doFacebookLogin() {
	var isLoginEnabled = isAutoLoginEnabled();

	if(isLoginEnabled) {
		if(document.getElementById("logoutdiv") == null) {
			//FB.getLoginStatus allows you to determine if a user is logged in to Facebook.
			
			FB.getLoginStatus(function(response) {
	            if (response.status == 'connected') {
	            	//handle the server side login failed error, two attempts will be taken to auto login
	            	if(loginAttemptCount < 1) {
	            		loginAttemptCount++;
	            		fbConnect();
	            	}
				}else {
	            	//alert('Facebook user is unauthorised or not logged in.');
	            }
			});
		}		
	}	
}

function fbConnect() {
//	window.open(getFacebookURL(''), '', getWindowSetting());
	window.location.href=getFacebookURL('');
}

function fbConnectCampaign() {
	var location = window.location;	
	var callBackURL = location.protocol + "//" + location.host + "/control/fbhandler?campaign=x";
	document.cookie = "fb_callHost=" + location.host;	
	var facebookURL = "https://www.facebook.com/v2.2/dialog/oauth?client_id="+faceBookAppId+"&redirect_uri=" + callBackURL + "&scope=email,user_birthday,user_about_me,user_photos";

	window.location.href=facebookURL;
}

function fbConnectCheckout() {
//	window.open(getFacebookURL('true'), '', getWindowSetting());
	window.location.href=getFacebookURL('true');
}

function getFacebookURL(isCheckoutConnect) {
	var location = window.location;	
	var callBackURL = location.protocol + "//" + location.host + "/control/fbhandler";
	if(isCheckoutConnect != '') {
		callBackURL = location.protocol + "//" + location.host + "/facebookresponse/showCheckoutShippingAddress/";
	}
	
	document.cookie = "fb_callHost=" + location.host;
	//var facebookURL = "https://graph.facebook.com/oauth/authorize?client_id="+faceBookAppId+"&display=popup&redirect_uri=" + callBackURL + "&scope=email,offline_access,user_birthday,user_about_me,user_photos";	
	var facebookURL = "https://www.facebook.com/v2.2/dialog/oauth?client_id="+faceBookAppId+"&redirect_uri=" + callBackURL + "&scope=email,user_birthday,user_about_me,user_photos";

return facebookURL;
}

function gPlusConnect(callbackUri,clientId) {
	var location = window.location;	
//staging
	var gplusclientId = "525498281262-7neqcitj55hpkd8htf6p4m51vv8vjv9u.apps.googleusercontent.com";
//LIVE
	//var gplusclientId = "359768235770-cnqqmusuul5tljfoda0dgb74g2ltt2jd.apps.googleusercontent.com";
	//var gplusclientId = "414327965620-j06vcimbd2ms96ipp0ng0ur4den2nnc7.apps.googleusercontent.com";
	var protocol = "https:";
//Localhost
//    var gplusclientId = "359768235770-nessoegemm6u3r8j10p7otvbpp56nq0a.apps.googleusercontent.com";
	
	var callBackURL = location.protocol + "//" + location.host + "/control/gplushandler";
	var w = screen.width/2;
	var h = screen.height/2;
	var LeftPosition = (screen.width) ? ((screen.width-w)/2+200) : 0;
	var TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
	var settings ='height=380,width=670,top=250,left=315,scrollbars=1,resizable=0,status=1';
	var location = window.location;

    var gplusbookURL="https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email&state=%2Fprofile&redirect_uri="+callBackURL+"&response_type=code&client_id="+gplusclientId+"&approval_prompt=force&access_type=offline";
	//var gplusbookURL = "https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.email&state=%2Fprofile&redirect_uri=http://www.shopping.indiatimes.com/oauth2callback&response_type=code&client_id=881749807958.apps.googleusercontent.com&access_type=offline";
    //var gplusbookURL ="https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/plus.login&state=%2Fprofile&redirect_uri=http://www.shopping.indiatimes.com/oauth2callback&response_type=code&client_id=881749807958.apps.googleusercontent.com&access_type=offline";
	//alert(gplusbookURL);
//    window.open(gplusbookURL,'',settings);
    window.location.href=gplusbookURL;
}

function gPlusConnectCampaign() {
	var location = window.location;	
	var gplusclientId = "359768235770-qjg80mjs9g9p7qcte5m1dt86k67ant5p.apps.googleusercontent.com";
	var protocol = "https:";
	var callBackURL = location.protocol + "//" + location.host + "/control/gplushandler?campaign=x";
	var w = screen.width/2;
	var h = screen.height/2;
	var LeftPosition = (screen.width) ? ((screen.width-w)/2+200) : 0;
	var TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
	var settings ='height=380,width=670,top=250,left=315,scrollbars=1,resizable=0,status=1';
    var gplusbookURL="https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email&state=%2Fprofile&redirect_uri="+callBackURL+"&response_type=code&client_id="+gplusclientId+"&approval_prompt=force&access_type=offline";
    window.location.href=gplusbookURL;
}


function getWindowSetting(height, width) {
	var w = screen.width/2;
	var h = screen.height/2;
	var LeftPosition = (screen.width) ? ((screen.width-w)/2+200) : 0;
	var TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
	var settings ='height=380,width=670,top=250,left=315,scrollbars=1,resizable=0,status=1';
	
	return settings;
}

function isAutoLoginEnabled() {
	var value =  $.cookie("doautologin");
	if (value == null || value == "" || value == "true") {
	    return true;
	} else {
	    return false;
	}
}
