$(document).ready(function(){
	var geo_country = $.cookie('geo_country');
	var geo_region = $.cookie('geo_region');
	if(isPrimeUser!=null){
		$("*[data-slot=256682]").remove();
	}else{
		$("body").removeClass("bodyctn");
	}
	
	 $("#mobile-casousel").owlCarousel({
	 	autoPlay: 3000, //Set AutoPlay to 3 seconds
	 	 navigation : true, // Show next and prev buttons
		 pagination: false,
		slideSpeed : 300,
		paginationSpeed : 400,
		navigationText : ["",""],
		singleItem:true
	 });
	$("#mobile-list, #mobile-list-recommended").owlCarousel({
	 	 pagination: false,
		 scrollPerPage : false,
		//  navigation : true, // Show next and prev buttons
		// navigationText : ["&lsaquo;","&rsaquo;"],
		// lazyLoad : true,
		 itemsCustom:[[300,1.2],[450,1.3],[600,2.2],[750,3.3]]
	 });
	 $("#additional-img").owlCarousel({
	 	 pagination: true,
		 navigation : false,
		 scrollPerPage : false,
		 autoHeight : true,
		 mouseDraggable:false,
		//  navigation : true, // Show next and prev buttons
		 navigationText : [" "," "],
		// lazyLoad : true,
		 itemsCustom:[[300,3],[450,3],[600,3],[610,2],[650,3],[800,3],[900,4]]
	 });
	 
	  $("#product-img-casousel").owlCarousel({
	 	autoPlay: false, //Set AutoPlay to 3 seconds
	 	navigation : false, // Show next and prev buttons
		pagination: true,
		lazyLoad : false,
		lazyFollow : true,
		lazyEffect : "fade",
		slideSpeed : 300,
		paginationSpeed : 400,
		navigationText : ["",""],
		startDragging : false,
		singleItem:true
	 });
	var owl = $("#product-gallery-casousel").owlCarousel({
	 	autoPlay: false, //Set AutoPlay to 3 seconds
	 	navigation : false, // Show next and prev buttons
		paginationNumbers: true,
		lazyLoad : false,
		lazyFollow : true,
		lazyEffect : "fade",
		slideSpeed : 300,
		paginationSpeed : 400,
		navigationText : ["",""],
		startDragging : false,
		singleItem:true
	 });
	 
	 $(".toi-owl-carousel").owlCarousel({
		pagination: false,
		 scrollPerPage : false,
		// navigation : true, // Show next and prev buttons
		//navigationText : ["&lsaquo;","&rsaquo;"],
		//lazyLoad : true,
		addClassActive: true,
		afterMove: moved,
		 itemsCustom:[[300,1.2],[400,1.3],[600,2.2],[750,2.3],[850,3.3]]
	 });
	
	  function moved(_this) {
		//var _this = this;
		//var owl = $(_this).data('owlCarousel');
		//$(".toi-owl-carousel").each(function(idx, value){
			var _cls = $(_this).attr("data-count");
			var _newClass = $(".nbtevent"+_cls);		
			_Ctnevent(_newClass);
			
		//});
			
	}
	function _Ctnevent(_findclass){
		var _n_array = _findclass.selector.split('.')		
		var _newClassName = _n_array[1];
		$("."+_newClassName).each(function(indx, obj){
				if($(obj).find(".owl-item").hasClass("active")){
					//setTimeout(function(){
						var _class = $(obj).find(".active");
						var _nclass = $(_class[0]).find(".colombiaad");
						var _id = $(_nclass).attr("id");
						if(typeof _id != 'undefined'){
							colombia.notifyColombiaAd(_id);							
							ctnAdcheck();
						}	
					//},0.5);
					
				}
			});
	}
	 $(".amazon-video-carousel").owlCarousel({
		pagination: false,
		 scrollPerPage : false,
		// navigation : true, // Show next and prev buttons
		//navigationText : ["&lsaquo;","&rsaquo;"],
		//lazyLoad : true,
		 itemsCustom:[[300,2.2],[400,2.3],[600,3.2],[750,3.3],[850,4.3]]
	 });
	 
	 $(".huawei-gallery").owlCarousel({
			nav: true,
			loop:true,
			autoplay : true,
		    autoplayHoverPause : true,
		    singleItem : true,
		    dots:true,
		    lazyLoad : true,
			items:1,
			navText: ["<span class='prev'></span>","<span class='next'></span>"]
		 });
	 $("#toi-casousel").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			navigation : false, // Show next and prev buttons
			pagination: true,
			slideSpeed : 300,
			paginationSpeed : 400,
			autoWidth:true,
			singleItem:true
	});
	
	var _owllength = $("#product-img-casousel .owl-pagination .owl-page").size();
	
	if(_owllength > 16){
		$("#product-img-casousel .owl-pagination .owl-page:gt(15)").addClass("displaynone");
	}
	
	$("body").on("click", ".mobile-ui-nav", function(e){
			e.preventDefault();
			$(".mobile-top-list").addClass("displayblock").removeClass("displaynone");
			$(".container").removeClass("mobile-top-list-in").addClass("mobile-top-list-out").removeClass("refine-top-list-in");
			$(".refine-search-ui").addClass("displaynone").removeClass("displayblock");
			$(".product-zoom-img").addClass("displaynone").removeClass("displayblock");
			$(".floating").addClass("flo-displaynone").removeClass("flo-displayblock");
	
	});
	$("body").on("click",".toi-login",function(e){
		e.preventDefault();
		e.stopPropagation();
		if($(".user-activity-menu").hasClass("displaynone")){
			$(".user-activity-menu").removeClass("displaynone");
			$(".mobile-ui-search").addClass("displaynone");
			$(".toi-sub-menu").addClass("displaynone").removeClass("displayblock");
			if($(".toi-more a").hasClass("active")){
					$(".toi-more").removeClass("arrowupactive");
					$(".toi-more a").removeClass("active");
			}
			if($(".toi-search").is(":visible")){
				$(".toi-search").css("display","none");
			}
		}else{
			$(".user-activity-menu").addClass("displaynone");
		}
		$(".toi-categories-list").css("display","none");
		
	});
	$("body").on("submit","form[name=bidwinnerform]",function(){
		return evalForm(this);		
	});	
	$("body").on("click","#toi-shop-button",function(e){
		e.preventDefault();
		_this = this;
		if($("#toi-more-button a").hasClass("active")){
				$(".toi-more").removeClass("arrowupactive");
				$(".toi-more a").removeClass("active");
		}
		if($("#toi-menu-shop").hasClass('displaynone')){
				$("#toi-menu-shop").removeClass("displaynone").addClass("displayblock");
		}else{
			$("#toi-menu-shop").removeClass("displayblock").addClass("displaynone");
		}
		if($("#toi-menu-more").hasClass('displayblock')){
				$("#toi-menu-more").removeClass("displayblock").addClass("displaynone");
		}
		if($(_this).hasClass("arrowupactive")){
			$(_this).removeClass("arrowupactive");
			$(_this).find("a").removeClass("active");
		}else{
			$(_this).addClass("arrowupactive");
			$(_this).find("a").addClass("active");
		}
		$(".user-activity-menu").addClass("displaynone");
		$(".toi-categories-list").css("display","none");
	});
	
	$("body").on("click","#toi-more-button",function(e){
		e.preventDefault();
		e.stopPropagation();
		_this = this;
		if($("#toi-shop-button a").hasClass("active")){
				$(".toi-more").removeClass("arrowupactive");
				$(".toi-more a").removeClass("active");
		}
		if($("#toi-menu-more").hasClass('displaynone')){
				$("#toi-menu-more").removeClass("displaynone").addClass("displayblock");
		}else{
			$("#toi-menu-more").removeClass("displayblock").addClass("displaynone");
		}
		if($("#toi-menu-shop").hasClass('displayblock')){
				$("#toi-menu-shop").removeClass("displayblock").addClass("displaynone");
		}
		if($(_this).hasClass("arrowupactive")){
			$(_this).removeClass("arrowupactive");
			$(_this).find("a").removeClass("active");
		}else{
			$(_this).addClass("arrowupactive");
			$(_this).find("a").addClass("active");
		}
		$(".user-activity-menu").addClass("displaynone");
		$(".toi-categories-list").css("display","none");
		//$("#toi-shop-button").click();
	});

	
	$("body").on("click",".toi-search-tab",function(e){
		e.preventDefault();
		$(".toi-search").slideToggle("slow");
		if($(".toi-search").show()){
			$("input[name=SEARCH_STRING]").focus();
		}
		if($(".toi-sub-menu").hasClass('displayblock')){
				$(".toi-sub-menu").addClass("displaynone");
				// if($(".toi-more a").hasClass("active")){
					// $(".toi-more").removeClass("arrowupactive");
					// $(".toi-more a").removeClass("active");
				// }
			}
		e.stopPropagation();
		$(".user-activity-menu").addClass("displaynone");
		$(".toi-categories-list").css("display","none");
		
	});
	$("body").on("click",".question-section dl a.readmore",function(e){
		e.preventDefault();
		$(".question-box").css("display",'block');
		$(this).css("display","none");
	});
	$("body").on("click",".rating-tab-open a.readmore",function(e){
		e.preventDefault();
		$(".reviewshow").css("display",'block');
		$(this).css("display","none");
	});
	
	
/*	$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
		if (scroll >= 5) {
			$(".header-menu").slideUp('slow');
			$(".header-menu").addClass("closed");
			$(".container").removeClass("mobile-top-list-in");
			
			if($(".container").hasClass("refine-top-list-in")){
				$(".container").removeClass("refine-top-list-in");
			}
			if($(".toi-sub-menu").hasClass('displayblock')){
				$(".toi-sub-menu").addClass("displaynone");
				if($(".toi-more a").hasClass("active")){
					$(".toi-more").removeClass("arrowupactive");
					$(".toi-more a").removeClass("active");
				}
			}
		}else{
			$(".header-menu").slideDown("fast");
			$(".header-menu").removeClass("closed");
		}
	}); //missing );
	*/	
	
	$("body").on("click","a.tab-t0-active",function(e){
		e.preventDefault();		
		if($(this).hasClass("active")){
			$(this).removeClass("active").addClass("inactive");	
		}else{
			$(this).addClass("active").removeClass("inactive");
		}
		$(".toi-categories-list").toggle();
	});
	
	
	$("body").on("click",".mobile-ui-cart",function(){
		window.location.href="/control/showcart";
	});
	
	$("body").on("click",".servererror",function(){
		$(this).addClass("displaynone");
	});
	
	$("body").on("click",".successmsg",function(){
		$(this).addClass("displaynone");
	});
	/*  */
	$("body").on("click",".remove-animation",function(){
		$(".container").removeClass("zeroheight");
		$(".user-activity-menu").addClass("displaynone");
		$(".floating").removeClass("flo-displaynone").addClass("flo-displayblock");
	});
	$("body").on("click",".mobile-list-cancel-ui", function(e){
			e.preventDefault();
			$(".container").addClass("mobile-top-list-in").removeClass("mobile-top-list-out").addClass("refine-top-list-in");
			$(".list-find-ui").removeClass("displaynone");
			$(".mobile-top-list nav dl dd").removeClass("displayblock").addClass("displaynone");
			$(".mobile-top-list nav dl dt").removeClass("mobile-back-ui-list").addClass("listprivew");
			$(".mobile-ul-ui-list").addClass("mobile-ul-ui-list-none");
	});
	$("body").on("click",".mobile-top-list nav dl dt.top-list-view-ui",function(e){
		e.preventDefault();
		_autoscrollwithCl2();
		$(".list-find-ui").addClass("displaynone");
		$(this).closest(".list-find-ui").removeClass("displaynone");
		$(this).addClass("mobile-back-ui-list").removeClass("listprivew");
		$(this).nextAll("dd").addClass("displayblock").removeClass("displaynone");
		
	});
	$("body").on("click","nav dl dt.mobile-back-ui-list",function(e){
		e.preventDefault();
		$(".list-find-ui").removeClass("displaynone");
		$(".mobile-top-list nav dl dd").removeClass("displayblock").addClass("displaynone");
		$(this).removeClass("mobile-back-ui-list").addClass("listprivew");
		$(".mobile-ul-ui-list").addClass("mobile-ul-ui-list-none");
		
	});
	$("body").on("click","a.catalogue-list-ui",function(){
		var _catalouge = $(this).attr("data-catlouge");
		var _category = $(this).attr("data-category");
		$(".mobile-ui-nav").click();
		$(".list-find-ui").find(".top-list-view-ui[data-catlouge='"+_catalouge+"']").click();		
		$(".list-find-ui").find(".top-list-view-ui[data-catlouge='"+_catalouge+"']").closest(".list-find-ui").find(".mobile-collapse-ui[data-category='"+_category+"']").click();
	});
	$("body").on("click",".list-find-ui .mobile-collapse-ui",function(e){
		e.preventDefault();
		_autoscroll(this);
	});
	$("body").on("click",".mobile-collapse-ui", function(e){
		e.preventDefault();
		if($(this).next(".mobile-ul-ui-list").hasClass("mobile-ul-ui-list-none")){
			$(this).removeClass("mobile-collapse-ui-plus").addClass("mobile-collapse-ui-minu").next(".mobile-ul-ui-list").removeClass("mobile-ul-ui-list-none");
			//$(this).removeClass("mobile-collapse-ui-plus").addClass("mobile-collapse-ui-minu");
		}else{
			$(this).removeClass("mobile-collapse-ui-minu").addClass("mobile-collapse-ui-plus").next(".mobile-ul-ui-list").addClass("mobile-ul-ui-list-none");
		}
		if($(".mobile-body-nav-view-more-list").hasClass("mobile-body-nav-view-less")){
			$(".mobile-body-nav-view-more-list").removeClass("mobile-body-nav-view-less").addClass("mobile-body-nav-view-more");
		}		
	});
	
	$('.mobile-body-nav-list dl dt.cat_list-ui:lt(5)').removeClass("displaynone");
	var _listsize = $(".mobile-body-nav-list dl dt a.catalogue-list-ui").size();
	if(_listsize<5){
		$("dt a.mobile-body-nav-view-more-list").addClass("displaynone");
	}
	$("body").on("click",".mobile-body-nav-view-more-list", function(e){
		e.preventDefault();
		_autoscrollwithCl();
		$('.mobile-body-nav-list dl dt.cat_list-ui:lt('+_listsize+')').addClass("displayblock").removeClass("displaynone");
		$(".mobile-body-nav-view-more-list").addClass("displaynone");
		$(".mobile-body-nav-view-less-list").removeClass("displaynone");
	});
	$("body").on("click",".mobile-body-nav-view-less-list", function(e){
		e.preventDefault();
		$('.mobile-body-nav-list dl dt.cat_list-ui:lt('+_listsize+')').removeClass("displayblock").addClass("displaynone");
		$('.mobile-body-nav-list dl dt.cat_list-ui:lt(5)').removeClass("displaynone");
		$(".mobile-body-nav-view-more-list").removeClass("displaynone");
		$(".mobile-body-nav-view-less-list").addClass("displaynone");
	});
	
	/* for categories explore categories */
	$('.mobile-body-nav-list dl dt.cat_list-ui:lt(5)').removeClass("displaynone");
	var _categorylistsize = $(".mobile-body-nav-list dl dt a.categoires-list").size();	
	if(_categorylistsize<5){
		$("dt a.mobile-body-nav-view-more").addClass("displaynone");
	}
	$("body").on("click",".mobile-body-nav-view-more-list-c", function(e){
		e.preventDefault();
		_autoscrollwithCl();
		$('.mobile-body-nav-list dl dt.cat_list-ui:lt('+_categorylistsize+')').addClass("displayblock").removeClass("displaynone");
		$(".mobile-body-nav-view-more").addClass("mobile-body-nav-view-less-list-c").removeClass("mobile-body-nav-view-more-list-c");
	});
	$("body").on("click",".mobile-body-nav-view-less-list-c", function(e){
		e.preventDefault();
		$('.mobile-body-nav-list dl dt.cat_list-ui:lt('+_categorylistsize+')').removeClass("displayblock").addClass("displaynone");
		$('.mobile-body-nav-list dl dt.cat_list-ui:lt(5)').removeClass("displaynone");
		$(".mobile-body-nav-view-more").removeClass("mobile-body-nav-view-less-list-c").addClass("mobile-body-nav-view-more-list-c");
	});
	/* for categories explore categories */
	$("body").on("click",".mobile-ui-search-button",function(e){
		e.preventDefault();
		if($(".mobile-ui-search").hasClass("searchdisplaynone")){
			$(".mobile-ui-search").removeClass("searchdisplaynone");
			$(".user-activity-menu").addClass("displaynone");
		}else{
			$(".mobile-ui-search").addClass("searchdisplaynone");
		}
	
	});
	
	$("body").on("click",".sort-articles .filter-sort",function(){
		if($(".sort-articles dl").hasClass("closed")){
			$(".sort-articles dl").removeClass("closed");
		}else{
			$(".sort-articles dl").addClass("closed");
		}
	});
	
		$("body").on("submit","form[name=searchform]",function(){
			return evalForm(this);			
		});
		/*$("body").on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",".refine-top-list-in, .mobile-top-list-in", function(){
			$(".container").removeClass("zeroheight");
		}); 
		*/
	/* rating pdp */	
	$('.rating input').change(function(){
	  var _radio = $(this);
	  var _radio_val = $(this).val();
	  var _productId = $('input[name=productId]').val();
	  var _userId = $('input[name=userId]').val();
	  $(".rating input[type=radio]").prop('checked', false);
	  $(this).prop('checked', true);
	  $('.rating .selected').removeClass('selected');
	  _radio.closest('label').addClass('selected');
	  $("input[name=productRating]").val(_radio_val);
	  var url = "/control/submitR";		      
		$.ajax({
			url:url,
			type:'POST',
			data:{"productRating":_radio_val,"userLoginId":_userId,"productId":_productId},
			success:function(data){
					
			},
			error:function(){
				//alert("Please retry.");
			},
		});
	});
	/* $("body").on("click",".rating-tab li",function(){
		$(".rating-tab li").removeClass("active");
		$(this).addClass("active");
		var _data_name = $(this).attr("data-name");
		$(".rating-tab-open").addClass("displaynone");
		$('.'+_data_name).removeClass("displaynone");
	}); */
	/* rating pdp */	
	$("body").on("submit","form[name=trackorder]",function(){
		/* Universal GA*/
		var _orderNo = $("#ordertrackid").val();
		var _orderemailid = "email";
		var _ordermobile = "mobile";
		if(_orderemailid!=''){
			_ga_attr_track = "track_order##"+_orderNo+"##show_status|"+_orderemailid+"##0";
		}else if(_ordermobile!=''){
			_ga_attr_track = "track_order##"+_orderNo+"##show_status|"+_ordermobile+"##0";
		}
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
		/* Universal GA*/
	   	return checkorderfun(this);
	});
	
	var _pass_total = $("#grand_total_id").val();
	$("#pass-grand-total").html(_pass_total);
	
	var _pass_cod_charges = $("#cod_charges").val();
	$("#pass-cod-charges").html(_pass_cod_charges);
	
	var _total_shipping = $("#totalshippingprice").val();
	var _pass_shipping_charges = $("#shipping_charges").val();
	if(_total_shipping == 0)
	{
		$("#shippingText").html("");
		$("#totalshipping").removeClass("offer-price-ui");
	}else
	{
		$("#shippingText").html("Includes Shipping ");
		$("#totalshipping").html(_total_shipping + " &amp; ");
		$("#totalshipping").addClass("offer-price-ui");
	}
/*	alert(_pass_shipping_charges);
	if(_pass_shipping_charges != undefined && _pass_shipping_charges != ""){
		$("#pass-shipping-charges").html(_pass_shipping_charges);
		$("#txt_shipping").html("Includes Shipping");
		}else
			{
			$("#txt_shipping").html("Includes Shipping");
			}*/
	//getLoginHeaderData();
	setHeaderLogo();
	getLoginSSOData();
	
	if(geo_continent!="EU" && geo_country!="US" && geo_region!="CA"){
		initLogin();
	}
	initShipping();
	_initCart();
	createVariantList();
	_initFortgotPassword();
	initInfinteScroll();
	initFilters();
	applyLazyLoad();
	_productPage();	
	initGAtracking();
	sizeSelectioninit();
	_initMyAccount();
	checkorderfun();
	initPasswordRecovery ();
	myprofile();
	utmInit(); 
	giftCoupon();
	bulkOrder();
	checkForWishlist();
	userReview();
	customerComment();
	contactUs();
	QuestionAndAnswer();
	onClickScroll();
	adsCheck();
	if(paramsObj.leafTitle =='dealofferArticle'){
		PerpetualScroll();
	}
	
	if(paramsObj.leafTitle=='compare'){
		compare();
	}
	if(paramsObj.leafTitle=='amazingdeals' || paramsObj.leafTitle=='mobile-offers' || paramsObj.leafTitle=='4gmobileoffer' || paramsObj.leafTitle=='bestBatteryPhones' || paramsObj.leafTitle=='best-selling-accessories'){
	_AmazingDeal();
	}
	if(paramsObj.leafTitle=='product' && paramsObj.showAmazonProduct!=true){
		getDateDelivery();
		//_amazon_timer();
	}
	if(paramsObj.leafTitle=='thankyou'){
	createThankuPageAlert();
	}
	setTimeout('ctnAdcheck()',500);
	if (window.location.hash == '#_=_') {
	    window.location.hash = ''; // for older browsers, leaves a # behind
	    if (window.history && history.pushState) history.pushState('', document.title, window.location.pathname); // nice and clean
	    e.preventDefault(); // no page reload
	}
	
	function onClickScroll(){
		 $("body").on("click",'.clickscroll',function(e){
			 e.preventDefault();
			_get_id = $(this).attr("data-target");
			$('html, body').animate({
				scrollTop: $("#"+_get_id).offset().top-50
			}, 2000);
		 });
	}
	if(paramsObj.leafTitle=='product'){
		_trackCTN();
		_addCompare();	
		if(geo_continent!="EU" && geo_country!="US" && geo_region!="CA"){
			social_share();	
		}
		if(paramsObj.instock == true){
			var fixmeTopbuy = $('.pdp-price').offset().top-46; 
			$(window).scroll(function() {                  // assign scroll event listener
				var currentScroll = $(window).scrollTop(); // get current position
				if (currentScroll >= fixmeTopbuy) {           // apply position: fixed if you
					$(".pdp-price").addClass("floating");
					if(paramsObj.PageProductId==paramsObj.DealProductId){
						$(".deals-timer").addClass("floating-banner");
					}
					
				} else {                                   // apply position: static
					$(".pdp-price").removeClass("floating");
					if(paramsObj.PageProductId==paramsObj.DealProductId){
						$(".deals-timer").removeClass("floating-banner");
					}
					
				}
			});
				  // get initial position of the element
		}
		var fixmeTop = $('.mh-4').offset().top;
		$(window).scroll(function() {                  // assign scroll event listener
				var currentScroll = $(window).scrollTop(); // get current position
				if (currentScroll >= fixmeTop) {           // apply position: fixed if you
					$(".blue-strip").addClass("floating-heading");					
				} else {                                   // apply position: static
					$(".blue-strip").removeClass("floating-heading");
				}
			});
	}
	/* pdp buy button floating
	$("body").on("click",".floating",function(e){
		e.preventDefault();
		//$(".buy-button").click();
	});
	*/
  $("body").on("click",".register-campaign form button",function(){
		$("#regGenderSelect").val($(this).attr("data-gender"));
	});
	$("body").on("submit",".register-campaign form[name=campaignRegisterUser]",function(){
		var _this=this;
		var _truefrom = evalForm(_this);
		if(_truefrom==true){
		var _event = "registration_pop_up_mobile##submit##"+paramsObj.leafTitle+"##0";
		var val = _event.split('##');
		_gaq.push(['_trackEvent',val[0],val[1],val[2],parseInt(val[3]),true]);
		dataLayer.push({'event': 'event', 'eventCat': val[0], 'eventAct': val[1], 'eventLbl': val[2], 'eventVal': val[3]});
		return true;
		}
		return false;
	});
	$("body").on("click","#register-tnc-link",function(){
		$("#register-tnc-list").slideToggle();
	});
	
	if(paramsObj.footer==true){
		$("footer").addClass("pdp-footer");
	}
	
	$("[data-initiator-id]").each(function(indx, obj){
		if($(obj).attr("data-initiator-id") != undefined){
			$("#"+ $(obj).attr("data-initiator-id")).attr("data-initiator","");
		}
	});
	$("body").on("click","*[data-initiator]",function(e){e.stopPropagation();})
	$("body").on("click",function(e){		   
		 $("*[data-initiator-id]:visible").each(function(idx, obj){
			 if($(e.target).closest("*[data-initiator-id]:visible").length < 1){
				$("#"+$(obj).attr("data-initiator-id")).trigger("click");
			 }
		 });
		 
		
		  
	});	
	/*$("body").on("click",".tabs",function(){
		var _rel = $(this).attr("rel");
		$(".abc").css("display","none");
		$("#"+_rel).css("display","block");
		$('html, body').animate({
			scrollTop: $("#"+_rel).offset().top - 55
		}, 2000);
	});
	*/
	$("body").on("click","a.tab", function(e){
		e.preventDefault();
		_this = this;
		$("a.tab").removeClass("active");
		$(_this).addClass("active");
		$(".helpbottom").css("display","none").removeClass("border-bottom");
		$(_this).next(".helpbottom").css("display","block").addClass("border-bottom");
		_autonewscroll(_this);
	});
	$("body").on("click",".accordion h3",function(){
		$(".accordion div").css("display","none");
		$(this).next("div").css("display","block");
		_autonewscroll(_this);
	});
});

function showRegistrationCampaignPopUp(){
	setTimeout(function(){
		if($("#showRegistrationCampaign").val()=="true" &&  $.cookie("showRegistrationCampaign") == undefined){
			 $("body").append("<div class='overlay opacity'></div><div class='conformation registration-popup'><a class='close' data-ga='registration_pop_up_mobile##pop_up_close##"+paramsObj.leafTitle+"##0' data-ga-track='registration_pop_up_mobile##pop_up_close##"+paramsObj.leafTitle+"##0' href='#'>X</a><div class='msg clr'>Sign Up And Get  <span class='offer-price-ui'>500 off</span> on Your First Purchase </div><div class='img-container'><img src='/mobile/img/shopping-tag-cloud.jpg' /></div><div class='oveflowhidden'><button data-ga='registration_pop_up_mobile##pop_up_shown##"+paramsObj.leafTitle+"##0' data-ga-track='registration_pop_up_mobile##pop_up_shown##"+paramsObj.leafTitle+"##0' class='yes'>Register Now</button></div></div>");
			 $("body").on("click",".registration-popup a.close",function(e){
				e.preventDefault();
				e.stopPropagation()
				$(".conformation.registration-popup, .overlay").remove();
				var date = new Date();
				 var minutes = 120;
				 date.setTime(date.getTime() + (minutes * 60 * 1000));
			  	$.cookie("showRegistrationCampaign","false",{expires : date ,path : '/'});
			 });
			 $("body").on("click",".conformation.registration-popup",function(e){
				 e.preventDefault();
				window.location.href="/registerCampaign/"; 
			 });
		}
	},2000);
}

function initPasswordRecovery (){
	$(".forgotpassword .disabled input").attr("disabled","disabled");
	$("body").on("submit",".password-recovery-popup form",function(){
		var _this = this;
		var _getid = $("#registeredemail").val();
		history.pushState("", "", "forgotpassword?emailId="+_getid);
		if (evalForm(_this)) {
		$(".response-message").html("").removeClass("success").removeClass("error");
		 $(".forgot-password-submit").attr("disabled","disabled");	
		 //$(this).closest("#TB_window").append('<span class="ajaxloader">loading</span>');
		 $("body").append("<div class='overlay opacity'></div><div class='loading'>Loading..</div>");
		 if($(_this).attr("name")=="forgotform" || $(_this).attr("name")=="otpform"){	
			 $.ajax({
			 	url:_this.action,
			 	dataType:'json',
			 	type:_this.method,
			 	data:$(_this).serializeArray()
			 	}).done(function(response){
			 		if(response.responseCode=="success"){
				 		$(_this).addClass("disabled");
				 		$(_this).find("input").attr("disabled","disabled");
				 		$(_this).next("form").removeClass("disabled");
				 		$(_this).next("form").find("input").removeAttr("disabled");
				 		$(_this).next("form").find(".response-message").addClass("success").html(response._EVENT_MESSAGE_);
				 		if($(_this).next("form").attr("name") == "otpform"){
				 			$("#authCode").val(response.ssoAuthCode);		
				 			$("#loginId").val(response.encodedLoginId);
				 			if(response.isMobNoAvailable=="false"){
				 				$("form[name=changepasswordform]").find(".update-mobile-number").removeClass("disabled");
				 				$("form[name=changepasswordform] input").removeAttr("disabled");
				 			}
				 		}
				 		if($(_this).next("form").attr("name") == "changepasswordform"){
				 			$("#USERNAMENEW").val($("#USERNAME").val());
				 		}
			 		}
			 		if(response.responseCode=="error"){
			 			$(_this).find(".response-message").addClass("error").html(response._EVENT_MESSAGE_);
			 			if(response.isOtpExpired != undefined && response.isOtpExpired=="Y"){
			 				$(_this).find(".response-message").append("<a id='resendOTP' href='#'>Resend verification code</a>")
			 			}
			 			
			 		}
				 }).fail(function(xhrObj,xhrStatus,xhrError){
				 	//alert("fail");					 
				    console.log(xhrError);	
				 }).always(function(){
				 	$(".forgot-password-submit").removeAttr("disabled");
				 	//$("#TB_window").find('.ajaxloader').remove();
				 	$(".overlay, .loading").addClass('displaynone');
				 	//$(".overlay").remove();				 	
				 });
			}
		  if($(_this).attr("name")=="changepasswordform"){
		  	 return true;
		  }		
		}
		return false;
	});
	$("body").on("click","#resendExpiredOtp",function(e){$("#resendOTP").click()});
	$("body").on("click","#resendOTP",function(e){
		e.preventDefault();
		$(this).closest("form").addClass("disabled");
		$(".password-recovery-popup form[name=requestOTPform]").removeClass("disabled");
		$(".password-recovery-popup form[name=forgotform] input").removeAttr("disabled");
		$(".password-recovery-popup form[name=forgotform]").submit();
	});
	$('body').on('keyup','#conf_newPassword', function () {
		var _newpass = $("#newPassword").val().length;
		var conf_pass = $("#conf_newPassword").val().length;;
		if (_newpass <= conf_pass) {	
			//return evalForm($(this).closest("form"));
			$('#conf_newPassword_error').html('').removeClass("error");
			$('#conf_newPassword').removeClass("onError");
			if ($(this).val() != $('#newPassword').val()) {
				$('#conf_newPassword_error').html('Both passwords do not match').addClass("error");
				$('#conf_newPassword').addClass("onError");
			}
			
		}
	});
	
	$('body').on('keyup','#inputOtp', function () {
		var _newOtp = $("#inputOtp").val().length;
		if (_newOtp == 4) {	
			$(this).closest("form").submit();	
		}
	});
	
	$("body").on("submit","form[name=forgotform_1]",function(){
		return	evalForm(this);
		
    });
//	$("body").on("click",".forgot-password-submit",function(e){
//		//e.preventDefault();
//		//$(".password-recovery-popup form:not(.disabled)").submit();
//	});
	$("body").on("click","form[name=registeredUser] a.forgot-password",function(e){
		e.preventDefault();
		var _email_elem=$("#registeredemail").val();
		var _redirect_url="";
		if(_email_elem!=""){_redirect_url=$(this).attr("href")+"?emailId="+_email_elem;}else{_redirect_url=$(this).attr("href");}
		window.location.href=_redirect_url;
	});
}
$("body").on("submit","form[name=feedbackform],form[name=otpform]",function(){
		return	reportIssueSubmit(this);
		
});

/* report an issue submit*/
function reportIssueSubmit(reportIssueForm){
	var cancelled_ordernumber = $(".order .value").text();
	var cancelled_ordername = $("#orderItemSeqId option:selected").text();
	_gaq.push(['_trackEvent','Request Cancellation',cancelled_ordernumber,cancelled_ordername,0,true]);
 var _validate_form= evalForm(reportIssueForm);
 if(_validate_form==true){  
  var _msg="";
  switch($("#custRequestCategoryId").val()){
  case "REQUEST_WEB_CASE":
	  reportIssueForm.submit();
	  addAjaxLoading();
	  break;
  case "REQUEST_CANCELLATI":
	  var _confirm_box = new confirmBox();
	  _msg="Are you sure you want to cancel your order?";
	  _confirm_box.setMessage(_msg);
	  _confirm_box.confirm=function(){
		addAjaxLoading();
	    reportIssueForm.submit();
	  };
	  break;
 default:
	 addAjaxLoading();
 	reportIssueForm.submit();
	 break;
  } 
 } 
  return false;
}


function initGAtracking(){
$("body").on("click","*[data-ga]",function(){
	var _addbuy = $(this).attr("data-ga");
	var val = _addbuy.split('##');
	_gaq.push(['_trackEvent',val[0],val[1],val[2],parseInt(val[3]),true]);
});
}
function initShipping(){
	$("body").on("click",".select-address .more-link", function(e){
		e.preventDefault();
		$(this).closest(".select-address").toggleClass("all-address");
	});
	$("body").on("click",".checkout-ui .unregistered", function(e){
		$(".register-content").removeClass("displaynone");
	});
	$("form[name=createAddress] input").focus(function(){
		$(".register-content").removeClass("displaynone");
	});
	$("body").on("submit","form[name=createAddress]",function(){
		var _this=this;
		var _truefrom = evalForm(_this);
		if(_truefrom==true){
			$('form[name=createAddress] select[name=state]').removeAttr("disabled");
			addAjaxLoading();
			return ture;
		}
		return false;
	});
	$("body").on("click",".edit-cancel",function(e){
		e.preventDefault();
		//$(this).addClass("displaynone");
		$("form[name=createAddress]").trigger("reset");
		$(".select-address .section-container-ui-edit .edit-overlay").css("display",'none');
		$(".select-address .section-container-ui-edit").removeClass("section-container-ui-edit").addClass("section-container-ui");
		var _dat_text= $("form[name=createAddress] button[type=submit]").attr("data-text");
		$("form[name=createAddress] button[type=submit]").html(_dat_text);
		$(".register-content").addClass("displaynone");
		$(".unregistered").html("ENTER A NEW ADDRESS").addClass("displaynone");
		$("form[name=createAddress] input[name=addredit]").val("false").attr('disabled','disabled');
		_autoscrollwithCl();
	});
	$("body").on("click", ".select-address .section-container-ui", function(e){
		e.preventDefault();
		id=$(this).attr('id').substr(7);
		if($(e.target).hasClass("addr-edit-link")){
			$(".select-address .section-container-ui-edit .edit-overlay").css("display",'none');
			$(".select-address .section-container-ui-edit").removeClass("section-container-ui-edit").addClass("section-container-ui");
			id_edit=$(this).attr('id').substr(7);
			//$(".edit-cancel").removeClass("displaynone");
			$(".checkout-ui .unregistered").click();
			$("form[name=createAddress] input[name=pincode]").val($(this).closest(".select-address .section-container-ui").find(".postalCode").html()).trigger("change");
			$("form[name=createAddress] input[name=city]").val($(this).closest(".select-address .section-container-ui").find(".city").html());
			$("form[name=createAddress] input[name=mobileno]").val(parseInt($(this).closest(".select-address .section-container-ui").find(".contactNumber").val()));
			$("form[name=createAddress] textarea[name=address]").val($(this).closest(".select-address .section-container-ui").find(".address1").html());
			$("form[name=createAddress] input[name=name]").val($(this).closest(".select-address .section-container-ui").find(".toName").html());
			$(this).closest(".select-address .section-container-ui").find(".edit-overlay").css("display",'inline-block');
			$(this).addClass("section-container-ui-edit").removeClass("section-container-ui");
			var _dat_edit_text = $("form[name=createAddress] button[type=submit]").attr("data-edit-text");
			$("form[name=createAddress] button[type=submit]").html(_dat_edit_text);
			$(".unregistered").html("EDIT ADDRESS").removeClass("displaynone");
			$(".register-content").removeClass("displaynone");
			$("form[name=createAddress] input[name=addredit]").val(id_edit).removeAttr('disabled','disabled');
			_autoscroll($("#top3"));
		}else{
			if(id){
				$("#name").val($(this).find(".toName").html());
				$("#pincode").val($(this).find(".postalCode").html());
				$("#city").val($(this).find(".city").html());
				$("#state").val($(this).find(".state").html());
				
				$("#address").val($(this).find(".address1").html());
				$("#shiptoadd").val(id);
				document.forms.shipAddress.submit();
				addAjaxLoading();
			}
		}
	});
	
	$("body").on("click",".recipientbox",function(){
		if ($('.recipientbox').is(":checked")){
				$(".recipiename").addClass("displaynone");}
		else{
			$(".recipiename").removeClass("displaynone");
		}
	});	 
	
}
$(".check-carousel").each(function(){
	_this = this;
	var _carousel_list_count = $(_this).find(".newcarousel li").length; 
	var _list_width = $(_this).find(".newcarousel").attr("data-width-list");
	$(_this).find(".newcarousel").css("width",_carousel_list_count*_list_width);
});

function _autoscroll(obj){
	$('html, body').animate({
		scrollTop: $(obj).offset().top
	}, 2000);
}
function _autonewscroll(obj){
	$('html, body').animate({
		scrollTop: $(obj).offset().top - 55
	}, 2000);
}
function _autoscrollwithCl(){
	$('html, body').animate({
		scrollTop: $("#top").offset().top
	}, 2000);
}
function _autoscrollwithCl2(){
	$('html, body').animate({
		scrollTop: $("#top2").offset().top
	}, 2000);
}
function initLogin(){
		$("body").on("click",".login ul.tabs-wrapper li a", function(e){
			e.preventDefault();
			$(".login ul.tabs-wrapper li").removeClass("active");
			$(this).closest("li").addClass("active");
			$(".tab-content").addClass("displaynone");
			$($(this).attr("href")).removeClass("displaynone");
			if($("form input").hasClass("onError")){
				$("input").removeClass("onError");
				$("span").removeClass('errorMsg').html("");
			}
		});
		
		if(paramsObj.isExistingUserDiv=="true"){
			$(".login ul.tabs-wrapper li a").click();
		}
		$("body").on("click",".login .unregistered button, p.addnew", function(e){
			$(".register-content").removeClass("displaynone");
			$(".unregistered").removeClass("displaynone");
			$(".separator").removeClass("displaynone");
				  _autoscroll('.unregistered');
			//_autoscroll('.unregistered');
			if($("form input").hasClass("onError")){
				$("input").removeClass("onError");
				$("span.error").removeClass('errorMsg').html("");
			}
			$("form[name=createAddress]").trigger("reset");
			$("form[name=createAddress] input[name=addredit]").val("false").attr('disabled','disabled');
			
		});
		

		//$("body").on("submit","form[name=registerUser],form[name=guestUser]",function(){
		//	return evalForm(this);
		//});
		$("body").on("submit","form[name=registeredUser]",function(){
			
			var _checkloginform = evalForm(this);
			if(_checkloginform == true){
				var hostname = window.location.hostname;
				var queryparam = window.location.search;
				var SSO_LOGIN_URL = "https://jsso.indiatimes.com/sso/crossdomain/genericLogin";
				var nru = hostname + "/control/loginSSO"+queryparam;
				var ru = hostname + "/control/loginSSO"+queryparam;
				
				var siteId= "bf4cd04f2b25f36787a30e068bc5a6a3";
				var loginId=$("#registeredemail").val(); 
				var passwd=$("#registeredpassword").val();				
				var finalURL = SSO_LOGIN_URL+"ru="+encodeURI(ru)+"&nru="+encodeURI(nru)+"&login="+loginId+"&passwd="+passwd+"&siteId="+siteId;
				$(this).append("<input type='hidden' name='login' value='"+loginId+"' />");
				$(this).append("<input type='hidden' name='passwd' value='"+passwd+"' />");
				$(this).append("<input type='hidden' name='ru' value='"+encodeURI(ru)+"' />");
				$(this).append("<input type='hidden' name='nru' value='"+encodeURI(nru)+"' />");
				$(this).append("<input type='hidden' name='siteId' value='"+siteId+"' />");
				$(this).attr("action",SSO_LOGIN_URL);
				//window.location=finalURL;
				return true;
			}
			//return false;
		});
}

function checkorderfun(ordertrackForm) {
					$("#validcheck").val("");
					
					
					if($("#ordertrackid").val() == "Order No."){
						$("#ordertrackid").val("");
					}
					if($("#ordertrackid").val() !== "Order No." && $("#ordertrackid").val() != ""){
						if($("#phonemobile").val() == "Contact No."){
							$("#phonemobile").val("");
						}
						if($("#checkeordermail").val() == "Email ID"){
							$("#checkeordermail").val("");
							$("#checkeordermail").focus();
						}
					}
					
					if($("#phonemobile").val() != "" && $("#phonemobile").val() != "Contact No."){
						$("#validcheck").val($("#phonemobile").val());
					}
					if($("#checkeordermail").val()!="" && $("#checkeordermail").val()!="Email ID"){
						$("#validcheck").val($("#checkeordermail").val());
					}
					var _validate = evalForm(ordertrackForm);
					//alert(_validate);
					if(_validate==false){
						return false;
					}	
					//alert(alert(23));
					return true;
				} 

function _initFortgotPassword(){
	$("body").on("submit","form[name=forgotform], form[name=resetpasswordorm]",function(){
			return evalForm(this);
	});
}
/* active my wishlist tab */
function _wishlisttabactive(){
	if($('.carttab').hasClass('active')){
		$('.carttab').removeClass('active');
		$('.shoppingcart').addClass('displaynone');
		$('.shoppingwishlist').removeClass('displaynone');
		$('.wishlisttab').addClass('active');
	}
	
}
function _initCart(){
	$("body").on("submit",$("form[name='cart-form']"), function(){
		if($("select.change-quantity[data-promotion]").val() > 1){
    	 	alert("The quantity for freecharge cannot be more than one");
    	 	return false;
    	 }
		 addAjaxLoading();
		 return true;
	});
	
	$("body").on("click",".cart-header span",function(){
		$(this).addClass("active");
		var _data_id = $(this).attr("rel");
		$('.shoppingwishlist').html("<div style='padding:50px; text-align:center;'><img src='/mobile/img/AjaxLoader.gif'></div>");
	   if(_data_id == "shoppingwishlist")
		   {
		      var url = "/control/showWishList";		      
				$.ajax({
					url:url,
					type:'POST',
					dataType:'html',
					contentType:"application/x-www-form-urlencoded",	
					success:function(data){
							$('.shoppingwishlist').html(data);
							applyLazyLoad();
					},
					error:function(){
						//alert("Please retry.");
					},
				});
		     // $("#testname").submit();
				$(this).siblings("span").removeClass("active");
				$(".tab-content").addClass("displaynone");
				$("."+_data_id).removeClass("displaynone");
				$(".gccalculator").closest("section").css("display","none");
		   }else
			   {
		$(this).siblings("span").removeClass("active");
		$(".tab-content").addClass("displaynone");
		$("."+_data_id).removeClass("displaynone");
			$(".gccalculator").closest("section").css("display","block");
			   }
	});
	if(paramsObj.wishlistactive=="true"){
		_wishlisttabactive();
		$('.cart-header .checktab').click();
	}

	$("body").on("click","form.cart-form .remove-cart",function(){	
		var _productId = $(this).parent(".confirmbox").attr("data-productId");
		$("form.cart-form input[name='productid']").val(_productId);
		$("form.cart-form input[name='cart_action']").val("delete");
		/* Universal GA*/
		_ga_attr_track = $(".remove-ga-track").val();
		_enhanced_ga_attr_track = $(".remove-ga-track").attr("enhanced-data-ga");
		enhancedEcommerce(_enhanced_ga_attr_track);
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
		/* Universal GA*/
		var _title = $(this).parent(".confirmbox").find(".cart-product-box figcaption div").text();
		var _new_Confirm_box = new confirmBox();
		_msg="Are you sure you want to remove <span>"+_title+"</span> from your Cart?";
		_new_Confirm_box.setMessage(_msg);
		_new_Confirm_box.confirm=function(){
			addAjaxLoading();
			setTimeout(function(){
				updatecart();
			},10);
			
		}
	});
	$("body").on("click","form.wishlist-form .remove-cart",function(){	
		var _productId = $(this).parents(".product-box").attr("data-productId");
		var _wishListid = $(this).parents(".product-box").attr("data-wishListid");
		$("form.wishlist-form input[name='productid']").val(_productId);
		$("form.wishlist-form input[name='cart_id']").val(_wishListid);
		$("form.wishlist-form input[name='cart_action']").val("delete");
		var _title = $(this).parent(".confirmbox").find(".cart-product-box figcaption div").text();
		var _new_Confirm_box = new confirmBox();
		_msg="Are you sure you want to remove <span>"+_title+"</span> from your Wishlist?";
		_new_Confirm_box.setMessage(_msg);
		_new_Confirm_box.confirm=function(){
			addAjaxLoading();
			setTimeout(function(){
				updatwishlist();
			},10);
		}
		
	});	
	$("body").on("click",".seoreadmore",function(e){
		e.preventDefault();
		$(this).siblings(".seocontent").css("height","auto");
		$(this).html("show less").addClass("showless");
	});
	$("body").on("click",".showless",function(e){
		e.preventDefault();
		$(this).siblings(".seocontent").css("height","");
		$(this).html("read more...").removeClass("showless");
	})
	$("body").on("keyup",".redeem-input-span",function(e){
		e.preventDefault();
		if(e.keyCode == 13){
			$(".redeem-button-span").click();
		}
		});
	$("body").on("keypress",".redeem-input-span",function (e) {
		  if (e.which == 13) {
			$(".redeem-button-span").click();
			return false; 
		  }
	});
	$("body").on("click",".redeem-button-span",function(e){
		e.preventDefault();
		var _redeemcoupon = $(".redeem-input-span").val();
		/* Universal GA*/
		var _checksaving = "buynow_popu";
		if(paramsObj.leafTitle=="thankyou"){
			_checksaving = "paymentpage";
		}
		_ga_attr_track = _checksaving+"##check_savings##"+_redeemcoupon+"##0"
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
		/* Universal GA*/
		if(_redeemcoupon != undefined  &&  _redeemcoupon!=null && (_redeemcoupon.length>0))
		{
			$(".couponredeem").val(_redeemcoupon);
			$("form[name=redeem_coupon]").submit();
		}else{
			$("#GC_error").addClass("error").removeClass("displaynone");
		//	addAjaxLoading();
		}
		
	});
	
	$("body").on("click",".couponcodelist ul li a",function(){
				
				var _gcid = $(this).attr("data-value");
				if(_gcid != undefined  &&  _gcid!=null && (_gcid.length>0))
				{
					$(".couponredeem").val(_gcid);
					$("form[name=redeem_coupon]").submit();
				}
				$(".couponcodelist ul li").addClass("displaynone");
				$(this).closest("li").addClass("active");
				$(".couponcodelist ul li:last-child").removeClass("displaynone").css("display","block");
	});
	$("body").on("click",".tryotherCoupon",function(){
		/*$(".couponcodelist ul li").removeClass("displaynone");
		$(".couponcodelist ul li").removeClass("active");
		$(this).closest("li").css("display",'none');*/
		window.location.href="/control/showcart?clearGc=true";
	});
	
	$("body").on("click",".clear-button-span",function(e){
		e.preventDefault();
			$("form[name=redeem_coupon]").submit();
	});
	$("body").on("click",".reviewreadmore",function(e){
		e.preventDefault();
		$(this).closest(".userreview").addClass("readmore");
		_autonewscroll(this);
	});
	
	/*$("body").on("click",".offer-text-cart-product",function(){
		var _redeemcoupon = $(this).find(".cart-coupon").text();
		var _productId = $(this).parents(".product-box").attr("data-productId");
		$("form.redeem_coupon input[name='productid']").val(_productId);
		$(".couponredeem").val(_redeemcoupon);
		$("form[name=redeem_coupon]").submit();
	});*/
	$("body").on("click",".move-to-cart a",function(){		
		var thisObj=$(this);
		var _productId = $(thisObj).parents(".confirmbox").attr("data-productId");
		var _productName = $(thisObj).parents(".confirmbox").attr("data-productName");
		
		url="/control/inventoryCheck?productId="+_productId;
		$.ajax({
			url:url,
			type:"GET",
			dataType:"json",
			success:function(data){
				if(data.inStock=="true"){
					var _cartitemseqid = $(thisObj).parents(".product-box").attr("data-cartitemseqid");
					var _wishListid = $(thisObj).parents(".product-box").attr("data-wishListid");
					$("form.wishlist-form input[name='cart_item_seq_id']").val(_cartitemseqid);
					$("form.wishlist-form input[name='cart_id']").val(_wishListid);
					$("form.wishlist-form input[name='productName']").val(_productName);
					$("form.wishlist-form input[name='cart_action']").val("move");
					updatwishlist();
					addAjaxLoading();
				}else{
					$(thisObj).parents(".move-to-cart").addClass("displaynone");
					$(thisObj).parents(".product-box").find(".out-of-stock").removeClass("displaynone");
				}
			},
			error:function(){
				alert("Please retry.");
			},
		});
	});
	
	$("body").on("click",".move-to-wishlist a",function(){
		var thisObj=$(this);
		var _productId = $(thisObj).parents(".confirmbox").attr("data-productId");
		$("form.cart-form input[name='productid']").val(_productId);
		$("form.cart-form input[name='cart_action']").val("move-to-wishlist");
		updatecart();
		addAjaxLoading();
	});

	
	$(".change-quantity").on("change",function(){
		var _productId = $(this).parents(".confirmbox").attr("data-productId");
		var _quantity = $(this).val();
		var _catalog_name = $(this).attr("catalog-name");
		$("form.cart-form input[name='productid']").val(_productId);
		$("form.cart-form input[name='quantity']").val(_quantity);
		/* Universal GA*/
		_ga_attr_track = "buynow_popup##changequantity##"+_quantity+"|"+_catalog_name+"|"+_productId+"##0"
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
		/* Universal GA*/
		updatecart();
		addAjaxLoading();
	});
	
	function updatecart(){
		$(".cart-form").submit();
	}
		
	function updatwishlist(){
		$(".wishlist-form").submit();
	}
	$("body").on("change","form[name=createAddress] #pincode", function(){		
		getCityByPin($(this).closest("form"));
	});
	
	$("body").on("keyup","form[name=createAddress] #pincode",function(){
		var value = $(this).val();
		if ( value.length == 6){
			 getCityByPin($(this).closest("form"));
		} 
	});
}

/* Address Pin Code */
function getCityByPin(formObj){
	if($(formObj).find("#pincode").val().length==6 && $(formObj).find("#pincode").val().match(/^\d+$/)){	
			 	$.ajax({
					url:"/control/getCityStateFromZipCodeJSON",
					dataType:"json",
					data:{"postalCode":$(formObj).find("#pincode").val()},
					success:function(response){
					 if(response.cityName != null){
						$(formObj).find("#city").val(response.cityName.toLowerCase()).blur();
						$(formObj).find("#state").val(response.stateGeoId).blur();
						/*if(response.stdCode !=0){
							$(formObj).find("#contactLandline").val('0'+response.stdCode).blur();
							}else{
								$(formObj).find("#contactLandline").val("").blur()
								}
						*/
							$(formObj).find("#mobileno").focus();	
						}else{
						$(formObj).find("#city").val("").blur();
						$(formObj).find("#state").val("").blur();
						//$(formObj).find("#contactLandline").val("").blur();	
							
						}
					},
					error:function(){
						
					}					
				});	
		}	
}

/* Generic confirmation text box*/
function confirmBox(){
	 var _this = this;
	_this.confirm=function(){};
	_this.cancel=function(){};
	_this.setMessage=function(msg){ $('.msg').html(msg);};
	$("body").append("<div class='overlay opacity'></div><div class='conformation '><div class='msg'></div><div class='oveflowhidden'><button class='yes'>Yes</button><button class='no'>No</button></div></div>");
	$("body").on("click",".yes",function(){
		_this.confirm();
	});
	$("body").on("click",".no",function(){
		_this.cancel();
		$(".overlay,.conformation").remove();
		$("form.cart-form input[type=hidden], form.wishlist-form input[type=hidden]").val("");
	});
	
}

function createVariantList(){
			  $(".featureselect select").hide();	
			  $(".featureselect").hide();
			  $(".featureselect:eq(0)").show();					
			   var variantSelect = $(".featureselect:eq(0)").find("select")[0];
				if(variantSelect != undefined){
					   if(variantSelect.options.length > 1){
						    /*$(variantSelect).parent().append("<span class='selectsize'> "+ variantSelect.options[0].text +":</span>");*/
							 $(variantSelect).parent().append("<ul class='sizetab oveflowhidden'></ul>");
							  for(var i=1;i<variantSelect.options.length;i++){
								$(".featureselect:eq(0)").find("ul.sizetab").append("<li> <a href='#' rel="+i+" title=\"" + variantSelect.options[i].text + "\">"+ variantSelect.options[i].text +"</a> </li>")
							  }
						}
		}
}

 $(".featureselect select").change(function(e){
					getList(this.name, (this.selectedIndex-1), 1);
					if($(this).parent().next(".featureselect").length > 0){
					  $(this).parent().next(".featureselect").hide();	
					  $(this).parent().next(".featureselect").each(function(index,featureSelectDiv){
					  	$(featureSelectDiv).find(".sizetab").remove();
					  })
					  $(this).parent().next(".featureselect").show();
					  var _next_feature_select = $(this).parent().next(".featureselect").find("select")[0];
					  $(_next_feature_select).parent().append("<ul class='sizetab oveflowhidden'></ul>");
					  for(var i=1;i<_next_feature_select.options.length;i++){
						$(_next_feature_select).parent().find("ul.sizetab").append("<li> <a href='#' rel=\""+i+"\" title=\"" + _next_feature_select.options[i].text + "\">"+ _next_feature_select.options[i].text +"</a> </li>")
					  }
					  if(_next_feature_select.options.length==2){
					  	$(_next_feature_select).parent().find("a").addClass("active");
					  	$(_next_feature_select).change();
					  }	
					}
					//applywishlistPDP();
				});		 
					 


function getList(name, index, src) {
    currentFeatureIndex = findIndex(name);        
    if (currentFeatureIndex == 0) {           
        // set the drop down index for swatch selection
        document.forms["addform"].elements[name].selectedIndex = (index*1)+1;
    }
    if (currentFeatureIndex < (OPT.length-1)) {
        // eval the next list if there are more
        var selectedValue = document.forms["addform"].elements[name].options[(index*1)+1].value;
        if (index == -1) {
          if(paramsObj.featureOrderFirstExists==true){ var Variable1 = eval("list" +paramsObj.Variable1+"()");}
        } else {
            var Variable1 = eval("list" + OPT[(currentFeatureIndex+1)] + selectedValue + "()");
        }
        // set the product ID to NULL to trigger the alerts
        setAddProductId('NULL');
        // set the variant price to NULL
        setVariantPrice('NULL');
       // setVariantInventory('NULL');
    } else {
        // this is the final selection -- locate the selected index of the last selection
        var indexSelected = document.forms["addform"].elements[name].selectedIndex;
        // using the selected index locate the sku
        var sku = document.forms["addform"].elements[name].options[indexSelected].value;
        // set the product ID
        setAddProductId(sku);
        // set the variant price
        setVariantPrice(sku);
        // check for amount box
        //toggleAmt(checkAmtReq(sku));
       // setVariantInventory(sku);
    }    
    checkOptions(currentFeatureIndex);
    //changeWishListStatus();
}
function GetURLParameter(sParam){
				    var sPageURL = window.location.search.substring(1);
				    var sURLVariables = sPageURL.split('&');
				    for (var i = 0; i < sURLVariables.length; i++){
				        var sParameterName = sURLVariables[i].split('=');
				        if (sParameterName[0] == sParam){ return sParameterName[1];}
				    }
}
function sizeSelectioninit(){
		if($(".featureselect .sizetab a").length > 0 && GetURLParameter('size') != undefined && GetURLParameter('size') !=null && GetURLParameter('size') != ""){
			$(".featureselect .sizetab a[title="+ GetURLParameter('size') +"]").click();
		}
	}
function setAddProductId(name) {
    document.addform.add_product_id.value = name;
    if (document.addform.quantity == null) return;
    if (name == '' || name == 'NULL' || isVirtual(name) == true) {
        var elem = document.getElementById('product_id_display');
        var txt = document.createTextNode('');
        if(elem.hasChildNodes()) {
            elem.replaceChild(txt, elem.firstChild);
        } else {
            elem.appendChild(txt);
        }
    } else {
        var elem = document.getElementById('product_id_display');
        var txt = document.createTextNode(name);
        if(elem.hasChildNodes()) {
            elem.replaceChild(txt, elem.firstChild);
        } else {
            elem.appendChild(txt);
        }
    }
}

function findIndex(name) {
    for (i = 0; i < OPT.length; i++) {
        if (OPT[i] == name) {
            return i;
        }
    }
    return -1;
}


 function checkOptions(currentFeatureIndex){
	 var _dd_array = $(".featureselect select");
	 if(_dd_array.length-1 > currentFeatureIndex){
	 	 //console.log(_dd_array[currentFeatureIndex+1].options.length);
	 	if(_dd_array[currentFeatureIndex+1].options.length==2){
   	 	    _dd_array[currentFeatureIndex+1].selectedIndex=1;
   	 	    getList(_dd_array[currentFeatureIndex+1].name,_dd_array[currentFeatureIndex+1].selectedIndex-1,1 )
   	 	}
	 }
}

 function isVirtual(product) {
    var isVirtual = false;
    if(paramsObj.virtualJavaScriptExists==true){
        for (i = 0; i < VIR.length; i++) {
            if (VIR[i] == product) {
                isVirtual = true;
            }
        }
    }
    return isVirtual;
}
		    
function setVariantPrice(sku) {
    $('.checkDeliverysla').html('');
    $('.checkDeliverylnk').css('margin-top','8px');
    if (sku == '' || sku == 'NULL' || isVirtual(sku) == true) {
        var elem = document.getElementById('variant_price_display');
        var txt = document.createTextNode(paramsObj.defaultVariantPrice);
        if(elem != undefined){
            if(elem.hasChildNodes()) {
                elem.replaceChild(txt, elem.firstChild);
            } else {
                elem.appendChild(txt);
            }
        }
    }
    else {
    	var sameDaySLA=getVariantSameDaySla(sku);
    	var tempSLA=parseInt(getVariantSla(sku));
    	var _pEmiVal=parseInt(getVariantEmi(sku));
    	//console.log(sku);
    	//console.log(_pEmiVal);
    	if(_pEmiVal > 0){
    	 $(".pemivalue").html(_pEmiVal);
    	 $(".buyfoot").show();
    	}else{
    	 $(".pemivalue").html('');
    	 $(".buyfoot").hide();        		
    	}
    	if(sameDaySLA=="true"){
    		$('.checkDeliverysla').html('Delivers in <span style="display:inline; font-size:11px" id="noofdays"> 2-4 business days*');
        	$('.checkDeliverylnk').css('margin-top','0px');
    	}else if(tempSLA>0)
        {
            $('.checkDeliverysla').html('Delivers in <span style="display:inline; font-size:11px" id="noofdays"> '+tempSLA +'-'+ (tempSLA+3)+' business days*');
        	$('.checkDeliverylnk').css('margin-top','0px');
    	}else{
    		$('.checkDeliverysla').html('Delivers in <span style="display:inline; font-size:11px" id="noofdays"> 7-10 business days*');
        	$('.checkDeliverylnk').css('margin-top','0px');
    	}
        var elem = document.getElementById('variant_price_display');
        var price = getVariantPrice(sku); // default price
        var shipprice=getVariantShipPrice(sku);
        var listprice=getVariantListPrice(sku);
        var variantStStdFeature = getVariantFeature(sku);
         $('.checkVariantStdFeature').html(variantStStdFeature);
        var txt = document.createTextNode(price);
        price=parseInt(price);
        shipprice=parseInt(shipprice);
        listprice=parseInt(listprice);
       if(paramsObj.whitelableExists){
            var convRate = parseFloat(paramsObj.convRate);
            price = Math.ceil(price/convRate);
            shipprice = Math.ceil(shipprice/convRate);
            listprice = Math.ceil(listprice/convRate);
        }
        if(shipprice > 0) // set shipping price
            {
            	$('.pdp-save .freeshipping').html('Shipping Charges: <span class="offer-price-ui">'+shipprice+'</span>');
            }
            else
            {
            	$('.pdp-save .freeshipping').html('Free Shipping');
            }
        //listprice=listprice+shipprice;
        //price=price+shipprice;
        var yousave=listprice-price;
        if(yousave>0){
            var discount=(yousave/listprice)*100;
            discount=parseInt(discount);
            }
        var emiThreeMonth = parseInt(price/3);
        var emiSixMonth = parseInt(price/6);
        var emiTwelveMonth = parseInt(price/12);
        if(price<3000){
        		$('.pdp-save p.emistr').hide();
        }else{
	        	$('.pdp-save p.emistr').html('EMI starting at <span class="offer-price-ui">&nbsp;'+emiTwelveMonth.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "</span>")).show();
        	}
		$('.pdp-price').html('<span class="offer-price-ui">&nbsp;'+price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "")+'</span>&nbsp;<span class="mrp-price-ui">&nbsp;'+listprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "")+'</span>');
		//$('.pdp-price').html('<span class="mrp-price-ui">&nbsp;'+listprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "</span>"));
		$('.pdp-save p.yousave').html('You Save&nbsp;<span class="offer-price-ui">'+yousave.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span> ('+discount+'%)');
		if(listprice==0 || price>listprice || price==listprice){$('.pdp-save p.yousave, .pdp-price span.mrp-price-ui').hide();}
        else{$('.pdp-save p.yousave, .pdp-price span.mrp-price-ui').show();}
		//$('.specstabprice').html(price);
		//$('.productsummary .priceinfo .pricesaving').html(discount+"%");
	
         if(elem!=null && elem.hasChildNodes()) {
            elem.replaceChild(txt, elem.firstChild);
        } else if(elem!=null){
            elem.appendChild(txt);
        }
    }
    $("#upsellloc").val(sku);
    $('a.changePinCode').click();
}

function _variantform(){
	$("form.featureselect").submit();
	return false;	
}
function _productPage(){
	$("body").on("click",".size-chart a",function(){
		$("form.sizechart").submit();
	});
	$("body").on("click", ".moreclasstext",function(e){
		e.preventDefault();
		/* Universal GA*/
		var _ga_get_value = $(this).attr("data-ga-get");
		var _ga_attr_track ='';
		/* Universal GA*/
		if($(".container .body-pdp .about-product-details-ui .detail-text").hasClass("displaynone")){
			$(this).addClass("moreclasstextless");
			$(".container .body-pdp .about-product-details-ui .detail-text").addClass("displayblock").removeClass("displaynone");
			/* Universal GA*/
			 _ga_attr_track = "view_more_summary##"+_ga_get_value;
			/* Universal GA*/
		}else{
			$(this).removeClass("moreclasstextless");
			$(".container .body-pdp .about-product-details-ui .detail-text").removeClass("displayblock").addClass("displaynone");
			/* Universal GA*/
			_ga_attr_track = "view_less_summary##"+_ga_get_value;
			/* Universal GA*/
		}
		/* Universal GA*/
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
		/* Universal GA*/
		_autonewscroll(this);
	});
	$("body").on("click", ".moreclass",function(e){
		e.preventDefault();
		if($(".moreclass").hasClass("lessclass")){
			$(".moreclass").removeClass("lessclass")
		}else{
			$(".moreclass").addClass("lessclass");
		}
	
		/* Universal GA*/
		var _ga_get_value = $(this).attr("data-ga-get");
		var _ga_attr_track ='';
		/* Universal GA*/
		if($(".container .body-pdp .about-product-table-ui table").hasClass("displaynone")){
			$(".container .body-pdp .about-product-table-ui table").removeClass("displaynone");
			/* Universal GA*/
			 _ga_attr_track = "view_more_details##"+_ga_get_value;
			/* Universal GA*/
		}else{
			$(".container .body-pdp .about-product-table-ui table").addClass("displaynone");
			/* Universal GA*/
			_ga_attr_track = "view_less_details##"+_ga_get_value;
			/* Universal GA*/
		}
		/* Universal GA*/
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
		/* Universal GA*/
		_autonewscroll(this);
	});
	$("body").on("click",".product-zoom-close",function(e){
		e.preventDefault();
	});
	$("body").on("click",".sizetab li a",function(e){
		e.preventDefault();
		var _feature_select = $(this).parents(".sizetab").siblings("select")[0];
		_feature_select.selectedIndex=$(this).attr("rel");
		$(this).parent().siblings().find("a").removeClass("active");
		$(this).addClass("active");
		$(_feature_select).change();
		
		// need to check $isPresentInWishlist
		checkForWishlist();
	});


	$("body").on("click",".buy-button",function(e){
		e.preventDefault();
		var getProductId=$('#add_product_id').val();
		var getProductName=$('#product_name').val();
		var getCatalogId=$('#catalog_id').val();
		getProductId=$.trim(getProductId);
		if(getProductId=="" || getProductId==null || getProductId=='NULL')
		{  			    
			$(".featureselect select").each(function(index, _obj){
				if($(_obj).attr('rel') != undefined && $(_obj).attr('rel') != null &&  $(_obj).val() ==''){
				_feature_name = $(_obj).attr('rel').toLowerCase();
				$(_obj).addClass("onError");
				 return false;
				}
				
			})
			alert('Please choose appropriate features');
		}
		else
		{
			addAjaxLoading();
			setTimeout(function(){
				var buyurl="";
				var buyurl="/control/additem?add_product_id="+getProductId+"&catalogId="+getCatalogId+"&productName="+getProductName;
				window.location.href=buyurl;
			},10);
			
			
			
		}
				
		//_variantform();		
	});
	
	$("body").on("click",".addtowishlist",function(e){
		e.preventDefault();
		var getProductId=$('#add_product_id').val();
		getProductId=$.trim(getProductId);
		if(getProductId=="" || getProductId==null || getProductId=='NULL')
		{  			    
			$(".featureselect select").each(function(index, _obj){
				if($(_obj).attr('rel') != undefined && $(_obj).attr('rel') != null &&  $(_obj).val() ==''){
				_feature_name = $(_obj).attr('rel').toLowerCase();
				$(_obj).addClass("onError");
				 return false;
				}
				
			})
			alert('Please choose appropriate features');
		}
		else
		{
			window.location.href="/control/addtowishlist?add_product_id="+getProductId;
		}
		//_variantform();		
	});
	
	var _dateimg = $(".product-preview-img img").attr("data-image");
	$(".product-zoom-img .clickable").css("display",'none');
	$(".product-preview-img img").attr('src',_dateimg);
	if($(".zoomimg").attr('data-image')==""){
		$(".zoomimg").attr('data-image',_dateimg);
	}
	
	$("body").on("click",".additional-img figure a",function(e){
		e.preventDefault();
		var _datesmallimg = $(this).attr("href");
		$(".product-preview-img img").attr('src',_datesmallimg);
		$(".zoomimg").attr('data-image',(_datesmallimg.indexOf('/medium/')==-1?_datesmallimg:_datesmallimg.replace("/medium/","/large/")));
		var _zompositon = $(this).attr('data-position');
		//alert(_zompositon);
		$(".zoomimg").attr('data-position',_zompositon);
		
	});
	$("body").on("click","#scbcribeUser",function(){
		var _true = evalForm(this)
		if(_true==true){
			addAjaxLoading();			
		}else{
			return false;
		}		
	});
	$("body").on("click",".zoomimg", function(e){
		e.preventDefault();		
		var _zoomimg = $(this).attr('data-image');
		var _zoomimg_position = $(this).attr('data-position');
		/*$(".product-zoom-img figure img").attr('src',_zoomimg);
		$(".product-zoom-img figure img").attr('src',_zoomimg);*/
		$(".mobile-top-list").removeClass("displayblock").addClass("displaynone");
		$(".product-zoom-img").removeClass("displaynone").addClass("displayblock").removeClass("zindex"); 
		$(".container").removeClass("refine-top-list-in").addClass("refine-top-list-out").removeClass("mobile-top-list-in");
		$(".product-zoom-img .clickable").css("display","block");
		$(".floating a.flat-orange").css("display",'none');
		owl = $("#product-gallery-casousel").data('owlCarousel');
		owl.jumpTo(_zoomimg_position);
		productZoomGallery();
	//console.log(_imgpath_array);
	 
	});
	$( window ).resize(function() {
		setTimeout(productZoomGallery, 500);
	});
	function productZoomGallery(){
		_imgpath_array = [];
		$("#product-gallery-casousel .owl-item img").each(function(){
			var _imgpath = $(this).attr("data-small-img");
			_imgpath_array.push(_imgpath);
		});
		var _ar = $("#product-gallery-casousel .owl-numbers");
		$.each( _imgpath_array, function( indx, val ) {
		  $(_ar[indx]).html( "<img src='" + val + "'/>" );
		});
	}
	$("body").on("click",".product-zoom-close", function(){
		$(".container").addClass("refine-top-list-in").removeClass("refine-top-list-out").addClass("mobile-top-list-in");
		$(".floating a.flat-orange").css("display",'block');
		$(".product-zoom-img").addClass("zindex"); 
	});
	
	$("body").on("submit","form[name='notification'], form[name='comingsoonForm']", function(){
		_notifyme(this);
		return false;
	});
	function gaInit(){
		/* Universal GA*/
		var _ga_value = $("input[name=ga_pin]").val();
		var _ga_pin_value = $("input[name=pincode]").val();
		var _ga_attr_track = "check_delivery##"+_ga_pin_value+"##"+_ga_value+"##"+0;
		//alert(_ga_attr_track);
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
		/* Universal GA*/
	}
	if ($.cookie('PINCODE')!=undefined) {
		$("#pincode").val($.cookie('PINCODE'));
		CheckDeliveryCod($("form[name='checkLocationReqForm']"));
		$(".deliverytime").css("display","none");
		gaInit();
	}
	$("body").on("click",".checkpinbutton", function(){
		CheckDeliveryCod($("form[name='checkLocationReqForm']"));
		gaInit();
		return false;
	});
	
	$("body").on("click",".changePin", function(e){
		e.preventDefault();
		$(".shippedbytab").addClass("displaynone");
		$("form[name='checkLocationReqForm']").removeClass("displaynone");
		$("#pincodeinput_error").text("");
		$('.reponsdata').html('').removeClass("error");
	});
}		

function _notifyme(form){
	var _emaildata = $(".notifyinput").val();
		_emaildata =$.trim(_emaildata);
		$(".notifyinput").val(_emaildata);
	 var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 $('.reponsdata').html("").removeClass("error");
	 if( _emaildata=="") {
		$(form).find("#notifyinput_error").addClass("errorMsg").text("Please enter Email ID");
		 return false;
	  }else{
		$(form).find("#notifyinput_error").removeClass("errorMsg").text("").addClass("displaynone");
	  }
	  if( !emailReg.test( _emaildata ) ) {
		$(form).find("#notifyinput_error").addClass("errorMsg").text("Invalid email address.Please use a valid email address");
		return false;
	  } else {
		$(form).find("#notifyinput_error").removeClass("errorMsg").text("").addClass("displaynone");
		$.ajax({
			url:$(form).attr("action"),
			type:$(form).attr("method"),
			data:$(form).serialize(),
			
			success:function(data){ 
				if(data.trim() == "success"){
					$('.notification-tab').addClass('displaynone');
					$('.reponsdata').removeClass('displaynone').addClass('messge').html($('.notification-tab .messge').html()+'<br><br>An email has been sent to '+_emaildata+'.Please click on the verification link to get an alert once the product is available for you to order.');
				}else{
					$('.reponsdata').html("Sorry, "+data.ErrorMsg).addClass('error');
				}
			},
			error:function(jqXHR, textStatus){ 
			},
		});
	  } 
}

function CheckDeliveryCod(form){
	$(".shippedbytab").addClass("displaynone");
	var pincode = $(form).find("#pincode").val();
	var getProductId=$('#add_product_id').val();
	//var getProductId=$('#upsellloc').val();
	var _pincode=/^[\d]{6}$/i;

	if(getProductId==""){
		getProductId=$("select[name="+OPT[0]+"] option:eq(1)").val();
	}
	if( pincode=="") {
		$(form).find("#pincodeinput_error").text("Please enter Pincode");
		 return false;
	  }else{
		$(form).find("#pincodeinput_error").text("");
	  }
	  if( !_pincode.test( pincode ) ) {
		$(form).find("#pincodeinput_error").text("Invalid pincode.Please enter valid pincode");
		return false;
	  } else {
		$(form).find("#pincodeinput_error").text("");
		url="/control/checkDeliveryLocation?pinNumber="+pincode+"&productId="+getProductId;
		$.ajax({
		url:url,
		type:$(form).attr("method"),
		data:$(form).serialize(),
		dataType: 'json',
		success:function(data){
			$(".deliverytime").css("display","none");
			$(form).addClass("displaynone");
			$(form).find("#pincodeinput_error").text("");
			//alert(data.maxPermitQuantity);
			var _message = '';
			if(data.message=="Not Servicable" || data.city=="NOT FOUND"){
					$('.reponsdata').html("<li>We can't ship the product to the pincode "+data.pinNumber+" you've entered. Please try again from a different location.</li><li><a href='#' class='changePin'>Change Pincode</a></li>").addClass('error');
			}else if(data.maxPermitQuantity < 1){
				$('.reponsdata').html("<li>We can't ship the product to the pincode "+data.pinNumber+" you've entered. Please try again from a different location.</li><li><a href='#' class='changePin'>Change Pincode</a></li>").addClass('error');
			}else{
				express = " ";
				if(data.PRE_EXPRESS_DELIVERY=="Y"){
					express = " express ";
					_message = "for Prepaid Order only.";
				}
				$('.reponsdata').html("<li class=\"exp\"><span></span>Yes! We"+express+"deliver on the pincode "+data.pinNumber+".</li><li class=\"cod\"><span></span>"+($.inArray("Cash On Delivery", data.payMentOption)!=-1?"Cash on Delivery Available <b id=\"checkwallet\"></b></li>":"<i style='color:#f00'>Cash on Delivery not available!</i></li>")+"<li class='pinchanglist'><a href='#' class='changePin'>Change Pincode</a></li>");
				$(".shippedbytab").removeClass("displaynone");
				if(data.PRE_EXPRESS_DELIVERY=="Y"){
					$(".exp").after('<li><span></span>Free Express Delivery on Pre-paid orders</li>');
				}
			}
			if(data.CARD_WALLET=="Y"){$("#checkwallet").text('(Pay by Card or Wallet at Delivery)');}
			if(data.deliverydate){$('.cod').after('<li><span></span><b>'+data.deliverydate+'</b></li>');}
			
		},
		error:function(){
		alert("Please retry.");
		},
		});
	  } 
}

/* check to wishlist on PDP  */	  
function checkForWishlist(){
  var getProductId=$('#add_product_id').val();
	getProductId=$.trim(getProductId);
	if(getProductId!="")
	{  			    
		url="/control/isInWishlist?productId="+getProductId;
		$.ajax({
			url:url,
			type:'post',
			dataType: 'json',
			contentType:"application/x-www-form-urlencoded",
			success:function(data){
				if(data.SuccessMsg!=null && data.SuccessMsg=="yes"){
					$('.checkwishlistpdp').addClass("addedtowishlist").removeClass("addtowishlist");
				}else{
					$(".checkwishlistpdp").removeClass("addedtowishlist").addClass("addtowishlist");
				}
			}
  		});
	} 
}
function isScrolledIntoView(elem){
		if($(elem).offset() != undefined){
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();			
		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();			
		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));		
	}else{
		return false;
	}
}

	
function applyLazyLoad(){
			$(window).scroll(function(){displayImages();});
			$(window).resize(function(){displayImages();}); 
			displayImages();   
			function displayImages(){    
				$("img.lazy").each(function(indx,elem){
			var _src='https://static.toiimg.com/photo/79837759.cms';
			var _data_original='https://static.toiimg.com/photo/79837759.cms'; 
			if($(elem).attr("src") != undefined){_src=$(elem).attr("src")}
			if($(elem).attr("data-original") != undefined){_data_original=$(elem).attr("data-original")}       		
			if(isScrolledIntoView(elem) && $(elem).attr("src")!=$(elem).attr("data-original") && $(elem).hasClass("lazy")){
				$(elem).attr("src",$(elem).attr("data-original"));
				$(elem).removeClass("lazy");
				$(elem).error(function(){
					$(elem).attr("src",'https://static.toiimg.com/photo/79837759.cms');
				})
			}
		});    	
	}	 
}

function initInfinteScroll(){
    if($(".mobile-product-list-view .more-results:first").length>0){        		
    		var _loading_flag=false;
    		$(window).scroll(function() {        		  
			  if(isScrolledIntoView($(".mobile-product-list-view ul li:last")) && _loading_flag==false) {	
			   		if($(".mobile-product-list-view .more-results a").attr("href") != undefined && _loading_flag==false){
			  		  _loading_flag = true;			  	    
			   			var _url = $(".mobile-product-list-view .more-results a").attr("href");
						 if(_url != ""){
			   			$(".mobile-product-list-view .more-results").html("<div style='text-align:center;margin:10px 0 20px 0'><img src='/mobile/img/AjaxLoader.gif' alt='Loading' /></div>");				   			
			   			
						$.ajax(_url).done(function(response) {
			   				$(".mobile-product-list-view").append($(response).find(".mobile-product-list-view").html());
			   				applyLazyLoad();
							adsCheck();
			   				if(!paramsconfig._isPrimeUser || paramsconfig._isPrimeUser!="true"){
			   					
								_isPrimeUserFun();
							}
			   			}).fail(function() { /*alert("error");*/ }).always(function() { _loading_flag = false;$(".mobile-product-list-view .more-results:first").remove();}); 
			   		}}
			   }
			});
	}		
 }
 
function initFilters(){
	$(window).scroll(function(_window){
		if ($(window).scrollTop() >= 1) {
		   $('.refine-header-ui').addClass('fixed-header');
		   $('.refine-search-ui nav dl dd .filter-list-back-ui').addClass("fixed-header-1");
		}
		else {
		   $('.refine-header-ui').removeClass('fixed-header');
		   $('.refine-search-ui nav dl dd .filter-list-back-ui').removeClass("fixed-header-1");
		}
	});
	
		/* function to check if element is in viewport */        	

		/*$("body").on("click",".filter-apply-ui",function(){
			$("header").addClass("refine-top-list-out");
			$("footer").addClass("refine-top-list-out");
			$(".body-listing").addClass("refine-top-list-out");
			$(".container header .mobile-top-list").addClass("displaynone");
			$("body").addClass("oveflowhidden");
		});	 */
		$("body").on("click",".filter-apply-ui",function(e){
				e.preventDefault();
				$(".refine-search-ui").addClass("displayblock").removeClass("displaynone").removeClass("zindexless");
				$(".mobile-top-list").addClass("displaynone").removeClass("displayblock");
				$(".container").removeClass("refine-top-list-in").addClass("refine-top-list-out").removeClass("mobile-top-list-in");
				$(".footeradslot").addClass("displaynone");
				$(".headeradslot").addClass("displaynone");
		});
		$("body").on("click",".refine-cancel-ui",function(e){
			e.preventDefault();
			$(".container").addClass("refine-top-list-in").removeClass("refine-top-list-out").addClass("mobile-top-list-in");
			$(".refine-search-ui").addClass("zindexless");
			$(".footeradslot").removeClass("displaynone");
			$(".headeradslot").removeClass("displaynone");
		});

	if($(".refine-search-ui nav dl dd ul li a").hasClass("notmobile")){
		$(".filter-reset-ui").addClass("displaynone");
		$("body").on("click",".refine-search-ui nav dl dd ul li a.notmobile",function(){
			var _filterurl = $(this).attr("data-filter-url");
			$(this).attr("href",_filterurl);
		});
		}
	if($(".refine-search-ui nav dl dd ul li a").hasClass("categories")){
		$("body").on("click",".refine-search-ui nav dl dd ul li a.categories",function(e){
		e.preventDefault();
			if($(this).hasClass("active")){
				$(this).removeClass("active");
			}else{
				$(this).addClass("active");
			}
			var counter = $(this).closest(".refine-search-ui .mobile-ul-ui-list").find('.active').length;
			$(this).closest("dd").find(".counter-list-ui").html(counter + " Selected ");
			if(counter == 0){
				$(this).closest("dd").find(".counter-list-ui").html("&nbsp;");
				$(this).closest("dd").find(".filter-reset-ui").addClass("displaynone");
				$(this).closest("dd").find(".filter-reset-back").removeClass("displaynone");
			}else{
				$(this).closest("dd").find(".filter-reset-ui").removeClass("displaynone");
				$(this).closest("dd").find(".filter-reset-back").addClass("displaynone");
			}
		
	});
}
		
		$("body").on("click", "dd .filter-list-refine",function(e){
			e.preventDefault();
			$(".refine-search-ui nav dd").addClass("displaynone");
			$("p.refine-list-apply-button-ui").addClass("displaynone");
			$(this).closest(".refine-search-ui nav dd").addClass("displayblock").removeClass("displaynone");
			$(this).addClass("displaynone");
			$(".filter-list-back-ui").addClass("displayblock").removeClass("displaynone");
			$(".refine-search-ui nav dl").css("max-height","auto");
		});
		$("body").on("click",".filter-list-back-ui",function(e){
			e.preventDefault();
			$(".refine-search-ui nav dd").removeClass("displaynone");
			$("p.refine-list-apply-button-ui").removeClass("displaynone");
			$(".refine-search-ui .mobile-ul-ui-list").addClass("mobile-ul-ui-list-none");
			$(".mobile-collapse-ui").removeClass("mobile-collapse-ui-minu").addClass("mobile-collapse-ui-plus");
			$(".filter-list-refine").removeClass("displaynone");
			$(".filter-list-back-ui").removeClass("displayblock").addClass("displaynone");
		});
		
		$("body").on("click",".apply-ui",function(e){
			e.preventDefault();
//					
			    var _urlparams="";
			    var _mt_type=$("#mtType").val();
			    var _urlappend="";
	    		var firstBrandFilterUrl ="";
	    		var firstBrandFilterValue ="";
	    		 var urlBrandFilterExist ="NO";
			    _mt_type="generalMT";	
                 var _ga_arr = [];		
			    	var _currentUrl = ""+window.location;
		    		 var currentUrlintIndex = _currentUrl.indexOf("/brand-"); 
				if($(".refine-search-ui nav dl dd ul li a.active").length>0){
					var filterLength = $(".refine-search-ui nav dl dd ul li a.active").length;
				    $(".refine-search-ui nav dl dd ul li a.active").each(function(index, obj){
				    	var _filter_type = $(obj).attr("data-filter-type");
				    	var _filter_value = $(obj).attr("data-filter-value");
				    	var _filter_url = $(obj).attr("data-filter-url");
			    		 var brandValue ="";
			    		var skipBrandParamFilter ="NO";
			    		 if(_filter_type =="BRAND")
			    			 {
			    			 	var brandUrlintIndex = _filter_url.indexOf("/brand-"); 
			    			 	var urlLength = _filter_url.length;
			    			 	var brandValueStr=_filter_url.substring((brandUrlintIndex+7),urlLength);
			    			 	var brandUrlValueIndex = brandValueStr.indexOf("/"); 
			    			 	 brandValue=  _filter_url.substring(0,(brandUrlintIndex+7+brandUrlValueIndex+1));
			    			 	 var currentbrandFilterUrl = _currentUrl.indexOf(brandValue); 
			    			 	 if(filterLength==1)
			    			 		{
				    				 _urlappend = brandValue;
				    				 skipBrandParamFilter = "yes";
			    			 		}
			    			 	 else{
			    			 	 if(currentUrlintIndex ==0  )
				    			 {
				    				 _urlappend = brandValue;
				    				 skipBrandParamFilter = "yes";
				    			 }
			    			 	 else {
			    			 		if(firstBrandFilterUrl.length==0)
	    			 		 		{
	    			 		 			firstBrandFilterUrl = brandValue;
	    			 		 			firstBrandFilterValue = _filter_value;
	    			 		 		}
			    			 		 	if(currentbrandFilterUrl>0){
			    			 		 			skipBrandParamFilter = "yes";
			    			 		 			urlBrandFilterExist = "yes";
			    			 		 	}
			    			 	 }
			    			 	 }
			    			 }
			    		 		
			    		 if(_filter_type !="BRAND"  || skipBrandParamFilter == "NO")
			    			 {						
			    			 if(index!=0 && _urlparams.length!=0){_urlparams=_urlparams+"&";}
			    			 if(_mt_type=="generalMT"){_urlparams =_urlparams+ "filter="+_filter_type+":\""+_filter_value+"\"";}
			    			 if(_mt_type=="mobileMT"){_urlparams = _urlparams+ _filter_type+"="+_filter_value;}	
			    			 }
						 _ga_arr.push(_filter_type+":"+_filter_value);
				    });
					var _new_str =_ga_arr.toString();
				    if($(".sort-by-form-ui form select").val()!=""){
				    	_urlparams=_urlparams+"&sort="+$(".sort-by-form-ui form select").val();
				    }	
				    $("body").append("<div class='overlay opacity'></div><div class='loading'>Loading..</div>");
						var _ga_attr_track = "refine_search_apply##"+_new_str.replace(/,/g,"|")+"##"+paramsObj.pageUri+"##"+0;
						var _ga_array_track = _ga_attr_track.split("##");
						gaEvent_track(_ga_array_track);
						if(_urlappend.length==0 && urlBrandFilterExist=="NO"   )
							{
							if(firstBrandFilterUrl.length>0 &&firstBrandFilterValue.length>0)
							{
							_urlappend = firstBrandFilterUrl;
			    			 if(_mt_type=="generalMT"){
			    				var  replaceStr ="filter=BRAND:\""+firstBrandFilterValue+"\"&" ; 
			    					if(_urlparams.indexOf(replaceStr)<0)
			    						{
			    						 replaceStr ="filter=BRAND:\""+firstBrandFilterValue+"\"";
			    						}
			    				_urlparams =  _urlparams.replace(replaceStr, ""); 
			    			 }
			    			 if(_mt_type=="mobileMT"){
				    				var  replaceStr ="BRAND="+firstBrandFilterValue+"&"  ;
				    				if(_urlparams.indexOf(replaceStr)<0)
		    						{
		    						 replaceStr ="BRAND="+firstBrandFilterValue ;
		    						}
			    				 _urlparams = urlparams.replace( replaceStr,"");
			    			 }	

							}
							else
								{
								if(currentUrlintIndex >=0  )
			   			 		 {
									var urlLength = _currentUrl.length;
				    			 	var brandValueStr=_currentUrl.substring((currentUrlintIndex+7),urlLength);
				    			 	var brandUrlValueIndex = brandValueStr.indexOf("/"); 
				    			 	 brandValue=  _currentUrl.substring(currentUrlintIndex,(currentUrlintIndex+7+brandUrlValueIndex));
				    			 	_urlappend = 	_currentUrl.replace(brandValue,"");	
				    			 	_urlappend = _urlappend.split('?')[0];
			   			 		 }
								}
							}
					    _urlparams =  escape(_urlparams);
						window.location =_urlappend+"?queryParam="+_urlparams;
				}else{
					
   			 	 if(currentUrlintIndex >=0  )
   			 		 {
	    			 	var brandUrlintIndex = _currentUrl.indexOf("/brand-"); 
	    			 	var urlLength = _currentUrl.length;
	    			 	var brandValueStr=_currentUrl.substring((brandUrlintIndex+7),urlLength);
	    			 	var brandUrlValueIndex = brandValueStr.indexOf("/"); 
	    			 	 brandValue=  _currentUrl.substring(brandUrlintIndex,(brandUrlintIndex+7+brandUrlValueIndex));
	    			 	_currentUrl = 	_currentUrl.replace(brandValue,"");	
   			 		 }
					var _urlwithoutquery=_currentUrl.split('?')[0]; 
					
					$("body").append("<div class='overlay opacity'></div><div class='loading'>Loading..</div>");
					window.location=_urlwithoutquery;
				}
				$(".container").addClass("refine-top-list-in").removeClass("refine-top-list-out").addClass("mobile-top-list-in");
				if($(".filter-list-back-ui").hasClass("displayblock")){
					$(".mobile-collapse-ui").addClass("displaynone");
				}
				 
//			}
		});
		
		function applyFilterCount(){
			$(".refine-search-ui nav dl dd ul li a.active").each(function(index, obj){
				/* Event triggered twice for showing the count of selected filters on page load.*/
				$(obj).click().click();
			});
			
		}
		
		$("body").on("click",".refine-search-ui .refine-list-apply-button-ui .reset-ui",function(e){
			e.preventDefault();
			$(".refine-search-ui nav dl dd ul li a").removeClass("active");
			$(".refine-search-ui nav dl dd .counter-list-ui").html("&nbsp;");
			$(".filter-reset-ui").addClass("displaynone");
			$(".filter-reset-back").removeClass("displaynone");
		});
		
		$("body").on("click",".filter-reset-ui", function(e){
		    e.stopPropagation();
			$(this).closest("dd").find("ul li a").removeClass("active");
			$(this).closest("dd").find(".counter-list-ui").html("&nbsp;");
			$(this).closest("dd").find(".filter-reset-ui").addClass("displaynone");
			$(this).closest("dd").find(".filter-reset-back").removeClass("displaynone");
		});
		
		$("body").on("click","#logout", function(e){
			$.cookie("isPrimeUser",null);
			var hostname = window.location.hostname;
			var logoutUrl = window.location.protocol+'//' + hostname  + '/control/logout'
			var SSO_LOGOUT_URL = "https://jsso.indiatimes.com/sso/identity/profile/logout/external";
			var finalURL = SSO_LOGOUT_URL + "?channel=gadgetsnow&ru="+ encodeURI(logoutUrl);
			window.location=finalURL;		
		});
		$("body").on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",".refine-top-list-out, .mobile-top-list-out", function(){
			$(".container").addClass("zeroheight");
		});
		$("body").on("change",".sort-by-form-ui form select", function(e){
			e.preventDefault();
			if($(".refine-search-ui nav dl dd ul li a").hasClass("active")){
				$(".apply-ui").click();
			}else{				
			    var _urlparams="";
			    var _mt_type=$("#mtType").val();
			    _mt_type="generalMT";	
			    _windowUrl = window.location.href;
			    var _urlwithoutquery = _windowUrl;
			    if(_windowUrl.indexOf('?') > -1)
			    	_urlwithoutquery=window.location.href.split('?')[0];
			    
				    if($(".sort-by-form-ui form select").val()!=""){
				    	_urlparams=_urlparams+"&sort="+$(".sort-by-form-ui form select").val();
						/* Universal GA*/
						var _ga_attr_track = "sort_option##"+$(".sort-by-form-ui form select").val()+"##"+_urlparams+"##"+0;
						var _ga_array_track = _ga_attr_track.split("##");
						gaEvent_track(_ga_array_track);
						/* Universal GA*/
				    	 $("body").append("<div class='overlay opacity'></div><div class='loading'>Loading..</div>");


						    	_urlparams =  escape(_urlparams);


							$.ajax({url:_urlwithoutquery,data:{queryParam:_urlparams},type:'get'}).done(function(response) {$(".mobile-product-list-view").html($(response).find(".mobile-product-list-view").html());$(".section-container-ui h3").html($(response).find(".section-container-ui h3").html());applyLazyLoad();}).fail(function() {}).always(function(){$(".overlay,.loading").remove();});
				    }	

				
				 
			}
		
			
		});

}


function productBid(loginStatus,userId){
	$("body").on("click",".notEligable",function(e){
		alert("You are the higest bidder Right Now");
		e.preventDefault();
	 })
	if(loginStatus=="login"){
		$(".bidbutton a").addClass("bidnow");
		setInterval(function(){
			var _bid = $(".bidding-container").attr("data-bid");
			_bidajax(userId,_bid);
		},5000 );
	}else{
		$(".bidbutton a").attr("href","/control/login");
		setInterval(function(){
			var _bid = $(".bidding-container").attr("data-bid");
			var userId ='';
			_bidajax(userId,_bid);
		},5000 );
	}
}
function _bidajax(userId,_bid){
	//console.log(userId+"+++++++++++++++"+_bid)
	var _userId = userId;
	var _bid = _bid;
 $.ajax({
		url: '/control/getBidDetail',
		data:{"productId":paramsObj.PageProductId,"userId":userId,"bidId":_bid},
		dataType: "json",
		cache: false
	}).done(function(data){
		 //console.log(data.bidDetail.LastBidderName);
		 if(data.status=="SUCCESS"){
				 $(".bidding-container").removeClass("displaynone").attr("data-bid",data.bidDetail.BidId);
				 $(".currentbid").html(data.bidDetail.LastBidPrice);
				 $(".bidvalue").html(data.bidDetail.BidincrementPrice);
				 $(".bidprice").html(data.bidDetail.BidbasePrice);
				$(".bidbutton a").removeClass("notEligable");
				if($.cookie("bidId") == undefined){
					 document.cookie = "bidId="+data.bidDetail.BidId+"; expires=0; path=/"
				 }
				if(data.bidDetail.LastBidderName!=undefined && data.bidDetail.LastBidderName!="undefined"){
					 $(".successMsg").html("<div class='blk'>Last Bid By: "+data.bidDetail.LastBidderName+"<div>").removeClass("hidden");
				 }
				 if(data.bidDetail.EligableToBid == "N"){
					 $(".bidbutton a").addClass("notEligable");
				 }else{
					 $(".bidbutton a").removeClass("notEligable");
					// $(".successMsg").addClass("displaynone").html("");
				 }
				 if(data.bidDetail.BidId!=$.cookie("bidId")){
					  document.cookie = "bidId="+data.bidDetail.BidId+"; expires=0; path=/";
					 window.location.reload(); 
				 }
				 var _end = data.bidDetail.WindowEndTime;
				 var _start = data.bidDetail.WindowStartTime;
				 var _now = new Date().getTime();
				 //console.log(_now);
				 if(_now < _start){
					 $(".timeB").html("Starts in :");
					 $(".bidbutton a").addClass("notEligable");
					 _bid_timer(data.bidDetail.WindowStartTime);
					 $(".bidding-container").removeClass("displaynone");
				 }else if(_now < _end){
					 $(".timeB").html("Time Left :");
					 _bid_timer(data.bidDetail.WindowEndTime);
					 $(".bidding-container").removeClass("displaynone");
				 }else{
					 $(".bidding-container").addClass("displaynone");
				 }
				 
				 if(data.bidDetail.isWinner!=undefined){
					 if(data.bidDetail.isWinner=='Yes'){
						 $(".bidding-container").removeClass("displaynone");
						 $(".bidding-container").html("<div class='message'><div class='congr'>Congratulations!</div>"+data.bidDetail.message+"</div><div class='claimProduct'><a href='/control/claimProductForm?userLoginId="+userId+"&windowId="+data.bidDetail.BidId+"'>Claim Your Product</a></div>");
					 }else{
						 $(".bidding-container").removeClass("displaynone");
						 $(".bidding-container").html("<div class='message'>"+data.bidDetail.message+"</div>")
						 $(".message").addClass("fail");
					 }
				 }						 
		 }else{
			 $(".bidding-container").addClass("displaynone"); 
		 }
	}).fail(function(jqXHR,response){
	 //alert(response.error);
 });
}
function _bid_timer(_newfulldate){
	  var amzFesDt = _newfulldate;
	  var countDownDate = new Date(amzFesDt).getTime();
	var x = setInterval(function() {
	  var now = new Date().getTime();
	  var distance = countDownDate - now;
	  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	  var _getHour = new Date();
	  var _newHour = _getHour.getHours();
	 // 
		  if(paramsObj.dealType=='Bid'){
			  if(days!=0){
				  $(".timeC").html(days +" d(s) :"+ hours +" hr(s) : " + minutes + " min(s) : " + seconds+" sec(s)");
			  }else if(hours!=0){
				  $(".timeC").html(hours +" hr(s) : " + minutes + " min(s) : " + seconds+" sec(s)").addClass("font18");
			  }else{
				  $(".timeC").html(minutes + " min(s) : " + seconds+" sec(s)").addClass("font18");
			  }
		  }
	 /* }else{
		  if(paramsObj.dealType=='today-deal'){
			  $(".deals-timer").html("<div><span> Gadgets Now 'Deal Of The Day' ends in </span><span> "+ days +" day(s) : "+ hours + " hr(s) : " + minutes + " min(s) : " + seconds + " sec(s)</span></div>"); 
		  }
	  } */
	  
	  if (distance < 0) {
		clearInterval(x);
		document.getElementById("timerS").innerHTML = "Bidding has been closed.";
	  }
	  setInterval(function() {
		  clearInterval(x);
	  },5000);
	 
	}, 1000);
	 
  }

$("body").on("click",".bidnow",function(e){
	e.preventDefault();
	var _bid = $(".bidding-container").attr("data-bid");
	var userId = $("#biduserid").val();
	$.ajax({
		url: '/control/doBid',
		data:{"widgetVerbose":"false","productId":paramsObj.PageProductId,"userId":userId,"bidId":_bid},
		dataType: "json",
		type:"post",
		cache: false
	}).done(function(data){
		if(data.status=="success"){
			$(".successMsg").html(data.statusMessage).removeClass("displaynone");
		}else{
			$(".successMsg").addClass("displaynone");
		}
	})
})


function getLoginHeaderData(ticketId,status)
{  		
	  var  ticketId = "ticketId="+ticketId;
$.ajax({
		url: '/control/loginheader',cache: false, data:ticketId, type : "POST",success: function(data) {
			$('#loginHeader').html(data);
			$('#apploginHeader').html(data);
			$(".checkcompare").attr('href',paramsconfig.compareUrl);
			/* for google UA  */
			dataLayer.push({
				 'event':'VirtualPageview',
				'dimension4' : paramsconfig._loginstatus,
					'userId' : paramsconfig._partyId,
				'dimension14': paramsconfig._partyId
			});
			/* for google UA  */
			// var _search_tab = $("#search-visible").val();
			// if(_search_tab!="true"){
				// $(".toi-search-tab").addClass("displaynone");
			// }
			if(paramsObj.leafTitle=="product" && paramsObj.dealType=='Bid' || paramsObj.leafTitle=="bid2win" && paramsObj.dealType=='Bid'){
					productBid(paramsconfig._loginstatus,paramsconfig._userId);
			}
			
			$("body").append("<input type='hidden' id='biduserid' value="+paramsconfig._userId+">")
			$('ul.user-activity-menu li a').each(function(){
				var _t = $(this).attr('rel');
				var a_href = $(".loginstatus").attr('href');
				var login_status = 'logout';
				if(a_href=='/control/logout'){
					login_status = 'login';
				}
				$(this).attr('data-ga','check_out_funnel_mobile##'+paramsObj.leafTitle+'##'+_t+'##0');
				$(this).attr('data-ga-track','header_page##'+login_status+'|'+_t+'##'+paramsObj.leafTitle+'##0');
			});
			if(paramsconfig._loginstatus!='login'){
				//showRegistrationCampaignPopUp();
			}
			//$("*[data-slot=256682]").css("display",'none');
			if(!paramsconfig._isPrimeUser || paramsconfig._isPrimeUser!="true"){
				$.cookie("isPrimeUser",null);
				_isPrimeUserFun();
				dataLayer.push({'Prime_User_Type' : 0,'event' : 'primeUser'});
			}else{
				 
				$.cookie("isPrimeUser",'true',{expiry:0,path:'/'});
				dataLayer.push({'Prime_User_Type' : 1,'event' : 'primeUser'});
			}
	}});
	//$('ul.user-activity-menu li:first-child').remove();
}

function setHeaderLogo()
{
	
	/*var cookieValue = readCookie("wl");
	if(cookieValue == "mtoi")
		{
			$('.toi-logo').html("<a href='/'><img src='/mobile/img/53501019.png'/></a>");
		}
	else{
			$('.toi-logo').html("<a href='/'><img src='/mobile/img/52745964.png'/></a>");
			}*/
	
//	var cookievalue = readCookie("uftoi");
//	if(cookievalue == "fromtoi")
//		{
//		$('.toi-logo').html("<a href='/'><img src='https://static.toiimg.com/photo/80111711.cms'/></a>");
//		}
//	else
//		{
//			$.cookie("uftoi","fromtoi");
//			$('.toi-logo').html("<a href='/'><img src='https://static.toiimg.com/photo/80111711.cms'/></a>");
//		}
}

function readCookie(cookieName)
{
   var allcookies = document.cookie;
   var name;
   var value="";
  
   
   // Get all the cookies pairs in an array
   cookiearray = allcookies.split(';');
   // Now take key value pair out of this array
   for(var i=0; i<cookiearray.length; i++){
      name = cookiearray[i].split('=')[0];
      value = cookiearray[i].split('=')[1];
      if(name.trim()== cookieName)
    	  {
    	   break;
    	  }
   }
   return value;
}

function getLoginSSOData()
{
	var url = window.location.href;
	if(url.indexOf("checkout") == -1 && url.indexOf("checkoutPayment") == -1 && url.indexOf("login"))
	{	
		var script = document.createElement('script');
		result= 'https://jsso.indiatimes.com/sso/crossdomain/getTicket?&callback=getDataByTicket';
		script.src =result;
		document.body.appendChild(script);
		return false;
	}
}

function getDataByTicket(data) 
{
	var jsonData = JSON.parse(JSON.stringify(data));
	
	var ticketId = jsonData.ticketId;
	var ticketId = jsonData.status;
	
	getLoginHeaderData(ticketId,status);
	
	/*if(ticketId=="") {
		document.getElementById("ssoLoginDiv").style.display = "block";
	}
	else
	{	
	var urlToHit = SSO_FOLDER+'/ssoreturnsuccess.php';
	alert(urlToHit);
	  $.ajax({
           type: 'POST',
           url: urlToHit,
		   data:{"ticketId" : String(ticketId)},
		   dataType:"",
		   processData:true,
           success: function (data) { alert(data);
			   populateWelcomeDiv();
           },
		   error: function(e){
				alert("error::"+e);
			}
	});
    }*/
}


function _initMyAccount(){
	$("body").on("click",".my-account-ui .more-link",function(e){
		e.preventDefault();
		addAjaxLoading();
		url="/control/orderhistorydata?VIEW_INDEX="+pageContains;
		$.ajax({url:url,type:'post',dataType: 'html'}).done(function(data){
			$('.my-account-ui ul').append(data);
			orderPageCount=$('.my-account-ui li').size();
			$("#orderPageCount").html(orderPageCount);
			if(orderPageCount<totalOrderCount){
				$(".my-account-ui .more-link").removeClass("displaynone");
				$(".my-account-ui .more-link").addClass("displayblock");
			}else{
				$(".my-account-ui .more-link").removeClass("displayblock");
				$(".my-account-ui .more-link").addClass("displaynone");
			}
			pageContains=pageContains+10;
		}).fail(function(){
			alert("Please retry.");	
		}).always(function(){
			removeAjaxLoading();
		});
	});
}

function addAjaxLoading(){
	removeAjaxLoading();
	$(".conformation ").remove();
	$('body').append("<div class='overlay opacity'></div><div class='loading'><img src='/mobile/img/AjaxLoader.gif'></div>");
}

function removeAjaxLoading(){
	$(".overlay, .loading").remove();	
}
function social_share(){
	
		var product_name = $(".body-pdp .section-container-ui h1").text();
		//var _params = escape("utm_source=w&utm_medium=referral");
		var current_url = encodeURIComponent(window.location.href);
		var whatsapp_url = "whatsapp://send?text=Hey, Look what I found on Gadgets Now - "+product_name+"&nbsp;"+current_url;
		$(".body-pdp .product-container-top .mobile_share_options .whatsapp_share").html("");
		/* Universal GA*/
		var ga_whatsapp = $("input[name=whatsapp_share]").val();
		/* Universal GA*/

	$(".body-pdp .product-container-top .mobile_share_options .whatsapp_share").append('<a href="' + whatsapp_url + '" data-ga-track="'+ga_whatsapp+'"></a>');
	/*			
	$(document).on("click","body",function (e){
		    var container = $(".mobile_share_options");
		    var _initiator = $(".mobile_share");
		    if (!container.is(e.target) // if the target of the click isn't the container...
		        && container.has(e.target).length === 0 // ... nor a descendant of the container
		        && !_initiator.is(e.target))
		    {container.css("display","none");container.css("cursor","");}
		});
	*/

	$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
		    FB.init({
		      appId: '577459282421311',
		      version: 'v2.3' // or v2.0, v2.1, v2.0
		    });     
		  });		
	  $("body").on("click","#shareOnfacebook",function(e){
		  	e.preventDefault();
		  	var _url =$(location).attr('href');
		  	//_url = _url.replace("local.","shopping.")
		  	FB.ui({
			  method: 'share',
			  href: _url+"?utm_source=f&utm_medium=referral"
			}, function(response){});
		
	   });
}
function myprofileTabOpen(_tabOpen){
	$(".information-tab dd").addClass("displaynone");
	$(".information-tab dt").addClass("in-active").removeClass("active");
	$(_tabOpen).addClass("active").removeClass("in-active");
	$(_tabOpen).next("dd").removeClass("displaynone");
	//_autoscroll(_tabOpen);
	_autonewscroll(_tabOpen);
	$(".errorMsg").text('');
	$("input,select").removeClass('onError');
	if(!$(".unregistered").hasClass("displaynone")){
		$(".unregistered,.register-content").addClass("displaynone");
	}
}

function contactUs(){
	$("body").on("click","input[name=queryType]",function(){
		if($("input#radio_anyInfo").is(":checked")){
		$(".radio_anyInfo").addClass("displaynone")
		$("#orderNumber").attr("disabled","disabled");
		}else{
			$(".radio_anyInfo").removeClass("displaynone")
			$("#orderNumber").removeAttr("disabled","disabled");
		}
	});
	$("body").on("change","select[name=issueType]",function(){
		var _issueType = $("#select_issue option:selected").attr('data-issue');
		if(_issueType!=undefined){
			$("#select_issue_sub").removeClass("displaynone").removeAttr("disabled","disabled").prop('selectedIndex',0);
		}else{
			$("#select_issue_sub").addClass("displaynone").attr("disabled","disabled").prop('selectedIndex',0);
		}
		$(".select-issue-type").addClass("displaynone");
		$('.'+_issueType).removeClass("displaynone");
	});
	$("body").on("change","select[name=issueSubType]",function(){
		var _issueSubType = $("#select_issue_sub option:selected").attr('data-issue');
		if(_issueSubType!=undefined){
			$("#select_issue_sub2").removeClass("displaynone").removeAttr("disabled","disabled").prop('selectedIndex',0);
		}else{
			$("#select_issue_sub2").addClass("displaynone").attr("disabled","disabled").prop('selectedIndex',0);
		}
		if(_issueSubType=="myorder"){
			$("#select_issue_WhereOrder").removeClass("displaynone");
			$("#select_issue_sub2").addClass("displaynone").attr("disabled","disabled");
		}else{
			$("#select_issue_WhereOrder, #select_issue_sub2_refund").addClass("displaynone");
		}
		$(".select_issue_sub2").addClass("displaynone");
		$('.'+_issueSubType).removeClass("displaynone");
	});
	$("body").on("change","select[name=issueSubType1]",function(){
		var _issueSubType2 = $("#select_issue_sub2 option:selected").attr('data-issue');
		if(_issueSubType2=="willrefund"){
			$("#select_issue_sub2_refund").removeClass("displaynone");
		}else{
			$("#select_issue_sub2_refund").addClass("displaynone");
		}
	});
	$("body").on("click",".attachment_initiate", function(){
		$(".upload-box").removeClass("displaynone");
	});
	$("body").on("click",".addtxt",function(e){
		e.preventDefault();
		var _a = $(this).closest(".copyhtml").html();
		var _countId = $(".copyhtml").length;
		if(_countId >= 5){
			alert("Only 5 Attachments Allowed!");
		}else{
			$(".upload-box").append("<div class='copyhtml newbox"+_countId+"'>"+ _a +"</div>");
			$(".newbox"+_countId+" input").attr('name','uploadedFile_'+_countId);
		}
	});
	$("body").on("click",'.removetxt',function(e){
		e.preventDefault();
		var _countId = $(".copyhtml").length;
		if(_countId <= 1){
			alert("We can't Remove");
		}else{
			alert("You want Remove");
			$(this).closest(".copyhtml").remove();
		}
	});
	$("body").on("click","a.linkGetNewImage",function(){
	   	var submitToUri = "/control/reloadCaptchaImage";
		$.ajax({
		    url: submitToUri,
	    	cache: false,
			success: function(html){
				console.log(html);
			}
		});
	});
$("body").on("submit","form[name=contactUsForm]",function(){
		_this = this;
		var _evalfrom = evalForm(_this);
		if(_evalfrom==false){
			$(".overlay,.loading").remove();
			return false;
		}
	});
}

function myprofile(){
	$("body").on("click",".information-tab dt",function(){
		var _this = this;
		myprofileTabOpen(_this);
	});
	// if(paramsObj._activeTab=='otherinfo'){
		// if($(".information-tab dt").hasClass("otherinfo")){
			// $(".otherinfo").click(function(){
				// alert(1);
				// var _this = this;
				// console.log(_this)
			// myprofileTabOpen(_this);
			// });
		// }
	// }
	$("body").on("submit","form[name=editpersonform]",function(){
		return evalForm(this);		
	});	
	$("body").on("submit","form[name=changepasswordform]",function(){
		return evalForm(this);		
	});
	$("body").on("submit","form[name=updatepersonform]",function(){
		var _date = $("input[name=showBirthDate]").val();
		if(_date!=''){
			re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
			if(_date != '' && !_date.match(re)) {
			  $('#showBirthDate_error').text("Invalid date format it should be dd/mm/yyyy");
			  return false;
			}
		}
		return evalForm(this);		
	});
}
function giftCoupon(){
	$("body").on("click",".coupon-list dt",function(){
		$(".coupon-list dt").addClass("in-active").removeClass("active");
		$(".coupon-list dd").addClass("displaynone");
		$(this).removeClass("in-active").addClass("active");
		$(this).next("dd").removeClass("displaynone");
	});
	
}

function bulkOrder(){
	$("body").on("submit","form[name=BulkForm], form[name=sellwithusForm]",function(){
		_this = this;
		var _evalfrom = evalForm(_this);
		var _mob = $("input[name=contactNumber]").val();
		var _url = $(_this).attr("action");
		if(_evalfrom==true){
			$(_this).append("<input type=hidden name=clickcount>");
			// var _Mob_length = parseInt($("input[name=contactNumber]").val().length);
			// if(_Mob_length!=10){
				// $("#PlaceHolder_txtMobile").addClass("onError");
				// $("#PlaceHolder_txtMobile_error").addClass("errorMsg").html("Please Enter Valid Mobile Number").css("display","block");
				// return false;
			// }
			$('body').append("<div class='overlay opacity'></div><div class='loading'><img src='/mobile/img/AjaxLoader.gif'></div>");
			$.ajax({
				url :_url,
				dataType:'json',
				type:'POST',
				data:$(_this).serializeArray()
			}).done(function(response){
				
				if(response=="OTPSENT"){
					$(".otpmsg").html("An OTP has been sent to +91-"+_mob);
					$('.field-wrapper input,.field-wrapper textarea, .field-wrapper select').attr("disabled","disabled");
					$(".field-wrapper input[type=submit]").addClass("displaynone");
					$("input[name=otp]").removeAttr("disabled").parent("div.field-wrapper").removeClass("displaynone");
				}
			}).fail(function(xhrError){
				console.log(xhrError);
			}).always(function(){
				$(".overlay,.loading").remove();
			})
			return false;
		}else{			
			return false;
		}
			
	});
	$("body").on("click","input[name=ecommerce]",function(){
		if($(this).prop("checked")==true){
			$("select[name=EcommercePlatform]").removeAttr("disabled").parent("div.field-wrapper").removeClass("displaynone");
		}else{
			$("select[name=EcommercePlatform]").attr("disabled","disabled").parent("div.field-wrapper").addClass("displaynone");
			$("input[name=sellername]").attr("disabled","disabled").parent("div.field-wrapper").addClass("displaynone");
			$("input[name=websitename]").attr("disabled","disabled").parent("div.field-wrapper").addClass("displaynone");
			$("select[name=EcommercePlatform]").prop('selectedIndex',0);
		}
	});
	$("body").on("change","select[name=EcommercePlatform]",function(){
		if(this.value!="Other"){
			$("input[name=sellername]").removeAttr("disabled").parent("div.field-wrapper").removeClass("displaynone");
			$("input[name=websitename]").attr("disabled","disabled").parent("div.field-wrapper").addClass("displaynone");
		}else{
			$("input[name=websitename]").removeAttr("disabled").parent("div.field-wrapper").removeClass("displaynone");
			$("input[name=sellername]").removeAttr("disabled").parent("div.field-wrapper").removeClass("displaynone");
		}
	});
	function validateOTP(_otpFor,_otpVal,_mob){
		var _notpFor = _otpFor;
		var _notpVal = _otpVal;
		var _nmob = _mob;
		//console.log(_notpFor+'+'+_notpVal+'+'+_nmob);
		$('body').append("<div class='overlay opacity'></div><div class='loading'><img src='/mobile/img/AjaxLoader.gif'></div>");
		$.ajax({
			url :'/control/validateOTP',
			dataType:'json',
			type:'POST',
			data:{'otpFor':_notpFor, 'otp':_notpVal,'mobile':_nmob}
		}).done(function(response){
			if(response=="success"){
				$("form[name=BulkForm], form[name=sellwithusForm]").addClass("displaynone");
				$(".servermsgsuccess").removeClass("displaynone");
				$("#bulkorderthanksmsg").addClass("displaynone");
				$("#OTP_request").removeClass("onError");
				$("#OTP_request_error").html("");
			}else if(response=="expired"){
				$("#OTP_request, .otpmsg").addClass("displaynone");
				$("#OTP_request_error").html();
				$(".stillnotOTP").html("<a class='sumbitwithoutOTP' href='#'>Still didn't get the verification code?</a>");
			}else{
				$("#OTP_request").addClass("onError");
				$("#OTP_request_error").html("Please Enter Valid OTP");
			}
			if(_notpVal=='NA'){
				$("form[name=BulkForm], form[name=sellwithusForm]").addClass("displaynone");
				$(".servermsgsuccess").removeClass("displaynone");
				$("#bulkorderthanksmsg").addClass("displaynone");
			}
		}).fail(function(xhrError){
			console.log(xhrError);
		}).always(function(){
			$(".overlay,.loading").remove();
		})
	}
	$("body").on("keyup","form[name=BulkForm] #OTP_request, form[name=sellwithusForm] #OTP_request",function(e){
			e.preventDefault();
			var _otpFor = $("input[name=otpFor]").val();
			var _otpVal = $(this).val();
			var _mob = $("input[name=contactNumber]").val();
			var _filled_count = $(this).val().length;
			if(_filled_count == 4){
				validateOTP(_otpFor,_otpVal,_mob);
			}
	});
	$("body").on("keyup","form[name=BulkForm] #PlaceHolder_txtMobile, form[name=sellwithusForm] #PlaceHolder_txtMobile",function(e){
			e.preventDefault();
			var _mobNum = $("input[name=contactNumber]").val();
			var _filled_count = $(this).val().length;
			if(_filled_count <= 10){
				//$(".contactnumber").html(_mobNum);
				$("#PlaceHolder_txtMobile_error").html("&nbsp;");
				$(this).removeClass("onError");

			}else{
				//$(".contactnumber").html("contact number");
				$("#PlaceHolder_txtMobile_error").html("Mobile Number should be 10 digit number");
				$(this).addClass("onError");
				
			}
	});
	$("body").on("keyup","form[name=BulkForm] input[name=quantity]",function(e){
		e.preventDefault();
		var _checkQty = $(this).val();
		if(_checkQty<=5){
			$(this).addClass("onError");
			$("#PlaceHolder_quantity_error").html("Please mention quantity of more than 5");
			$("form[name=BulkForm] input[type=submit]").attr("disabled","disabled");
		}else{
			$(this).removeClass("onError");
			$("#PlaceHolder_quantity_error").html("&nbsp;");
			$("form[name=BulkForm] input[type=submit]").removeAttr("disabled");
		}
	});
	
	$("body").on("click",".lyfpopclose",function(e){
		e.preventDefault();
		$(".lyfpop").addClass("displaynone");
	});
	var _clickcount = 0;
	$("body").on("click",".resendOTP",function(e){
		e.preventDefault();
		_clickcount = _clickcount+1;
		$("input[name=clickcount]").val(_clickcount);
		var _otpFor = $("form[name=BulkForm] input[name=otpFor], form[name=sellwithusForm] input[name=otpFor]").val();
		var _mob = $("form[name=BulkForm] input[name=contactNumber], form[name=sellwithusForm] input[name=contactNumber]").val();
		if($("input[name=clickcount]").val()<3){
			$('body').append("<div class='overlay opacity'></div><div class='loading'><img src='/mobile/img/AjaxLoader.gif'></div>");
			$.ajax({
				url:"/control/sendOTPGeneric",
				dataType:"json",
				type:"POST",
				data :{"mobileNo":_mob,'otpFor':_otpFor}
			}).done(function(response){
				if(response=="success"){
					$(".otpmsg").html("An OTP has been resent to +91-"+_mob);
				}
			}).fail(function(xhrError){
				console.log(xhrError);
			}).always(function(){
				$(".overlay,.loading").remove();
				$("#OTP_request").removeClass("onError").val('');
				$("#OTP_request_error").html("");
			});
		}
		if($("input[name=clickcount]").val()==2){
			$(".stillnotOTP").html("<a class='sumbitwithoutOTP' href='#'>Still didn't get the verification code?</a>")
		}
	});
	$("body").on("click",".sumbitwithoutOTP",function(e){
		e.preventDefault();
		var _otpFor = $("input[name=otpFor]").val();
		var _mob = $("input[name=contactNumber]").val();
		var _otpVal = "NA";
		validateOTP(_otpFor,_otpVal,_mob);
	});
}

function customerComment(){
	$("body").on("submit","form[name=customerComment]",function(e){
		var _url = $(this).prop("action");
		_partyId = '';
		if(paramsconfig._loginstatus=='login'){
			_partyId = '&userId='+paramsconfig._partyId;
		}
		if(evalForm(this)){
			$("body").append("<div class='overlay opacity'></div><div class='loading'>Loading..</div>");
			$.ajax({
	     			url:_url,
	     			dataType:'json',
	     			type:$(this).prop("method"),
	     			data: $(this).serializeArray()
	     		}).done(function(response){
	     			
	     			 
	     			
	     			if(response.status=="success"){
						if(paramsconfig._loginstatus!='login'){
							$(".username").html('<input type="text" name="userId" value="" placeholder="Please Enter You Name" />');
						}else{
							$(".questionpost").remove();
						}
						$("input[name=token]").val(response.id);
						$(".customercommentmsg").addClass("success").html(response.responseMsg);
						$("#TakeComment, #TakeComment_error").remove();
					}
	     			if(response.status=="error"){
						$(".customercommentmsg").addClass("error").html(response.responseMsg);
					}	     			
	     		}).always(function(){
					 $(".overlay, .loading").remove();
				}); 
		}
		return false;
	});
	$("body").on("submit","form[name=replyComment]",function(e){
		if($(".replyReponse").hasClass("success")){
			$(".replyReponse").removeClass("success")
		}
		if($(".replyReponse").hasClass("error")){
			$(".replyReponse").removeClass("error")
		}
		if(evalForm(this)){
			$("body").append("<div class='overlay opacity'></div><div class='loading'>Loading..</div>");
			$.ajax({
	     			url:$(this).prop("action"),
	     			type:$(this).prop("method"),
	     			data: $(this).serializeArray()
	     		}).done(function(response){
					if(response.status=='success'){
						$(this).find(".replyReponse").addClass("success").html(response.msg);
						$(this).find("textarea").val("");
					}
					if(response.status=="error"){
						$(this).find(".replyReponse").addClass("error").html(response.msg);
					}	
	     		}).always(function(){
					 $(".overlay, .loading").remove();
				}); 
		}
		return false;
	});
}

function userReview(){
		 $("body").on("submit","form[name=reviewProduct]",function(e){	    	 
	     	if(evalForm(this)){
				$("body").append("<div class='overlay opacity'></div><div class='loading'>Loading..</div>");
	     		$.ajax({
	     			url:$(userReviewForm).prop("action"),
	     			type:$(userReviewForm).prop("method"),
	     			data: $(userReviewForm).serializeArray()
	     		}).done(function(response){
	     			response={
	     				"status":"success",
	     				"_EVENT_MESSAGE_":"This is a dummy message. Will be replaced by actual message"
	     			}
	     			if(response.status=="success"){
	     				$(_top_container).html("<div class=\"server-message text-align-center success-message\"><span class=\"icon\">&nbsp;</span><br/>"+ response._EVENT_MESSAGE_ +" </div>");
		     				if($(".user-review-container").length > 1){	
		     				setTimeout(function(){
		     					$(_top_container).slideUp("slow");
		     				},5000);
	     				}else{
							$(_top_container).find(".server-message").append("<em>Redirecting to Home</em>");
	     					setTimeout(function(){
		     					window.location.href="/";
		     				},5000);
	     				}
	     			}
	     			if(response.status=="error"){
	     				$("<div class=\"server-message text-align-center error-message\"><span class=\"icon\">&nbsp;</span><br/>"+ response._EVENT_MESSAGE_ +"</div> ").insertBefore($(userReviewForm));
						if($(".user-review-container").length > 1){
		     				setTimeout(function(){
		     					$(_top_container).find(".error-message").slideUp("slow");
		     				},5000);
					   }
	     			}
	     		}).always(function(){
					 $(".overlay, .loading").remove();
				}); 
	     	}
	     	return false;
	    });
	 
}

function createThankuPageAlert(){
    //alert(paramsObj.orderId);
    var  formData = "orderId="+paramsObj.orderId;
    $.ajax({
url : "/control/createThankuPageAlert",
type: "POST",
data : formData,
success: function(data, textStatus, jqXHR)
                    {
			  //data - response from server
                    }, 
    error: function (jqXHR, textStatus, errorThrown)
                    {
                               
                    }
    });
}


function utmInit() {
	var referer_source = document.referrer;
	if (referer_source == null || referer_source == "" || referer_source.match("^http://shop.gadgetsnow.com") == "http://shop.gadgetsnow.com")
		referer_source = 'DIRECT';
	var utmkey = getURLParameter("utm_source");
	var utmkeyUp = utmkey;
	var _utmuppercase = utmkeyUp.toUpperCase();
	var _Currdate = new Date();
	var _timestamp = _Currdate.getTime();
	var oldReferSource = $.cookie("refer_source");

	if (_utmuppercase !== "NULL" && _utmuppercase !== null) {
		//var oldCookieValue = $.cookie("utmsource");
		//alert(oldCookieValue +"+"+ _utmuppercase);
		var strGetting = _utmuppercase;
		var digitInStar = strGetting.substring(0, 3);
		if (digitInStar == "DGM") {
			$.cookie("utmsource", digitInStar, {
				expires : 7,
				path : '/'
			});
		} else {
			$.cookie("utmsource", _utmuppercase, {
				expires : 7,
				path : '/'
			});
		}
		var firstSource = $.cookie("first_utm");
		if (firstSource != null && firstSource != "") {
			second_utm_value = _utmuppercase + '##' + getURLParameter("utm_medium") + '##' + getURLParameter("utm_campaign") + '##' + referer_source + '##' + _timestamp;
			$.cookie("second_utm", second_utm_value, {
				expires : 7,
				path : '/'
			});
		} else {
			first_utm_value = _utmuppercase + '##' + getURLParameter("utm_medium") + '##' + getURLParameter("utm_campaign") + '##' + referer_source + '##' + _timestamp;
			$.cookie("first_utm", first_utm_value, {
				expires : 7,
				path : '/'
			});
		}
		//set this in referer
		$.cookie("refer_source", referer_source, {
			path : '/'
		});
	} else if ((oldReferSource == null || oldReferSource == "") && referer_source != null) {
		var firstSource = $.cookie("first_utm");
		if (firstSource != null && firstSource != "") {
			second_utm_value = 'null##' + getURLParameter("utm_medium") + '##' + getURLParameter("utm_campaign") + '##' + referer_source + '##' + _timestamp;
			$.cookie("second_utm", second_utm_value, {
				expires : 7,
				path : '/'
			});
		} else {
			first_utm_value = 'null##' + getURLParameter("utm_medium") + '##' + getURLParameter("utm_campaign") + '##' + referer_source + '##' + _timestamp;
			$.cookie("first_utm", first_utm_value, {
				expires : 7,
				path : '/'
			});
		}
		$.cookie("refer_source", referer_source, {
			path : '/'
		});
	}
}


function _AmazingDeal(){
	var _landingUrl = $("#landingurl").val();
	if(_landingUrl !=''){
		$('html, body').animate({
			scrollTop: $("#"+_landingUrl).offset().top
		}, 2000);
	}
	$(window).scroll(function() {  
		$('.sub-amazing').each(function() {
			var _id = this.id;
			var _refreshAd = $("#"+_id).offset().top+46; 
			var currentScroll = $(window).scrollTop();
			var _url = $("#"+_id).attr("data-url");  
			if (currentScroll >= _refreshAd) { 
				if($(this).hasClass("active")){
					history.pushState(null, null, _url);
					$(this).removeClass("active");
					googletag.pubads().refresh([window.slot]);
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
					ga('create', 'UA-2021280-10', 'auto');
					ga('set', 'dimension5', id[1]);
					ga('send', 'pageview');
					setTimeout(function(){ 
						(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
						  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
						  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
						  })(window,document,'script','https://www.google-analytics.com/analytics.js','_ga');
						_ga('create', 'UA-198011-4', 'auto');
						_ga('set', 'dimension5', id[1]);
						_ga('send', 'pageview');
					}, 2000);
				}
				
			 }
			 if (currentScroll < 44){
				$('.sub-amazing').addClass("active") 
			 }
		});
	});
}
// Amazon timer deal
function _amazon_timer(_newfulldate){
	  //var amzFesDt = document.getElementById("amzFesDt").value;
	  var amzFesDt = _newfulldate;
	  var countDownDate = new Date(amzFesDt.replace(/-/g, '/')).getTime();
	var x = setInterval(function() {
	  var now = new Date().getTime();
	  var distance = countDownDate - now;
	  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	  var _hours = hours-1;
	  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	  var _getHour = new Date();
      var _newHour = _getHour.getHours();
	 /* if(days==0){
		  document.getElementById("deals-timer").innerHTML = hours + " hrs : " + minutes + " mins : " + seconds + " secs ";
	  }else{
		  document.getElementById("deals-timer").innerHTML = days + " days : " + hours + " hrs : " + minutes + " mins : " + seconds + " secs ";  
	  }
	  */
	  if(paramsObj.dealType=='flash-sale'){
		  if(_newHour>=15 && _newHour < 16){
			$(".deals-timer").html("<div><span> Gadgets Now 'Flash Sale' on this product ends in </span><span> "+ hours + " hr(s) : "+ minutes + " min(s) : " + seconds + " sec(s)</span></div>");  
		  }else{
			$(".deals-timer").html("<div><span> Gadgets Now 'Flash Sale' on this product starts in </span><span> "+ _hours + " hr(s) : " + minutes + " min(s) : " + seconds + " sec(s)</span></div>"); 
		  }  
	  }else if(paramsObj.dealType=='today-deal'){
		  $(".deals-timer").html("<div><span> Gadgets Now 'Deal Of The Day' for this product ends in </span><span> "+ hours + " hr(s) : " + minutes + " min(s) : " + seconds + " sec(s)</span></div>");
	  }
	  
	  if (distance < 0) {
		clearInterval(x);
		document.getElementById("deals-timer").innerHTML = "EXPIRED";
	  }else{
		  $('.deals-timer').css('display','block');
	  }
	}, 1000);
	}
          
// Amazon timer deal
//flash sale condition
	  if(paramsObj.PageProductId==paramsObj.DealProductId && paramsObj.dealType=='flash-sale'){
			_amazon_timer(paramsObj.dealends);
		}
// flash sale condition

function getDateDelivery(){
	       // Express Delivery timer and date funcation
          
          var dt = new Date();
          function getTomorrow(d,offset){
              if (!offset){
                  offset = 1;
              }
              if(typeof(d) === "string"){
                  var t = d.split("-"); /* splits dd-mm-year */
                  	d = new Date(t[2],t[1] - 1, t[0]);
              	}
              	return new Date(d.setDate(d.getDate() + offset));
          }    
          	
          	var month = new Array();
          	month[0] = "Jan";
          	month[1] = "Feb";
          	month[2] = "Mar";
          	month[3] = "Apr";
          	month[4] = "May";
          	month[5] = "Jun";
          	month[6] = "Jul";
          	month[7] = "Aug";
          	month[8] = "Sep";
          	month[9] = "Oct";
          	month[10] = "Nov";
          	month[11] = "Dec";

          	var week = new Array();
          	week[1] = "Mon";
          	week[2] = "Tue";
          	week[3] = "Wed";
          	week[4] = "Thu";
          	week[5] = "Fri";
          	week[6] = "Sat";
          	week[0] = "Sun";
          	
          	var nextthree = getTomorrow(dt,1);
          	var _month = month[nextthree.getMonth()];
          	var _week = week[nextthree.getDay()];
          	var newdate = nextthree.getDate();
          	var _fulldate = _month + ', ' + newdate + ', ' + dt.getFullYear();
          	var _newfulldate = _month + ', ' + newdate + ', ' + dt.getFullYear();
          	setTimeout(function(){
				//console.log(_newfulldate);
          		if(document.getElementById("sladate") != null){
          		document.getElementById("sladate").innerHTML = _newfulldate;
          		}
				if(paramsObj.PageProductId==paramsObj.DealProductId && paramsObj.dealType=='today-deal'){
					_amazon_timer(_newfulldate);
				}
          	},1000);
          function _getnewDate(){
          		var d = new Date();
				d.setHours(d.getHours() - 12);
          		var offset = 2;
          		    if(typeof(d) === "string"){
          		        var t = d.split("-"); /* splits dd-mm-year */
          		        d = new Date(t[2],t[1] - 1, t[0]);
          		    	}
          		    _a = new Date(d.setDate(d.getDate() + offset));
          		    var _month = month[_a.getMonth()];
          		    return _month+", " +_a.getDate()+", "+_a.getFullYear();			
          }
          var countDownDate = new Date(_fulldate).getTime();
          var x = setInterval(function() {
          	var now = new Date().getTime();
          	var distance = countDownDate - now;
          	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          	var seconds = Math.floor((distance % (1000 * 60)) / 1000);
          	var _ELE_DIV = document.getElementById("getting-started");
          	if(_ELE_DIV != null){
          	if(hours >= 12){
          			var hours = hours-12;
          			
          			if(hours==0){
          				_ELE_DIV.innerHTML = minutes + " mins "+ seconds+ " secs" ;
          			}else{
          				_ELE_DIV.innerHTML = hours + " hours " + minutes + " mins "+ seconds+ " secs" ;
          			}
          			}else{
          			var hours = hours+12;
          			var _resetDate = _getnewDate(); 
          			document.getElementById("sladate").innerHTML = _resetDate;
          			_ELE_DIV.innerHTML = hours + " hours " + minutes + " mins "+ seconds+ " secs" ;
          		
          	}
          	
          	if (distance < 0) {
          	    clearInterval(x);
          	  _ELE_DIV.innerHTML = "Usually Delivers in 1-3 days.";
          	  }
          }
          }, 1000);
          
	}
/* CTN ads check condtion     */
function ctnAdcheck(){
	if(paramsObj.leafTitle!='category'){
		$(".colombiaFail").parent().css("display","none");
	}
}

/* pixl condtion     */
function getURLParameter(name) {
	return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]);
}

/* Track Order */
function addCookie(key, value) {
	try {
		$.cookie(key, value, {
			path : '/'
		});
	} catch(e) {/*Do nothing*/
	}

}

function _trackCTN(){
		var _source = document.referrer;
		var utmcampaign = getURLParameter("utm_campaign");
		$.cookie("utm_campaign", utmcampaign, {expires : 1,path : '/'	});
		if($.cookie('utm_campaign')== "amzctn"){
			$("body").on("click",".buy-button-amazon",function(e){
				_url = $(this).attr("href");
				e.preventDefault();
				(function(){
					var colombiaPixelURL = 'https://ade.clmbtech.com/cde/eventTracking.htm?pixelId=905&_w=1&rd='+new Date().getTime();
					(new Image()).src  = colombiaPixelURL;
				})();
				$.cookie("utm_campaign", null, { path: '/' });
				window.open(_url,'_blank','','');
				//setTimeout(function(){  }, 1000);
			});
		}
}

function QuestionAndAnswer(){
	$(".answer-box").each(function(){
		var _this = this;
		var _count = $(_this).find("dd").length;
		if(_count > 2){
			$(_this).find("dd:gt(1)").addClass("displaynone");
			$( _this).find(".moreanswer").removeClass("displaynone").addClass("displayblock");
		}	
	});
	$("body").on("click",".moreanswer",function(e){
		e.preventDefault();
		$(this).closest(".answer-box").find("dd").removeClass("displaynone");
		$(this).addClass("displaynone").removeClass("displayblock");
	});
	$("body").on("click",".leavecomment",function(e){
		e.preventDefault();
		$(this).addClass("displaynone");
		$(".QuestionAnswer").addClass("displaynone");
		$(this).next("form").addClass("displayblock").removeClass("displaynone");
	});
	$("body").on("click",".commentcancel",function(e){
		e.preventDefault();
		$(this).closest("form").addClass("displaynone");
		$(this).closest("form").find("textarea").removeClass("onError");
		$(this).closest("form").find(".error").html('&nbsp;').removeClass("errorMsg");
		$(".leavecomment").removeClass("displaynone");
	});
	$("body").on("submit","form[name=customerQuestion]",function(){
		var _this=this;
		var _evalForm = evalForm(_this);
		if($(".customerQuestionReponse").hasClass("success")){
			$(".customerQuestionReponse").removeClass("success")
		}
		if($(".customerQuestionReponse").hasClass("error")){
			$(".customerQuestionReponse").removeClass("error")
		}
		if( _evalForm == true){
			$("body").append("<div class='overlay opacity'></div><div class='loading'>Loading..</div>");
			$.ajax({
			 	url:_this.action,
			 	dataType:'json',
			 	type:_this.method,
			 	data:$(_this).serializeArray()
			 	}).done(function(response){
			 		if(response.responseCode=="success"){
						$(".customerQuestionReponse").html(response.responseMsg).addClass("success");
						$("#askQuestion").val("");
					}else{
						$(".customerQuestionReponse").html(response.responseMsg).addClass("error");
					}
				 		
				 }).fail(function(xhrObj,xhrStatus,xhrError){
				 	alert("something wrong please try again");
				    console.log(xhrError);	
				 }).always(function(){
				 	$(".overlay, .loading").remove();				 	
				 });
				 return false;
			}
		
		return false;
	});
	$("body").on("submit","form[name=QuestionAnswer]",function(){
		var _this=this;
		var questionId = $(_this).attr("data-id");
		var _evalForm = evalForm(_this);
		if( _evalForm == true){
			$("body").append("<div class='overlay opacity'></div><div class='loading'>Loading..</div>");
			$.ajax({
			 	url:_this.action,
			 	dataType:'json',
			 	type:_this.method,
			 	data:$(_this).serializeArray()
			 	}).done(function(response){
			 		//console.log(response);
			 		if(response.responseCode=="success"){
						$("#"+questionId).html(response.responseMsg).addClass("success");
						$(_this).addClass("displaynone");
						$("#"+questionId).next("a.response").addClass("displaynone").removeClass("displayblock");
					}else{
						$("#"+questionId).html(response.responseMsg).addClass("error");

				}
				 		
				 }).fail(function(xhrObj,xhrStatus,xhrError){					 
				  //  console.log(xhrError);
					alert("something wrong please try again");				  ;
				 }).always(function(){
				 	$(".overlay, .loading").remove();					 	
				 });
				 return false;
			}
		return false;	
	});
}

function adsCheck(){
		$("*[data-slot=256682]").addClass("adblock");
}


function PerpetualScroll(){
	$(window).scroll(function() {  
		$('.nextArticleCall').each(function() {
			var _id = this.id;
			var _refreshAd = $("#"+_id).offset().top+50; 
			var currentScroll = $(window).scrollTop();
			var _url = $("#"+_id).attr("href");
			var _current_url = $("#"+_id).attr("data-current-url");  
			var _next_title = $("#"+_id).attr("data-next-title");  
			var _current_title = $("#"+_id).attr("data-current-title");
			if (currentScroll >= _refreshAd) {
			if($(this).hasClass("datacheck")){
					$(this).removeClass("datacheck");
					history.pushState(null, null, _url);
					$("#article-"+_id).html("<div class='ptag'><img src='/mobile/img/AjaxLoader.gif'><div class='loading-text'>Please wait Next Article is loading...</div></div>");
					$.ajax({
						  url: _url,
						  dataType: "html"
						}).done(function(data) {;
							
							$("#article-"+_id).html($(data).find(".article-show").html());
							 $("title").html(_next_title);
							 if(!paramsconfig._isPrimeUser || paramsconfig._isPrimeUser!="true"){
									_isPrimeUserFun();
								}
						}).fail(function(data) {
							//alert("Next article is not exists.");
						}).always(function() {
							
					  });
				}
				if($(this).hasClass("callnext")){
					history.pushState(null, null, _url);
					$(this).removeClass("callnext").addClass("callback");
					_pushPageViewData();
				}
				
			 }
			if (currentScroll < _refreshAd){
				if($(this).hasClass("callback")){
					$(this).removeClass("callback").addClass("callnext");
					 $("title").html(_current_title);
					history.pushState(null, null, _current_url);
					_pushPageViewData();
				}
			 }
		});
	});
		
}
function _pushPageViewData(){
	googletag.pubads().refresh([window.slot]);
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-2021280-10', 'auto');
	ga('set', 'dimension5', id[1]);
	ga('send', 'pageview');
	setTimeout(function(){ 
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','_ga');
		_ga('create', 'UA-198011-4', 'auto');
		_ga('set', 'dimension5', id[1]);
		_ga('send', 'pageview');
	}, 2000);
}
function compare(){
	if($(".card").hasClass("checkdiff")){
		_fixcomparescroll();
	}
	
	$("body").on("click",".closeX", function(e){
		$(".brand, .brandlist").addClass("active");
		$(".brand, .brandlist li").show();
		$(".model, .modellist").removeClass("active");
		$(".brandName, .modelName").val('');
		$(this).closest(".compare-form").remove();
		$(".tab-wrapper").css("display",'block');
		$("body").removeClass('fixed-position');
		
	});
	var _form_html = $(".compare-form").clone();
	$("body").on("click",".tab-wrapper",function(){
		$(this).css("display","none");
		$(_form_html).insertAfter(this);
		setTimeout(function(){$("#compareFrom input[name=brandName]").focus(); }, 500);
		$("body").addClass('fixed-position');
	});
	
	$("body").on("keyup",".brandName, .modelName",function(){
		_this = this;
	    var yourtext = $(_this).val();
	    if (yourtext.length > 0 && yourtext!='') {
	        var abc = $(this).parent(".field-wrapper").find("ul.brandsName li").filter(function () {
	            var str = $(this).text();
	            var re = new RegExp(yourtext, "i");
	            var result = re.test(str);
	            if (!result) {
	                return $(this);
	            }
	        }).hide();
	    } else {
	        $(_this).parent(".field-wrapper").find("ul.brandsName li").show();
		}
	});	
	
	$("body").on("click",".brandlist .brandsName li",function(){
		_loading();
		$("#_modelname").html('');
		var _brandname = $(this).attr("data-brand");
		var _category_page = $(this).attr("data-category");
		_check_click = $(this).parents("li").attr("data-check");
		$("#compare input[name=brand]").val(_brandname);
		$(".brand, .brandlist").removeClass("active");
		$(".model, .modellist").addClass("active");
		if($(".modellist li").hasClass(_brandname)){
			$("."+_brandname).removeClass("displaynone");
		}
		_url = '/getBrandModel?brandModel='+_brandname+'&category='+_category_page+'';
		fetch(_url,{ headers: { "Content-Type": "application/json; charset=utf-8" }})
		  .then(function(response) {
			return response.json();
		  })
		  .then(function(myJson) {
			  if(myJson.modelList != ''){
				  $.each(myJson.modelList, function(k, v) {
						$(".overlay, .loading").addClass('displaynone');
						setTimeout(function(){$("#compareFrom input[name=modelName]").focus(); }, 500);
						$("#_modelname").append("<li class='"+v['BRAND']+"' data-id='"+v['PRODUCT_ID']+"' >"+v['PRODUCT_NAME']+"</li>")
					});
				  }else{
					  alert('Please try with different brand.'); 
					  $(".brand").click();
					  $(".overlay, .loading").addClass('displaynone');
				  }
			
		  })
		  .catch(err => {
				alert("sorry, there are no results for your search");
				$(".overlay, .loading").addClass('displaynone');
			});
	});
	
	$("body").on("click",".modellist .brandsName li",function(){
		var _modelid = $(this).attr('data-id');
		var _modelname = $(this).text();
		_modelname = _modelname.replace(/[^a-zA-Z0-9\s]/g, "-").replace(/[ ]/g,"-");
		_modelname = _modelname.replace(/-+/g, "-")
		var _com_id = $("#compare input[name=fModel]").val();
		_check_click = $(this).parents("li").attr("data-check");
		if(_com_id !=_modelid){			
			if(_check_click=='1'){
				$("#compare input[name=fModel]").val(_modelid);
				$("#compare input[name=fModelN]").val(_modelname);
			}else if(_check_click=='2'){
				$("#compare input[name=sModel]").val(_modelid);
				$("#compare input[name=sModelN]").val(_modelname);
			}
			$("#compare input[name=model]").val(_modelid);
			
			var _currentUrl = createCompareUrl();
			$("form[name=compare]").attr('action', _currentUrl);	
			$("form[name=compare]").submit();
		}else{
			alert("This product is already selected, Please try another Product");
		}
	});
	$("body").on("click",".brand",function(e){
		e.preventDefault();
		$(".brand, .brandlist").addClass("active");
		$(".model, .modellist").removeClass("active");	
		$(".compare-form")[0].reset();
	});
	
	$("body").on("click",".proclose",function(e){
		e.preventDefault();
		_loading();
		var _remove_position = $(this).attr("data-close");
		var _remove_id = $(this).attr("data-product");
		_url = '/removeModel?removeIndex='+_remove_position+'&productId='+_remove_id;
		fetch(_url,{ headers: { "Content-Type": "application/json; charset=utf-8" }})
		  .then(function(response) {
			return response.json();
		  })
		  .then(function(myJson) {
			  if(myJson != ''){
				  $.each(myJson, function(k, v) {
						window.location.href=myJson.newUrl;
					});
				  }else{
					  alert('Please try again.'); 
					  $(".overlay, .loading").addClass('displaynone');
				  }
			
		  })
		  .catch(err => {
				alert("sorry, there are no results for your search");
				$(".overlay, .loading").addClass('displaynone');
			});	
	})
	
/*	$("body").on("click",".productAdded", function(){
		//$(".selectlist").addClass("listhover");
		if($(".selectlist").hasClass("hidden")){
			$(".selectlist").removeClass("hidden");
			$("body").removeClass('fixed-position');
		}else{
			$(".selectlist").addClass("hidden");
		}
	});
	*/
	$("body").on("mouseout",".selectlist", function(){
		if($(".selectlist").hasClass("hidden")){
			$(".selectlist").removeClass("hidden");
		}else{
			$(".selectlist").addClass("hidden");
		}
	});
	
	$("body").on("click",".selectlist li", function(){
		var _sr = $(this).attr("data-id");
		$(".selectFeatures").html(_sr);
		$(this).closest("ul").addClass("hidden");
		if($("#"+_sr).hasClass("collapse")){
			$("#"+_sr).removeClass("collapse").addClass("expanded");
			$("#"+_sr).next(".listrow").removeClass("hidden");
		}
		$('html, body').animate({
			scrollTop: $("#"+_sr).offset().top-100
		}, 2000);
	});
	$("body").on("click",".listhead", function(){
		if($(this).hasClass("collapse")){
			$(this).removeClass("collapse").addClass("expanded");
			$(this).next(".listrow").removeClass("hidden");
		}else{
			$(this).addClass("collapse").removeClass("expanded");
			$(this).next(".listrow").addClass("hidden");
			//$(this).next(".listrow").removeClass("collapsecheck");
		}
		
	});
	$("body").on("click",".showdifferences span", function(){
		if($(this).hasClass("check-right")){
			$(this).removeClass("check-right").addClass("not-check");
			
			$(".comparelist").removeClass("comparelistshow");
		}else{
			$(this).addClass("check-right").removeClass("not-check");
			$(".comparelist").addClass("comparelistshow");
		}
		
	});
	_checknew();
	function _checknew(){
		var _arrayCount = $("#arrayCount").val();
			_arrayCountNew = _arrayCount.split("##");
			$.each( _arrayCountNew, function( key, value ) {
					if($("#"+value).next("similar")){
						$("#"+value).addClass("collapsecheck");
					}
			});
			 
	}
	
	
	function _fixcomparescroll(){
		var _lastdivId = $(".listhead").last().attr('id');
		var a = function() {
	        var b = $(window).scrollTop();
	        var d = $(".compareheader").offset().top;
	        var _ctn_p = $('#'+_lastdivId).offset().top-48;
	        var c = $(".admobilecompare");
	        if (b > d && b < _ctn_p) {
	        	c.addClass('fixed-ads').removeClass('absolute-ads');
	        }
	        if(b < d){
	        	c.removeClass('fixed-ads').removeClass('absolute-ads');
	        }
	        if(b > _ctn_p){
	        	c.addClass('absolute-ads').removeClass('fixed-ads');
	        }
	    };
	    $(window).scroll(a);a();
	}
	
	function _loading(){
		$("body").append("<div class='overlay opacity'></div><div class='loading'><img src='/mobile/img/AjaxLoader.gif'></div>");
	}
	
	function createCompareUrl()
	{
		var _urlSeprator = '';
		var _currentUrl = ""+window.location;
    	var fIndex = _currentUrl.indexOf("/compare-"); 
    	var sIndex = _currentUrl.lastIndexOf("/"); 
    	if(fIndex != sIndex)
    		{
    		_currentUrl = _currentUrl.substring(0,sIndex);
    		}
    	_currentUrl = _currentUrl+'/';
		if(undefined != $("#compare input[name=fModelN]").val() && ''!=$("#compare input[name=fModelN]").val())
			{
			_currentUrl = _currentUrl+($("#compare input[name=fModelN]").val())+'-'+($("#compare input[name=fModel]").val());
			_urlSeprator = '-vs-';
			}
		if(undefined !=$("#compare input[name=sModelN]").val()  && ''!=$("#compare input[name=sModelN]").val())
		{
			_currentUrl = _currentUrl+_urlSeprator+($("#compare input[name=sModelN]").val())+'-'+($("#compare input[name=sModel]").val());
		}
		
		return _currentUrl;
	}
	
	
}
function _addCompare(){
 $("body").on("click",".compareProduct",function(e){
		e.preventDefault();
		var _getCategory = $(this).attr("data-ctg");
		var  _getProductid= $(this).attr("data-product");
		_url = '/pdpCompare?categoryId='+_getCategory+'&productId='+_getProductid;
		fetch(_url,{ headers: { "Content-Type": "application/json; charset=utf-8" }})
		  .then(function(response) {
			return response.json();
		  })
		  .then(function(myJson) {
			  if(myJson != ''){
				  $.each(myJson, function(k, v) {
						window.location.href=myJson.newUrl;
					});
				  }else{
					  alert('Please try again.'); 
				  }
			
		  })
		  .catch(err => {
				alert("sorry, there are no results for your search");
							});	
	})
			 
}
