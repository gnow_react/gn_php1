(function() {
  jQuery(function() {
  	if(jQuery('#ccNumber,#ccNumberemi,#dcNumber').length > 0){
	    return jQuery('#ccNumber,#ccNumberemi,#dcNumber').validateCreditCard(function(result) {
	      $(".maestro-info").css("display","none");
	      $("#ccNumber_error").html("").hide();
	      $("#ccNumberemi_error").html("").hide();
	      $("#dcNumber_error").html("").hide();
	      if (!(result.card_type != null)) {
//	        jQuery('.cards li').removeClass('off');
//	        jQuery('#ccNumber,#ccNumberemi,#dcNumber').removeClass('valid');
	    	$("#ccNumber_error").html("").hide();
	    	$("#ccNumberemi_error").html("").hide();
	    	$("#dcNumber_error").html("").hide();
			setcardMessage("cardMessageContainer","");
	        return;
	      }
	      jQuery('#ccNumber,#ccNumberemi,#dcNumber').removeClass('onError');
		  setcardMessage("cardMessageContainer",result.card_type.name);
		  initCardFeatures(result);
		  isCardBinValid(result);
//	      jQuery('.cards li').addClass('off');
//	      jQuery('.cards .' + result.card_type.name).removeClass('off');
	      if (result.length_valid && result.luhn_valid) {
	    	  $("#ccNumber_error").html("").hide();
	    	  $("#ccNumberemi_error").html("").hide();
	    	  $("#dcNumber_error").html("").hide();
	    	  return;
//	        return jQuery('#ccNumber,#ccNumberemi,#dcNumber').addClass('valid');
	      } else {
		    	$("#ccNumber_error").html("Invalid Card").hide();
		    	  $("#ccNumberemi_error").html("Invalid Card").hide();
		    	  $("#dcNumber_error").html("Invalid Card").hide();
		    	  return;
//	        return jQuery('#ccNumber,#ccNumberemi,#dcNumber').removeClass('valid');
	      }
	    });
  		
  	}	
  });
}).call(this);

/*---------  Dynamically change the CVV maxlength on detecting the card type ------------*/
		$("#ccNumber,#ccNumberemi,#dcNumber").blur(function(){
			cardTypeOnPayment=getCardType(this.value);
			var _form = $(this).closest('form');
			var _length = 4;
			switch(cardTypeOnPayment){
			case 'VISA':
			    _length = 3;				
				break;
			case 'MC':
			    _length = 3;
				break;
			case 'AMEX':
				  _length = 4;
			default:	
				 _length = 4;
				break;	
			}
			$(_form).find("#ccCVV").attr("maxlength",_length);
			$(_form).find("#ccCVV").attr("value","");
		});


/*-------------- eithr of two phone no validation---------------*/
$(document).ready(function(){
	$('.mobNum').blur(function(){
		var newval = "no";
		if($('#contactMobile').val().split(' ').join('')==""){
			if($('#contactLandline').val().split(' ').join('')==""){;
				$('#phonevalidate').val("");
			}else{
				newval=$('#contactLandline').val().split(' ').join('');
				$('#phonevalidate').val(newval);
			}
		}else{
			newval=$('#contactMobile').val().split(' ').join('');
			$('#phonevalidate').val(newval);
		}
	});



$("body").on("submit","form.paymode",function(){
	var form = this;
	if(jQuery('#order_amount').val() <= 0){
	form.mode.value = "GIFT_CARD";
	submitForm(form, "GIFT_CARD", "");
		 	return;
	}
	var formvalidate = evalForm(form);
	var mode =form.mode.value;
	var value = "";
	if(formvalidate == 'true' || formvalidate == true)
	{
		if(mode == "EXT_COD")
		{ 
			var _check_capcha = $("#check_capcha").val();
			if(_check_capcha=='true') {
			$('#cod_captcha').removeClass('onError');
			$("#cod_captcha_error").hide();
			var submitToUri = "/control/captchaValidate";
			jQuery.ajax({
				type: "POST",
				async: false,
				url: submitToUri,
				data: 'input='+jQuery('#cod_captcha').val(),
				success: function(html){
				
					if($.trim(html) == "false" || $.trim(html) == false)
					{
					    $('#cod_captcha').addClass('onError');
						$("#cod_captcha_error").addClass('errorMsg').html('Wrong Captcha Value').show();
						return false;
					}
					else
					{
						submitForm(form, mode, value);

					}
				},
				error:function(){alert('error');}
			});
		//	submitForm(form, mode, value);
			}else{
				submitForm(form, mode, value);
			}
		}
		else if(mode == "PAYMATE")
		{ 
			var mobilenumber = $('#paymatemobile').val();
			var submitToUri = "/control/paymateValidator";
			$.ajax({
				type: "GET",
				async: false,
				url: submitToUri,
				data: 'mobileNo='+$('#paymatemobile').val(),
				success: function(val){ 
					
					if(val.trim() != "true")
					{
						alert('You are not a registered Paymate user!!');
						return;
					}
					else
					{
					 	submitForm(form, mode, value);
					}
				}
			
			});
		}
		else if(mode == "PAYTM")
		{submitForm(form, mode, value);}
		else if(mode == "MOBIKWIK")
		{submitForm(form, mode, value);}
		else if(mode == "JIOWALLET")
		{submitForm(form, mode, value);}
		else if(mode == "OXIGEN")
		{submitForm(form, mode, value);}
		else if(mode == "AMEXEZ")
		{submitForm(form, mode, value);}
		else if(mode == "CREDIT_CARD")
		{
			var ccNumber = form.ccNumber.value;
			form.cardNumber.value = ccNumber.encrypt();
			form.binNumber.value = ccNumber.substring(0, 6);
			var ccCVV = form.ccCVV.value;
			form.cardCVV.value = ccCVV.encrypt();
			form.payment_mode.value = mode;
			cardTypeOnPayment=getCardType(ccNumber);
			form.cardType.value = cardTypeOnPayment;
			$('#ccNumber').remove();
			$('#ccCVV').remove();
			if(jQuery('#sameAsShipCB').is(':checked')){
				jQuery('#sameAsShip').attr('value','y');
			}
			var gcno = jQuery('#gcno').val();
			//keep this hack for future AMEX promotion
			if(checkoutPayment.isAmexGC && "AMEX" != jQuery("#cardType").val())
			{
				alert('This offer is valid only on Amex cards');
				return;
			}
			submitForm(form, mode, value);
		}
		else if(mode == "DEBIT_CARD")
		{
			var dcNumber = form.dcNumber.value;
			form.cardNumber.value = dcNumber.encrypt();
			form.binNumber.value = dcNumber.substring(0, 6);			
			var dcCVV = form.dcCVV.value;
			form.cardCVV.value = dcCVV.encrypt();
			cardTypeOnPayment=getCardType(dcNumber);
			form.cardType.value = cardTypeOnPayment;
			$('#dcNumber').remove();
			$('#dcCVV').remove();
			submitForm(form, mode, value);
		}
		else if(mode.indexOf("_EMI") != -1)
		{
			var ccNumberemi = form.ccNumberemi.value;
			cardTypeOnPayment=getCardType(ccNumberemi);
			form.cardNumber.value = ccNumberemi.encrypt();
			form.binNumber.value = ccNumberemi.substring(0, 6);	
			var ccCVVemi = form.ccCVVemi.value;
			form.cardCVV.value = ccCVVemi.encrypt();
			form.cardType.value = cardTypeOnPayment;
			$('#ccNumberemi').remove();
			$('#ccCVVemi').remove();
			submitForm(form, mode, value);
		}else if(mode == "NETBANKING")
			{
				var bank = form.netBankSelect.value;
				//alert(bank);
				submitForm(form,mode,bank);
			}
		else if(mode == "UPI")
			{	
				//alert(bank);
				submitForm(form,mode,bank);
			}
		else
		{
	    	//alert("type:"+form.type.value+"------is_indian_card:"+form.is_indian_card.value+"-----Mode:"+form.payment_mode.value);
	    	submitForm(form, mode, value);
	   	}
		return false;
	}
	else{
		return false;
	}
	
});
	// $("body").on("click","input[name=chqPayMode]",function(){
		// var _paycheck = $(this).prop("checked", true).val();
		// $("form[name=othercheckoutInfoForm] input[name=mode]").val(_paycheck);
		// if(_paycheck=="PAYZAPP"){
			// $("#paymatemobile").removeClass("onError").attr("disabled","disabled");
			// $("#paymatemobile_error").html("&nbsp;");
		// }
		// if(_paycheck=="PAYMATE"){
			// $("#paymatemobile").removeAttr("disabled");			
		// }
	// });	
	
	$("body").on("change",".Others form[name=othercheckoutInfoForm] select[name=payselect]",function(){
			var _pay_mode = $(this).val();
			$("form[name=othercheckoutInfoForm] input[name=mode]").val(_pay_mode);
			$(".paymodecheck").addClass("displaynone");
			$(this).attr('validate','{true,blank,Please select paymode}');
			$("#paymatemobile").attr('validate','{true,blank,Please enter your 10 digit mobile number,phoneIND,Please enter your 10 digit mobile number}');
			$(".Others form[name=othercheckoutInfoForm] input[name=chqPayMode]").prop("checked",false);
			if(_pay_mode=='PAYMATE'){
				$("#paymatemobile").removeAttr("disabled").removeClass("displaynone");
			}else{
				$("input[name=paymatemobile]").attr("disabled","disabled").addClass("displaynone");
				$("#paymatemobile_error").html("");
				$("#paymatemobile").removeClass("onError");
			}
			$("."+_pay_mode).removeClass("displaynone");
			
	});
	$("body").on("click", ".Others form[name=othercheckoutInfoForm] input[name=chqPayMode]",function(){
		if($(this).prop("checked", true)){ 
		var _pay_mode = $(this).val();
		$("form[name=othercheckoutInfoForm] input[name=mode]").val(_pay_mode);
			$("#payselect").removeAttr("validate");
			$("#paymatemobile").removeAttr("validate").addClass("displaynone").removeClass("onError");	
			$('#payselect option').prop('selected', function() {
				return this.defaultSelected;
			});
			$("#paymatemobile_error").html("&nbsp;");
		}
	});
	/*payment page*/
	_initPayment();
});

function _initPayment(){
	$("body").on("click",".payment-type", function(e){ 
		e.stopPropagation();
		$(".expresstag").removeClass("displaynone");
		$(".payment-type").removeClass("acitve").removeClass("text-bold");
		if($(".payable .codAdded")!=null && !$(".payable .codAdded").hasClass("displaynone")){
			$(".payable .codAdded").addClass("displaynone");
			$(".payable .noCod").removeClass("displaynone");
			$(".codAddedTotal").addClass("displaynone");
			$(".noCodTotal").removeClass("displaynone");
		}
		$(this).addClass("text-bold");
		$(".payment-content form").addClass("displaynone");
		$(".payment-content form input[type='radio']").prop( "checked", false );
		$(".payment-widget input[type='text']").val("");
		$("select").prop( 'selectedIndex',0);
		$(".emi-table").addClass("displaynone");
		$(".emi-bank").addClass("displaynone");
		if($(this).next(".payment-content form").hasClass("displaynone")){
			$(this).next(".payment-content form").removeClass("displaynone").addClass("displayblock");
			$(this).addClass("acitve");
			if($(this).hasClass("codCharge")){
				$(".expresstag").addClass("displaynone");
				if($(".payable .codAdded").hasClass("displaynone")){
					$(".payable .codAdded").removeClass("displaynone");
					$(".payable .noCod").addClass("displaynone");
					$(".codAddedTotal").removeClass("displaynone");
					$(".noCodTotal").addClass("displaynone");
				}
			}
			$('html, body').animate({
                        scrollTop: $(this).offset().top -80
             }, 200);
		}
		try{
		$("#cardMessageContainer").remove();
		$(".payment-content.credit-card button.paynow").show();
		}catch(errMg){
			
		}
	});
	$("body").on("change",".emi-bank-select",function(){
		var _optionselect = $(this).val();
		$(".emi-table").addClass("displaynone");
		$(".emi-bank").addClass("displaynone");
		$("table input[type='radio']").prop( "checked", false );
		$("#_emi_"+_optionselect).removeClass("displaynone");
	});
	
	$("body").on("click","input[type='radio']", function(){
		if($(".emi-bank").hasClass("displaynone")){
			$(".emi-bank").removeClass("displaynone");
	}	
	});
	
	$("body").on("keyup","#cod_captcha",function(e){
		e.preventDefault();
		if(e.keyCode == 13){
			$("form[name=CodcheckoutInfoForm] .paynow").click();
		}
		});

	$("body").on("click",".reloadcapcha",function(e){
		e.preventDefault();
		applyLazyLoad();
		reloadCaptcha();
	});
	initCashOnDeliveryOTP();
}
$(document).on("click",".resend-link",function(e){
	e.preventDefault();
	$("input.otp-textbox").removeClass("otp-filled").val("");
	$("#isResendClicked").val("fromResend");
});

function initCashOnDeliveryOTP(){
	var _submit_button = $(".cod-send-otp");
	$(document).on("click",".cashondelivery button.cod-send-otp, .resend-link",function(e){
		e.preventDefault();
		$('body').append("<div class='overlay opacity'></div><div class='loading'><img src='/mobile/img/AjaxLoader.gif'></div>");	
		$.ajax({url:"/control/sendCODOTP",dataType:"JSON",data:{"isResendClicked":"fromResend"}}).done(function(response){
		if(response.response == "success"){	
			  $(".cashondelivery .actionable-container").removeClass("displaynone");
			  $(_submit_button).html("Place order").addClass("disabled").removeClass("cod-send-otp").prop("disabled",true);
			  $(".cashondelivery .actionable-container .message").removeClass("displaynone error success").addClass("success").html("Verification code sent to "+response.mobileNo);
			  $(".cod-text-verification").hide();
			}else{
		      $(".cashondelivery .actionable-container .message").removeClass("displaynone error success").addClass("error").html(response.message);				
			}
		
		 if(response.show_resend == "true"	){
			 $(".resend-link").removeClass("displaynone");
			 $(".done-resend-link").addClass("displaynone");
		 }else{
			 $(".resend-link").addClass("displaynone");
			  $(".done-resend-link").removeClass("displaynone");
		 }	
		}).fail(function(xhrError){
			
		}).always(function(){
		   $(".overlay,.loading").remove();
		});
	});

	$(document).on("click",".done-resend-link",function(e){
		e.preventDefault();
		//$("form[name=CodcheckoutInfoForm] :input").prop("disabled",true);
		$("form[name=CodcheckoutInfoForm]").append("<input type=\"hidden\" id=\"codOtpVerified\" name=\"codOtpVerified\" value=\"N\" />");
		$("form[name=CodcheckoutInfoForm] input[type=number]").attr("disabled","disabled");
		submitForm($("form[name=CodcheckoutInfoForm]"), "EXT_COD", "");
	});
	$(document).on("submit","form[name=CodcheckoutInfoFormMobiWik]",function(e){
		e.preventDefault();
		var _checkloginform = evalForm(this);
			if(_checkloginform == true){
				submitForm($("form[name=CodcheckoutInfoFormMobiWik]"), "MOBIKWIKNEW", "");
			}
	});
	$(document).on("submit","form[name=CodcheckoutInfoFormAmazonPay]",function(e){
		e.preventDefault();
		var _checkloginform = evalForm(this);
			if(_checkloginform == true){
				submitForm($("form[name=CodcheckoutInfoFormAmazonPay]"), "AMAZONPAY", "");
			}
	});
	$(document).on('click', '.clearOTP', function(e){
		e.preventDefault();
		$("input.otp-textbox").val("");
		//$(".otpBoxes").removeClass("error success");
		$(".cashondelivery .actionable-container .message").html("").removeClass("error success").addClass("displaynone");
	});
	function validateOTP(){		
		$('body').append("<div class='overlay opacity'></div><div class='loading'><img src='/mobile/img/AjaxLoader.gif'></div>");
		var _otp = "";
		$(".otp-textbox").each(function(indx, elem){_otp += $(elem).val();});
		$(".actionable-container .message").html("");
		$.ajax({"url":"/control/vaildateCODOTP","dataType":"JSON",data:{"inputOtp":_otp}}).done(function(response){
		if(response.response == "success"	){
			if(response.isOtpCorrect=="Y"){				
				  if(response.isOtpExpired=="Y"){
					$(".cashondelivery .actionable-container .message").removeClass("displaynone error success").addClass("error").html(response._EVENT_MESSAGE_);
					$(".cashondelivery .actionable-container .message").append("<div class=\"clearOTP-container\"><a class=\"clearOTP\" href=\"#\">Clear</a></div>");
					//$(".resend-link").hide();
					$("#isResendClicked").val("expired");
					$(_submit_button).html("Place order").addClass("disabled").removeClass("cod-send-otp").prop("disabled",true);					
				  }
				  if(response.isOtpExpired=="N"){
					$(".cashondelivery .actionable-container .message").removeClass("displaynone error success").addClass("success").html(response._EVENT_MESSAGE_);
					$("form[name=CodcheckoutInfoForm] input[type=number]").attr("disabled","disabled").attr("readOnly",true);
					 $(".resend-link").addClass("displaynone");
					  //$(".done-resend-link").removeClass("displaynone");
					$(_submit_button).html("Place order").addClass("disabled").removeClass("disabled").prop("disabled",false);
					//submitForm($("form[name=CodcheckoutInfoForm]"), "EXT_COD", "");
					//console.log("Submit form");
				  }
			  }
			  if(response.isOtpCorrect=="N"){
		    	$(".cashondelivery .actionable-container .message").removeClass("displaynone error success").addClass("error").html(response._EVENT_MESSAGE_);	
				$(".cashondelivery .actionable-container .message").append("<div class=\"clearOTP-container\"><a class=\"clearOTP\" href=\"#\">Clear</a></div>");
				$(_submit_button).html("Place order").addClass("disabled").removeClass("cod-send-otp").prop("disabled",true);
				//$(".cashondelivery button.validate-otp").addClass("disabled").prop("disabled",true);				
			  }
			  
         }
		}).fail(function(xhrError){
			
		}).always(function(){
		  $(".overlay,.loading").remove();
		});
		  return false;
	};
	
		
	$(document).on("keyup",".otp-container input.otp-textbox",function(){
		if (this.value.length > 1) {this.value = this.value.slice(0,1);}
		$(".cashondelivery .actionable-container .message").removeClass("error success").addClass("displaynone").html("");
		if($(this).val() != ""){$(this).addClass("otp-filled")}else{$(this).removeClass("otp-filled")}
		var _filled_count = parseInt($("input.otp-filled").length);
		if(_filled_count < 4){
			 if($(this).val() != "" && $(this).val().length == 1){
				$(this).next("input.otp-textbox").focus();
			 }else{
				$(".cashondelivery button.validate-otp").addClass("disabled").prop("disabled",true);
			 }
		}else{
			$(".cashondelivery button.validate-otp").removeClass("disabled").prop("disabled",false);
			validateOTP();
		}
	});
}

function submitForm(form, mode, value) {
	//form.payment_mode.value=mode;
    //form.action="/payment/";
	
	$('body').append("<div class='overlay opacity'></div><div class='loading'><img src='/mobile/img/AjaxLoader.gif'></div>");	
	form.submit();
}

function getCardType(_val){	
	var _first_digit=_val.substr(0,1);
	var _card_type;
	switch(_first_digit){
	case '4':
		_card_type="VISA"; 
		break;
	case '5':
		if(isMaestroCard(_val))
			_card_type="MAEST";
		else
			_card_type="MC";
		break;
	case '6':
		if(isMaestroCard(_val))
			_card_type="MAEST";
		else
			_card_type="";
		break;
	case '3':
		_card_type="AMEX";
		break;
	default:
		_card_type="";
		break;
	}
	setBillingIntroText(_card_type);
	setCVVvalidateCode(_card_type);
	return _card_type;	
}
function setCVVvalidateCode(_card_type)
{
	if("MAEST" == _card_type)
		$('#dcselectMonth, #dcselectYear, #dcCVV').removeAttr('validate');
	else
	{
		$('#dcselectMonth').attr('validate','{true,blank,Please choose Month}');
		$('#dcselectYear').attr('validate','{true,blank,Please choose Year,chkexp,Invalid Expiry Date}');
		$('#dcCVV').attr('validate','{true,blank,Card number required,customCardNo,dcCVV}');
	}
}

function isMaestroCard(_val)
{
	//var bin = ["502260","504433","504434","504435","504437","504645","504681","504753","504775","504809","504817","504834","504848","504884","504973","504993","508125","508126","508159","508192","508227","600206","603123","603741","603845","622018"];
	var bin =["502964","504433","504435","504437","504441","504445","504465","504493","504645","504753","504774","504775","504809","504817","504834","504883","504884","504958","504973","504993","508000","508125","508126","508159","508192","508227","508588","588599","600206","601794","603845","606362","606989","622018","603123","603741","504848","504681","504434","502260"]
	for(n=0;n<bin.length;n++){
		if(_val.match("^"+bin[n]) == bin[n]){
			return true;
		}		
	}
	return false;  
}

/*------------ set  billing address text description based on credit/debit card type  ------------*/
function setBillingIntroText(_card_type){
	$("#billingIntroText").html("");
	var _text;
	switch(_card_type){
	case "VISA":
		_text="You are requested to provide the address matching with your card statement in order to process your transaction favorably.";
		break;
	case "AMEX":
		_text="American Express uses Address Verification System(AVS) as an additional layer of authentication to avoid unauthorized usage on your card. Hence,AMEX card users are requested to provide billing address and pin code matching with address on their card statement.<strong>Please note that mismatch in Billing address with that in the bank records will lead to transaction FAILURE</strong>. ";
		break;
	case "MC":
		_text="You are requested to provide the address matching with your card statement in order to process your transaction favorably.";
		break;	
	default:
		_text="You are requested to provide the address matching with your card statement in order to process your transaction favorably.";
		break;
	
	}
	$("#billingIntroText").html(_text);
}

function ifEmiAvailable(){	
	if(!$("input[name=radioemi]:checked").attr("disabled")){
		var value = $("input[name=radioemi]:checked").val();
		if(value == 'ICICI_3_EMI' || value == 'ICICI_6_EMI' || value == 'ICICI_9_EMI' || value == 'ICICI_12_EMI')
		{
			return isValidIciciEmiCard($('#ccNumberemi').val());
		}
		else if(value == 'EXT_HDFC_3_EMI' || value == 'EXT_HDFC_6_EMI' || value == 'EXT_HDFC_9_EMI' || value == 'EXT_HDFC_12_EMI')
		{
			return isValidHDFCEmiCard($('#ccNumberemi').val());
		}
		else if($("input[name=radioemi]:checked").val() == 'STANC_3_EMI' || $("input[name=radioemi]:checked").val() == 'STANC_6_EMI')
		{
			return isValidStanCEmiCard($('#ccNumberemi').val());
		}
		else if($("input[name=radioemi]:checked").val() == 'CITI_3_EMI' || $("input[name=radioemi]:checked").val() == 'CITI_6_EMI')
		{
			return isValidCitiEmiCard($('#ccNumberemi').val());
		}
		
	}
}

var checkoutPayment={
	order_amount:'',
	cart_price:'',
	wlt_amt:0,
	//wltRewardPoints:parseInt($('.rewardspoints1').html()),
	gc_val:$('#gcval').val(),
	gc_appl:$('#isgcAppl').val(),
	selectedEmiCharges:0,
	//shippingCharges:parseInt($('#summaryShipping').val().replace('`','')),
	codCharges:0	
};




function payment(form, mode, value) {	
	if(jQuery('#order_amount').val() <= 0){
		if(checkoutPayment.wlt_amt==0){
			submitForm(form, "GIFT_CARD", value);
		 	return;
		}
		if(checkoutPayment.gc_val >0 && checkoutPayment.gc_appl==true && checkoutPayment.wlt_amt!=0 ){
			submitForm(form, "GIFT_CARD", value);
		 	return;
		}
		if(checkoutPayment.gc_val==0 && checkoutPayment.gc_appl=="false" && checkoutPayment.wlt_amt!=0 ){
			submitForm(form, "WALLET", value);
		 	return;
		}
	}
	
	if(mode == "AMEX_EZE")
	{
		submitForm(form, "AMEX_EZE", value);
	 	return;
	}
	// var formvalidate = evalForm(form);
	// var validFlag=false;
	
	
}
 
function reloadCaptcha()
{	
	var submitToUri = "/control/getCaptcha";
	$('body').append("<div class='overlay opacity'></div><div class='loading'><img src='/mobile/img/AjaxLoader.gif'></div>");
	 $.ajax({
     url: submitToUri,
	 dataType:'json',
	 success: function(data){
		$('.capcha').attr('src', data.captchaImage);
	 }
	 }).fail(function(xhrError){
			
		}).always(function(){
		   $(".overlay,.loading").remove();
		});;
}

function setcardMessage(containerId,cardName){
 // $("#"+containerId).html("");
 $("#cardMessageContainer").remove();
  switch(cardName){
  case 'amex':
  	 //$("#"+containerId).html("We accept only Safekey enabled American Express credit cards. Amex cards issued outside India, Singapore and UK will not be acceptable due to security reasons.");
 //	 $("#ccard_content .creditCardDiv div.note").html("<strong>NOTE:</strong>  After clicking \"PAY NOW\" you will be directed to your bank's website for AMEX Safekey authentication.");
     
	// $("<span id='cardMessageContainer' style='color:#ff0000'><strong>NOTE:</strong> Payments through American Express credit card is currently unavailable. Please use other credit cards to carry this transaction. </span>").insertBefore(".payment-content.credit-card button.paynow")
	// $(".payment-content.credit-card button.paynow").hide();
  break;
  default:
   // $("#"+containerId).html("");
    $("#ccard_content .creditCardDiv div.note").html("<strong>NOTE:</strong> After clicking Pay Now, you will be redirected to your bank's website for 3D authentication to verify yourself.");
  // $(".payment-content.credit-card button.paynow").show();
  break;
  }
}

function initCardFeatures(result){
			//  Dynamically change the CVV maxlength on detecting the card type 
			var _length = 4; // lets keep the default length at the max			
			if(result.card_type.name != undefined || result.card_type.name != null ){
				switch(result.card_type.name){
					case 'visa':
					    _length = 3;				
						break;
					case 'mastercard':
					    _length = 3;
						break;
					case 'amex':
						  _length = 4;
					case 'maestro':
					      $(".maestro-info").css("display","block");
					  break;	  
					default:	
						 _length = 4;
						break;	
				}
			}
			$(".ccCVV").attr("maxlength",_length);
			$(".ccCVV").attr("value","");
    	       			
			
}

function isCardBinValid(result){
	if(result.length_valid && result.luhn_valid){
		var _id="";
		var _card_num="";

		jQuery('#ccNumber:visible,#ccNumberemi:visible,#dcNumber:visible').each(function(idx,obj){
			if($(obj).is(':disabled')==false){
				_id = $(obj).attr("id");
				_card_num= $(obj).val();
			}
		});		
		$.ajax({
			type: "GET",	
			url:"/control/getCardBinInfo?cardBin="+_card_num.substring(0,6),
			dataType:"json",
			success:function(data){
				if(data.status =="SUCCESS"){
					if((typeof data.IS_INTERNATIONAL !="undefined" && data.IS_INTERNATIONAL=="Y") || (typeof data.IS_PAY_ENABLED !="undefined" && data.IS_PAY_ENABLED=="N")){
						$("#"+_id+"_error").html("We are sorry! Your card is either issued outside India or Bank doesn\'t support it").show();
						$("#"+_id).addClass("onError");
						return false;
					}else{
						$("#"+_id+"_error").hide();
					}
				}else{
					return true;
				}
			},
			error:function(){
				return true;
			}
	  });
	}

	return true;
}


/*------------------------- Card identification for ICICI EMI option ---------------------------*/

function isValidAmexEmiCard(_card_num){
		if(_card_num.length>5){
		var _extracted_card_num=_card_num.substring(0,6);
		var _valid_arr=["37693"];
		for(n=0;n<_valid_arr.length;n++){
			if(_extracted_card_num.search(_valid_arr[n]) != -1){
				return true;
			}		
		}
	}
	return false;
	
}

function isValidIciciEmiCard(_card_num){
	if(_card_num.length>5){
		var _extracted_card_num=_card_num.substring(0,6);
		var _valid_arr=["524376","517653","517638","540282","517638","517654","517638","545207","517654","523951","517638","517654","517638","517653","517637","517653","517654","517638","51772E","517654","51772E","523951","51772E","517653","525996","523951","517654","517654","540282","517637","547467","523951","523951","517719","517653","517719","517653","447746","462986","462986","447747","402369","447747","447747","40766E","447747","447748","447748","462986","447746","462987","447746","447747","402368","402368","447758","407651","462987","447747","447747","447748","447748","447748","407652","407659","447747","407651","40766E","462986","447747","407659","470573","402368","447747","447747","447746","462987","42058E","447748","407659","407651","407659","402368","407652","447748","447747","447758","447747","407651","447748","447746","407651","444341","447746","447747","447747","447747","447746","407652","447747","447748","40766E","462986","447747","447748","447748","40766E","447747","407651","444341","447747","447747","447748","437551","524193","524193","552418","474846","405533","405533","461133"];
		for(n=0;n<_valid_arr.length;n++){
			if(_extracted_card_num.search(_valid_arr[n]) != -1){
				return true;
			}		
		}
	}
	return false;
}

function isValidStanCEmiCard(_card_num){
	if(_card_num.length>5){
		var _extracted_card_num=_card_num.substring(0,6);
		var _valid_arr=["402874", "412903", "412904", "412905", "419607", "429344", "454198", "456398", "457036", "462270", "462271", "462272", "462273", "466269", "466271", "523988", "523990", "540460", "540461", "540711", "543186", "544438", "547359", "549124", "549132", "553160", "554374", "554375", "554378", "554623", "558959"];
		for(n=0;n<_valid_arr.length;n++){
			if(_extracted_card_num.search(_valid_arr[n]) != -1){
				return true;
			}		
		}
	}
	return false;
}
function isValidHDFCEmiCard(_card_num){
	if(_card_num.length>5){
		var _extracted_card_num=_card_num.substring(0,6);
		var _valid_arr=["517652","434678","436306","552088","524368","405028","461786","418136","461787","517635","434677","524111","528945","552344","545226","437546","524216","545964","552260","552274","485498","485499","552385","489377","533744","524931","559300","522852","552394","489376","556042", "558818", "436520", "556620", "467741", "553162", "467742", "524877", "404276", "488994", "532961", "532973", "457262", "553583", "558983", "360825", "360826", "360827", "457704", "524181"];
		for(n=0;n<_valid_arr.length;n++){
			if(_extracted_card_num.search(_valid_arr[n]) != -1){
				return true;
			}		
		}
	}
	return false;
}

function isValidCitiEmiCard(_card_num){
	if(_card_num.length>5){
		var _extracted_card_num=_card_num.substring(0,6);
		var _valid_arr=["517652","434678","436306","552088","524368","405028","461786","418136","461787","517635","434677","524111","528945","552344","545226","437546","524216","545964","552260","552274","485498","485499","552385","489377","533744","524931","559300","522852","552394","489376"];
		for(n=0;n<_valid_arr.length;n++){
			if(_extracted_card_num.search(_valid_arr[n]) != -1){
				return true;
			}		
		}
	}
	return false;
}

function ifEmiAvailable(){	
	if(!$("input[name=radioemi]:checked").attr("disabled")){
		var value = $("input[name=radioemi]:checked").val();
		if(value == 'ICICI_3_EMI' || value == 'ICICI_6_EMI' || value == 'ICICI_9_EMI' || value == 'ICICI_12_EMI')
		{
			return isValidIciciEmiCard($('#ccNumberemi').val());
		}
		else if(value == 'EXT_HDFC_3_EMI' || value == 'EXT_HDFC_6_EMI' || value == 'EXT_HDFC_9_EMI' || value == 'EXT_HDFC_12_EMI')
		{
			return isValidHDFCEmiCard($('#ccNumberemi').val());
		}
		else if($("input[name=radioemi]:checked").val() == 'STANC_3_EMI' || $("input[name=radioemi]:checked").val() == 'STANC_6_EMI')
		{
			return isValidStanCEmiCard($('#ccNumberemi').val());
		}
		else if($("input[name=radioemi]:checked").val() == 'CITI_3_EMI' || $("input[name=radioemi]:checked").val() == 'CITI_6_EMI')
		{
			return isValidCitiEmiCard($('#ccNumberemi').val());
		}
		if(value == 'AMEX_3_EMI' || value == 'AMEX_6_EMI' || value == 'AMEX_9_EMI' || value == 'AMEX_12_EMI')
		{
			return isValidAmexEmiCard($('#ccNumberemi').val());
		}
		
	}
}
var _mobiwiki_num = $("#mobikicontactMobile").val();
$("body").on("click",".mobiwikiedit",function(e){
	e.preventDefault();
	$(".mobiwiki_number").addClass("displaynone");
	$("#mobiwiki_number").removeAttr("disabled").removeClass("displaynone").val(_mobiwiki_num).focus();;
	$(".mobiwikicancel").removeClass("displaynone");
});
$("body").on("click",".mobiwikicancel",function(e){
	e.preventDefault();
	$(".mobiwiki_number").removeClass("displaynone");
	$("#mobiwiki_number").attr("disabled","disabled").addClass("displaynone").val('').removeClass("onError");
	$(".mobiwikicancel").addClass("displaynone");
	$("#mobiwiki_number_error").html("");
});

function applyLazyLoad(){
	$(window).scroll(function(){displayImages();});
	$(window).resize(function(){displayImages();}); 
	displayImages();   
	function displayImages(){    
		$("img.lazy").each(function(indx,elem){
	var _src='/mobile/img/defaultImage.jpg';
	var _data_original='/mobile/img/defaultImage.jpg'; 
	if($(elem).attr("src") != undefined){_src=$(elem).attr("src")}
	if($(elem).attr("data-original") != undefined){_data_original=$(elem).attr("data-original")}       		
	if(isScrolledIntoView(elem) && $(elem).attr("src")!=$(elem).attr("data-original") && $(elem).hasClass("lazy")){
		$(elem).attr("src",$(elem).attr("data-original"));
		$(elem).removeClass("lazy");
		$(elem).error(function(){
			$(elem).attr("src",'/mobile/img/defaultImage.jpg');
		})
	}
});    	
}	 
}
checkoutPayment.exBinList  = [];
checkoutPayment.inBinList  = [];
checkoutPayment.exBank  = [];
checkoutPayment.inBank  = [];
checkoutPayment.payMode  = [];
checkoutPayment.payCardTypes  = [];
