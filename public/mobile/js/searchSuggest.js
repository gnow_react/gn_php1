/**
 * @author ashutosh.joshi
 */

		var autocomplete_textbox;
		var query, pos, timer, xhr;
		var seggestionsCache = {};
				
	   function _initialize(autocomplete_textbox_id) {
	    autocomplete_textbox = autocomplete_textbox_id;
	    $("<div id='suggestions' class='searchSuggest'></div>").insertAfter($('#' + autocomplete_textbox));
	
	//	$('#' + autocomplete_textbox).val("");
		//$('#' + autocomplete_textbox).focus();
		$("body").on("click",'.searchSubmit',function() {
			if(trim($('#autocomplete_query').val()).length > 0 && $('#autocomplete_query').val() != 'Search Products') {
				var search_string = $('#autocomplete_query').val();
				var search_cat_param = searchFilters();
				search_string = search_string.replace(":", ";");
				search_string = escape(encodeURI(search_string));
				if($('#cloneglobalsearch').length > 0)
					{
					document.location = '/control/pinpointsearch?SEARCH_STRING=' + search_string+$('#cloneglobalsearch').val();
					}
					else{
					document.location = '/control/mtkeywordsearch?SEARCH_STRING=' + search_string+ search_cat_param;
					}
			}
		});
		$("body").on("click",".clear_txt",function(){
			$("#autocomplete_query").val("");
			$(this).addClass("displaynone");
		});
		$("body").on("keydown",'#' + autocomplete_textbox,function(event) { });
		$("body").on("bind",'#' + autocomplete_textbox,'paste', function() {
		    $(this).keyup();	   
		});		
	    $("body").on("keyup",'#' + autocomplete_textbox, function(event) {
	      var  string = $.trim(this.value);
		  if(this.value.length > 0) {
			 $(".cls_btn").removeClass("displaynone");
			 $("#autocomplete_query").removeClass("onError");
		  }else{
			 $(".cls_btn").addClass("displaynone");
		  }
		  
		  
		 string = string.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/g,' ');
	        if(event.keyCode == 38) {
	            var elem = $('a[suggestion]');
	            if(elem.length && elem.eq(pos).length && $('#suggestions').is(':visible')) {
	                pos--;
	                if(pos < 0) pos = elem.size() - 1;
	
	                resetHover();
	                if(elem.eq(pos).attr('suggestion').length == 0)
	                    this.value = decodeURI(query);
	                else if(this.value != elem.eq(pos).attr('suggestion'))
	                    this.value = unescape(decodeURI(elem.eq(pos).attr('suggestion')));
	                elem.eq(pos).addClass('searchSuggestHover');
	            }
	        }else if(event.keyCode == 40) {
	            var elem = $('a[suggestion]');
	            if(elem.length && elem.eq(pos).length && $('#suggestions').is(':visible')) {
	                pos++;
	                if(pos > elem.size() - 1) pos = 0;
	
	                resetHover();
	                if(elem.eq(pos).attr('suggestion').length == 0)
	                    this.value = decodeURI(query);
	                else if(this.value != elem.eq(pos).attr('suggestion'))
	                    this.value = unescape(decodeURI(elem.eq(pos).attr('suggestion')));
	                elem.eq(pos).addClass('searchSuggestHover');
	            }
	        }else if(event.keyCode == 13) {
	            var url;
	            if(pos < 0 || typeof pos == "undefined") {
	            	if($.trim(this.value).length > 0) {
						var search_string = $('#autocomplete_query').val();
						search_string = search_string.replace(":", ";");
						search_string = escape(encodeURI(search_string));
						var search_cat_param = searchFilters();
						url = "/control/mtkeywordsearch?SEARCH_STRING=" + search_string;
						if($('#cloneglobalsearch').length > 0)
						{
						document.location = '/control/pinpointsearch?SEARCH_STRING='+search_string+$('#cloneglobalsearch').val();
						}
						else{
						document.location = url+search_cat_param;
						}
	                }
	            }else {
	                var elem = $('a[suggestion]');
	                url = elem.eq(pos).attr('href');
	            	document.location = url;
	            }
	        }else if(this.value.length < 3 || event.keyCode == 27) {
	            pos = -1;
	            query = "";
	            $('#suggestions').fadeOut();
	            if(xhr) xhr.abort();
	//            $('#statistics').html('&nbsp;');
	//            $('#' + autocomplete_textbox).css('background-image', '');
	            //$('#suggestions').fadeOut();
	//            setTimeout("$('#suggestions').html('')", 500);
	        }else {
	           // if( (event.keyCode >= 46 && event.keyCode <= 90) || (event.keyCode == 8) ) {
	                pos = -1;
	                resetHover();
	
	                clearTimeout(timer);
	                timer = setTimeout("_suggestions('" + string + "'), 15");
	//                _suggestions(string);
	           // }
	        }
	    });
	    $('#' + autocomplete_textbox).blur(function() {
	        pos = -1;
	        query = "";
	        
	        if(xhr) xhr.abort();
	        
	        
	//        $('#statistics').html('&nbsp;');
	//        $('#' + autocomplete_textbox).css('background-image', '');
	        $('#suggestions').fadeOut();
	//        setTimeout("$('#suggestions').html('')", 500);
	    });
	}
	
	function _suggestions(string) {
	    if(query != string) {
	        if(xhr) xhr.abort();
	        if(!containsInSearch(seggestionsCache, string)){
	            query = encodeURI(string);
	    		var search_cat_param = searchFilters();
	    		
	          //-------------------- URL For live site
	    		var _xhr_host = "https://shop.gadgetsnow.com/control/searchsuggestion";
	          //var _xhr_host = "https://m.shop.gadgetsnow.com/gadgetssuggestion";///control/searchsuggestion";
	          _xhr_host =  _xhr_host+"?keyword=" + escape(query) + "&primaryCatalogId="+$('#searchcategory_query').val();
	          console.log(_xhr_host);
	          $.ajax({
	        	    type: "GET",
	        	    url: _xhr_host,
	        	    dataType: "JSONP",
	        		success: autosuggetCallback,
	        		error: function(data){
	        			//console.log(data);
	        		}
	        	});
	        }else{
	        	query = string;
	        	autosuggetCallback(seggestionsCache[containsInSearch(seggestionsCache, string)]);
	        }
	    }
	}
	
	
	function containsInSearch(hayStack, needel){
		var result  = false;
		try {
			for(key in hayStack)
			{
				if(key == null || $.trim(key) == ""){
					continue;
				}
				if(key.toLowerCase() == needel.toLowerCase()){
					result =  key;
				}
			}
		}catch (e) {
		}
		return result;
	}
	
	function autosuggetCallback(data){
		var suggestions = "";
		query = unescape(decodeURI(query));
		var tokens = query.split(" ");
		if(seggestionsCache[query])	{
			suggestions = seggestionsCache[query];
		}else{
			var facets = data["facets"];
			if(facets && facets.length  && facets.length > 0){
				seggestionsCache[query] = data;
				
		        for(var i=0;i<facets.length;i++) {
		            var items = facets[i]["itemList"];
		            if(items.length > 0) {
		               // suggestions += "<div class='catMajor'><span>" + facets[i]["head"] + "</span><br />";
		                
		                for(var j=0;j<items.length;j++) {
		                    var display_str = items[j]["productName"];
		                    var tags = items[j]["tags"];
		                    suggestions += "<a suggestion='" + query + "' href='" + items[j]["url"] + "' onmouseover='pos=-1;resetHover();'>";
		                    suggestions += "<div style='text-align:left;padding:0 0 3px;'>";
		                    suggestions += "<div style='overflow:hidden;height:20px;'>" + display_str + "</div>";
		                    suggestions += "</div>";
		                    suggestions += "</a>";
		                }
		            }
		        }
		    }
		}
		
		if(suggestions != ""){
			seggestionsCache[query] = suggestions;
		}else {
			suggestions  =  $('#suggestions').html();
		} 
		
		for(var x=0;x<tokens.length;x++) {
	    	var matcher = new RegExp("(<div style='overflow:hidden;height:20px;'>)(.*?)("+tokens[x]+")(.*?)(</div>)", "ig" );
	    	suggestions =  suggestions.replace(matcher, "$1$2<b>$3</b>$4$5");    	
	    }
	    if(suggestions !=""){
			$('#suggestions').html(suggestions);
		    $('#suggestions').show();    	
	    }
	    
	 }
	
	
	function resetHover() {
	    var elem = $('a[suggestion]');
	    for(var i=0;i<elem.size();i++)
	        elem.eq(i).removeClass('searchSuggestHover');
	}
	
	function searchFilters(){
		var _params="";
		 if($('#searchcategory_query')){
		 	_params="&catalog="+$('#searchcategory_query').val();
		 }
		 return _params;
		
	}
	_initialize("autocomplete_query");
