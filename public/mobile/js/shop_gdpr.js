

/*
*** Defaults object have some variables defined based on projects (e.g.: 'logoutIframeSrc').

*** consentHTML variable have HTML snippet to be displayed and respective CSS is part of gdpr_css file
*** logoutIframeSrc variable have api path to logout user. Pass blank (e.g.: ''),if we don't need this functionality.

*** consentBannerClass variable have element class name, and it would be assigned to one div containing consentHTML.
*** consentAgreeButtonId varaible have element id of agree button, it is mentioned in consentHTML.
*** personalisedAdsCheckBoxId varaible have element id of checkbox button, it is mentioned in consentHTML.

*** consentCookieName variable have cookie name w.r.t cookie consent is done or not. If cookie is present in browser. it means cookie consent is done by user.
*** consentCookieDefaultVal is '1' - To be checked
*** consentCookieAge is 'consentCookieName' cookie age ( no of days )

*** geoContCookieName variable have cookie name w.r.t geo-continent, instead of calling geoapi again, we are storing continent information in cookie.
*** geoContCookieAge is 'geoContCookieName' cookie age ( no of days )

*** consentReqCont variable contains list of continent where consent is required.
*** consentReqCountryRegions variable contains list of country_region (states) where consent is required.
*** domainName for which we need to store cookie.
*** dynamicCssSrc  variable have path of CSS, if we want EU user specfic CSS. If not pass blank ('') in value.

*** TimesGDPR.common.consentModule.isConsentGiven(); will true/ false based on consent is given or not.
*** TimesGDPR.common.consentModule.isEUuser(); will give true/ false. Same as window._euuser
*** gdprCallback function - Sample form of getting callback
TimesGDPR.common.consentModule.gdprCallback(function(dataObj){
   console.log("User belongs to EU continent: ", dataObj.isEUuser);
});

*/

TimesGDPR = window.TimesGDPR || {};
TimesGDPR.common = window.TimesGDPR.common || {};
TimesGDPR.common.getParameterByName = function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
TimesGDPR.common.checkIfPopShouldShow = function(){
    /*
    *check if url has frmapp=yes, don't show
    *check if url has self=1, don't show
    */

    return !(TimesGDPR.common.getParameterByName('frmapp') === 'yes' || TimesGDPR.common.getParameterByName('self') === '1');
}
TimesGDPR.common.consentModule = (function () {

    var cookiePolicyUrl = "https://www.gadgetsnow.com/cookiepolicy.cms";
    var privacyPolicyUrl = "https://www.gadgetsnow.com/staticpage/64123180.cms";
    
    var propertyName = 'Gadgets Now';
    
    var getConsentHTML = function(consentFor, isPersoanalizedAdsAutoTick){
        var consentHTML;
        if(consentFor === 'CCPA'){
            consentHTML = '<div class="consent-banner wrapper"><p>We use cookies and other tracking technologies to provide services while browsing the Website to show personalise content and targeted ads, analyse site traffic and understand where our audience is coming from in order to improve your browsing experience on our Website. By continuing to use our Website, you consent to the use of these cookies and accept our Privacy terms. If you wish to see more information about how we process your personal data, please read our <a target="_blank" href="' + cookiePolicyUrl + '"> Cookie Policy </a> and  <a target="_blank" href="' + privacyPolicyUrl + '"> Privacy Policy</a></p>';
            consentHTML += '<button class="close" id="consent-continue-button" type="button"></button></div>';
        }
        else{
            consentHTML = '<div class="consent-banner container wrapper"><h2>Accept the updated privacy &amp; cookie policy</h2>';
            consentHTML += '<div class="column1" id="consent-prompt"><p>We use cookies and other tracking technologies to provide services in line with the preferences you reveal while browsing the Website to show personalize content and targeted ads, analyze site traffic, and understand where our audience is coming from in order to improve your browsing experience on our Website. By continuing to browse this Website, you consent to the use of these cookies. If you wish to object such processing, please read the instructions described in our ';
            consentHTML += '<a target="_blank" href="' + cookiePolicyUrl + '"> Cookie Policy</a>';
            consentHTML += ' / ';
            consentHTML += '<a target="_blank" href="' + privacyPolicyUrl + '">Privacy Policy</a>';
            consentHTML += '.</p></div>';
            consentHTML += '<div class="column2">';
            consentHTML += '<ul><li><button id="consent-continue-button" type="button">Accept</button></li>';
            consentHTML += '</ul></div>';
            if(isPersoanalizedAdsAutoTick){
                consentHTML += '<div class="personalisedadsBlk checkbox"><input type="checkbox" checked name="personalisedads" id="personalisedadsCheckBox" value="1">';
            }else{
                consentHTML += '<div class="personalisedadsBlk checkbox"><input type="checkbox" name="personalisedads" id="personalisedadsCheckBox" value="1">';
            }
            consentHTML += '<label for="personalisedadsCheckBox">I agree to see customized ads that are tailor-made to my preferences</label></div>';
            consentHTML += '</div>';
        }
        return consentHTML;
    };
    
    var getpolicyUpdateHTML = function(){
       
        var policyUpdateHTML = '<div class="consent-banner container wrapper consent-policy-update-msg"><div class="column1"><p>Our Privacy Policy has been revised. Please review the updated ';
        policyUpdateHTML += '<a target="_blank" href="' + privacyPolicyUrl + '">Privacy Policy Here</a>';
        policyUpdateHTML += '</p></div><button class="close" id="policy-update-close-btn" type="button"></button></div>';
        return policyUpdateHTML;
    };

    // Keep this variable private inside this closure scope
    var defaults = {
            'consentHTML' : function(consentFor, isPersoanalizedAdsAutoTick){
                return getConsentHTML(consentFor, isPersoanalizedAdsAutoTick);
            },
            'policyUpdateHTML': function(){
                return getpolicyUpdateHTML();
            },
            'policyUpdateCloseButtonId' : 'policy-update-close-btn',
            'consentBannerClass' : 'consent-popup',
            'consentAgreeButtonId' : 'consent-continue-button',
            'personalisedAdsCheckBoxId' : 'personalisedadsCheckBox',
            'consentVersionCookieName': 'consent_verison',
            'consentVersionCookieVal': 'v1',
            'consentVersionCookieAge': 365,
            'consentCookieName': 'ckns_policyV2',
            'consentCookieVal': '1',
            'consentCookieAge': 365,
            'geoContCookieName' : 'geo_continent',
            'geoCountryCookieName': 'geo_country',
            'geoRegionCookieName': 'geo_region',
            'geoRegionCookieAge': 3, // cant store more than 3 days, becuase of CCPA constraint
            'geoContCookieAge' : 1,
            'optOutCookieName' : 'optout',
            'optOutCookieDefaultVal' : 1,
            'optOutAcceptCookieVal' : 0,
            'optOutCookieAge' : 365,
            'consentReqCont' : ["EU"],
            'consentReqCountryRegions': ["US_CA"],
            'domainName' : '',
            'columbiaCoookie' : '_col_uuid',
            'dynamicCssSrc' : '/gdpr_css/version-8,minify-1.cms',
            'productCode' : 'gn-web'
        };
  
    var apiRef = {
        'cbfn' : [],
        'domReady' : false,
        'displayPopupCalled' : false
    };

    //This function would hide pop-up
    var hidePopup = function() {
        var _consentBannerEles = document.querySelectorAll('.'+ defaults.consentBannerClass)
        if(_consentBannerEles.length > 0){
            _consentBannerEles[0].style.display = "none";
        }
    };

    //This function would show pop-up for EU user, if consent is not received yet
    var displayPopup = function() {
        if(apiRef.domReady && (typeof defaults.geoDataAvailable != 'undefined') && !apiRef.displayPopupCalled){
            apiRef.displayPopupCalled = true;
            if(defaults.isEUuser){
                document.body.setAttribute("data-consentfor", defaults.consentFor);
                document.body.className += ' ' + 'euuser';
            }
            if(defaults.isEUuser && TimesGDPR.common.checkIfPopShouldShow()){
                var _gdpr_ckns_policy_val = getCookie(defaults.consentCookieName);
                var _consent_version_val = getCookie(defaults.consentVersionCookieName);
                if(!_gdpr_ckns_policy_val){
                    renderHtml(false);
                    attachEvents();
                }
                //consent version in cookie is not matching with code version, means policy is upadted.
                else if(_consent_version_val !== defaults.consentVersionCookieVal){
                    renderHtml(true);
                    attachEventsForPolicyUpdateMessage();
                    setCookie(defaults.consentVersionCookieName, defaults.consentVersionCookieVal , defaults.domainName , defaults.consentVersionCookieAge);
                }
            }
        }
    };

    var attachEvents = function(){
        document.getElementById(defaults.consentAgreeButtonId).onclick = agreeButtonClickHandler;
    };
    
    var attachEventsForPolicyUpdateMessage = function(){
        document.getElementById(defaults.policyUpdateCloseButtonId).onclick = hidePopup;
    };

    var loadDynamicResource = function(){
        var s = document.createElement("link"), _head = document.getElementsByTagName("head")[0];
        s.rel  = 'stylesheet';
        s.type = 'text/css';
        s.href = defaults.dynamicCssSrc;
        if (typeof dynamicCssOnloadFn === 'function') {
        	if (typeof s.addEventListener != 'undefined') {
                s.addEventListener('load', dynamicCssOnloadFn, false);
            }
            else if(typeof s.onreadystatechange != 'undefined') {
                s.onreadystatechange = function() {
                    ieCallback(s, dynamicCssOnloadFn);
                };
            }
            else{
                setTimeout(dynamicCssOnloadFn , 100);
            }
        }
        _head.appendChild(s);
    };

    var dynamicCssOnloadFn = function(){
        displayPopup();
    };

    //This function check geo cookie and geoinfo object and set cookie after getting geoinfo object (if required)
    var cbGeoLocation = function(){
        var _geo_contCookieVal = getCookie(defaults.geoContCookieName);
        var _geo_countryCookieVal = getCookie(defaults.geoCountryCookieName);
        var _geo_regionCookieVal = getCookie(defaults.geoRegionCookieName);
        
        if(typeof window.geoinfo == 'undefined' && (!_geo_contCookieVal || !_geo_countryCookieVal || !_geo_regionCookieVal)){
            var s = document.createElement("script"), _head = document.getElementsByTagName("head")[0];
        	s.src = "https://geoapi.indiatimes.com/?cb=1";
        	if (typeof geoapiOnloadFn === 'function') {
        	    if (typeof s.addEventListener != 'undefined') {
                    s.addEventListener('load', geoapiOnloadFn, false);
                    s.addEventListener('error', geoapiOnloadFn, false);
                }
                else if(typeof s.onreadystatechange != 'undefined'){
                    s.onreadystatechange = function() {
                        ieCallback(s, geoapiOnloadFn);
                    };
        	    }
        	    else{
        	        setTimeout(geoapiOnloadFn, 100);
        	    }
        	}
        	_head.appendChild(s);
        }
        else if (typeof window.geoinfo != 'undefined' && (!_geo_contCookieVal || !_geo_countryCookieVal || !_geo_regionCookieVal)){
            geoapiOnloadFn();
        }
        else{
            setResource(_geo_contCookieVal, _geo_countryCookieVal, _geo_regionCookieVal);
        }
    };

    var geoapiOnloadFn = function(response){
        if(typeof window.geoinfo != 'undefined' && !!window.geoinfo.Continent){
            var _geoContUpper = window.geoinfo.Continent.toUpperCase();
            var _geoCountryUpper = window.geoinfo.CountryCode? window.geoinfo.CountryCode.toUpperCase(): '';
            var _geoRegionUpper = window.geoinfo.region_code? window.geoinfo.region_code.toUpperCase(): '';
            setCookie(defaults.geoContCookieName, _geoContUpper, '' , defaults.geoContCookieAge);
            setCookie(defaults.geoCountryCookieName, _geoCountryUpper, '' , defaults.geoContCookieAge);
            setCookie(defaults.geoRegionCookieName, _geoRegionUpper, '' , defaults.geoRegionCookieAge);
            setResource(_geoContUpper, _geoCountryUpper, _geoRegionUpper);
		}
		else{
		    var falioverContName = 'EU', falioverCookAge  = 1/(24*60); // 1 minute cookie life in case of error, in days
            setCookie(defaults.geoContCookieName, falioverContName, '', falioverCookAge);
		    setResource(falioverContName, '', '');
		}
    };

    //This function loads EU user specific CSS and set some HTML class, JS object property to check, user is EU user or not
    var setResource = function(_geo_contCookieVal, _geoCountryCookieVal, _geo_regionCookieVal){
        defaults.geoDataAvailable = true;
        var concatContinentData;
        var concatCountryRegionData;
        if (!Array.prototype.indexOf){
            concatContinentData = defaults.consentReqCont.join(",");
            concatCountryRegionData = defaults.consentReqCountryRegions.join(',');
        }
        else{
            concatContinentData = defaults.consentReqCont;
            concatCountryRegionData = defaults.consentReqCountryRegions;
        }
        var _geoCountry_RegionCookieVal = _geoCountryCookieVal + '_' + _geo_regionCookieVal;
        var isConsentRequiredCont = concatContinentData.indexOf(_geo_contCookieVal.toUpperCase()) > -1 ;
        var isConsentRequiredRegion = concatCountryRegionData.indexOf(_geoCountry_RegionCookieVal.toUpperCase()) > -1;

        if (isConsentRequiredCont || isConsentRequiredRegion) {
            if(isConsentRequiredRegion){
                defaults.consentForCountryRegion = _geoCountry_RegionCookieVal;
                defaults.consentFor = 'CCPA';
            }
            else{
                defaults.consentForContinent = _geo_contCookieVal;
                defaults.consentFor = 'GDPR';
            }
            defaults.isEUuser = true;
            window._euuser = true;
            var _optout_val = getCookie(defaults.optOutCookieName);
            window._optoutV2 = _optout_val || "0";
            var _gdpr_ckns_policy_val = getCookie(defaults.consentCookieName);
            if(!_gdpr_ckns_policy_val && !_optout_val && defaults.consentFor != 'CCPA'){
                setCookie(defaults.optOutCookieName, defaults.optOutCookieDefaultVal, defaults.domainName , defaults.optOutCookieAge);
            }
            executeCalbacks();

            if(!!defaults.dynamicCssSrc){
                loadDynamicResource();
            }
            else{
                displayPopup();
            }
        }
        else{
            defaults.isEUuser = false;
            executeCalbacks();
        }
    };

    var getReturnObject = function(){
        return { 
            'isEUuser' : TimesGDPR.common.consentModule.isEUuser(), 
            'isUS' : TimesGDPR.common.consentModule.isUS(),
            'isocc' : TimesGDPR.common.consentModule.isocc(),
            'isForeign':TimesGDPR.common.consentModule.isForeign(), 
            'isDSCountry' : TimesGDPR.common.consentModule.isDSCountry(),
            'consentGiven': TimesGDPR.common.consentModule.isConsentGiven(),
            'didUserOptOut': TimesGDPR.common.consentModule.didUserOptOut(),
            'userRegion': getCookie(defaults.geoRegionCookieName),
            'userContinent': getCookie(defaults.geoContCookieName),
            'userCountry': getCookie(defaults.geoCountryCookieName)
        };
    }

    var executeCalbacks = function(){
        if(apiRef.cbfn.length){
            var isEUuserObj = getReturnObject();
            for( var i=apiRef.cbfn.length-1; i>= 0; i--){
                typeof apiRef.cbfn[i] == "function" && apiRef.cbfn[i](isEUuserObj);
                apiRef.cbfn[i] = null;
            }

        }
    }

    //This is continue/agree button event handler
    var agreeButtonClickHandler =  function(){
        setConsentOptoutCookies();
    };

    /*Dont pass days to set session cookie*/
    var setCookie = function(name, value, domainName, days){
        if(!!domainName){
            var domain = ";domain="+domainName;
        }
        if(!!days){
            //Below variable is one day time in mili seconds.
            var _oneDayinMS = 24*60*60*1000;
            var d = new Date();
            d.setTime(d.getTime() + (days*_oneDayinMS));
            var expires = ";expires="+d.toUTCString();
        }
        document.cookie = name+"=" +value + ( ( expires ) ? expires : '' ) + ( ( domain ) ? domain : '' ) + ";path=/";
    };

    var getCookie = function(name) {
        var re = new RegExp(name + "=([^;]+)");
        var value = re.exec(document.cookie);
        return (value != null) ? unescape(value[1]) : null;
    };

    var getDomain = function( url ) {
		return ( url || document.location.host ).split( ":" )[ 0 ].split( "." ).reverse().slice( 0, 2 ).reverse().join( "." );
	};

    var getUuidv4 = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    var renderHtml = function(isPolicyUpdated) {
        //var isPersoanalizedAdsAutoTick = defaults.consentFor === 'CCPA';
        var isPersoanalizedAdsAutoTick = false;
        var htmlToBeRendered = !!isPolicyUpdated ? defaults.policyUpdateHTML(): defaults.consentHTML(defaults.consentFor, isPersoanalizedAdsAutoTick);
        var cssClassName = defaults.consentBannerClass;
        
        var htmlElement = document.querySelectorAll('.'+cssClassName);
        //safe check to avoid duplicate HTML
        if(htmlElement.length){
            //html element is already present, so retun from here.
            return;
        }

        var _html= document.createElement('div');

        _html.innerHTML= htmlToBeRendered;
        _html.className= cssClassName;

        // Append into text.
        var body = document.getElementsByTagName("body")[0];
        body.insertBefore(_html, body.childNodes[0]);
    };
    
    var ieCallback = function(el, callback) {
        if(el.readyState === 'loaded' || el.readyState === 'complete'){
            callback();
        }
    };

    var gdprCallback = function(callbackFn) {
        if (typeof defaults.geoDataAvailable != 'undefined') {
            var _geo_contCookieVal = getCookie(defaults.geoContCookieName);
            var isEUuserObj = getReturnObject();
            callbackFn(isEUuserObj);
        }
        else {
            apiRef.cbfn.push(callbackFn);
        }
    };

    var isEUuser = function() {
        if(typeof defaults.isEUuser != 'undefined'){
            return defaults.isEUuser;
        }
        return;
    };
    
    var isForeign = function() {
        var _contryCookieVal =  getCookie(defaults.geoCountryCookieName);
        if(_contryCookieVal == 'undefined' || _contryCookieVal == null || _contryCookieVal == 'IN'){
            return false;
        } else { 
            return true;
        }
    };
    
    var isUS = function() {
        var _contryUSCookieVal =  getCookie(defaults.geoCountryCookieName);
        if(_contryUSCookieVal != 'undefined' && _contryUSCookieVal != null && _contryUSCookieVal == 'US'){
            return true;
        } else { 
            return false;
        }
    };
    
    var isocc = function() {
        var _contryUSCookieVal =  getCookie(defaults.geoCountryCookieName);
        if(_contryUSCookieVal != 'undefined' && _contryUSCookieVal != null && _contryUSCookieVal != ''){
            return _contryUSCookieVal;
        } else { 
            return '';
        }
    };
    
    var isDSCountry = function() {
        var strcountry = ',JP,NL,TH,ZA,AE,PT,IE,BE,GR,HK,FR,DE,SG,GB,US,NZ,AU,CA,';
        var _contryDSCookieVal =  getCookie(defaults.geoCountryCookieName);
        if(_contryDSCookieVal != 'undefined' && _contryDSCookieVal != null && (strcountry.indexOf(',' + _contryDSCookieVal + ',') > -1)){
            return true;
        } else { 
            return false;
        }
    };
    
    var isConsentGiven = function() {
        var _consentCookieVal =  getCookie(defaults.consentCookieName);
        if(!!_consentCookieVal){
            return true;
        }
        return false;
    };

    var didUserOptOut = function() {
        if(defaults.consentFor != 'CCPA'){
            var _optOutCookieVal = getCookie(defaults.optOutCookieName) ? parseInt(getCookie(defaults.optOutCookieName)) : defaults.optOutCookieDefaultVal;
            return _optOutCookieVal !== defaults.optOutAcceptCookieVal;
        }
        return false;

    };

    var resetOldConsent =  function(){
        var _gdpr_ckns_policy_val = getCookie(defaults.consentCookieName);
        var _optoutV2_val = getCookie('optoutV2');
        // if outoutV2 cookie is present along with consent cookie, convert it to optout.
        //As ads, DMP teams are using optout not optoutV2, hence preserving value to optout.
        if(_gdpr_ckns_policy_val && _optoutV2_val){
            setCookie('optoutV2', '', '' , -1);
            setCookie('optoutV2', '', defaults.domainName , -1);
            if(defaults.consentFor != 'CCPA'){
                setCookie(defaults.optOutCookieName, _optoutV2_val , defaults.domainName , defaults.optOutCookieAge);
            }
        }
        //deleting old cookie, as we renamed cookies to ckns_policyV2.
        //Should not create multiple cookie to user systems
        setCookie('ckns_policy', '', '' , -1);
        setCookie('ckns_policy', '', defaults.domainName , -1);
    };

    var setConsentOptoutCookies = function(){
        var _optOut, eventAction;
        var _personalisedAdsCheckBoxElem  = document.getElementById(defaults.personalisedAdsCheckBoxId);
        if(_personalisedAdsCheckBoxElem != null && typeof(_personalisedAdsCheckBoxElem) != 'undefined' && _personalisedAdsCheckBoxElem.checked){
            _optOut = defaults.optOutAcceptCookieVal;
            eventAction = "Accept_with_personalized_ads";
        }
        else{
            eventAction = "Accept_without_personalized_ads"
            _optOut = defaults.optOutCookieDefaultVal;
        }
        setConsentToCookies(defaults.consentCookieVal, _optOut);
        ga('send', {
                hitType: 'event',
                eventCategory: defaults.consentFor || 'GDPR',
                eventAction: eventAction,
                eventLabel: window.location.href
            });
    };

    var convertToInt = function(input){
        var outputInt;
        if(input !== 'undefined' && isNaN(parseInt(input))){
            //not a number
            outputInt = (input === true || input === 'true') ? 1 : 0;
        }else{
            //is a number
            outputInt = input;
        }
        return outputInt;
    }

    var setConsentToCookies = function(consentAcceptVal, personalizedConsentVal){
        if(typeof consentAcceptVal != 'undefined'){
            var consentAcceptInteger = convertToInt(consentAcceptVal);
            setCookie(defaults.consentCookieName,  consentAcceptInteger,  defaults.domainName , defaults.consentCookieAge);
        }
        if(typeof personalizedConsentVal != 'undefined' && defaults.consentFor !== 'CCPA'){
            personalizedConsentInteger = convertToInt(personalizedConsentVal);
            setCookie(defaults.optOutCookieName, personalizedConsentInteger, defaults.domainName , defaults.optOutCookieAge);
        }
        setCookie(defaults.consentVersionCookieName, defaults.consentVersionCookieVal , defaults.domainName , defaults.consentVersionCookieAge);
        hidePopup();
    }

    // It starts the flow either to display consent pop-up or not for EU user based on consent acceptance status
    var start = function(){
        defaults.domainName = getDomain();
        /*Deleteing old consent and reset outoutV2 to optout, as EU policy is updated. So reading consent based on new cookies*/
        resetOldConsent();
        cbGeoLocation();
        if(typeof document.addEventListener != 'undefined'){
            document.addEventListener('DOMContentLoaded', function(){
                apiRef.domReady = true;
                displayPopup();
            });
        }
        else if(typeof document.attachEvent != 'undefined'){
            document.attachEvent('onreadystatechange', function(){
                if(document.readyState === 'loaded' || document.readyState === 'complete'){
                    apiRef.domReady = true;
                    displayPopup();
                }
            });
        }
        else{
            setTimeout( function(){
                apiRef.domReady = true;
                displayPopup();
            }, 1000);
        }
    };

    // Explicitly reveal public pointers to the private functions
    // that we want to reveal publicly
    return {
        hidePopup : hidePopup,
        setConsentToCookies : setConsentToCookies,
        agreeButtonClickHandler : agreeButtonClickHandler,
        isConsentGiven : isConsentGiven,
        didUserOptOut: didUserOptOut,
        gdprCallback : gdprCallback,
        isEUuser : isEUuser,
        isForeign: isForeign,
        isUS: isUS,
        isocc:isocc,
        isDSCountry: isDSCountry,
        start: start
    }
})();
TimesGDPR.common.consentModule.start();


	