//This is the "Offline copy of pages" service worker
var today = new Date();
var _newRevision = today.getTime();
importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.6.2/workbox-sw.js');
if (workbox) {	
	/*workbox.routing.registerRoute(
	  new RegExp('.*\.js'),
	  workbox.strategies.networkFirst()
	); */
  workbox.routing.registerRoute(
  // Cache CSS files
	  /.*\.css/,
	  // Use cache but update in the background ASAP
	  workbox.strategies.staleWhileRevalidate({
		// Use a custom cache name
		cacheName: 'css-cache',
		plugins: [
		  new workbox.expiration.Plugin({
			maxAgeSeconds: 30 * 60,
		  })
		],
	  })
	);

	workbox.routing.registerRoute(
	  // Cache image files
	  /.*\.(?:png|jpg|jpeg|svg|gif)/,
	  // Use the cache if it's available
	  workbox.strategies.cacheFirst({
		// Use a custom cache name
		cacheName: 'image-cache',
		plugins: [
		  new workbox.expiration.Plugin({
			// Cache only 20 images
			maxEntries: 2,
			maxAgeSeconds: 30 * 60,
		  })
		],
	  })
	);
	
	
	const articleHandler = workbox.strategies.networkFirst({
	  cacheName: 'articles-cache',
	  plugins: [
		new workbox.expiration.Plugin({
		  maxEntries: 5,
		  maxAgeSeconds: 30 * 60 // 30 minutes
		})
	  ]
	});

	workbox.routing.registerRoute(/(.*)\/p_(.*)/, args => {
	  return articleHandler.handle(args);
	});
	
	workbox.routing.registerRoute('/', workbox.strategies.staleWhileRevalidate({
      cacheName: 'home-cache',
      plugins: [new workbox.expiration.Plugin({
        maxEntries: 1,
        maxAgeSeconds: 5 * 60 // 30 minutes
      })]
    }));
	
	
	workbox.precaching.precacheAndRoute([
    '/pwa/js/jquery-1.10.2.min.js',
    { url: '/', revision: _newRevision }
	]);
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}