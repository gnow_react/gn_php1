$("document").ready(function(){
	/*forgot password*/
	$("body").on("submit",".password-recovery-popup form",function(){
		var _this = this;
		var _ga_for_track = '';
		if($(_this).attr("name")=="forgotform"){
			_ga_for_track = "forgot_password##checkout_options##email_submit##0";
		}else if($(_this).attr("name")=="otpform"){
			_ga_for_track = "forgot_password##checkout_options##code_submit##0";
		}else if($(_this).attr("name")=="changepasswordform"){
			var _ga_mob = $("#contactMobile").val();
			var _ga_for_mob = 'new_password';
			if(_ga_mob==''){
				_ga_for_mob = 'new_password_mobile_not';
			}else if(_ga_mob!=''){
				_ga_for_mob = 'new_password_with_mobile_no';
			}
			_ga_for_track = "forgot_password##checkout_options##"+_ga_for_mob+"##0";
		}
		var _ga_for_track_n = _ga_for_track.split("##");
		gaEvent_track(_ga_for_track_n);
	});
	/*forgot password*/
	
	$("body").on("click","*[data-ga-track]",function(){
		var _ga_attr_track = $(this).attr("data-ga-track");
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
	});
	$("body").on("click","*[data-ga-promotion]",function(){
		var _ga_promo_track = $(this).attr("data-ga-promotion");
		var _ga_promo_track_n = _ga_promo_track.split("##");
		onPromoClick(_ga_promo_track_n);
	});	
	$("body").on("click","*[data-ga-en]", function(){
	var _ga_attr_en = $(this).attr("data-ga-en");
	enhancedEcommerce(_ga_attr_en);
});
});


if(paramsObj.leafTitle=="product"){
	var _ga_attr = $("input#instockevent").val();
	var _ga_array = _ga_attr.split("##");
	gaEvent_track(_ga_array);
}

function enhancedEcommerce(_trackEnsplit){
    var obj ={};
	var _ga_array = _trackEnsplit.split("##");
	//alert(_ga_array);
	$(_ga_array).each(function(index, value){		
		var _ghi = value.split("~");
		obj[_ghi[0]] = _ghi[1];
	});
		  if(obj.event=="productClick"){
		  dataLayer.push({
			'event': 'productClick',
			'ecommerce': {
			  'click': {
				'actionField': {'list': obj.list},      // Optional list property.
				'products': [{
				  'name': obj.name,                      // Name or ID is required.
				  'id': obj.id,
				  'price': obj.price,
				  'brand': obj.brand,
				  'category': obj.category,
				  'quantity': obj.quantity,
				  'position': obj.position,
				 }]
			   }
			 },
			 'eventCallback': function() {
				//console.log(obj.url)
			   document.location = obj.url
			 }
		  });
		  }
		  if(obj.event=="addToCart"){
		  dataLayer.push({
			  'event': 'addToCart',
			  'ecommerce': {
				'currencyCode': 'Rupees',
				'add': {                                // 'add' actionFieldObject measures.
				  'products': [{                        //  adding a product to a shopping cart.
					'id': obj.id,
					'name': obj.name,                      // Name or ID is required.
					'price': obj.price,
					'brand': obj.brand,
					'category': obj.category,
					'quantity': obj.quantity
				   }]
				}
			  }
			});
		  }
		  if(obj.event=="removeFromCart"){
		  dataLayer.push({
			'event': 'removeFromCart',
			  'ecommerce': {
				'remove': {                               // 'remove' actionFieldObject measures.
				  'products': [{                          //  removing a product to a shopping cart.
					'name': obj.name,                      // Name or ID is required.
					'id': obj.id,
					'price': obj.price,
					'brand': obj.brand,
					'category': obj.category,
					'quantity': obj.quantity,
					'position': obj.position
				  }]
				}
			  }
			});
		  }
}

/*cart page */
$("body").on("click",".check-out-button",function(){
		var _userloingstatus = $("input[name=userloingstatus]").val();
		var _ch_option = 3;
		if(_userloingstatus=="false"){
			_ch_option = 1;
		}
		var objGa =[];
		$(".remove-ga-track").each(function(){
			var obj ={};
			_enhanced_ga_attr_track = $(this).attr("enhanced-data-ga");
			var _ga_array = _enhanced_ga_attr_track.split("##");
			$(_ga_array).each(function(index, value){		
				var _ghi = value.split("~");
				//console.log(_ghi[0]);
				if(_ghi[0]!='event'){
					//console.log(_ghi[0]);
					obj[_ghi[0]] = _ghi[1];
				}
				
			});
			objGa.push(obj);
		});
		dataLayer.push({
			'event': 'checkout',
			'ecommerce': {
			  'checkout': {
				'actionField': {'step': _ch_option, 'option': 'checkoutOption'},
				'products': objGa
			 }
		   },
		   'eventCallback': function() {
			  document.location = '/control/checkout';
		   }
		  });
		//console.log(objGa);
	});
if(paramsObj.leafTitle=="checkout" || paramsObj.leafTitle=="payment"){
		
		var _loginstatus = $("input[name=loginstatus]").val();

		var objGa =[];
		$("input.ua-cart-size").each(function(){
			var obj ={};
			_enhanced_ga_attr_track = $(this).val();
			var _ga_array = _enhanced_ga_attr_track.split("##");
			$(_ga_array).each(function(index, value){		
				var _ghi = value.split("~");
				//console.log(_ghi[0]);
				if(_ghi[0]!='event'){
					//console.log(_ghi[0]);
					obj[_ghi[0]] = _ghi[1];
				}
				
			});
			objGa.push(obj);
		});

		if(paramsObj.leafTitle=="checkout"){
			
			dataLayer.push({
			'event': 'checkoutOption',
			'ecommerce': {
			'checkout': {
					'actionField': {'step': 2, 'option': _loginstatus},
					'products': objGa
			}
			}
			});
		}
		if(paramsObj.leafTitle=="payment"){
			dataLayer.push({
			'event': 'checkoutOption',
			'ecommerce': {
			'checkout': {
					'actionField': {'step': 4, 'option': _loginstatus},
					'products': objGa
			}
			}
			});
		}
		//console.log(objGa);
	};	
/*cart page */
	
if(paramsObj.leafTitle=="thankyou"){
	var objGa =[];
	
	$(".order-ga-class").each(function(){
		var obj ={};
			_enhanced_ga_attr_track = $(this).attr("data-ga-order");
			var _ga_array = _enhanced_ga_attr_track.split("##");
			$(_ga_array).each(function(index, value){		
				var _ghi = value.split("~");
				//console.log(_ghi[0]);
					obj[_ghi[0]] = _ghi[1];
				
			});
			objGa.push(obj);
	});
	
	var objTransation ={};
	_ga_transation = $("#ga-transation").val();
	var _array_ga_transation = _ga_transation.split("##");
	$(_array_ga_transation).each(function(index, value){
		//console.log(objTransation);
		var _ghiTrans = value.split("~");
		objTransation[_ghiTrans[0]] = _ghiTrans[1];
	});
	
	dataLayer.push({
		  'ecommerce': {
			'purchase': {
			  'actionField': {
				'id': objTransation.transation_id,                         // Transaction ID. Required for purchases and refunds.
				'revenue': objTransation.revenue,                     // Total transaction value (incl. tax and shipping)
				'tax':objTransation.tax,
				'shipping': objTransation.shipping,
				'coupon': objTransation.coupon
			  },
			  'products': objGa
			}
		  },
		});
	// dataLayer.push({
			// 'event': 'checkoutOption',
			// 'ecommerce': {
			// 'checkout': {
					// 'actionField': {'step': 5, 'option': 'registration_user'},
					// 'products': objGa
			// }
			// }
	// });
}	
	
function gaEvent_track(_tracksplit){
		//alert(_tracksplit.toString());
		dataLayer.push({'event': 'event', 'eventCat': _tracksplit[0], 'eventAct': _tracksplit[1], 'eventLbl': _tracksplit[2], 'eventVal': _tracksplit[3]});
		// ga('send', 'event', _tracksplit[0], _tracksplit[1], _tracksplit[2],_tracksplit[3], {
			// nonInteraction: true
		// });
	}	
// function gaEvent_promo(_promosplit){
		// alert(_promosplit);
		// ga('ec:addPromo', {
			// 'id': _promosplit[0],
			// 'name': _promosplit[1],
			// 'creative': _promosplit[2],
			// 'position': _promosplit[3]
		// });
		// ga('ec:setAction', 'promo_click');
	// }
if(paramsObj.leafTitle=="mainscreen"){
	var _array_ga_promo=[];
	$(".ga-promotion").each(function(idx, objElem){
		_ga_promation = $(objElem).attr("data-ga-promo");
		var _array_ga_promoation = _ga_promation.split("##");
		var objPromoation ={};
		//alert(_array_ga_promoation);
		$(_array_ga_promoation).each(function(index, value){
			var _ghiPro = value.split("~");
			objPromoation[_ghiPro[0]] = _ghiPro[1];
		});
		_array_ga_promo.push(objPromoation);
	});
	//console.log(_array_ga_promo.length);
	dataLayer.push({
	  'ecommerce': {
		'promoView': {
		  'promotions': _array_ga_promo
		}
	  }
	});
}
	
	
function onPromoClick(promoObj) {
	//alert(promoObj[0]);
  dataLayer.push({
    'event': 'promotionClick',
    'ecommerce': {
      'promoClick': {
        'promotions': [
         {
           'id': promoObj[0],                         // Name or ID is required.
           'name': promoObj[1],
           'creative': promoObj[2],
           'position': promoObj[3]
         }]
      }
    },
    'eventCallback': function() {
      document.location = promoObj[0];
    }
  });
}

	
