$(document).ready(function(){
	applyLazyLoad();
	getDateDelivery();
	userReview();
	customerComment();
	QuestionAndAnswer();
	initInfinteScroll();
	 getLoginSSOData();
	utmInit();
	HomePageBrand();
	adsCheck();
	closeTermsPopup();
	//_counter();
	var geo_country = $.cookie('geo_country');
	var geo_region = $.cookie('geo_region');
	
	if(geo_country!=null  && geo_country!="IN"){
		$("body").addClass("country");
		$(".countryblock").html("");
	} 
	if(isPrimeUser!=null){
		$("*[data-slot=256682]").remove();
		$("*[data-slot=256682]").closest('li').remove();
	}
	if(geo_continent=="EU" || (geo_country=="US" && geo_region=="CA")){
		$("*[data-slot=256682]").closest('li').remove();
	}
	if(paramsObj.leafTitle=='best-sellers'){
		bestseller();
	}

	if(paramsObj.leafTitle=='compare'){
		compare();
	}
	if(paramsObj.leafTitle =='dealofferArticle'){
		PerpetualScroll();
	}
	if(paramsObj.leafTitle =='amazingdeals' || paramsObj.leafTitle =='mobile-offers'){
		_AmazingDeal();
	}
	if(geo_country!=null && geo_country !="US"){
		$(".us-showing").addClass("display-none");
		$(".us-showing").html("");
	}else{
		$(".us-showing-not").addClass("display-none");
	}	
	$("#toi-carousel").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			navigation : false, // Show next and prev buttons
			pagination: true,
			loop:true,
			slideSpeed : 300,
			paginationSpeed : 400,
			autoWidth:true,
			singleItem:true
	});
	
	$(".common-gallery").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			nav: true,
			loop:true,
		    singleItem : true,
		    dots:true,
			slideSpeed : 300,
			paginationSpeed : 400,
		    lazyLoad : true,
			items:1,
			navText: ["<span class='prev'></span>","<span class='next'></span>"]
	});
	
	$(".toi-owl-carousel").owlCarousel({
		pagination: false,
		 scrollPerPage : false,
		// navigation : true, // Show next and prev buttons
		//navigationText : ["&lsaquo;","&rsaquo;"],
		lazyLoad : true,
		addClassActive: true,
		afterMove: moved,
		 itemsCustom:[[300,1.2],[400,1.3],[600,2.2],[750,2.3],[850,3.3]]
	 });
	
	  function moved(_this) {
		//var _this = this;
		//var owl = $(_this).data('owlCarousel');
		//$(".toi-owl-carousel").each(function(idx, value){
			var _cls = $(_this).attr("data-count");
			var _newClass = $(".nbtevent"+_cls);		
			_Ctnevent(_newClass);
			
		//});			
	}
	  function closeTermsPopup()
		{
		  var isTermsOpened = localStorage.getItem("newTermsPopupClosed");
		  if(isTermsOpened != "yes")
			  {
			    var _ga_attr_track = "WAP_Popup_TNC##view##TnC##0";
				var _ga_array_track = _ga_attr_track.split("##");
				gaEvent_track(_ga_array_track);
			  	$(".termscontainer").removeClass("display-none");
			  }
		 
		  $("body").on("click",function(){
			  	var _ga_attr_track = "WAP_Popup_TNC##Close##TnC##0";
				var _ga_array_track = _ga_attr_track.split("##");
				gaEvent_track(_ga_array_track);
				localStorage.setItem("newTermsPopupClosed", "yes");
				$(".termscontainer").addClass("display-none");
			});
			$("body").on("click",".termscontainer .ok",function(){
				
				var _ga_attr_track = "WAP_Popup_TNC##click##TnC##0";
				var _ga_array_track = _ga_attr_track.split("##");
				gaEvent_track(_ga_array_track);
				localStorage.setItem("newTermsPopupClosed", "yes");
				$(".termscontainer").addClass("display-none");
			});
		}
	function bestseller(){
		var _height = $(".pagecontent").attr("data-hight");
		$(".pagecontent").css('height',_height+'px');
		$("body").on("click",".pagecontent .read",function(){
			var _this = this;
			var _height = $(".pagecontent").attr("data-hight");
			if($(_this).hasClass("more")){
				$(_this).removeClass("more");
				$(".pagecontent").css('height',_height+'px');
				$(_this).text("Read More");
			}else{
				$(_this).addClass("more");
				$(".pagecontent").css('height','auto');
				$(_this).text("Read Less");
			}
		});
		$("#tile-1 .nav-tabs a").click(function() {
 		 var position = $(this).parent().position();
 		 var width = $(this).parent().width();
		$("#tile-1 .nav-tabs a").removeClass("active");
		$(this).addClass("active");
    		$("#tile-1 .slider").css({"left":+ position.left,"width":width});
		});
		var actWidth = $("#tile-1 .nav-tabs").find(".active").parent("li").width();
		var actPosition = $("#tile-1 .nav-tabs .active").position();
		$("#tile-1 .slider").css({"left":+ actPosition.left,"width": actWidth});
	
	}
	function _Ctnevent(_findclass){
		var _n_array = _findclass.selector.split('.')		
		var _newClassName = _n_array[1];
		$("."+_newClassName).each(function(indx, obj){
				if($(obj).find(".owl-item").hasClass("active")){
					//setTimeout(function(){
						var _class = $(obj).find(".active");
						var _nclass = $(_class[0]).find(".colombiaad");
						var _id = $(_nclass).attr("id");
						if(typeof _id != 'undefined'){
							colombia.notifyColombiaAd(_id);							
							ctnAdcheck();
						}	
					//},0.5);
					
				}
			});
	}
	setTimeout('ctnAdcheck()',500);
	$("#product-img-casousel").owlCarousel({
	 	autoPlay: false, //Set AutoPlay to 3 seconds
	 	navigation : true, // Show next and prev buttons
		pagination: false,
		lazyLoad : true,
		loop:true,
		lazyFollow : true,
		lazyEffect : "fade",
		slideSpeed : 300,
		paginationSpeed : 400,
		navigationText : ["",""],
		startDragging : false,
		singleItem:true
		
	 });
	/*var _owllength = $("#product-img-casousel .owl-pagination .owl-page").size();
	if(_owllength > 16){
		$("#product-img-casousel .owl-pagination .owl-page:gt(15)").addClass("display-none");
	} */
	
	$("body").on("click",".hamburger-menu,.hamburger-nav",function(e){
		e.preventDefault();
		var _maskhead = $("#maskhead").val();
		if(_maskhead=='Y'){
			_autoscroll('.maskhead');
		}		
		if($(".navigation").hasClass("mobile-top-list-out")){
			$(".overlay").removeClass("display-none").addClass("humbig");
			$(".navigation").removeClass("mobile-top-list-out").addClass("mobile-top-list-in");
			$("body").addClass("overflowhidden");
			//$(".consent-popup").css("display","none");
		}
	});
	$("body").on("click",".humbig",function(e){
		e.preventDefault();
			$(".overlay").addClass("display-none").removeClass("humbig");
			$(".navigation").removeClass("mobile-top-list-in").addClass("mobile-top-list-out");
			$("body").removeClass("overflowhidden");
			//$(".consent-popup").css("display","");
	});
	$("body").on("click",".click-tab",function(e){
		e.preventDefault();
		_tab = $(this).attr("data-tab");
		_bt = _tab.split("##");
		_closest = $(this).closest(".closest");
		$(_closest).find(".click-tab").removeClass("btn-color");
		$("."+_bt[1]).css("height","0");
		$(this).addClass("btn-color");
		$("#"+_bt[0]).css("height","auto");
		applyLazyLoad();
	});
	
	$("body").on("click",".gdgt_nav a",function(e){
		e.preventDefault();
		_tab = $(this).attr("data-tab-c");
		_url = $(this).attr("href");
		$(".gdgt_nav a").removeClass("active");
		$(".widgets-tab").css("height","0");
		$(this).addClass("active");
		$("#"+_tab).css("height","auto");
		if(_url!="#"){
			window.location =_url;
		}
	});
	
	$("body").on("click",".main-nav li a.right-arrow", function(e){
		e.preventDefault();
		$(".navigationpage").removeClass("display-none");
		$(".ct-cap").addClass("display-none");
		_cat_name = $(this).attr("data-cat");
		_data_vision = $(this).attr("data-vision");
		$(".ct-text").html(_cat_name);
		$("."+_data_vision).removeClass("display-none").addClass("display-block");
	});
	
	$("body").on("click",".navigationpage .navclose", function(e){
		e.preventDefault();
		$(".navigationpage").addClass("display-none");
	});
	
	$("body").on("click",".navigationpage a.right-arrow", function(e){
		e.preventDefault();
		$(".navigationpage-second").removeClass("display-none");
		$(".navigationpage .ct-cap").addClass("display-none");
		_cat_name = $(this).attr("data-cat");
		_data_vision = $(this).attr("data-vision");
		_data_back = $(this).attr("data-back");
		$(".navigationpage-second .ct-text").html(_cat_name);
		$("."+_data_vision).removeClass("display-none").addClass("display-block");
		$(".navigationpage-second .navclose").attr('data-close',_data_back);
	});
	
	$("body").on("click",".navigationpage-second .navclose", function(e){
		e.preventDefault();
		$(".navigationpage-second").addClass("display-none");
		var _close = $(this).attr("data-close");
		$(".navigationpage ."+_close).removeClass("display-none");
		$(".navigationpage-second .ct-cap").addClass("display-none");
	});
	
	if($(".adcode iframe")!=''){
		$("iframe").closest(".adcode").css("display","none");
	}
	
	/* global click loader handling   */
	$("*[data-ga-track]").addClass("callevents");
	$("body").on("click",".callevents", function(){
		var _this = $(this).attr("href");
		var _target = $(this).attr("target");
		if(_this != "" && _this != "#" && _this != "#search" && _this != "#filter" && _this != "#filter-l2" && _target != "_blank"){
			$(".overlaywhite").removeClass("display-none");
		}
	});
	/* global click loader handling   */
	
	$("body").on("click",".sharing-button",function(){
		$(".social-sharing").addClass("changePosition");
		$(".overlay").removeClass("display-none").addClass("closeoverlay");
	});
	$("body").on("click",".sharing-close, .closeoverlay, .social-sharing ul li a",function(){
		//e.preventDefault();
		$(".social-sharing").removeClass("changePosition");
		$(".overlay").addClass("display-none").removeClass("closeoverlay");
	});	
	$("body").on("click",".copytoclipboard",function(e){
		e.preventDefault();
		 Copytoclipboard();
	});
	$("#copyurl").val(window.location.href);
	function Copytoclipboard() {
		var copyText = document.getElementById("copyurl");
		copyText.select()
		document.execCommand('copy');
	}
	$(".check-carousel").each(function(){
		_this = this;
		var _carousel_list_count = $(_this).find(".newcarousel li").length; 
		var _list_width = $(_this).find(".newcarousel").attr("data-width-list");
		$(_this).find(".newcarousel").css("width",_carousel_list_count*_list_width);
	});
	
	
	if(paramsObj.leafTitle!='MobileFinder'){
		$("body").on("click",".searchOpen,.filteropen",function(){
			if (window.history && history.pushState) history.pushState('', document.title, window.location.pathname); 
		});
	}
	$(window).on('hashchange', function() {
		var hash = location.hash;
		if(hash == "#search"){
			$(".searchpage").addClass("searchon");
			$("#autocomplete_query").val('');
			$("body").addClass("overflowhidden");
			$( "#autocomplete_query" ).focus();
		}else{
			$(".searchpage").removeClass("searchon");
			$("body").removeClass("overflowhidden");
		}
		if(hash == "#filter"){
			$(".filterpage").addClass("filteron");
			//$("body .container").addClass("display-none");
			if($(".click").hasClass("done")){
				$(".done").addClass("check").removeClass("uncheck");
			}
			_checkFliter();
			if (window.history && history.pushState) history.pushState('', window.location.pathname); 
		}else{
			$(".filterpage").removeClass("filteron");
			
		}
		if(hash == "#filter-l2"){
			_checkFliter();
			_checkFliterSecond();
			$(".filterpage-l2").addClass("filter-l2on");
			//$("body .container").addClass("display-none");
		}else{
			$(".filterpage-l2").removeClass("filter-l2on");
			
		}
	 }); 
	 
	 $("body").on("click",".click",function(e){
		 e.preventDefault();
		 if($(this).hasClass("uncheck")){
			$(this).addClass("check").removeClass("uncheck");
		 }else{
			$(this).addClass("uncheck").removeClass("check");
		 }
		 var counter = $(this).closest("ul").find('.check').length;
		 var _classname = $(this).attr('data-filter-class')
		 $("."+_classname+" .selectcount").html(counter);
		 if( counter > 0){
			$("."+_classname).find(".btn").removeClass("btn-color-grey").addClass("btn-color").addClass("added").removeClass("noaction");
			$(".filterbutton").removeClass("btn-color-grey").addClass("btn-color").addClass("apply-ui").removeClass("noaction");
			}else{
				$("."+_classname).find(".btn").addClass("btn-color-grey").removeClass("btn-color").removeClass("added").addClass("noaction");
			}
	 });
	 $("body").on("click",".added",function(){
		 _classname = $(this).attr("data-filter");
		 _count = $(this).closest(".filterapply").find(".selectcount").text();
		 $("."+_classname).find(".selected").html(_count +" selected");
		 $(this).closest(".filter-col").find(".uncheck").removeClass("done");
		 $(this).closest(".filter-col").find(".check").addClass("done");
	 });	 
	 $("body").on("click",".fliterreset",function(e){
		e.preventDefault();
		_url = $(this).attr("href");
		$(".filter-list-second").find(".click").removeClass("check").addClass("uncheck");
		$(".filter-list").find(".selected").html("");
		$(".added").addClass("btn-color-grey").removeClass("btn-color").removeClass("added").addClass("noaction");
		$(".click").removeClass("check done");
		if(_url!="#"){
			window.location = _url;
		}
		_checkFliter();
		
		 
	 });
	 $("body").on("click",".logoutUser", function(e){ //alert('helo');
			$.cookie("isPrimeUser",null);
			var hostname = window.location.hostname;
			var logoutUrl = window.location.protocol+'//' + hostname  + '/control/logout'
			var SSO_LOGOUT_URL = "https://jsso.indiatimes.com/sso/identity/profile/logout/external";
			var finalURL = SSO_LOGOUT_URL + "?channel=gadgetsnow&ru="+ encodeURI(logoutUrl);
			
			window.location = finalURL;		
			return false;	
		});
	 $("body").on("click",".noaction",function(e){
		e.preventDefault();
	 });	 
	 $("body").on("click",".clear",function(e){
		e.preventDefault();
		$(this).closest(".filter-col").find(".click").removeClass("check").addClass("uncheck").removeClass('done');
		$(this).closest(".filter-col").find(".selectcount").text('0');
		var _rescat = $(this).closest(".filter-col").find(".btn").attr('data-filter');
		 $("."+_rescat).find(".selected").html("");
		 $("."+_rescat).find(".btn").addClass("btn-color-grey").removeClass("btn-color").removeClass("added").addClass("noaction");
		 _checkFliter();
	 });	 
	 
	 $("body").on("click",".filter-list a",function(){
		 $(".filtercatname").html('');
		 $(".filter-col").addClass("display-none");
		 var _class = $(this).attr("data-cat");
		 var _filterName = $(this).attr("data-name");
		 $(".filtercatname").html(_filterName);
		 $("."+_class).removeClass("display-none");
		 $(".click").addClass("uncheck").removeClass("check")
		 if($(".click").hasClass("done")){
			 $(".done").addClass("check").removeClass("uncheck");
		 }
	 });
	
	$("body").on("click",".readmoretext",function(){
		$(this).closest(".gradiant").css("height",'auto');
		$(this).addClass("display-none");
		$(this).closest(".specifications-text").find(".hide-Summary").addClass("display-none");
	});
	if(paramsObj.leafTitle=='category'){
	 $("body").on("click",".apply-ui",function(e){
		 e.preventDefault();
			    var _urlparams="";
			    var _mt_type=$("#mtType").val();
			    var _urlappend="";
	    		var firstBrandFilterUrl ="";
	    		var firstBrandFilterValue ="";
	    		 var urlBrandFilterExist ="NO";
			    _mt_type="generalMT";	
                 var _ga_arr = [];		
			    	var _currentUrl = ""+window.location;
		    		 var currentUrlintIndex = _currentUrl.indexOf("/brand-"); 
				if($(".filterpage-l2 .filter-list-second ul li a.done").length>0){
					var filterLength = $(".filterpage-l2 .filter-list-second ul li a.done").length;
				    $(".filterpage-l2 .filter-list-second ul li a.done").each(function(index, obj){
				    	var _filter_type = $(obj).attr("data-filter-type");
				    	var _filter_value = $(obj).attr("data-filter-value");
				    	var _filter_url = $(obj).attr("data-filter-url");
			    		 var brandValue ="";
			    		var skipBrandParamFilter ="NO";
			    		 if(_filter_type =="BRAND")
			    			 {
			    			 	var brandUrlintIndex = _filter_url.indexOf("/brand-"); 
			    			 	var urlLength = _filter_url.length;
			    			 	var brandValueStr=_filter_url.substring((brandUrlintIndex+7),urlLength);
			    			 	var brandUrlValueIndex = brandValueStr.indexOf("/"); 
			    			 	 brandValue=  _filter_url.substring(0,(brandUrlintIndex+7+brandUrlValueIndex+1));
			    			 	 var currentbrandFilterUrl = _currentUrl.indexOf(brandValue); 
			    			 	 if(filterLength==1)
			    			 		{
				    				 _urlappend = brandValue;
				    				 skipBrandParamFilter = "yes";
			    			 		}
			    			 	 else{
			    			 	 if(currentUrlintIndex ==0  )
				    			 {
				    				 _urlappend = brandValue;
				    				 skipBrandParamFilter = "yes";
				    			 }
			    			 	 else {
			    			 		if(firstBrandFilterUrl.length==0)
	    			 		 		{
	    			 		 			firstBrandFilterUrl = brandValue;
	    			 		 			firstBrandFilterValue = _filter_value;
	    			 		 		}
			    			 		 	if(currentbrandFilterUrl>0){
			    			 		 			skipBrandParamFilter = "yes";
			    			 		 			urlBrandFilterExist = "yes";
			    			 		 	}
			    			 	 }
			    			 	 }
			    			 }
			    		 		
			    		 if(_filter_type !="BRAND"  || skipBrandParamFilter == "NO")
			    			 {						
			    			 if(index!=0 && _urlparams.length!=0){_urlparams=_urlparams+"&";}
			    			 if(_mt_type=="generalMT"){_urlparams =_urlparams+ "filter="+_filter_type+":\""+_filter_value+"\"";}
			    			 if(_mt_type=="mobileMT"){_urlparams = _urlparams+ _filter_type+"="+_filter_value;}	
			    			 }
						 _ga_arr.push(_filter_type+":"+_filter_value);
				    });
					var _new_str =_ga_arr.toString();
				    if($(".sort-by-form-ui form select").val()!=""){  
				    	_numberofproducts=$("#numberofproducts").val();
				    	_urlparams=_urlparams;//+"&sort="+$(".sort-by-form-ui form select").val()+"&pc="+_numberofproducts;
				    }	
				   $(".overlaywhite").removeClass("display-none");
						var _ga_attr_track = "refine_search_apply##"+_new_str.replace(/,/g,"|")+"##"+paramsObj.pageUri+"##"+0;
						var _ga_array_track = _ga_attr_track.split("##");
						gaEvent_track(_ga_array_track);
						if(_urlappend.length==0 && urlBrandFilterExist=="NO"   )
							{
							if(firstBrandFilterUrl.length>0 &&firstBrandFilterValue.length>0)
							{
							_urlappend = firstBrandFilterUrl;
			    			 if(_mt_type=="generalMT"){
			    				var  replaceStr ="filter=BRAND:\""+firstBrandFilterValue+"\"&" ; 
			    					if(_urlparams.indexOf(replaceStr)<0)
			    						{
			    						 replaceStr ="filter=BRAND:\""+firstBrandFilterValue+"\"";
			    						}
			    				_urlparams =  _urlparams.replace(replaceStr, ""); 
			    			 }
			    			 if(_mt_type=="mobileMT"){
				    				var  replaceStr ="BRAND="+firstBrandFilterValue+"&"  ;
				    				if(_urlparams.indexOf(replaceStr)<0)
		    						{
		    						 replaceStr ="BRAND="+firstBrandFilterValue ;
		    						}
			    				 _urlparams = urlparams.replace( replaceStr,"");
			    			 }	

							}
							else
								{
								if(currentUrlintIndex >=0  )
			   			 		 {
									var urlLength = _currentUrl.length;
				    			 	var brandValueStr=_currentUrl.substring((currentUrlintIndex+7),urlLength);
				    			 	var brandUrlValueIndex = brandValueStr.indexOf("/"); 
				    			 	 brandValue=  _currentUrl.substring(currentUrlintIndex,(currentUrlintIndex+7+brandUrlValueIndex));
				    			 	_urlappend = 	_currentUrl.replace(brandValue,"");	
				    			 	_urlappend = _urlappend.split('?')[0];
			   			 		 }
								}
							}
					    _urlparams =  escape(_urlparams);
					    if(_urlparams != "" && _urlparams != undefined)
					    	_urlappend =_urlappend+"?queryParam="+_urlparams;
					    window.location=_urlappend;
				}else{
					
   			 	 if(currentUrlintIndex >=0  )
   			 		 {
	    			 	var brandUrlintIndex = _currentUrl.indexOf("/brand-"); 
	    			 	var urlLength = _currentUrl.length;
	    			 	var brandValueStr=_currentUrl.substring((brandUrlintIndex+7),urlLength);
	    			 	var brandUrlValueIndex = brandValueStr.indexOf("/"); 
	    			 	 brandValue=  _currentUrl.substring(brandUrlintIndex,(brandUrlintIndex+7+brandUrlValueIndex));
	    			 	_currentUrl = 	_currentUrl.replace(brandValue,"");	
   			 		 }
					var _urlwithoutquery=_currentUrl.split('?')[0]; 
					
					$(".overlaywhite").removeClass("display-none");
					window.location=_urlwithoutquery;
				}		 
	 });
	}else{
		$("body").on("click",".apply-ui",function(e){
			var _n_obj = {};
			$(".filter-col").each(function(){
					var _key = $(this).find(".filter-list-second").attr("data-key");
					_n_obj[_key] = [];
					$($(this).find(".filter-list-second .done")).each(function(){	
						_n_obj[_key].push($(this).attr("data-filter-value"));						
					})	
			});
			//console.log(_n_obj);
			$.each(_n_obj,function(indx,obj){
					console.log(indx);
					console.log(obj);
					if(obj != "" && obj != null){   
					$.each(obj,function (idx,_v) {
						$("#gadgetslistfind").append("<input type='text' name="+indx+"[] value="+_v+" />");
					});
					//$("#gadgetslistfind").append("<input type='text' name="+indx+"[] value="+obj+" />");
					}
			});
			$("#gadgetslistfind").submit();
		});
		
	}
	
	if(paramsObj.leafTitle=='MobileFinder'){
		$("body").on("click",".filteropen",function(e){
			e.preventDefault();
			if(!$(".filterpage").hasClass("filteron")){
				$(".filterpage").addClass("filteron");
				$("body .container").addClass("display-none");
			if($(".click").hasClass("done")){
				$(".done").addClass("check").removeClass("uncheck");
			}
			_checkFliter();
			}else{
				$(".filterpage").removeClass("filteron");
				
			}
			/*if(hash == "#filter-l2"){
				_checkFliter();
				_checkFliterSecond();
				$(".filterpage-l2").addClass("filter-l2on");
				$("body .container").addClass("display-none");
			}else{
				$(".filterpage-l2").removeClass("filter-l2on");
				
			} */
		});
		$("body").on("click",".findfilerclose",function(e){
			e.preventDefault();
			$(".filterpage").removeClass("filteron");
			$("body .container").removeClass("display-none");
		})

		$("body").on("click",".backfilter, .arrow",function(e){
			e.preventDefault();
			if(!$(".filterpage-l2").hasClass("filter-l2on")){
				_checkFliter();
				_checkFliterSecond();
				$(".filterpage-l2").addClass("filter-l2on");
				$("body .container").addClass("display-none");
			}else{
				$(".filterpage-l2").removeClass("filter-l2on");
				
			}
		})
		$("body").on("click",".filternext",function(e){
			e.preventDefault();
			if(!$(".filterpage-l2").hasClass("filter-l2on")){
				_checkFliter();
				_checkFliterSecond();
				$(".filterpage-l2").addClass("filter-l2on");
				$("body .container").addClass("display-none");
			}else{
				$(".filterpage-l2").removeClass("filter-l2on");
				
			}
				
			
		})
		
	}
	 function gaInit(){
		/* Universal GA*/
		var _ga_value = $("input[name=ga_pin]").val();
		var _ga_pin_value = $("input[name=pincode]").val();
		var _ga_attr_track = "check_delivery##"+_ga_pin_value+"##"+_ga_value+"##"+0;
		//alert(_ga_attr_track);
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
		/* Universal GA*/
	}
	 
	 if ($.cookie('PINCODE')!=undefined) {
		$("#pincode").val($.cookie('PINCODE'));
		CheckDeliveryCod($("form[name='checkLocationReqForm']"));
		$(".deliverytime").css("display","none");
		gaInit();
	}
	$("body").on("click",".checkpinbutton", function(){
		CheckDeliveryCod($("form[name='checkLocationReqForm']"));
		gaInit();
		return false;
	});
	
	$("body").on("click",".changePin", function(e){
		e.preventDefault();
		$(".shippedbytab").addClass("display-none");
		$("form[name='checkLocationReqForm']").removeClass("display-none");
		$("#pincodeinput_error").text("");
		$('.reponsdata').html('').removeClass("error");
	});
	if(paramsObj.leafTitle!='MobileFinder'){	
	$("body").on("change",".sort-by-form-ui form select", function(e){
			e.preventDefault();
			if($(".filterpage-l2 .filter-list-second ul li a.done").hasClass("active")){
				$(".apply-ui").click();
			}else{				
			    var _urlparams="";
			    var _mt_type=$("#mtType").val();
			    _mt_type="generalMT";	
			    _windowUrl = window.location.href;
			    var _urlwithoutquery = _windowUrl;
			    if(_windowUrl.indexOf('?') > -1)
			    	_urlwithoutquery=window.location.href.split('?')[0];
			    
				    if($(".sort-by-form-ui form select").val()!=""){
				    	
				    	_numberofproducts=$("#numberofproducts").val();
				    	_urlparams=_urlparams+"sort="+$(".sort-by-form-ui form select").val();
						/* Universal GA*/
						var _ga_attr_track = "sort_option##"+$(".sort-by-form-ui form select").val()+"##"+_urlparams+"##"+0;
						var _ga_array_track = _ga_attr_track.split("##");
						gaEvent_track(_ga_array_track);
						console.log(_urlwithoutquery+"?"+_urlparams);
						window.location.href=_urlwithoutquery+"?"+_urlparams;
						/* Universal GA*/
				    	 $(".overlaywhite").removeClass("display-none");


						    	_urlparams =  escape(_urlparams);


							/*$.ajax({url:_urlwithoutquery,data:{queryParam:_urlparams,pc:_numberofproducts},type:'get'}).done(function(response) 
							{$(".product-list-option").html($(response).find(".product-list-option").html());
							//$(".section-container-ui h3").html($(response).find(".section-container-ui h3").html());
							applyLazyLoad();
							}).fail(function() {}).always(function(){$(".overlaywhite").addClass("display-none");});*/
				    }	

				
				 
			}
		
			
	});
}else	{
	$("body").on("change",".sort-by-form-ui form select", function(){
		//e.preventDefault();
		var locationUrl = window.location.href;   
		var decodedurl = decodeURIComponent(locationUrl); 
		var sortIndx = decodedurl.indexOf("sort=");
		var sortval = $(".sort-by-form-ui form select").val(); 
		if(sortIndx != -1)
		{
			var sortstring = decodedurl.substring(sortIndx,decodedurl.length);  
			sortstringonly = sortstring.substring(0,sortstring.indexOf('&')); 
			decodedurl = decodedurl.replace(sortstringonly,"sort="+sortval);
		}else
		{
			decodedurl=decodedurl+"&sort="+sortval; 
		}
		window.location.href=""+decodedurl;
		/*$("form[name=findersort]").attr("action",locationUrl);
		$("form[name=findersort]").submit();*/
	});
	}
	$("body").on("click",".seoreadmore",function(e){
		e.preventDefault();
		$(this).siblings(".seocontent").css("height","auto");
		$(this).html("show less").addClass("showless");
	});
	$("body").on("click",".showless",function(e){
		e.preventDefault();
		$(this).siblings(".seocontent").css("height","");
		$(this).html("read more...").removeClass("showless");
	})
	 $("body").on("click",".question-section dl a.readmore",function(e){
		e.preventDefault();
		$(".question-box").css("display",'block');
		$(this).css("display","none");
	});
	$("body").on("click",".rating-tab-open a.readmore",function(e){
		e.preventDefault();
		$(".reviewshow").css("display",'block');
		$(this).css("display","none");
	});
	$("body").on("click",".reviewreadmore",function(e){
		e.preventDefault();
		$(this).closest(".userreview").addClass("readmore");
		_autonewscroll(this);
	});
	
	$('.rating input').change(function(){
	  var _radio = $(this);
	  var _radio_val = $(this).val();
	  var _productId = $('input[name=productId]').val();
	  var _userId = $('input[name=userId]').val();
	  $(".rating input[type=radio]").prop('checked', false);
	  $(this).prop('checked', true);
	  $('.rating .selected').removeClass('selected');
	  _radio.closest('label').addClass('selected');
	  $("input[name=productRating]").val(_radio_val);
	  var url = "/control/submitR";		      
		$.ajax({
			url:url,
			type:'POST',
			data:{"productRating":_radio_val,"userLoginId":_userId,"productId":_productId},
			success:function(data){
					
			},
			error:function(){
				//alert("Please retry.");
			},
		});
	});
	
	/* $(window).on('orientationchange', function(event) {
       if(orientation==90 || orientation==-90){
		   $(".landscape-mode ").removeClass("display-none");
	   }else{
		   $(".landscape-mode ").addClass("display-none");
	   } //console.log(orientation);
    });
	*/
	$("body").on("submit","form[name='notification'], form[name='comingsoonForm']", function(){
		_notifyme(this);
		return false;
	});
	/* for product page  */
	if(paramsObj.leafTitle=='product'){
		_addCompare();
		compare();
		social_share();
		if(paramsObj.instock == true){
			var fixmeTopbuy = $('.productBuyTab').offset().top; 
			
			$(window).scroll(function() {                  // assign scroll event listener
				var currentScroll = $(window).scrollTop(); // get current position
				if (currentScroll >= fixmeTopbuy) {           // apply position: fixed if you
					$(".productBuyTab").addClass("floating");
					if(paramsObj.PageProductId==paramsObj.DealProductId){
						$(".deals-timer").addClass("floating-banner");
					}
					
				} else {                                   // apply position: static
					$(".productBuyTab").removeClass("floating");
					if(paramsObj.PageProductId==paramsObj.DealProductId){
						$(".deals-timer").removeClass("floating-banner");
					}
					
				}
			});
				  // get initial position of the element
		}
		
	var fixmeTop = $('.mh-4').offset().top;
		$(window).scroll(function() {                  // assign scroll event listener
				var currentScroll = $(window).scrollTop(); // get current position
				if (currentScroll >= fixmeTop) {           // apply position: fixed if you
					$(".productBuyTab").addClass("floating");					
				} else {                                   // apply position: static
					$(".productBuyTab").removeClass("floating");
				}
			});
	}
	
	$("body").on("click",".specs h4",function(){
		//$(".specs .list1").slideUp("slow");
		//$(".specs h4").addClass("collapsed");
		$(this).addClass("collapsed");
		$(this).closest(".specs").find(".list1").slideUp("slow");
	});
	$("body").on("click",".specs h4.collapsed",function(){
		//$(".specs .list1").slideUp("slow");
		//$(".specs h4").addClass("collapsed");
		$(this).removeClass("collapsed");
		$(this).closest(".specs").find(".list1").slideDown("slow");
	});
	
	/* news */
	$("body").on("click",".sort-articles .filter-sort",function(){
		if($(".sort-articles dl").hasClass("closed")){
			$(".sort-articles dl").removeClass("closed");
		}else{
			$(".sort-articles dl").addClass("closed");
		}
	});
	var _authorlist = $(".author-articles ul li").length;
	if(_authorlist < 5){
		$(".seemore_btn").css("display","none");
	}
	$("body").on("click",".seemore_btn",function(){
		$(".author-articles ul li").css("display","block");
		$(this).css("display","none");
	});
	$("body").on("click",".contentreadmore",function(e){
		e.preventDefault();
		$(this).closest(".article-text").find(".productcontent").css("height","auto");
		$(this).css("display","none");
	});
	/*news */
		
});
// body ready close;
 function _checkFliter(){
	 if($(".filterpage-l2 .filter-list-second ul li a.done").length > 0){
		 $(".filterbutton").removeClass("btn-color-grey").addClass("btn-color").addClass("apply-ui").removeClass("noaction");
	 }else{
		$(".filterbutton").addClass("btn-color-grey").removeClass("btn-color").removeClass("apply-ui").addClass("noaction");
	 }
 }
 
 function _checkFliterSecond(){
	 $(".filterpage-l2 .filter-list-second ul li a.click").removeClass("check").addClass("uncheck");
	 $(".filterpage-l2 .filter-list-second ul li a.done").addClass("check").removeClass("uncheck");
	 $(".filter-col").each(function(index){
		 if($(this).hasClass("display-none")){
		 }else{
			 var _thiscout = $(this).find(".filter-list-second ul li a.done").length;
			 $(this).find(".selectcount").html(_thiscout);
			 if(_thiscout > 0){
				 $(this).find(".btn").removeClass("btn-color-grey noaction").addClass("btn-color added");
			 }
		 }
	 })
 }

 function CheckDeliveryCod(form){
	$(".shippedbytab").addClass("display-none");
	var pincode = $(form).find("#pincode").val();
	var getProductId=$('#add_product_id').val();
	//var getProductId=$('#upsellloc').val();
	var _pincode=/^[\d]{6}$/i;

	if(getProductId==""){
		getProductId=$("select[name="+OPT[0]+"] option:eq(1)").val();
	}
	if( pincode=="") {
		$(form).find("#pincodeinput_error").text("Please enter Pincode");
		 return false;
	  }else{
		$(form).find("#pincodeinput_error").text("");
	  }
	  if( !_pincode.test( pincode ) ) {
		$(form).find("#pincodeinput_error").text("Invalid pincode.Please enter valid pincode");
		return false;
	  } else {
		$(form).find("#pincodeinput_error").text("");
		url="/control/checkDeliveryLocation?pinNumber="+pincode+"&productId="+getProductId;
		$.ajax({
		url:url,
		type:$(form).attr("method"),
		data:$(form).serialize(),
		dataType: 'json',
		success:function(data){
			$(".deliverytime").css("display","none");
			$(form).addClass("display-none");
			$(form).find("#pincodeinput_error").text("");
			//alert(data.maxPermitQuantity);
			var _message = '';
			if(data.message=="Not Servicable" || data.city=="NOT FOUND"){
					$('.reponsdata').html("<li>We can't ship the product to the pincode "+data.pinNumber+" you've entered. Please try again from a different location.</li><li><a href='#' class='changePin'>Change Pincode</a></li>").addClass('error');
			}else if(data.maxPermitQuantity < 1){
				$('.reponsdata').html("<li>We can't ship the product to the pincode "+data.pinNumber+" you've entered. Please try again from a different location.</li><li><a href='#' class='changePin'>Change Pincode</a></li>").addClass('error');
			}else{
				express = " ";
				if(data.PRE_EXPRESS_DELIVERY=="Y"){
					express = " express ";
					_message = "for Prepaid Order only.";
				}
				$('.reponsdata').html("<li class=\"exp\"><span></span>Yes! We"+express+"deliver on the pincode "+data.pinNumber+".</li><li class=\"cod\"><span></span>"+($.inArray("Cash On Delivery", data.payMentOption)!=-1?"Cash on Delivery Available <b id=\"checkwallet\"></b></li>":"<i style='color:#f00'>Cash on Delivery not available!</i></li>")+"<li class='pinchanglist'><a href='#' class='changePin'>Change Pincode</a></li>");
				$(".shippedbytab").removeClass("display-none");
				if(data.PRE_EXPRESS_DELIVERY=="Y"){
					$(".exp").after('<li><span></span>Free Express Delivery on Pre-paid orders</li>');
				}
			}
			if(data.CARD_WALLET=="Y"){$("#checkwallet").text('(Pay by Card or Wallet at Delivery)');}
			if(data.deliverydate){$('.cod').after('<li><span></span><b>'+data.deliverydate+'</b></li>');}
			
		},
		error:function(){
		alert("Please retry.");
		},
		});
	  } 
}
 
function isScrolledIntoView(elem){
	//console.log(elem);
	if($(elem).offset() != undefined){
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();			
		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();			
		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));		
	}else{
		return false;
	}
}
/// Scroll function to show amazon nudge on scroll
$(window).scroll(function(){ 
  	var offset = 500;
	var sticky = false;
	var top = $(window).scrollTop();
	var requestUrl = window.location.href;	
	if ($("header").offset().top < top) {
		$(".amazon_nudge").addClass("amazon_nudge_active");
		var nudgeelem = $(".amazon_nudge");
		if($(nudgeelem).hasClass("nudgeviewgaevent"))
			{
			  $(nudgeelem).removeClass("nudgeviewgaevent");
			  var _ga_attr_track = "SGN_Widget_nudge##View##"+requestUrl+"##0";
			  var _ga_array_track = _ga_attr_track.split("##");
			  gaEvent_track(_ga_array_track);			
			}
		sticky = true;
	} else {
		$(".amazon_nudge").removeClass("amazon_nudge_active");
	}
});


$(window).scroll(function(){onScrollGA();});
function onScrollGA()
{
	var elem = $(".viewgaevent");
	var productname = $("h1").text();
	if(isScrolledIntoView(elem) && $(elem).hasClass("viewgaevent")){
		$(elem).removeClass("viewgaevent");
		var _ga_attr_track = "gnshop_mweb_pdp_Latestnews_view##View_"+productname+"##view##"+0;
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);		
	}
	//////////
	var elemSimilar = $(".similarCont");
	if(isScrolledIntoView(elemSimilar) && $(elemSimilar).hasClass("viewSGgaevent")){
		$(elemSimilar).removeClass("viewSGgaevent");
		var _ga_attr_track = "gnshop_mweb_pdp_CompetitorList_view##View_"+productname+"##view##"+0;
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);		
	}
	
	////////// Nudge
	
	
	
	///////////////////
	
	var element = $(".pricefilterView");
	var productname = $("h1").text();
	if(isScrolledIntoView(element) && $(element).hasClass("pricefilterView")){
		$(element).removeClass("pricefilterView");
		var _ga_attr_track = "gnshop_mweb_pdp_Gadgetunder_view##View_"+productname+"##view##"+0;
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);		
	}
}
function applyLazyLoad(){
			$(window).scroll(function(){displayImages();});
			$(window).resize(function(){displayImages();}); 
			displayImages();   
	function displayImages(){    
		$("img.lazy").each(function(indx,elem){
		var _src='https://static.toiimg.com/photo/79837759.cms';
		var _data_original='https://static.toiimg.com/photo/79837759.cms'; 
		if($(elem).attr("src") != undefined){_src=$(elem).attr("src")}
		if($(elem).attr("data-original") != undefined){_data_original=$(elem).attr("data-original")}       		
		if(isScrolledIntoView(elem) && $(elem).attr("src")!=$(elem).attr("data-original") && $(elem).hasClass("lazy")){
				$(elem).attr("src",$(elem).attr("data-original"));
				$(elem).removeClass("lazy");
				$(elem).error(function(){
					$(elem).attr("src",'https://static.toiimg.com/photo/79837759.cms');
				})
			}
		});    	
	}	 
}


function getDateDelivery(){
	       // Express Delivery timer and date funcation
          
          var dt = new Date();
          function getTomorrow(d,offset){
              if (!offset){
                  offset = 1;
              }
              if(typeof(d) === "string"){
                  var t = d.split("-"); /* splits dd-mm-year */
                  	d = new Date(t[2],t[1] - 1, t[0]);
              	}
              	return new Date(d.setDate(d.getDate() + offset));
          }    
          	
          	var month = new Array();
          	month[0] = "Jan";
          	month[1] = "Feb";
          	month[2] = "Mar";
          	month[3] = "Apr";
          	month[4] = "May";
          	month[5] = "Jun";
          	month[6] = "Jul";
          	month[7] = "Aug";
          	month[8] = "Sep";
          	month[9] = "Oct";
          	month[10] = "Nov";
          	month[11] = "Dec";

          	var week = new Array();
          	week[1] = "Mon";
          	week[2] = "Tue";
          	week[3] = "Wed";
          	week[4] = "Thu";
          	week[5] = "Fri";
          	week[6] = "Sat";
          	week[0] = "Sun";
          	
          	var nextthree = getTomorrow(dt,1);
          	var _month = month[nextthree.getMonth()];
          	var _week = week[nextthree.getDay()];
          	var newdate = nextthree.getDate();
          	var _fulldate = _month + ', ' + newdate + ', ' + dt.getFullYear();
          	var _newfulldate = _month + ', ' + newdate + ', ' + dt.getFullYear();
          	setTimeout(function(){
				//console.log(_newfulldate);
          		if(document.getElementById("sladate") != null){
          		document.getElementById("sladate").innerHTML = _newfulldate;
          		}
				if(paramsObj.PageProductId==paramsObj.DealProductId && paramsObj.dealType=='today-deal'){
					_amazon_timer(_newfulldate);					
				}
          	},1000);
          	
          	
          function _getnewDate(){
          		var d = new Date();
				d.setHours(d.getHours() - 12);
          		var offset = 2;
          		    if(typeof(d) === "string"){
          		        var t = d.split("-"); /* splits dd-mm-year */
          		        d = new Date(t[2],t[1] - 1, t[0]);
          		    	}
          		    _a = new Date(d.setDate(d.getDate() + offset));
          		    var _month = month[_a.getMonth()];
          		    return _month+", " +_a.getDate()+", "+_a.getFullYear();			
          }
          var countDownDate = new Date(_fulldate).getTime();
          var x = setInterval(function() {
          	var now = new Date().getTime();
          	var distance = countDownDate - now;
          	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          	var seconds = Math.floor((distance % (1000 * 60)) / 1000);
          	var _ELE_DIV = document.getElementById("getting-started");
          	if(_ELE_DIV != null){
          	if(hours >= 12){
          			var hours = hours-12;
          			
          			if(hours==0){
          				_ELE_DIV.innerHTML = minutes + " mins "+ seconds+ " secs" ;
          			}else{
          				_ELE_DIV.innerHTML = hours + " hours " + minutes + " mins "+ seconds+ " secs" ;
          			}
          			}else{
          			var hours = hours+12;
          			var _resetDate = _getnewDate(); 
          			document.getElementById("sladate").innerHTML = _resetDate;
          			_ELE_DIV.innerHTML = hours + " hours " + minutes + " mins "+ seconds+ " secs" ;
          		
          	}
          	
          	if (distance < 0) {
          	    clearInterval(x);
          	  _ELE_DIV.innerHTML = "Usually Delivers in 1-3 days.";
          	  }
          }
          }, 1000);
          
	}
function customerComment(){
	$("body").on("submit","form[name=customerComment]",function(e){
		var _url = $(this).prop("action");
		_partyId = '';
		if(paramsconfig._loginstatus=='login'){
			_partyId = '&userId='+paramsconfig._partyId;
		}
		if(evalForm(this)){
			$("overlaywhite").removeClass("display-none");
			$.ajax({
	     			url:_url,
	     			dataType:'json',
	     			type:$(this).prop("method"),
	     			data: $(this).serializeArray()
	     		}).done(function(response){
	     			
	     			 
	     			
	     			if(response.status=="success"){
						if(paramsconfig._loginstatus!='login'){
							$(".username").html('<input type="text" name="userId" value="" placeholder="Please Enter You Name" />');
						}else{
							$(".questionpost").remove();
						}
						$("input[name=token]").val(response.id);
						$(".customercommentmsg").addClass("success").html(response.responseMsg);
						$("#TakeComment, #TakeComment_error").remove();
					}
	     			if(response.status=="error"){
						$(".customercommentmsg").addClass("error").html(response.responseMsg);
					}	     			
	     		}).always(function(){
					 $("overlaywhite").addClass("display-none");
				}); 
		}
		return false;
	});
	$("body").on("submit","form[name=replyComment]",function(e){
		if($(".replyReponse").hasClass("success")){
			$(".replyReponse").removeClass("success")
		}
		if($(".replyReponse").hasClass("error")){
			$(".replyReponse").removeClass("error")
		}
		if(evalForm(this)){
			$("overlaywhite").removeClass("display-none");
			$.ajax({
	     			url:$(this).prop("action"),
	     			type:$(this).prop("method"),
	     			data: $(this).serializeArray()
	     		}).done(function(response){
					if(response.status=='success'){
						$(this).find(".replyReponse").addClass("success").html(response.msg);
						$(this).find("textarea").val("");
					}
					if(response.status=="error"){
						$(this).find(".replyReponse").addClass("error").html(response.msg);
					}	
	     		}).always(function(){
					 $("overlaywhite").addClass("display-none");
				}); 
		}
		return false;
	});
}	
	
function userReview(){
		 $("body").on("submit","form[name=reviewProduct]",function(e){	    	 
	     	if(evalForm(this)){
				$("overlaywhite").removeClass("display-none");
	     		$.ajax({
	     			url:$(userReviewForm).prop("action"),
	     			type:$(userReviewForm).prop("method"),
	     			data: $(userReviewForm).serializeArray()
	     		}).done(function(response){
	     			response={
	     				"status":"success",
	     				"_EVENT_MESSAGE_":"This is a dummy message. Will be replaced by actual message"
	     			}
	     			if(response.status=="success"){
	     				$(_top_container).html("<div class=\"server-message text-align-center success-message\"><span class=\"icon\">&nbsp;</span><br/>"+ response._EVENT_MESSAGE_ +" </div>");
		     				if($(".user-review-container").length > 1){	
		     				setTimeout(function(){
		     					$(_top_container).slideUp("slow");
		     				},5000);
	     				}else{
							$(_top_container).find(".server-message").append("<em>Redirecting to Home</em>");
	     					setTimeout(function(){
		     					window.location.href="/";
		     				},5000);
	     				}
	     			}
	     			if(response.status=="error"){
	     				$("<div class=\"server-message text-align-center error-message\"><span class=\"icon\">&nbsp;</span><br/>"+ response._EVENT_MESSAGE_ +"</div> ").insertBefore($(userReviewForm));
						if($(".user-review-container").length > 1){
		     				setTimeout(function(){
		     					$(_top_container).find(".error-message").slideUp("slow");
		     				},5000);
					   }
	     			}
	     		}).always(function(){
					 $("overlaywhite").addClass("display-none");
				}); 
	     	}
	     	return false;
	    });
	 
}

function QuestionAndAnswer(){
	$(".answer-box").each(function(){
		var _this = this;
		var _count = $(_this).find("dd").length;
		if(_count > 2){
			$(_this).find("dd:gt(1)").addClass("display-none");
			$( _this).find(".moreanswer").removeClass("display-none").addClass("display-block");
		}	
	});
	$("body").on("click",".moreanswer",function(e){
		e.preventDefault();
		$(this).closest(".answer-box").find("dd").removeClass("display-none");
		$(this).addClass("display-none").removeClass("display-block");
	});
	$("body").on("click",".leavecomment",function(e){
		e.preventDefault();
		$(this).addClass("display-none");
		$(".QuestionAnswer").addClass("display-none");
		$(this).next("form").addClass("display-block").removeClass("display-none");
	});
	$("body").on("click",".commentcancel",function(e){
		e.preventDefault();
		$(this).closest("form").addClass("display-none");
		$(this).closest("form").find("textarea").removeClass("onError");
		$(this).closest("form").find(".error").html('&nbsp;').removeClass("errorMsg");
		$(".leavecomment").removeClass("display-none");
	});
	$("body").on("submit","form[name=customerQuestion]",function(){
		var _this=this;
		var _evalForm = evalForm(_this);
		if($(".customerQuestionReponse").hasClass("success")){
			$(".customerQuestionReponse").removeClass("success")
		}
		if($(".customerQuestionReponse").hasClass("error")){
			$(".customerQuestionReponse").removeClass("error")
		}
		if( _evalForm == true){
			$("overlaywhite").removeClass("display-none");
			$.ajax({
			 	url:_this.action,
			 	dataType:'json',
			 	type:_this.method,
			 	data:$(_this).serializeArray()
			 	}).done(function(response){
			 		if(response.responseCode=="success"){
						$(".customerQuestionReponse").html(response.responseMsg).addClass("success");
						$("#askQuestion").val("");
					}else{
						$(".customerQuestionReponse").html(response.responseMsg).addClass("error");
					}
				 		
				 }).fail(function(xhrObj,xhrStatus,xhrError){
				 	alert("something wrong please try again");
				    //console.log(xhrError);	
				 }).always(function(){
				 	$("overlaywhite").addClass("display-none");			 	
				 });
				 return false;
			}
		
		return false;
	});
	$("body").on("submit","form[name=QuestionAnswer]",function(){
		var _this=this;
		var questionId = $(_this).attr("data-id");
		var _evalForm = evalForm(_this);
		if( _evalForm == true){
			$("overlaywhite").removeClass("display-none");
			$.ajax({
			 	url:_this.action,
			 	dataType:'json',
			 	type:_this.method,
			 	data:$(_this).serializeArray()
			 	}).done(function(response){
			 		//console.log(response);
			 		if(response.responseCode=="success"){
						$("#"+questionId).html(response.responseMsg).addClass("success");
						$(_this).addClass("display-none");
						$("#"+questionId).next("a.response").addClass("display-none").removeClass("displayblock");
					}else{
						$("#"+questionId).html(response.responseMsg).addClass("error");

				}
				 		
				 }).fail(function(xhrObj,xhrStatus,xhrError){					 
				  //  console.log(xhrError);
					alert("something wrong please try again");				  ;
				 }).always(function(){
				 	$("overlaywhite").addClass("display-none");					 	
				 });
				 return false;
			}
		return false;	
	});
}
/* pixl condtion     */
function getURLParameter(name) {
	return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]);
}
function utmInit() {
	var referer_source = document.referrer;
	if (referer_source == null || referer_source == "" || referer_source.match("^http://shop.gadgetsnow.com") == "http://shop.gadgetsnow.com")
		referer_source = 'DIRECT';
	var utmkey = getURLParameter("utm_source");
	var cashkaroId = getURLParameter("clickID");
	var utmkeyUp = utmkey;
	var _utmuppercase = utmkeyUp.toUpperCase();
	var _Currdate = new Date();
	var _timestamp = _Currdate.getTime();
	var oldReferSource = $.cookie("refer_source");
	var _expires = 30;
	if (cashkaroId !== "NULL" && cashkaroId !== null) {
		$.cookie("cashkaroId", cashkaroId, {
				expires : _expires,
				path : '/'
			});	
	
	}
	if (_utmuppercase !== "NULL" && _utmuppercase !== null) {
		//var oldCookieValue = $.cookie("utmsource");
		//alert(oldCookieValue +"+"+ _utmuppercase);
		var strGetting = _utmuppercase;
		var digitInStar = strGetting.substring(0, 3);
		if (digitInStar == "DGM") {
			$.cookie("utmsource", digitInStar, {
				expires : 7,
				path : '/'
			});
		} else {
			$.cookie("utmsource", _utmuppercase, {
				expires : 7,
				path : '/'
			});
		}
		var firstSource = $.cookie("first_utm");
		if (firstSource != null && firstSource != "") {
			second_utm_value = _utmuppercase + '##' + getURLParameter("utm_medium") + '##' + getURLParameter("utm_campaign") + '##' + referer_source + '##' + _timestamp;
			$.cookie("second_utm", second_utm_value, {
				expires : 7,
				path : '/'
			});
		} else {
			first_utm_value = _utmuppercase + '##' + getURLParameter("utm_medium") + '##' + getURLParameter("utm_campaign") + '##' + referer_source + '##' + _timestamp;
			$.cookie("first_utm", first_utm_value, {
				expires : 7,
				path : '/'
			});
		}
		//set this in referer
		$.cookie("refer_source", referer_source, {
			path : '/'
		});
	} else if ((oldReferSource == null || oldReferSource == "") && referer_source != null) {
		var firstSource = $.cookie("first_utm");
		if (firstSource != null && firstSource != "") {
			second_utm_value = 'null##' + getURLParameter("utm_medium") + '##' + getURLParameter("utm_campaign") + '##' + referer_source + '##' + _timestamp;
			$.cookie("second_utm", second_utm_value, {
				expires : 7,
				path : '/'
			});
		} else {
			first_utm_value = 'null##' + getURLParameter("utm_medium") + '##' + getURLParameter("utm_campaign") + '##' + referer_source + '##' + _timestamp;
			$.cookie("first_utm", first_utm_value, {
				expires : 7,
				path : '/'
			});
		}
		$.cookie("refer_source", referer_source, {
			path : '/'
		});
	}
}

function _addCompare(){
	 $("body").on("click",".compareProduct",function(e){
			e.preventDefault();
			var _getCategory = $(this).attr("data-ctg");
			var  _getProductid= $(this).attr("data-product");
			_url = '/pdpCompare?categoryId='+_getCategory+'&productId='+_getProductid;
			fetch(_url,{ headers: { "Content-Type": "application/json; charset=utf-8" }})
			  .then(function(response) {
				return response.json();
			  })
			  .then(function(myJson) {
				  if(myJson != ''){
					  $.each(myJson, function(k, v) {
							window.location.href=myJson.newUrl;
						});
					  }else{
						  alert('Please try again.'); 
					  }
				
			  })
			  .catch(err => {
					alert("sorry, there are no results for your search");
								});	
		})
				 
	}
function _autoscroll(obj){
	$('html, body').animate({
		scrollTop: $(obj).offset().top
	}, 2000);
}
function getLoginSSOData()
{
	var url = window.location.href;
	if(url.indexOf("checkout") == -1 && url.indexOf("checkoutPayment") == -1 && url.indexOf("login"))
	{	
		var script = document.createElement('script');
		result= 'https://jsso.indiatimes.com/sso/crossdomain/getTicket?&callback=getDataByTicket';
		script.src =result;
		document.body.appendChild(script);
		return false;
	}
}

function getDataByTicket(data) 
{
	var jsonData = JSON.parse(JSON.stringify(data));
	
	var ticketId = jsonData.ticketId;
	var status = jsonData.status;
	
	_loginHead(ticketId,status);
}
function _loginHead(ticketId,status){
	var _url = "/control/getLoginheader";
	var  ticketId = "ticketId="+ticketId;  
	$.ajax({
			url:_url,
			data:ticketId,
			type : "POST",
			dataType:'json',
	    }).done(function(response){
			//console.log(response.login);
			if(response.login.loginStatus=='true'){
				$(".count").html(response.login.cartCount);
				$(".loginname").html(response.login.userName);
				$(".loginMy").attr("href","#").addClass("hamburger-nav");
				$(".logoutUser").html('<a href="/control/logout">log out</a>');
				$(".withoutlogin").remove();
				$(".loginUser").removeClass("display-none").html(response.login.userName.charAt(0));
				
			}
			
			$(".checkcompare").attr("href",response.login.compareUrl);
			dataLayer.push({
				'event':'VirtualPageview',
				'dimension4' : response.login._loginstatus,
					'userId' : response.login._partyId,
				'dimension14': response.login._partyId
			});
			if(!response.login.isPrimeUser || response.login.isPrimeUser!="true"){
				$.cookie("isPrimeUser",null);
				_isPrimeUserFun();
				dataLayer.push({'Prime_User_Type' : 0,'event' : 'primeUser'});
			}else{
				$.cookie("isPrimeUser",'true',{expiry:0,path:'/'});
				dataLayer.push({'Prime_User_Type' : 1,'event' : 'primeUser'});
			}
	    })
}
function _autonewscroll(obj){
	$('html, body').animate({
		scrollTop: $(obj).offset().top - 55
	}, 2000);
}

function initInfinteScroll(){
    if($(".product-list-option .more-results:first").length>0){        		
    		var _loading_flag=false;
    		$(window).scroll(function() {        		  
			  if(isScrolledIntoView($(".product-list-option li:last")) && _loading_flag==false) {	
			   		if($(".product-list-option .more-results a").attr("href") != undefined && _loading_flag==false){
			  		  _loading_flag = true;			  	    
			   			var _url = $(".product-list-option .more-results a").attr("href");
						 if(_url != ""){
			   			$(".product-list-option .more-results").html("<div style='text-align:center;margin:10px 0 20px 0'><img src='/mobile/img/AjaxLoader.gif' alt='Loading' /></div>");				   			
			   			_numberofproducts=$("#numberofproducts").val();
			   			if(_numberofproducts != "")
			   				_url=_url+"&pc="+_numberofproducts;
						$.ajax(_url).done(function(response) {
			   				$(".product-list-option").append($(response).find(".product-list-option").html());
			   				applyLazyLoad();
							adsCheck();
			   				if(!paramsconfig._isPrimeUser || paramsconfig._isPrimeUser!="true"){
			   					
								_isPrimeUserFun();
							}
			   			}).fail(function() { /*alert("error");*/ }).always(function() { _loading_flag = false;$(".product-list-option .more-results:first").remove();}); 
			   		}}
			   }
			});
	}		
 }
 function adsCheck(){
		$("*[data-slot=256682]").addClass("adcode");
}
/* CTN ads check condtion     */
function ctnAdcheck(){
	if(paramsObj.leafTitle!='category'){
		$(".colombiaFail").parent().css("display","none");
	}
}
function HomePageBrand(){
		$("body").on("click","#ankush .click-for-brands",function(e){
			e.preventDefault();
			$(this).removeClass("click-for-brands");
			var _get_url = $(this).attr("href");
			var	_id_Array = $(this).attr("data-tab");
			var	_get_id = _id_Array.split("##");
			$.ajax(_get_url).done(function(response) {
				$("#"+_get_id[0]).find(".newcarousel").html(response);
				$(".check-carousel").each(function(){
					_this = this;
					var _carousel_list_count = $(_this).find(".newcarousel li").length; 
					var _list_width = $(_this).find(".newcarousel").attr("data-width-list");
					$(_this).find(".newcarousel").css("width",_carousel_list_count*_list_width);
				});
			}).fail(function() { alert("Something is wrong, Please try again"); }).always(function() { 
				applyLazyLoad();
			});
		});
		
	
}
function _notifyme(form){
	var _emaildata = $(".notifyinput").val();
		_emaildata =$.trim(_emaildata);
		$(".notifyinput").val(_emaildata);
	 var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 $('.reponsdata').html("").removeClass("error");
	 if( _emaildata=="") {
		$(form).find("#notifyinput_error").addClass("errorMsg").text("Please enter Email ID");
		 return false;
	  }else{
		$(form).find("#notifyinput_error").removeClass("errorMsg").text("").addClass("display-none");
	  }
	  if( !emailReg.test( _emaildata ) ) {
		$(form).find("#notifyinput_error").addClass("errorMsg").text("Invalid email address.Please use a valid email address");
		return false;
	  } else {
		$(form).find("#notifyinput_error").removeClass("errorMsg").text("").addClass("display-none");
		$.ajax({
			url:$(form).attr("action"),
			type:$(form).attr("method"),
			data:$(form).serialize(),
			
			success:function(data){ 
				if(data.trim() == "success"){
					$('.notification-tab').addClass('display-none');
					$('.reponsdata').removeClass('display-none').addClass('messge').html($('.notification-tab .messge').html()+'<br><br>An email has been sent to '+_emaildata+'.Please click on the verification link to get an alert once the product is available for you to order.');
				}else{
					$('.reponsdata').html("Sorry, "+data.ErrorMsg).addClass('error');
				}
			},
			error:function(jqXHR, textStatus){ 
			},
		});
	  } 
}

//Amazon timer deal
function _amazon_timer(_newfulldate){
	  //var amzFesDt = document.getElementById("amzFesDt").value;
	  var amzFesDt = _newfulldate;
	  var countDownDate = new Date(amzFesDt.replace(/-/g, '/')).getTime();
	  var x = setInterval(function() {
	  var now = new Date().getTime();
	  var distance = countDownDate - now;
	  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	  var _hours = hours-1;
	  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	  var _getHour = new Date();
      var _newHour = _getHour.getHours();
	 /* if(days==0){
		  document.getElementById("deals-timer").innerHTML = hours + " hrs : " + minutes + " mins : " + seconds + " secs ";
	  }else{
		  document.getElementById("deals-timer").innerHTML = days + " days : " + hours + " hrs : " + minutes + " mins : " + seconds + " secs ";  
	  }
	  */
	  if(paramsObj.dealType=='flash-sale'){
		  if(_newHour>=16 && _newHour < 18){
			$(".deals-timer").html("<div><span> Gadgets Now 'Flash Sale' on this product ends in </span><span> "+ hours + " hr(s) : "+ minutes + " min(s) : " + seconds + " sec(s)</span></div>");  
		  }else{
			$(".deals-timer").html("<div><span> Gadgets Now 'Flash Sale' on this product starts in </span><span> "+ _hours + " hr(s) : " + minutes + " min(s) : " + seconds + " sec(s)</span></div>"); 
		  }  
	  }else if(paramsObj.dealType=='today-deal'){
		  $(".deals-timer").html("<div><span> Gadgets Now 'Deal Of The Day' for this product ends in </span><span> "+ hours + " hr(s) : " + minutes + " min(s) : " + seconds + " sec(s)</span></div>");
	  }
	  
	  if (distance < 0) {
		clearInterval(x);
		document.getElementById("deals-timer").innerHTML = "EXPIRED";
	  }else{
		  $('.deals-timer').css('display','block');
	  }
	}, 1000);
	}
          
// Amazon timer deal
// _counter deal
function _counter(){
	  //var amzFesDt = document.getElementById("").value;
	var d = new Date(2022,1,22,18,00,00); // new Date("2022-02-22 00:18:00");;
	var _gaviewevent = "mweb_Countdowntimer_shop##Galaxy Unpacked##View##0"
	var _ga_array = _gaviewevent.split("##");
		gaEvent_track(_ga_array);
	var countDownDate = d.getTime();console.log("----"+d);console.log("cntd---"+countDownDate);
	var x = setInterval(function() {
	  var now = new Date().getTime();
	  var distance = countDownDate - now;
	  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	  var _hours = hours-1;
	  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	  seconds = seconds+'';
	  if(seconds.length == 1 )
	  {
	  seconds = '0'+seconds;
	  }
	  var secondsArr = seconds.split('');
	  
	   minutes = minutes+'';
	  if(minutes.length == 1 )
	  {
	  minutes = '0'+minutes;
	  }
	  var minutesArr = minutes.split('');
	  
	   hours = hours+'';
	  if(hours.length == 1 )
	  {
	  hours = '0'+hours;
	  }
	  var hoursArr = hours.split('');
	  
	  var _getHour = new Date();
      var _newHour = _getHour.getHours();
	  $(".timer").html('<span class="timer_box"><span class="time"><span>0</span><span>'+days+'</span></span><span class="label">days</span></span><span class="timer_box"><span class="time"><span>'+hoursArr[0]+'</span><span>'+hoursArr[1]+'</span></span><span class="label">hrs</span></span><span class="timer_box"><span class="time"><span>'+minutesArr[0]+'</span><span>'+minutesArr[1]+'</span></span><span class="label">min</span></span><span class="timer_box"><span class="time"><span>'+secondsArr[0]+'</span><span>'+secondsArr[1]+'</span></span><span class="label">sec</span></span>');
	  if (distance < 0) {
		clearInterval(x);
		$('.timer_container').css('display','none');
	  }
	}, 1000);
	}
          
// _counter timer deal


//flash sale condition
	  if(paramsObj.PageProductId==paramsObj.DealProductId && paramsObj.dealType=='flash-sale'){
			_amazon_timer(paramsObj.dealends);
		}
// flash sale condition

function social_share(){	
		var product_name = $(".product-detail-tab .product-detail h1").text();
		//var _params = escape("utm_source=w&utm_medium=referral");
		var current_url = encodeURIComponent(window.location.href);
		var whatsapp_url = "whatsapp://send?text=Hey, Look what I found on Gadgets Now - "+product_name+"&nbsp;"+current_url;
		//$(".whatsapp").html("");
		/* Universal GA*/
		var ga_whatsapp = $("input[name=whatsapp_share]").val();
		/* Universal GA*/
		$(".whatsapp").attr('href', whatsapp_url);
		$(".whatsapp").attr('data-ga-track', ga_whatsapp);
		$(".gplus-icon").attr('href', "https://plus.google.com/share?url="+current_url);
		$(".twitter").attr('href', "https://twitter.com/intent/tweet?text=Hey, Look what I found on Gadgets Now - "+product_name+":%20"+current_url);
		$(".linkedin").attr('href', "http://www.linkedin.com/shareArticle?mini=true&url="+current_url+"&title="+product_name);
		$(".copytoclipboard").attr('href', current_url);
		//$(".email-icon").attr('href', "mailto:ankush.kumar@timesinternet.in?subject="+product_name+"&body="+current_url);
	
	/*			
	$(document).on("click","body",function (e){
		    var container = $(".mobile_share_options");
		    var _initiator = $(".mobile_share");
		    if (!container.is(e.target) // if the target of the click isn't the container...
		        && container.has(e.target).length === 0 // ... nor a descendant of the container
		        && !_initiator.is(e.target))
		    {container.css("display","none");container.css("cursor","");}
		});
	*/
	//console.log($(location));
	$.getScript('//connect.facebook.net/en_US/sdk.js', function(){
		    FB.init({
		      appId: '577459282421311',
		      version: 'v2.3' // or v2.0, v2.1, v2.0
		    });     
		  });		
	  $("body").on("click","#shareOnfacebook",function(e){
		  	e.preventDefault();
			if(paramsObj.leafTitle=="honorselfie"){
				var _url =$(this).attr('href');
			}else{
				var _url =$(location).attr('href');
			}
		  	//_url = _url.replace("local.","shopping.")
		  	FB.ui({
			  method: 'share',
			  href: _url+"?utm_source=f&utm_medium=referral"
			}, function(response){});
		
	   });
}


function compare(){
	if($(".card").hasClass("checkdiff")){
		_fixcomparescroll();
	}
	
	$("body").on("click",".closeX", function(e){
		$(".brand, .brandlist").addClass("active");
		$(".brand, .brandlist li").show();
		$(".model, .modellist").removeClass("active");
		$(".brandName, .modelName").val('');
		$(this).closest(".compare-form").remove();
		$(".tab-wrapper").css("display",'block');
		$("body").removeClass('fixed-position');
		
	});
	var _form_html = $(".compare-form").clone();
	$("body").on("click",".tab-wrapper",function(){
		$(this).css("display","none");
		$(_form_html).insertAfter(this);
		setTimeout(function(){$("#compareFrom input[name=brandName]").focus(); }, 500);
		$("body").addClass('fixed-position');
	});
	$("body").on("click",".compareSearch",function(e){
		//$(_form_html).insertAfter(this);
		e.preventDefault();
		$(".comparepdpform").html(_form_html);
		setTimeout(function(){$("#compareFrom input[name=brandName]").focus(); }, 500);
		$("body").addClass('fixed-position');
	});
	
	$("body").on("keyup",".brandName, .modelName",function(){
		_this = this;
	    var yourtext = $(_this).val();
	    if (yourtext.length > 0 && yourtext!='') {
	        var abc = $(this).parent(".field-wrapper").find("ul.brandsName li").filter(function () {
	            var str = $(this).text();
	            var re = new RegExp(yourtext, "i");
	            var result = re.test(str);
	            if (!result) {
	                return $(this);
	            }
	        }).hide();
	    } else {
	        $(_this).parent(".field-wrapper").find("ul.brandsName li").show();
		}
	});	
	
	$("body").on("click",".brandlist .brandsName li",function(){
		_loading();
		$("#_modelname").html('');
		var _brandname = $(this).attr("data-brand");
		var _category_page = $(this).attr("data-category");
		$("#compare input[name=brand]").val(_brandname);
		$(".brand, .brandlist").removeClass("active");
		$(".model, .modellist").addClass("active");
		if($(".modellist li").hasClass(_brandname)){
			$("."+_brandname).removeClass("display-none");
		}
		_url = '/getBrandModel?brandModel='+_brandname+'&category='+_category_page+'';
		fetch(_url,{ headers: { "Content-Type": "application/json; charset=utf-8" }})
		  .then(function(response) {
			return response.json();
		  })
		  .then(function(myJson) {
			  if(myJson.modelList != ''){
				  $.each(myJson.modelList, function(k, v) {
						$(".overlaywhite").addClass('display-none');
						setTimeout(function(){$("#compareFrom input[name=modelName]").focus(); }, 500);
						$("#_modelname").append("<li class='"+v['BRAND']+"' data-id='"+v['PRODUCT_ID']+"' >"+v['PRODUCT_NAME']+"</li>")
					});
				  }else{
					  alert('Please try with different brand.'); 
					  $(".brand").click();
					  $(".overlaywhite").addClass('display-none');
				  }
			
		  })
		  .catch(err => {
				alert("sorry, there are no results for your search");
				$(".overlaywhite").addClass('display-none');
			});
	});
	
	$("body").on("click",".modellist .brandsName li",function(){
		var _modelid = $(this).attr('data-id');
		var _modelname = $(this).text();
		_modelname = _modelname.replace(/[^a-zA-Z0-9\s]/g, "-").replace(/[ ]/g,"-");
		_modelname = _modelname.replace(/-+/g, "-")
		var _com_id = $("#compare input[name=fModel]").val();
		if(paramsObj.leafTitle=="product"){
			_check_click = '2';
		}else{
			_check_click = $(this).parents("li").attr("data-check");
		}
		if(_com_id !=_modelid){			
			if(_check_click=='1'){
				$("#compare input[name=fModel]").val(_modelid);
				$("#compare input[name=fModelN]").val(_modelname);
			}else if(_check_click=='2'){
				$("#compare input[name=sModel]").val(_modelid);
				$("#compare input[name=sModelN]").val(_modelname);
			}
			$("#compare input[name=model]").val(_modelid);
			
			
			if(paramsObj.leafTitle=="product"){
				var _currentUrl = Product_createCompareUrl();
			}else{
				var _currentUrl = createCompareUrl();
			}
			$("form[name=compare]").attr('action', _currentUrl);	
			$("form[name=compare]").submit();
		}else{
			alert("This product is already selected, Please try another Product");
		}
	});
	$("body").on("click",".brand",function(e){
		e.preventDefault();
		$(".brand, .brandlist").addClass("active");
		$(".model, .modellist").removeClass("active");	
		$(".compare-form")[0].reset();
	});
	
	$("body").on("click",".proclose",function(e){
		e.preventDefault();
		_loading();
		var _remove_position = $(this).attr("data-close");
		var _remove_id = $(this).attr("data-product");
		_url = '/removeModel?removeIndex='+_remove_position+'&productId='+_remove_id;
		fetch(_url,{ headers: { "Content-Type": "application/json; charset=utf-8" }})
		  .then(function(response) {
			return response.json();
		  })
		  .then(function(myJson) {
			  if(myJson != ''){
				  $.each(myJson, function(k, v) {
						window.location.href=myJson.newUrl;
					});
				  }else{
					  alert('Please try again.'); 
					  $(".overlaywhite").addClass('display-none');
				  }
			
		  })
		  .catch(err => {
				alert("sorry, there are no results for your search");
				$(".overlaywhite").addClass('display-none');
			});	
	})
	
/*	$("body").on("click",".productAdded", function(){
		//$(".selectlist").addClass("listhover");
		if($(".selectlist").hasClass("hidden")){
			$(".selectlist").removeClass("hidden");
			$("body").removeClass('fixed-position');
		}else{
			$(".selectlist").addClass("hidden");
		}
	});
	*/
	$("body").on("mouseout",".selectlist", function(){
		if($(".selectlist").hasClass("hidden")){
			$(".selectlist").removeClass("hidden");
		}else{
			$(".selectlist").addClass("hidden");
		}
	});
	
	$("body").on("click",".selectlist li", function(){
		var _sr = $(this).attr("data-id");
		$(".selectFeatures").html(_sr);
		$(this).closest("ul").addClass("hidden");
		if($("#"+_sr).hasClass("collapse")){
			$("#"+_sr).removeClass("collapse").addClass("expanded");
			$("#"+_sr).next(".listrow").removeClass("hidden");
		}
		$('html, body').animate({
			scrollTop: $("#"+_sr).offset().top-100
		}, 2000);
	});
	$("body").on("click",".listhead", function(){
		if($(this).hasClass("collapse")){
			$(this).removeClass("collapse").addClass("expanded");
			$(this).next(".listrow").removeClass("hidden");
		}else{
			$(this).addClass("collapse").removeClass("expanded");
			$(this).next(".listrow").addClass("hidden");
			//$(this).next(".listrow").removeClass("collapsecheck");
		}
		
	});
	$("body").on("click",".showdifferences span", function(){
		if($(this).hasClass("check-right")){
			$(this).removeClass("check-right").addClass("not-check");
			
			$(".comparelist").removeClass("comparelistshow");
		}else{
			$(this).addClass("check-right").removeClass("not-check");
			$(".comparelist").addClass("comparelistshow");
		}
		
	});
	if(paramsObj.leafTitle!="product"){
		_checknew();
	}
	function _checknew(){
		var _arrayCount = $("#arrayCount").val();
			_arrayCountNew = _arrayCount.split("##");
			$.each( _arrayCountNew, function( key, value ) {
					if($("#"+value).next("similar")){
						$("#"+value).addClass("collapsecheck");
					}
			});
			 
	}
	
	
	function _fixcomparescroll(){
		var _lastdivId = $(".listhead").last().attr('id');
		var a = function() {
	        var b = $(window).scrollTop();
	        var d = $(".compareheader").offset().top;
	        var _ctn_p = $('#'+_lastdivId).offset().top-48;
	        var c = $(".admobilecompare");
	        if (b > d && b < _ctn_p) {
	        	c.addClass('fixed-ads').removeClass('absolute-ads');
	        }
	        if(b < d){
	        	c.removeClass('fixed-ads').removeClass('absolute-ads');
	        }
	        if(b > _ctn_p){
	        	c.addClass('absolute-ads').removeClass('fixed-ads');
	        }
	    };
	    $(window).scroll(a);a();
	}
	
	function _loading(){
		$(".overlaywhite").removeClass("display-none");
	}
	
	function createCompareUrl()
	{
		var _urlSeprator = '';
		var _currentUrl = ""+window.location;
    	var fIndex = _currentUrl.indexOf("/compare-"); 
    	var sIndex = _currentUrl.lastIndexOf("/"); 
    	if(fIndex != sIndex)
    		{
    		_currentUrl = _currentUrl.substring(0,sIndex);
    		}
    	_currentUrl = _currentUrl+'/';
		if(undefined != $("#compare input[name=fModelN]").val() && ''!=$("#compare input[name=fModelN]").val())
			{
			_currentUrl = _currentUrl+($("#compare input[name=fModelN]").val())+'-'+($("#compare input[name=fModel]").val());
			_urlSeprator = '-vs-';
			}
		if(undefined !=$("#compare input[name=sModelN]").val()  && ''!=$("#compare input[name=sModelN]").val())
		{
			_currentUrl = _currentUrl+_urlSeprator+($("#compare input[name=sModelN]").val())+'-'+($("#compare input[name=sModel]").val());
		}
		
		return _currentUrl;
	}
	
	function Product_createCompareUrl()
	{
    	_currentUrl = $(".compareSearch").attr("href");
		if(undefined != $("#compare input[name=fModelN]").val() && ''!=$("#compare input[name=fModelN]").val()){
			_currentUrl = _currentUrl+($("#compare input[name=fModelN]").val())+'-'+($("#compare input[name=fModel]").val());
			_urlSeprator = '-vs-';
			}
		if(undefined !=$("#compare input[name=sModelN]").val()  && ''!=$("#compare input[name=sModelN]").val()){
			_currentUrl = _currentUrl+_urlSeprator+($("#compare input[name=sModelN]").val())+'-'+($("#compare input[name=sModel]").val());
		}		
		return _currentUrl;
	}
	
	
}
function _addCompare(){
 $("body").on("click",".compareProduct",function(e){
		e.preventDefault();
		var _getCategory = $(this).attr("data-ctg");
		var  _getProductid= $(this).attr("data-product");
		_url = '/pdpCompare?categoryId='+_getCategory+'&productId='+_getProductid;
		fetch(_url,{ headers: { "Content-Type": "application/json; charset=utf-8" }})
		  .then(function(response) {
			return response.json();
		  })
		  .then(function(myJson) {
			  if(myJson != ''){
				  $.each(myJson, function(k, v) {
						window.location.href=myJson.newUrl;
					});
				  }else{
					  alert('Please try again.'); 
				  }
			
		  })
		  .catch(err => {
				alert("sorry, there are no results for your search");
							});	
	})
			 
}

function PerpetualScroll(){
	$(window).scroll(function() {  
		$('.nextArticleCall').each(function() {
			var _id = this.id;
			var _refreshAd = $("#"+_id).offset().top+50; 
			var currentScroll = $(window).scrollTop();
			var _url = $("#"+_id).attr("href");
			var _current_url = $("#"+_id).attr("data-current-url");  
			var _next_title = $("#"+_id).attr("data-next-title");  
			var _current_title = $("#"+_id).attr("data-current-title");
			if (currentScroll >= _refreshAd) {
			if($(this).hasClass("datacheck")){
					$(this).removeClass("datacheck");
					history.pushState(null, null, _url);
					$("#article-"+_id).html("<div class='ptag'><img src='/mobile/img/AjaxLoader.gif'><div class='loading-text'>Please wait Next Article is loading...</div></div>");
					$.ajax({
						  url: _url,
						  dataType: "html"
						}).done(function(data) {;
							
							$("#article-"+_id).html($(data).find(".article-show").html());
							 $("title").html(_next_title);
							 if(!paramsconfig._isPrimeUser || paramsconfig._isPrimeUser!="true"){
									_isPrimeUserFun();
								}
						}).fail(function(data) {
							//alert("Next article is not exists.");
						}).always(function() {
							
					  });
				}
				if($(this).hasClass("callnext")){
					history.pushState(null, null, _url);
					$(this).removeClass("callnext").addClass("callback");
					_pushPageViewData();
				}
				
			 }
			if (currentScroll < _refreshAd){
				if($(this).hasClass("callback")){
					$(this).removeClass("callback").addClass("callnext");
					 $("title").html(_current_title);
					history.pushState(null, null, _current_url);
					_pushPageViewData();
				}
			 }
		});
	});
		
	
}
