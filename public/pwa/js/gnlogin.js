$(document).ready(function () {

	getLoginSSOData();
	function getLoginSSOData() {
		var url = window.location.href;
		var script = document.createElement('script');
		result = 'https://jsso.indiatimes.com/sso/crossdomain/getTicket?version=v1&callback=callbackfunc';
		script.src = result;
		document.body.appendChild(script);
		return false;
	}
});
function callbackfunc(data) {
	var jsonData = JSON.parse(JSON.stringify(data));
	var ticketId = jsonData.ticketId;
	var status = jsonData.status;
	console.log("tc st " + ticketId + status);
	//_loginHead(ticketId);
    getLoginUserData(ticketId);

}

function getLoginUserData(ticketId) {
	var jssoObj = new JssoCrosswalk("gadgetsnow", "web");
	var responseJson = null;
	jssoObj.getUserDetails(function (response) {
		if (response.status !== undefined && response.status === "SUCCESS") { 
			setLoginInfo(response,ticketId);
		} else {
			console.log("err in checking user status");
		}

	});
	return responseJson;
}
function setLoginInfo(response,ticketId)
{
	var ssoId=response.data.ssoid;
	var primeProfile=response.data.primeProfile !== null ? response.data.primeProfile :'';
	var ticketId= ticketId;
	var now = new Date();
    now.setTime(now.getTime() + 1 * 3600 * 1000);
	document.cookie = "ssoid="+ssoId+"; expires="+now.toUTCString()+"; path=/";
	if(primeProfile !== "")
		document.cookie = "prc="+primeProfile+"; expires="+now.toUTCString()+"; path=/";
	document.cookie = "ticketId="+ticketId+"; expires="+now.toUTCString()+"; path=/";

}
function openSSO() {
	var ru = window.location.protocol+"//"+window.location.host+"/sso.cms";
	let url = "https://jsso.indiatimes.com/sso/identity/login?channel=gadgetsnow&ru=" + ru;
	if(typeof localStorage != "undefined"){
		localStorage.setItem('ru', window.location.href);
	}
	openPopup(url,
		{
			width: 850,
			height: 780,
			scrollbars: 0,
		});
}
function openPopup(url, options) {

	const defaultOptions = {
		width: 300,
		height: 300,
		name: 'Window',
		mask: true,
		resizable: false,
		disableScroll: false,
		closeCallback() { },
	};
	const opt = { ...defaultOptions, ...options };

	const x = window.screen.width / 2 - opt.width / 2;
	const y = window.screen.height / 2 - opt.height / 2;
	let params = [
		'width=300',
		'height=300',
		`left=${typeof opt.left !== 'undefined' ? opt.left : x}`,
		`top=${typeof opt.top !== 'undefined' ? opt.top : y - 20}`,
		`scrollbars=${typeof opt.scrollbars !== 'undefined' && opt.scrollbars === 1
			? opt.scrollbars
			: 0
		}`,
		`resizable=${opt.resizable}`,
	];

	const popup = window.open(url, opt.name, params.join(','));
	if (popup) {
		popup.focus();
	}
	var timer = setInterval(function() { 
		if(popup && popup.closed) {
			clearInterval(timer);
			window.location=window.location.href;
		}
	}, 1000);
}







