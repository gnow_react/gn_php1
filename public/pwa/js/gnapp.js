function getParamNameValueString(name, value, url = "") {
	return name && value
	  ? `${url.includes("?") ? "&" : "?"}${name}=${value}`
	  : "";
  }
function loadSaleNudge() {
	let saleNudgeData = {};
	$.ajax({
	  url: "https://www.gadgetsnow.com/site_config.cms?feedtype=json",
	  cache: false,
	  success: function (data) {
		if (data && data.amazon_sale_nudge) {
		  _render_nudge(data.amazon_sale_nudge);
		  saleNudgeData = data.amazon_sale_nudge;
		}
	  },
	  failure: function () {},
	});
	function _render_nudge(data) {
	  const nudgeImageUrl =
		data.imageURL || "https://static.toiimg.com/photo/79512588.cms";
	  $("#saleNudgeContainer").append(
		'<div class="saleNudgeBox"><span class="saleNudgeClick"><img alt="amazon_sale" src="' +
		  nudgeImageUrl +
		  '" /></span></div>'
	  );
	  $("#saleNudgeContainer").hide();
	  let isNudgeVisible = false;
	  $(window).on("load scroll", function () {		
		if (window.scrollY > 400) {
		  if (!isNudgeVisible && saleNudgeData.url) {
			ga('send', 'event', data.ga.category || "Widget_nudge", 'View', data.ga.label || 'SaleNudge');        
		  } 
		  isNudgeVisible = true;
		  $("#saleNudgeContainer").show();
		}else {
		  isNudgeVisible = false;
		  $("#saleNudgeContainer").hide();
		}
	  });
	}
	$("body").on("click", ".saleNudgeClick", function () {
	  const nudgeGA = saleNudgeData.ga || {};
	  const landingURL = saleNudgeData.url;
	  const url = `https://www.gadgetsnow.com/affiliate_amazon.cms?url=${encodeURIComponent(
		`${landingURL}${getParamNameValueString(
		  "tag",
		  saleNudgeData.tag,
		  landingURL
		)}`
	  )}`;
	  ga('send', 'event', nudgeGA.category || "Widget_nudge", 'Click', nudgeGA.label || "Nudge");        
	  window.open(url, "_blank");
	});
  }
function getAffilateData(type){
	var category = $("body").attr("data-category");
	var uname = $("body").attr("data-uname");
	var dataURL = `/affiliate/`+ type +`/`+ category +`/`+ uname;
	var _loader = "<div style='text-align:center; display:block; padding:20px 0'><img src='https://static.toiimg.com/photo/90219844.cms' /></div>";
	$("#dataContainer_"+ type).html(_loader);
	$.ajax({url:dataURL,type:'GET',dataType: 'html'}).done(function(data){
		$("#dataContainer_"+ type).html(data);
	}).fail(function(){
		console.log("some error found.");
		$("#dataContainer_"+ type).html("");	
	}).always(function(){
		
	});
}
function getCounterData(){
	var category = $("body").attr("data-category");
	var uname = $("body").attr("data-uname");
	var dataURL = `/affiliate/COUNTER/mobile-phones/type?check=2`;
	var _loader = "<div style='text-align:center; display:block; padding:20px 0'><img src='https://static.toiimg.com/photo/90219844.cms' /></div>";
	//$("#dataContainer_countdowntimer").html(_loader);
	$.ajax({url:dataURL,type:'GET',dataType: 'html'}).done(function(data){
		$("#dataContainer_countdowntimer").html(data);
	}).fail(function(){
		console.log("some error found.");
		$("#dataContainer_countdowntimer").html("");	
	}).always(function(){
		
	});
}
// _counter deal
function _counter(){
 var d = new Date(endDate); // new Date("2022-02-22 00:18:00");
  var _gaviewevent = "Countdowntimer##"+timerTitle+"##View##0"
  var _ga_array = _gaviewevent.split("##");
	  gaEvent_track(_ga_array);
  var countDownDate = d.getTime();
  //console.log("----"+d);console.log("cntd---"+countDownDate);
  var x = setInterval(function() {
	var now = new Date().getTime();
	var distance = countDownDate - now;
	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	var _hours = hours-1;
	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	seconds = seconds+'';
	days=days+'';
	if(days.length == 1)
	{
		days = '0'+days;
	}
	if(seconds.length == 1 )
	{
	seconds = '0'+seconds;
	}
	var secondsArr = seconds.split('');
	
	 minutes = minutes+'';
	if(minutes.length == 1 )
	{
	minutes = '0'+minutes;
	}
	var minutesArr = minutes.split('');
	
	 hours = hours+'';
	if(hours.length == 1 )
	{
	hours = '0'+hours;
	}
	var hoursArr = hours.split('');
	var daysArr = days.split('');
	var _getHour = new Date();
	var _newHour = _getHour.getHours();
	$(".timer").html('<span class="timer_box"><span class="time"><span>'+daysArr[0]+'</span><span>'+daysArr[1]+'</span></span><span class="label">days</span></span><span class="timer_box"><span class="time"><span>'+hoursArr[0]+'</span><span>'+hoursArr[1]+'</span></span><span class="label">hrs</span></span><span class="timer_box"><span class="time"><span>'+minutesArr[0]+'</span><span>'+minutesArr[1]+'</span></span><span class="label">min</span></span><span class="timer_box"><span class="time"><span>'+secondsArr[0]+'</span><span>'+secondsArr[1]+'</span></span><span class="label">sec</span></span>');
	if (distance < 0) {
	  clearInterval(x);
	  $('.timer_container').css('display','none');
	}
  }, 1000);
  }
		
// _counter timer deal
$("body").on("click",".seeAllAffiliate",function(){
	$(this).addClass('hide');
    $(".affiliateCard").removeClass('hide');
});
$("body").on("click",".readmore",function(){
	var _ht = $(".aftbox").attr("data-height");
	_this = $(this);
	
	if($(this).hasClass("active")){			
		$(this).removeClass("active");
		$(this).html("... Read Less");
		$(".aftbox").css("height","auto");
		_autonewscroll(".aftbox",160);
		ga('send', 'event', $('body').attr('data-gacategory'), 'ReadMore', 'ATF');
	}else{
		$(this).addClass("active");
		$(this).html("... Read More");
		$(".aftbox").css("height",_ht);
		_autonewscroll(".aftbox",160);
		ga('send', 'event', $('body').attr('data-gacategory'), 'ReadLess', 'ATF');
	}
});
$("body").on("click",".spec-open",function(){
	if($(this).hasClass("spec-close")){
		ga('send', 'event', $('body').attr('data-gacategory'), $(this).text(), 'Expand');
		$(this).removeClass("spec-close");
		$(this).closest('.spec-box').find('.spec-table').removeClass("close");
	}else{
		ga('send', 'event', $('body').attr('data-gacategory'), $(this).text(), 'Collapse');
		$(this).addClass("spec-close");
		$(this).closest('.spec-box').find('.spec-table').addClass("close");
	}

});

$("body").on("click","*[data-ga]",function(){
		var _ga_attr_track = $(this).attr("data-ga");
		var _ga_array_track = _ga_attr_track.split("##");
		gaEvent_track(_ga_array_track);
});

function _autonewscroll(obj, _top){
	$('html, body').animate({
		scrollTop: $(obj).offset().top - _top
	}, 'slow');
}
$(window).on('resize scroll', function() {
	var viewEvent = $('.pageview');
	if (viewEvent.length && viewEvent.isInViewport()) {
        var _gatrack = $(".pageview").attr("data-view-ga");
		$(".pageGA").removeClass("pageview");
		var _gaarray = _gatrack.split("##");
		gaEvent_track(_gaarray);	
    }
});
$.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};
function getCookie(name) {
	  var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
	  if (match) return match[2];
	}
function submitRateReview()
 {
	var rating =  $('input[name=rateval]').val();
	if(rating !== undefined && rating !== "")
	{
		var vote = 2*rating;
		var msid= $('input[name=reviewmsid]').val();
		var reviewBoxInput=$('textarea[name=reviewtext]').val();//"Nice Phone";
		var urlForRate = 'https://www.gadgetsnow.com/rate_gadgets.cms?msid='+msid+'&getuserrating='+rating+'&criticrating=&vote='+vote;
		$.ajax(urlForRate).done(function(response) {});
		if(reviewBoxInput !== "")
		{
		var jssoObj = new JssoCrosswalk("gadgetsnow", "web");
		var response = jssoObj.getUserDetails(function(response){ 
             if(response.status!==undefined && response.status==="SUCCESS"){ 
				 var urlMytimes= "https://myt.indiatimes.com/mytimes/profile/info/v1/?ssoid=87cuarspfb7ial8xgs62yn3kx";
            	 $.ajax({
                	 url:urlMytimes,
                	 dataType:"json",
                	 xhrFields: {
                         withCredentials: true
                    },}).done(function(userdata)
                    	 {
                	 		callbackUserDetail(userdata);
                	 		}).fail(function(){console.log("Error");});
            	}else{ 
				 	
 		         console.log("err in checking user status");
              }

         }); 
		} 
	}
	$(".ratingpop").removeClass("openpop");
}	
function encodeDataForFormUrlEncodedRequest(obj) {
	  if (!obj && typeof obj !== 'object') {
	    return obj;
	  }

	  const data = Object.keys(obj)
	    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`)
	    .join('&');

	  return data;
	}
function callbackUserDetail(user)
{
	var rating =  $('input[name=rateval]').val();
	var msid =  $('input[name=reviewmsid]').val();;
	var vote = 2*rating; var msid=72510517;
	var reviewBoxInput=$('textarea[name=reviewtext]').val();//"Nice Phone";
	const urlForComment = 'https://www.gadgetsnow.com/postro4.cms';
    let payload = {
      fromname: user.FL_N,
      fromaddress: user.EMAIL,
      userid: user.uid,
      imageurl: user.profile,
      loggedstatus: '1',
      message: reviewBoxInput,
      roaltdetails: 1,
      ArticleID: msid,
      msid,
      parentid: 0,
      rootid: 0,
      url: encodeURIComponent(window.location.href),
      configid: 52847546,
      urs: rating,
      rotype: 0,
      pcode: 'gadgetsnow',
    };
    payload = encodeDataForFormUrlEncodedRequest(payload);
	 $.ajax({
	    url: urlForComment,
	    data:payload,
	    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
	    success: function (result, status, xhr) {
			$(".ratingbtn span").text("Your Rating: "+rating);
			$(".topreviewlist").prepend('<div class="topreview" data-name="P"><div class="topname">'+user.F_N+'</div><div class="topcomment">'+reviewBoxInput+'</div><div class="toprating"><span>'+rating+'</span></div></div>');
	        console.log("Success");
	    },
	    error: function (xhr, status, error) { 
	    	console.log("Error");
	    }
	}); 
}
$("body").on("click",".submitbutton",function(){
	 submitRateReview();
});
$("body").on("click",".ratingbtn span, .rate",function(){
	var lgn=getCookie('ticketId');
	if(lgn!= undefined)
	{
		$(".ratingpop").addClass("openpop");
	}
	else {
		openSSO();
	}
});
$("body").on("click",".closepopbtn",function(){
	$(".ratingpop").removeClass("openpop");
});
$("body").on("click",".searchicon",function(){
	$(".searchcontainer").addClass("active");
});
$("body").on("click",".searchback",function(){
	$(".searchcontainer").removeClass("active");
});
$("body").on("keypress","#search_input",function(e){
	onInputKeyPress(e);
});
$("body").on("keyup","#search_input",function(e){
	debouncedOnSearchInput(e);
});
$("body").on("paste","#search_input",function(e){
	e.preventDefault();
    return false;
});
function gotoSearchItemLink(item, setInRecent){
    if (setInRecent) {
      setRecentSearchesData(item);
    }
    let url = item.url || item.wu;
    if (url.indexOf('/') === 0 && url.indexOf('//') !== 0) {
      url = 'https://www.gadgetsnow.com'+url;
    }
    window.location.href = url;
};
function getLocalStorage(key) {
  let value;
  if (key && typeof localStorage === 'object') {
    try {
      value = localStorage.getItem(key);
    } catch (err) {
      console.warn(err);
    }
  }
  return value;
};
function setLocalStorage(key, value) {
  if (typeof localStorage === 'object') {
    try {
      localStorage.setItem(key, value);
      return true;
    } catch (err) {
      console.warn(err);
      return false;
    }
  }
  return undefined;
};
function getRecentSearchesData(){
	let recentSearches = getLocalStorage("recent-search");
    if (recentSearches) {
      try {
        recentSearches = JSON.parse(recentSearches);
      } catch (e) {
        console.warn(
          'error parsing recent search',
        );
      }
    }
	let recentSearchItems = null;
	if ( recentSearches && recentSearches.data){
		recentSearchItems = recentSearches.data;
	}
    return recentSearchItems;
}
function setRecentSearchesData(item){
    let existingRecentSearches = getRecentSearchesData();
    let updatedRecentSearches = [];
    updatedRecentSearches.push({
      name: item.Product_name,
      url: item.url,
    });
    if (existingRecentSearches && existingRecentSearches.length) {
      // filter out current item from existing recent items list
      existingRecentSearches = existingRecentSearches.filter(
        (existingItem) => existingItem.name !== item.Product_name,
      );
      // keep 2 items from the existing items. 3 items to be max
      updatedRecentSearches = updatedRecentSearches.concat(
        existingRecentSearches.slice(0, 2),
      );
    }
    setLocalStorage(
		"recent-search",
      JSON.stringify({
        data: updatedRecentSearches,
      }),
    );
}
function getRecentSearches(){
	const recentSearches = this.getRecentSearchesData();
    if (!(recentSearches && recentSearches.length)) {
      return null;
    }
	var content = '';
	content += '<h6>Recent</h6><div class="strending">';
	$.each(recentSearches,function(i,j){
		content +="<a onclick='gotoSearchItemLink("+ JSON.stringify(j) +");' data-ga='"+ $('body').attr('data-gacategory') +"##Search_Recent##"+ j["name"]+"'>"+j["name"]+"</a>";
	});
	content += '</div>';
	$('#recentSearchesContainer').html(content);
}
$("body").on("click",".clearBox",function(){
	$('#search_input').val('');
	$('#filterSearchesContainer').removeClass('active');
	$('#filterSearchesContainer').html('');
	$(this).removeClass('active');
});
function removeUnsupportedCharacters(str) {
  return (str || '').replace(/[^0-9a-z\s_-]/gi, '');
}
function debounce(func, timeout = 300){
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => { func.apply(this, args); }, timeout);
  };
}
function debouncedOnSearchInput(e){
	debounce(onSearchInput(e));
}
function onSearchInput(e){
	const keyword = removeUnsupportedCharacters(e.target.value);
	if ( keyword ){
		$(".clearBox").addClass('active');
		var dataURL = `https://www.gadgetsnow.com/pwafeeds/gnow/mweb/list/search/json?path=/search/keyword/&keyword=`+ keyword;
		$.ajax({url:dataURL,type:'GET',dataType: 'json'}).done(function(data){
			if ( data.jsonFeed.data ){
				var searchData = data.jsonFeed.data;
				var resultObj = searchData.find((obj) => obj.gadgets);
				var searchDataArr = [];
				if ( resultObj && resultObj.gadgets && resultObj.gadgets.data){
					searchDataArr = resultObj.gadgets.data;
				}
				var content = '';
				if ( searchDataArr && searchDataArr.length > 0 ){
					content += `<ul class="keywordlist">`;
					$.each(searchDataArr,function(i,j){
						var itemHTML = j["Product_name"].replace(
							new RegExp(keyword, 'gi'),
							(match) => `<mark>${match}</mark>`,
						);
						content +="<li><a onclick='gotoSearchItemLink("+ JSON.stringify(j) +", true);' data-ga='"+ $('body').attr('data-gacategory') +"##Search_Product_"+ keyword+"##"+ j["Product_name"]+"'>"+itemHTML+"</a></li>";
					});
					content += `</ul>`;
					
				}else{
					content += '<span>No Record Found</span>';
				}
				$('#filterSearchesContainer').addClass('active');
				$('#filterSearchesContainer').html(content);
			}
		}).fail(function(){
			console.log("some error found.");	
		}).always(function(){
			
		});
	}else{
		$(".clearBox").removeClass('active');
		$('#filterSearchesContainer').removeClass('active');
		$('#filterSearchesContainer').html('');
	}
}
function onInputKeyPress(e){
    const keyword = removeUnsupportedCharacters(e.target.value);
	if (keyword && e.key === 'Enter') {
      //this.goToTopicPage(keyword);
      ga('send', 'event', $('body').attr('data-gacategory'), 'Search', keyword);
    }
  }

function gaEvent_track(_tracksplit){
	ga('send', 'event', _tracksplit[0], _tracksplit[1], _tracksplit[2], _tracksplit[3]);
}
function getTrendinSearchData(){
	var dataURL = `https://www.gadgetsnow.com/pwafeeds/gnow/mweb/list/search/json`;
	$.ajax({url:dataURL,type:'GET',dataType: 'json'}).done(function(data){
		if ( data.jsonFeed.sections["Trending Searches"]["data"]["items"] ){
			var trendingData = data.jsonFeed.sections["Trending Searches"]["data"]["items"];
			var content = '';
			if ( trendingData.length > 0){
				content += '<h6>Trending</h6><div class="strending">';
				$.each(trendingData,function(i,j){
					content +="<a onclick='gotoSearchItemLink("+ JSON.stringify(j) +");' data-ga='"+ $('body').attr('data-gacategory') +"##Search_Trending##"+ j["hl"]+"'>"+j["hl"]+"</a>";
				});
				content += '</div>';
			}
			$('#trendingSearchesContainer').html(content);
		}
	}).fail(function(){
		console.log("some error found.");	
	}).always(function(){
		
	});
}
$('body').on('change', '#searchCategorySelect',function(){
	var categoryId= $(this).val();
	if (categoryId) {
		$.ajax({
			url: "https://www.gadgetsnow.com/pwafeeds/gnow/web/list/gadgets/json?path=/filter/&perpage=1&category="+ categoryId +"&isInLine=N",
			type: "GET",
			dataType: "json",
			success: function(data){
				if ( data && data.jsonFeed && data.jsonFeed.filter && data.jsonFeed.filter.gadgetfacetnodes ){
					let fieldList = data.jsonFeed.filter.gadgetfacetnodes;
					const field = fieldList.find(
						(currField) =>
						currField.queryparam === 'brand',
					);
					if ( field ){
						let fieldOptions = field.filtervaluenodes;
						fieldOptions = fieldOptions.filter((option) => option.count > 0);
        				const parsedOptions = [];
						fieldOptions.forEach((option) => {
							parsedOptions.push({
								id: option.queryname,
								value: option.displayname,
							});
						});
						if( parsedOptions){
							$('#searchBrandSelect').attr('disabled', false);
							$.each(parsedOptions,function(key,data){
								$('select[name="brandselect"]').append('<option value="'+data["id"]+'">'+data["value"]+'</option>');
							});
						}
					}
				}
				
			}
		});
	}
});
$('body').on('change', '#searchBrandSelect',function(){
	var categoryId= $("#searchCategorySelect").val();
	var brandId= $(this).val();
	if (categoryId && brandId) {
		$.ajax({
			url: "https://www.gadgetsnow.com/pwafeeds/gnow/web/list/gadgets/json?path=/filter/&perpage=1&category="+ categoryId +"&brand="+ brandId +"&isInLine=N",
			type: "GET",
			dataType: "json",
			success: function(data){
				if ( data && data.jsonFeed && data.jsonFeed.filter && data.jsonFeed.filter.gadgetfacetnodes ){
					let fieldList = data.jsonFeed.filter.gadgetfacetnodes;
					const field = fieldList.find(
						(currField) =>
						currField.queryparam === 'price-range',
					);
					if ( field ){
						let fieldOptions = field.filtervaluenodes;
						fieldOptions = fieldOptions.filter((option) => option.count > 0);
        				const parsedOptions = [];
						fieldOptions.forEach((option) => {
							parsedOptions.push({
								id: option.queryname,
								value: option.displayname,
							});
						});
						if( parsedOptions){
							$('#searchBudgetSelect').attr('disabled', false);
							$.each(parsedOptions,function(key,data){
								$('select[name="budgetselect"]').append('<option value="'+data["id"]+'">'+data["value"]+'</option>');
							});
						}
					}
				}
				
			}
		});
	}
});
$('body').on('change', '#searchBudgetSelect',function(){
	var categoryId= $("#searchCategorySelect").val();
	var brandId= $('#searchBrandSelect').val();
	var budgetId= $(this).val();
	if ( categoryId &&  brandId && budgetId){
		$("#searchGadgetButton").attr('disabled', false).removeClass('notallowed');
	}
});
function gadgetSearch(){
	var categoryId= $("#searchCategorySelect").find(':selected').attr('data-seoname');
	var brandId= $('#searchBrandSelect').val();
	var budgetId= $('#searchBudgetSelect').val();
	if ( categoryId &&  brandId && budgetId){
		ga('send', 'event', $('body').attr('data-gacategory'), 'GadgetSearch', 'SearchGadget_click');
		window.location.href = 'https://www.gadgetsnow.com/'+ categoryId +'/filters/brand='+ brandId + '&&price-range='+ budgetId;
	}else{
		$("#searchGadgetButton").attr('disabled', true).addClass('notallowed');
	}
}
$("body").on("click",".rate input",function(){
	var _rating = $(this).attr('checked', true).val();
	$('input[name=rateval]').val(_rating);
	if(_rating == 5){
		$(".ratinghealth").html("Excellant");
	}
	else if(_rating == 4){
		$(".ratinghealth").html("Good");	
	}else if(_rating == 3){
		$(".ratinghealth").html("Buyable");	
	}else if(_rating == 2){
		$(".ratinghealth").html("Average");	
	}else if(_rating == 1){
		$(".ratinghealth").html("Poor");	
	}
	
});

const observe = function(entries, observer){
	entries.forEach(entry => {
		if ( entry.intersectionRatio > 0){
			const img = entry.target;
			const src = img.dataset.src;
			const srcset = img.dataset.srcset;
			if ( srcset ){
				img.src = src;
			}
			img.src = src;
			observer.unobserve(img);
		}
	});
}
const observer = new IntersectionObserver(observe, {});
const images = document.querySelectorAll('img.lazy');
images.forEach(function(img){
	observer.observe(img);
});
const viewObserver = new IntersectionObserver(viewObserve, {});
const viewEle = document.querySelectorAll('.pageGA');
viewEle.forEach(function(el){
	viewObserver.observe(el);
});
