sendPageLoadGAEvent();
$("document").ready(function(){
	if (window && window.grx) {
      const objGrx = {};
      objGrx.params = {};
      objGrx.params.url = window.location.href;
      window.grx('track', 'page_view', objGrx);
  }
	setTimeout(function(){		
		getAffilateData('AFF_BUY');
		getAffilateData('AFF_SEE_ALSO');
	}, 6000);
	setTimeout(function(){
		getTrendinSearchData();
		getRecentSearches();
	}, 12000);
  $(".sectionTabs").click(function (){
	  var elementIdToScroll = $(this).attr("data-sectionid");
	  $(".sectionTabs").removeClass('active');
	  		$(this).addClass('active');
			_autonewscroll("#"+elementIdToScroll, 130);
	});
	$(".keyFeatureDots").click(function (){
	  var elementIdToScroll = $(this).attr("data-slideid");
	  $(".keyFeatureDots").removeClass('active');
	  	$(this).addClass('active');
		$("#"+elementIdToScroll)[0].scrollIntoView({
			behavior: 'smooth',
			block: 'nearest',
			inline: 'nearest',
		});
	});	
	let touchstartX = 0;let touchstartY = 0;let touchendX = 0;let touchendY = 0;
	$('.slidesbox').on('touchstart', function(event) {
		touchstartX = event.changedTouches[0].screenX;
		touchstartY = event.changedTouches[0].screenY;
	});
	$('.slidesbox').on('touchend', function(event) {  
		touchendX = event.changedTouches[0].screenX;
		touchendY = event.changedTouches[0].screenY;
		handleKeyFeatureFolds(this);
	});
	function handleKeyFeatureFolds(element) {
		var totalSlides = $('#keyFeatureFoldsId').find('.slidesbox').length;
		var slideId = parseInt($(element).data("item"));
		if (touchendX <= touchstartX) {
			slideId = slideId + 1;
			if ( slideId < totalSlides){
				var elementIdToScroll = $(this).attr("id");
				$(".keyFeatureDots").removeClass('active');
				$("#dotKeyFeaturesSlides_"+slideId).addClass('active');
				ga('send', 'event', $('body').attr('data-gacategory'), 'Keyfeatures_Swipe_left', $('body').attr('data-uname'));
			}
		}
		if (touchendX >= touchstartX) {
			slideId = slideId - 1;
			if ( slideId >= 0){
				var elementIdToScroll = $(this).attr("id");
				$(".keyFeatureDots").removeClass('active');
				$("#dotKeyFeaturesSlides_"+slideId).addClass('active');
				ga('send', 'event', $('body').attr('data-gacategory'), 'Keyfeatures_Swipe_right', $('body').attr('data-uname'));
			}
		}
	}
});
const viewObserve = function(entries, observer){
	entries.forEach(entry => {
		if ( entry.intersectionRatio > 0){
			const viewObj = entry.target;
			const _gatrack = $(viewObj).attr("data-view-ga");
			var _gaarray = _gatrack.split("##");
			gaEvent_track(_gaarray);
			observer.unobserve(viewObj);
		}
	});
}
function sendPageLoadGAEvent(){
	if (window.TimesGDPR  && TimesGDPR.common.consentModule.gdprCallback){
		TimesGDPR.common.consentModule.gdprCallback(function(dataObj){
			if(window.ga){
				ga('send', 'event', $('body').attr('data-gacategory'), 'PageLoad', dataObj.userCountry || 'IN', {'nonInteraction': true});
			}
		});
	}
}