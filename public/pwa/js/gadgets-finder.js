$(document).ready(function(){
	 $("body").on("click",".gadgets-brands .close, .gadgets-feature .close, .gadgets-price .close",function(e){
		 e.preventDefault();
		 $(this).closest(".gadgets-brands, .gadgets-feature, .gadgets-price").addClass("display-none");
		 $("body").removeClass("fixed-position");
	 });
	 $("body").on("click",".brands-tab, .feature-tab, .price-tab",function(e){
		 e.preventDefault();
		 var _class = $(this).attr("data-tab");
		 $(".gadgets-"+_class).removeClass("display-none");
		 $("body").addClass("fixed-position");
	 });
	 $("body").on("click",".gadgets-finder-submit",function(){
		 if($(this).hasClass("btn-color")){
			 $("form[name=gadgets-form]").submit();
		 }
	 });
	 
	 $("body").on("click",".gadgetscommon-list dd:not(.popular-brands)",function(e){
		 e.preventDefault();
		 if($(this).hasClass("active")){
			$(this).removeClass("active");
			$(this).find("input").remove();
		 }else{
			 $(this).addClass("active");
			 var _getArray = $(this).attr("data-input");
			 var _getSplit = _getArray.split("##");
			$(this).append("<input type='hidden' name="+_getSplit[0]+" value='"+_getSplit[1]+"' />");
		 }
		 _activeFormButton();
	 });
	 
});

function _activeFormButton(){
	$("#selection-result").html("");
	$(".gadgets-finder dd.active").each(function(){
		$("#selection-result").append("<span>"+$(this).text()+"</span>");
	})
	if($(".gadgets-finder dd").hasClass("active")){
		$(".gadgets-finder-submit").addClass("btn-color").removeClass("btn-color-grey");
		$(".selected-filters").removeClass("display-none");
		
	}else{
		$(".gadgets-finder-submit").removeClass("btn-color").addClass("btn-color-grey");
		$(".selected-filters").addClass("display-none");
	}
	
}