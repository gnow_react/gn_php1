<?php

use \Phalcon\Mvc\Dispatcher;
error_reporting(E_ALL & ~E_WARNING);


try {

	/**
	 * Read the configuration
	 */
	include __DIR__ . "/../app/config/UtilObject.php";
	$config = include(__DIR__."/../app/config/config.php");

	$loader = new \Phalcon\Loader();
	
	/**
	 * We're a registering a set of directories taken from the configuration file
	 */
	$loader->registerDirs(
		array(
			$config->application->controllersDir,
			$config->application->modelsDir
		)
	)->register();

	/**
	 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
	 */
	$di = new \Phalcon\DI\FactoryDefault();

	/**
	 * The URL component is used to generate all kind of urls in the application
	 */
	$di->set('url', function() use ($config) {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri($config->application->baseUri);
		return $url;
	});

	/**
	 * Setting up the view component
	 */
	$di->set('view', function() use ($config) {
		$view = new \Phalcon\Mvc\View();
		$view->setViewsDir($config->application->viewsDir);
		return $view;
	});

	/**
	 * Database connection is created based in the parameters defined in the configuration file
	 */
	$di->set('db', function() use ($config) {
		return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
			"host" => $config->database->host,
			"username" => $config->database->username,
			"password" => $config->database->password,
			"dbname" => $config->database->name
		));
	});

	/**
	 * If the configuration specify the use of metadata adapter use it or use memory otherwise
	 */
	$di->set('modelsMetadata', function() use ($config) {
		if (isset($config->models->metadata)) {
			$metadataAdapter = 'Phalcon\Mvc\Model\Metadata\\'.$config->models->metadata->adapter;
			return new $metadataAdapter();
		} else {
			return new \Phalcon\Mvc\Model\Metadata\Memory();
		}
	});

	/**
	 * Start the session the first time some component request the session service
	 */
	$di->set('session', function() {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});
	$router = function(){
		require __DIR__.'/../app/config/router.php';
		return $router;
	};
	$di->set('router', $router);	
	//echo "ControllerName: ".$router->getControllerName()."<br/>";
	//echo "ActionName: ".$router->getActionName()."<br/>";
	
	$di->set(
			'dispatcher',
			function() use ($di) {
				$eventsManager = $di->getShared('eventsManager');
				$eventsManager->attach(
						'dispatch:beforeException',
						function($event, $dispatcher, $exception) {
							switch ($exception->getCode()) {
								case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
								case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
					 error_log('404 error occurred...from:'.$_SERVER['REQUEST_URI']."--error is --:".$exception->getMessage());error_log($exception);
									$dispatcher->forward(
									array(
									'controller' => 'error',
									'action' => 'notFound',
									)
									);
									return false;
									break; // for checkstyle
								default:
									if($exception->getCode()==404){
									error_log('OOPS error occurred...from:'.$_SERVER['REQUEST_URI']."--error is --:".$exception->getMessage());
									error_log($exception);
										$dispatcher->forward(
										array(
										'controller' => 'error',
										'action' => 'notFound',
										)
										);
									}else{
										
									error_log('In ELSE  OOPS error occurred...from:'.$_SERVER['REQUEST_URI']."--error is --:".$exception->getMessage());
										header("HTTP/1.1 500 Internal Server Error");
										/*error_log('OOPS error occurred...from:'.$_SERVER['REQUEST_URI']."--error is --:".$exception->getCode()."- -".$exception->getMessage());
										error_log($exception);
										$dispatcher->forward(
										array(
										'controller' => 'error',
										'action' => 'uncaughtException',
										)
										);*/
									}
									return false;
									break; // for checkstyle
							}
						}
				);
				$dispatcher = new Dispatcher();
				$dispatcher->setEventsManager($eventsManager);
				return $dispatcher;
			},
			true
	);
	
	
	
	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application();
	$application->setDI($di);
	echo $application->handle()->getContent();
	
} catch (Phalcon\Exception $e) {
	echo $e->getMessage();
} catch (PDOException $e){
	echo $e->getMessage();
}
