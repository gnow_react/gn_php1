import { FEED_SUB_TYPE_VIDEO, FEED_SUB_TYPE_NEWS, DATA_LOADER, REQUEST_PARAMS_TYPE, FEED_SUB_TYPE_FOOTER, FEED_SUB_TYPE_HEADER, REQUEST_HOME_TYPE, FEED_SUB_TYPE_EDITOR_SHOW, FEED_SUB_TYPE_SHOP, FEED_SUB_TYPE_BANNER, FEED_SUB_TYPE_GADGETS, FEED_SUB_TYPE_BRAND, FEED_SUB_TYPE_REVIEW, REQUEST_PARAMS_FEEDSUBTYPE, FEED_SUB_TYPE_DEAL, FEED_SUB_TYPE_COMPARE, FEED_SUB_TYPE_COMPAREPDP, FEED_SUB_TYPE_SEARCH, REQUEST_PARAMS_SEARCH_EXCLUDE_PID, FEED_SUB_TYPE_GADGETS_FILTER, PAYTM_MOBILE_BRAND, PAYTM_OTHER_CAT, REQUEST_PARAMS_PATH, REQUEST_PARAMS_PAGENUM, REQUEST_PARAMS_SORT, FEED_SUB_TYPE_TOP_GADGETS_SHOW  ,FEED_SUB_TYPE_GADGETS_USER_REVIEW, FEED_SUB_TYPE_RSS,FEED_SUB_TYPE_GADGETS_SUMMARY, REQUEST_TYPE_NEWS_TN, FEED_SUB_TYPE_FAQ, REQUEST_PARAMS_FAQ_CATEGORY ,REQUEST_PARAMS_FAQ_UNAME, REQUEST_PARAMS_REQTYPE  } from "../helpers/feed-constants";
import Item from "../components/models/Item";
import Product from "../components/models/product";
import Compare from "../components/models/common/Compare"
import News from "../components/models/list/News";
import Videos from "../components/models/list/Videos";
import Footer from "../components/models/common/Footer";
import Seo from "../components/models/common/Seo";
import Products from "../components/models/list/Products";
import { getFilterDefaultSEO, getInlineFilters ,getAffiliatesDetail_lowPrice } from './GNListJsonParserHelper'
import GnJsonDataParserHelper from './GnJsonDataParserHelper'
import {category_INFO} from './../config/gn/web-catagories-config'

export default class 
 {



    constructor(requestparams, $) {
        this._$ = $;
        this._requestparams = requestparams;
    }

    getKeyWords() { 
        let jsonobject = this._$;
        let datanode = jsonobject['response']['data'];

    }


    getDetails() {
        let jsonobject = this._$;

        return this.parseDetails(jsonobject);
    }


    parseDetails(jsonobject, feedSubType, mode, isInline) {

        let lpt = 'lpt',
            title = '',
            ad,
            path ='',
            seoPath = '',
            pp,
            parenttitle,
            inlineFilters,
            totalRecord = 0,
            cp = '1',
            lp = [],
            np = '',
            id = 'id',
            dl = 'dl',
            type = 'type',
            upd = 'upt',
            kws = '',
            icon = 'icon',
            imageid = 'imageid',
            items,
            filter,
            paramCategory = '',
            category = [],
            excludePIDS = [],
            ga,
            popularData,
            recentData;
        let seoArr = [];
        let seoObject;
        let timerObject = null;

        let inlineResult = {};
        let headerObj = jsonobject['header'];
        let responseroot = null;
        let _responseroot = null;

        if (jsonobject['response'] && jsonobject['response']['params']) {
            responseroot = jsonobject['response'];
            let cacherequestObj = responseroot['params'];
            var   req = this._requestparams ;
            cp = cacherequestObj.pageno;
            pp = cacherequestObj.perpage;
            path = req[REQUEST_PARAMS_PATH];
            var reqType = req[REQUEST_PARAMS_REQTYPE] ? req[REQUEST_PARAMS_REQTYPE] : null;
            if(req[REQUEST_PARAMS_PATH]== '/PhotoNews/trending/' &&  cp == undefined)
            {
                cp = parseInt(req[REQUEST_PARAMS_PAGENUM]);
                pp = pp/cp;
            }
            if (cacherequestObj.category) {
                paramCategory = cacherequestObj.category;
            }
        }
        else if (jsonobject['header'] && jsonobject['header']['params']) {
            _responseroot = jsonobject['header'];
            let cacherequestObj = _responseroot['params'];
            var   req = this._requestparams ;
            cp = cacherequestObj.pageno;
            pp = cacherequestObj.perpage;
            path = req[REQUEST_PARAMS_PATH];
            if(req[REQUEST_PARAMS_PATH]== '/PhotoNews/trending/' &&  cp == undefined)
            {
                cp = parseInt(req[REQUEST_PARAMS_PAGENUM]);
                pp = pp/cp;
            }
            if (cacherequestObj.category) {
                paramCategory = cacherequestObj.category;
            }
        
        }
        if (jsonobject['ga']) {
            ga = jsonobject['ga'];
        }
        if (jsonobject['pgAdd']) {
            ad = jsonobject['pgAdd'];
        }

        if (jsonobject['categories']) {
            category = jsonobject['categories'];
        }
        if (!feedSubType && jsonobject['type']) {
            feedSubType = jsonobject['type'];
        }
        responseroot = jsonobject['response'];
        if (jsonobject['response']) {
            let datanode = responseroot.data;
            let metaInfo = [];
            if (null == feedSubType || typeof feedSubType == undefined) {
                feedSubType = FEED_SUB_TYPE_NEWS;
            }
            if (jsonobject['mode']) {
                mode = jsonobject['mode'];
            }
            if(responseroot.parentTitle)
            {
                parenttitle = responseroot.parentTitle;
            }
            
            console.log('-----------------------'+mode);
            if(datanode !== undefined){
                if (datanode.length == 1 && mode != FEED_SUB_TYPE_COMPAREPDP && mode != FEED_SUB_TYPE_GADGETS_FILTER && mode != FEED_SUB_TYPE_GADGETS_SUMMARY ) {
                    //console.log(datanode);
                    var objValue = datanode[0];
                    
                    if (objValue.children &&  this._requestparams[REQUEST_PARAMS_PATH] == '/PhotoNews/trending/')
                        {
                            if(objValue.children.length > pp){
                                var index =  objValue.children.length - pp;
                                objValue.children = objValue.children.slice(index);
                            }
                        }
                    
                    datanode.map((obj ) => {
                        lpt = obj.insertdate; upd = obj.updatedate; totalRecord = obj.childcount; metaInfo = obj.metainfo;
                        title = obj.title; seoPath = obj.seopath;
                        if(obj.synopsis)
                        {
                            parenttitle = obj.synopsis;
                        }
                        if(  feedSubType =='Timer' &&   metaInfo)
                        {
                            let startDate = '';
                            let endDate = '';
                            let title = '';
                            let url;
                            let suffix;
                            let category;

                            if(metaInfo.EntertainmentEpisodeStartDateTime) 
                            {
                                startDate =metaInfo.EntertainmentEpisodeStartDateTime.value;
                                //console.log ('startDate  --------'+startDate);
                            }
                            if(metaInfo.TemplateH1TopHeading) 
                            {
                                title =metaInfo.TemplateH1TopHeading.value;
                                //console.log ('startDate  --------'+title);
                            }if(metaInfo.EntertainmentEpisodeEndDateTime) 
                            {
                                endDate =metaInfo.EntertainmentEpisodeEndDateTime.value;
                                //console.log ('startDate  --------'+endDate);
                            }if(metaInfo.AlternateURL) 
                            {
                                url =metaInfo.AlternateURL.value;
                                //console.log ('startDate  --------'+url);
                            }if(metaInfo.Suffix) 
                            {
                                suffix =metaInfo.Suffix.value;
                                //console.log ('startDate  --------'+suffix);
                            }if(metaInfo.RelatedGadgets) 
                            {
                                category =metaInfo.RelatedGadgets.value;
                                //console.log ('startDate  --------'+suffix);
                            }
                            var date = new Date();
                            var timestamp = date.getTime();
                            //console.log ('startDate  --------'+timestamp);
                            let status = "SUCCESS";

                            if( timestamp>=startDate && timestamp<endDate )
                            {
                                timerObject =   {status,startDate,endDate,title,url,suffix,category};
                            }

                            else
                            {
                                let errorMsg = "Article is not in the proper time range";
                                let status = "FAILED";
                                timerObject =   {status,errorMsg};
                            }
                            //console.log ('timerObject  --------'+timerObject);

                        }
                        if (obj.children) {
                            items = obj.children.map(objChild => { return new Item(this.getDetailList(objChild, feedSubType, mode, jsonobject)) });
                        }
                    });
                }
                else if (datanode.length > 1 || ( mode == FEED_SUB_TYPE_COMPAREPDP  || mode==FEED_SUB_TYPE_GADGETS_FILTER || mode == FEED_SUB_TYPE_GADGETS_SUMMARY )) {
                    if (this._requestparams[REQUEST_PARAMS_SEARCH_EXCLUDE_PID]) {
                        var pids = this._requestparams[REQUEST_PARAMS_SEARCH_EXCLUDE_PID];
                        excludePIDS = pids.split(',');
                    }
                    let utm_campaign =""
                if(   jsonobject.utm)
                            {
                                utm_campaign = jsonobject.utm;
                        }

                    items = datanode.map(objChild => {
                        switch (mode) {

                            case FEED_SUB_TYPE_BRAND:
                                return new Item(this.getGnBrandsItem(objChild, category[paramCategory]));
                            case FEED_SUB_TYPE_GADGETS:
                                return new Product(this.getGnGadgetsItem(objChild, category,utm_campaign));
                            case FEED_SUB_TYPE_COMPARE:
                                return new Compare(this.getGnGadgetsCompare(objChild, category));
                            case FEED_SUB_TYPE_GADGETS_SUMMARY:
                                return (this.getGnGadgetSummary(objChild, paramCategory, category,utm_campaign ));
                            case FEED_SUB_TYPE_COMPAREPDP:
                            case FEED_SUB_TYPE_TOP_GADGETS_SHOW:    
                                return (this.getGnGadgetDetail(objChild, paramCategory, category,utm_campaign ));
                            case FEED_SUB_TYPE_GADGETS_FILTER:
                                return (this.getGnGadgetDetail(objChild, paramCategory, category,utm_campaign));
                            case FEED_SUB_TYPE_SEARCH:
                                return (this.getGnGadgetSearch(objChild, excludePIDS));
                            case FEED_SUB_TYPE_BANNER:
                                return (new Products(this.parseShopGadgetsBanner(inline)));
                            default:
                                return new Item(this.getDetailList(objChild, feedSubType, mode))
                        }
                    });
                        if(mode==FEED_SUB_TYPE_SEARCH)
                        {
                            for(let idx  in items)
                            {
                                if(null==items[idx])
                                {
                                    items.splice(idx,1); 

                                }
                            }
                        }

                    if (mode == FEED_SUB_TYPE_GADGETS) {
                        var subAction = /_category_/gi;
                        if(ga){
                            var action = ga.action;
                            if (action && action.match(subAction)) {
                                var newAction = action.replace(subAction, "_" + paramCategory + "_");
                                ga.action = newAction;
                            }
                        }
                    }
                }
            }


         if( this._requestparams &&   this._requestparams['feedsubtype'] && this._requestparams['feedsubtype'] == REQUEST_TYPE_NEWS_TN)
         {
            let dataContent =   jsonobject[DATA_LOADER.CONTENT];
            if( items && items.length>0 && dataContent.mode != 'multiMedia' && dataContent.mode != 'trendingWidget' && dataContent.mode != 'slideShow' )
             {
                 
                for(var index =0 ; index<items.length ; index++ )
                    {    
                     let url = items[index].wu;
                     if(items[index].wu && url.indexOf('/slideshows/')>0 || url.indexOf('/photolist/')>0 )
                     {
                         items.splice(index,1); 
                         index = index-1;
                     }
                 }  
             }
         }

            //append advertisement in items
            if (items && items.length > 0 && jsonobject['advertisement']) {
                let advertisement = jsonobject['advertisement'];
                if(this._requestparams['feedsubtype'] == 'photo'){
                    if(this._requestparams['old'] == '1'){
                        advertisement.map((ad) => { items.splice(ad.pos, 0, ad); });
                    }
                } else {
                    if(this._requestparams['showmeta'] == '1'){

                    } else {
                        advertisement.map((ad) => { items.splice(ad.pos, 0, ad); });
                    }
                }
            }


            //handel ga 
            if(mode==FEED_SUB_TYPE_COMPAREPDP)
            {
                let prodUnames = [];
                for(var index in  items)
                {
                    let uname = items[index].productUName;
                if(prodUnames.indexOf(uname)>=0)
                {
                 //delete items[index]; 
                 items.splice(index,1); 
                }
                else
                {
                    prodUnames.push(uname);
                }
                }
            }


            for (var key in metaInfo) {
                seoArr.push([key, metaInfo[key].value]);
            }
            if (mode == FEED_SUB_TYPE_GADGETS_FILTER && responseroot.filtersData !== undefined) {
                let gadgetfacetnodes = responseroot.filtersData.gadgetfacetnodes;
                gadgetfacetnodes.splice(0, 0, jsonobject['SortObject']);
                responseroot.filtersData.gadgetfacetnodes = gadgetfacetnodes;
                filter = responseroot.filtersData;
                totalRecord = responseroot.filtersData.numfound;
            }
            if (totalRecord > pp * cp) {
                np = cp + 1;
            }
            if (totalRecord > 1) {
                let arrP = [];
                for (let i = cp - 5; i <= cp + 10; i++) {
                    if (i > 0 && arrP.length <= 10 && i <= Math.ceil(totalRecord / pp)) {
                        arrP.push(i);
                    }
                } 
                lp = arrP;
            }
            if (mode == FEED_SUB_TYPE_GADGETS_FILTER && responseroot.filtersData) {
                filter = responseroot.filtersData;

            }
        }
        else if (jsonobject.type == FEED_SUB_TYPE_SHOP || jsonobject.type == FEED_SUB_TYPE_DEAL) {
            
                let product = jsonobject.data;
                let utmParam = jsonobject.utm;
                items = new Products(this.parseShopGadgets(product, null, jsonobject.type, utmParam));
                if (jsonobject.type == FEED_SUB_TYPE_DEAL) {
                    let index = jsonobject.offset;
                    let limit = jsonobject.offset + jsonobject.limit;
                    let data = items.data.items;
                    if (limit >= data.length) {
                        limit = data.length - 1;
                    }
                    data = data.slice(index, limit);
                    items.data.items = data;
                }
        }
        else if(  jsonobject.type ==  FEED_SUB_TYPE_GADGETS_USER_REVIEW )
         {
            items  = this.parseGadgetsUserReview(jsonobject);
         }
         else if(  jsonobject.type ==  FEED_SUB_TYPE_FAQ )
         {
             var faqCategory = this._requestparams[REQUEST_PARAMS_FAQ_CATEGORY];
            items  = this.getFaqGadgets(jsonobject,faqCategory);
         }
         else if(jsonobject.type ==  FEED_SUB_TYPE_BANNER)
         {
            var   req = this._requestparams ;
            var bannerName = 'gadgetsHP';
            var country = '';
                if(req['bannerName'])
                {
                    bannerName = req['bannerName'];
                }
                if(req['country'])
                {
                    country = req['country'];
                }
            items = (new Products(this.parseShopGadgetsBanner(jsonobject,bannerName,country)));

         }
                 if (jsonobject['SEO_DATA']) {
            let seoData = jsonobject['SEO_DATA'];
            let itemLength = 0;
            if(items && items.length)
            {
                itemLength = items.length;
            }
            seoObject = new Seo(this.getSeoInfo(seoData ,jsonobject['categoriesBrandSeo'] ,category_INFO, itemLength));

        } else if(mode != FEED_SUB_TYPE_GADGETS_FILTER) {
            seoObject = new Seo(getFilterDefaultSEO(this._requestparams));
        }
        if (mode == FEED_SUB_TYPE_GADGETS_FILTER) {

            let filterSeoObj = new Seo(getFilterDefaultSEO(this._requestparams, jsonobject['categoriesBrandSeo'] ,category_INFO,items.length ));
             if(  filterSeoObj && seoObject)
             {

            if(  !seoObject.hasOwnProperty('metaDescription') || ( seoObject.metaDescription).length <1)
            {
                seoObject.metaDescription = filterSeoObj.metaDescription;
            }
            if(  !seoObject.hasOwnProperty('h1Title') ||  (seoObject.h1Title).length < 1)
            {
                seoObject.h1Title = filterSeoObj.h1Title;
            }if(  !seoObject.hasOwnProperty('altTitle') ||  (seoObject.altTitle).length <1)
            {
                seoObject.altTitle = filterSeoObj.metaTitle;
                
            }
            if(filterSeoObj.metaTitle &&  (filterSeoObj.metaTitle).length >6   && ( seoObject.hasOwnProperty('altTitle') ||  (seoObject.altTitle).length >6))
            {
                seoObject.metaTitle =  filterSeoObj.metaTitle;
            }
            if( seoObject.hasOwnProperty('altTitle') ||  (seoObject.altTitle).length >6)
            {
                seoObject.metaTitle =  seoObject.altTitle;
            }
            seoObject.metaIndex = filterSeoObj.metaIndex;
             }
            inlineFilters = getInlineFilters(this._requestparams, filter);
            inlineFilters = Object.assign(inlineFilters, { 'Seofilter': jsonobject['Seofilter'] });
            let bottomfilterKey = 'other';
            if (paramCategory == 'mobile' || paramCategory == 'laptop') {
                bottomfilterKey = paramCategory;
            }

            let filterParam = this._requestparams['filterparam']
            let filterParamKey = null ;

            for(var index in  filterParam)
            {
                let keyParam = filterParam[index];
                let value = this._requestparams[keyParam];
                let tmp = {[keyParam] : value};
                if(null== filterParamKey)
                {
                    filterParamKey = tmp;
                }
                else 
                filterParamKey=Object.assign(filterParamKey,tmp); 
            }    
            if(typeof jsonobject['bottomfilter'] == 'undefined'){
                inlineFilters = Object.assign(inlineFilters ,{'currentFilterParam':  filterParamKey});
            } else {
                inlineFilters = Object.assign(inlineFilters, { 'Bottomfilter': jsonobject['bottomfilter'][bottomfilterKey] } ,{'currentFilterParam':  filterParamKey});
            }

        }
        let inLineArray = jsonobject[DATA_LOADER.INLINE_CONTENT];
        for (let index in inLineArray) {
            let inline = JSON.parse(inLineArray[index]);
            if (!inline || !inline[DATA_LOADER.CONTENT]) {
                continue;
            }

            if (inline[DATA_LOADER.CONTENT].advertisement) {
                inline['advertisement'] = inline[DATA_LOADER.CONTENT].advertisement;
            }
            let content_type = inline[DATA_LOADER.CONTENT][REQUEST_PARAMS_TYPE];
            let inlineFeedObject = null;
            if(category && !inline.categories && content_type == FEED_SUB_TYPE_COMPARE){
                inline['categories'] = category;
            }
            switch (content_type) {
                case FEED_SUB_TYPE_DEAL:
                    inlineFeedObject = new Products(this.parseShopGadgets(inline, FEED_SUB_TYPE_DEAL, inline[DATA_LOADER.CONTENT].mode, inline[DATA_LOADER.CONTENT].utm));
                break;
                case FEED_SUB_TYPE_FOOTER:
                    inlineFeedObject = new Footer(this.parseDetails(inline, FEED_SUB_TYPE_FOOTER, inline[DATA_LOADER.CONTENT].mode, true));
                    break;
                case FEED_SUB_TYPE_VIDEO:
                    inlineFeedObject = new Videos(this.parseDetails(inline, FEED_SUB_TYPE_VIDEO, inline[DATA_LOADER.CONTENT].mode, true));
                    break;
                case FEED_SUB_TYPE_NEWS:
                    inlineFeedObject = new News(this.parseDetails(inline, FEED_SUB_TYPE_NEWS, inline[DATA_LOADER.CONTENT].mode, true));
                    break;
                case FEED_SUB_TYPE_EDITOR_SHOW:
                    inlineFeedObject = new News(this.parseDetails(inline, FEED_SUB_TYPE_EDITOR_SHOW, inline[DATA_LOADER.CONTENT].mode, true));
                    break;
                case FEED_SUB_TYPE_REVIEW:
                    inlineFeedObject = new News(this.parseDetails(inline, FEED_SUB_TYPE_REVIEW, inline[DATA_LOADER.CONTENT].mode, true));
                    break;
                case FEED_SUB_TYPE_SHOP:
                    inlineFeedObject = new Products(this.parseShopGadgets(inline));
                    break;
                case FEED_SUB_TYPE_BANNER:
                    inlineFeedObject = new Products(this.parseShopGadgetsBanner(inline));
                    break;
                case FEED_SUB_TYPE_GADGETS:
                    {
                        inlineFeedObject = new Products(this.parseShopGadgets(inline, category, FEED_SUB_TYPE_GADGETS));
                        var gaGadgets = inline[DATA_LOADER.CONTENT].ga;
                        var categoryName = inline[DATA_LOADER.CONTENT].category;
                        var subAction = /_category_/gi;
                        if(gaGadgets){
                            var action = gaGadgets.action;
                            if (action && action.match(subAction)) {
                                var newAction = action.replace(subAction, "_" + categoryName + "_");
                                gaGadgets.action = newAction;
                                inline[DATA_LOADER.CONTENT].ga = gaGadgets;
                            }
                        }
                        break;
                    }
                case FEED_SUB_TYPE_BRAND:
                    inlineFeedObject = new Products(this.parseShopGadgets(inline, category, FEED_SUB_TYPE_BRAND));
                    break;
                default:
                    inlineFeedObject = new News(this.parseDetails(inline, content_type, inline[DATA_LOADER.CONTENT].mode, true));

                    break;
            }
            inlineFeedObject.pos = inline[DATA_LOADER.CONTENT].pos;
            inlineFeedObject.path = inline[DATA_LOADER.CONTENT].pathParam;
            inlineFeedObject.ga = inline[DATA_LOADER.CONTENT].ga;
            inlineFeedObject.utm = inline[DATA_LOADER.CONTENT].utm;
            inlineFeedObject.sectionName = inline[DATA_LOADER.CONTENT].sectionName;
            inlineFeedObject.type = inline[DATA_LOADER.CONTENT][REQUEST_PARAMS_TYPE];
            if(inline[DATA_LOADER.CONTENT].sectionName === 'mediawire_blog')
            {
                let data = inlineFeedObject.data.items;
                data.forEach(element => {
                    element.wu = element.wu+'?'+inline[DATA_LOADER.CONTENT].utm;
                });
                inlineFeedObject.data.items=data;

            }
            if (inline[DATA_LOADER.CONTENT].sectionName) {
                let obj = {};
                obj[inline[DATA_LOADER.CONTENT].sectionName] = inlineFeedObject;
                inlineResult = Object.assign(inlineResult, obj);
            }
            else {
                inlineResult = Object.assign(inlineFeedObject);
            }
        }
        if(jsonobject['type'] == 'SEO'){
            if(jsonobject['response']){
                //seoArr = jsonobject['response']['data'][0];
                let itemSize = 0;
                if(items && items.length >0)
                {
                    itemSize = items.length;
                }
                seoArr = new Seo(this.getSeoInfo(JSON.stringify(jsonobject),null,null,itemSize));
            }
            category = null;
            ad = null;
            seoObject = null;
        }
        if(this._requestparams['showmeta'] == '1'){
            category = null;
            ad = null;
            seoObject = null;
            ga = null;
        }
        if(jsonobject.content && jsonobject.content.sectionName && jsonobject.content.sectionName == 'COMPARISION' && items && items.length >0){
            popularData = jsonobject.pcData;
            recentData = items;
            items = null;
            category = null;
            ad = null;
            ga = null;
       
        }
        if(jsonobject.content && jsonobject.content.sectionName && jsonobject.content.sectionName == 'Trending Searches' &&  jsonobject.content.pinnedItem ){
            items.splice(0, 0, jsonobject.content.pinnedItem );
        }
        if (isInline) {
            return Object.assign({}, { cp, pp, np, lp, title, type, id, imageid, items, inlineResult, ga });
        }
       if(reqType == 'seoDetails')
                {
                    return Object.assign({},{seoObject});
                }
        else {
            return Object.assign({}, {
                upd,timerObject, lpt, cp, pp, np, lp, title, seoPath, seoArr, dl, type,path,parenttitle,
                id, kws, imageid, icon, items, filter, inlineResult, ga, ad, category, mode, seoObject, inlineFilters,popularData,recentData
            });
        }
    }

    getDetailList(obj, feedSubType, mode,jsonobject) {


        if(mode == 'multiMedia')
        {
            var feedType =  obj.cmstype
            switch (feedType) {
                    case 'MEDIAVIDEO':
                        {
                        mode = 'videoDetail';
                        break;
                        }
                    case 'PHOTOGALLERYSLIDESHOWSECTION':
                        {
                            mode = 'slideShow';  
                            break
                        }   
                     default:
                          mode= 'article';        
            }


        }

        switch (mode) {
            case 'summary':
                return this.getshortItem(obj, feedSubType);
            case 'medium':
                return this.getmediumItem(obj, feedSubType);
            case 'nav':
                return this.getNavItem(obj, feedSubType);
            case 'video':
                return this.getvideoItem(obj, feedSubType);
            case 'videoDetail':
                return this.getvideoDeatailItem(obj, feedSubType);
            case 'slideShow':
            case 'webStory':
                return this.getSlideItem(obj, feedSubType, mode);
            case 'gadgets':
                return this.getGnGadgetsItem(obj, feedSubType);
            case 'topGadgetshow':  
                return this.gettopGadgetshowItem(obj, feedSubType,jsonobject);
            
            default:
                return this.getDetailItem(obj, feedSubType);
        }

    }

    /**
     * parse Item get the full detail
     * @param {contain Item Detail} obj 
     * @param {type of the article} feedSubType 
     */
    getDetailItem(obj, feedSubType) {

        let lpt = 'lpt',
            id = 'id',
            dl = 'dl',
            type = 'type',
            dm = 'dm',
            hl = 'hl',
            keywords = '',
            summary = '',
            sl='',
            status = 'INACTIVE',
            parenttitle = '',
            upd = 'upd',
            kws = '',
            hasVideo = 'false',
            icon = 'icon',
            imageid = 'imageid',
            wu = 'wu',
            imageSize = '12345';
            let isReviewd = false;
        let newsObj = obj;
        {
            dl = newsObj.insertdate; upd = newsObj.updatedate; parenttitle = newsObj.parenttitle, id = newsObj.msid; type = newsObj.cmstype; dm = 'gadgetsnow';
            hl = obj.title; summary = newsObj.synopsis; status = newsObj.status; hasVideo = 'false';
            if (newsObj.thumbsize && newsObj.thumbsize !== '') {
                imageSize = newsObj.thumbsize;
            }
            if (newsObj.metainfo && newsObj.metainfo.LastPublishMilliTime) {
                lpt = newsObj.metainfo.LastPublishMilliTime.value;
            }
            if(newsObj.metainfo && newsObj.metainfo.GadgetsMarkedReviewed)
            {
                if(newsObj.metainfo.GadgetsMarkedReviewed.value === "1")
                {
                    isReviewd=true;
                }
            }
            imageid = 'imgsize=' + imageSize;
            if (feedSubType == FEED_SUB_TYPE_NEWS || feedSubType == FEED_SUB_TYPE_FOOTER || feedSubType == FEED_SUB_TYPE_HEADER) {
                wu = newsObj.seopath + '/articleshow/' + id + '.cms';
                if(isReviewd)
                {
                    var splittedSeo = newsObj.seopath.split("/");
                    wu = 'reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + id + '.cms';
                   
                }
            }
            else if (feedSubType == FEED_SUB_TYPE_VIDEO) {
                wu = newsObj.seopath + '/videoshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_EDITOR_SHOW) {
                wu = newsObj.seopath + '/editorschoiceshow/' + id + '.cms';
            }
            sl=newsObj.seopath;
            if(isReviewd)
            {
                var splittedSeo = newsObj.seopath.split("/");
                sl =  'reviews/' + splittedSeo[splittedSeo.length - 1];
            }
            wu = 'https://www.gadgetsnow.com/' + wu;
            if (newsObj.overridelink) {
                wu = newsObj.overridelink;
            }
            
           
            let keyWrd = newsObj.keywords;
            let seoKeyWords = [];
            //console.log(newsObj.keywords);
            if (newsObj.keywords) {
                seoKeyWords = keyWrd.map((keys) => { return keys.name });
                keywords = seoKeyWords.join(',');

                //console.log(seoKeyWords);
            }

        };

        //console.log(obj);

        return Object.assign({}, {
            upd,
            lpt,
            dl,
            sl,
            id,
            type,
            dm,
            hl,
            summary,
            status,
            hasVideo,
            imageid,
            imageSize,
            wu,
            keywords,
            parenttitle
        });
    }
    /**
     * Prse Item get the short detail
     * Heading , URL , ID
     * @param {contain Item Detail} obj 
     * @param {type of the article} feedSubType 
     */
    getshortItem(obj, feedSubType) {
        let hl = 'hl',
            id = 'id',
            wu = 'wu',
            isReviewd = false,
            sl = 'sl';
        let newsObj = obj;
        {
            hl = obj.title;
            id = newsObj.msid
            
            if(newsObj.metainfo && newsObj.metainfo.GadgetsMarkedReviewed)
            {
                if(newsObj.metainfo.GadgetsMarkedReviewed.value === "1")
                {
                    isReviewd=true;
                }
            }
            
            if (feedSubType == FEED_SUB_TYPE_NEWS || feedSubType == FEED_SUB_TYPE_FOOTER || feedSubType == FEED_SUB_TYPE_HEADER) {
                wu = newsObj.seopath + '/articleshow/' + id + '.cms';
                if(isReviewd)
                {
                    var splittedSeo = newsObj.seopath.split("/");
                    wu = 'reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + id + '.cms';
                   
                }
            }
            else if (feedSubType == FEED_SUB_TYPE_VIDEO) {
                wu = newsObj.seopath + '/videoshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_EDITOR_SHOW) {
                wu = newsObj.seopath + '/editorschoiceshow/' + id + '.cms';
            }
            wu = 'https://www.gadgetsnow.com/' + wu;
            if (newsObj.overridelink) {
                wu = newsObj.overridelink;
            }

            sl = newsObj.seopath;
        };


        return Object.assign({}, { hl, id, wu,sl });
    }

    /**
     * Prse Item get the sme detail
     * Heading , URL , ID , ImageSize , type, ParentType
     * @param {contain Item Detail} obj 
     * @param {type of the article} feedSubType 
     */
    getmediumItem(obj, feedSubType) {

        let id = 'id',
            type = 'type',
            hl = 'hl',
            sl = '',
            imageid = 'imageid',
            wu = 'wu',
            parenttitle,
            imageSize = '12345',
            authorName,
            criticRating,
            allMetaInfo,
            isReviewd = false,
            upd = '000';

        let newsObj = obj;
        {
            id = newsObj.msid;
            type = newsObj.cmstype;
            hl = obj.title;
            upd = newsObj.updatedate
            if (newsObj.thumbsize && newsObj.thumbsize !== '') {
                imageSize = newsObj.thumbsize;
            }

            if(newsObj.metainfo && newsObj.metainfo.GadgetsMarkedReviewed)
            {
                if(newsObj.metainfo.GadgetsMarkedReviewed.value === "1")
                {
                    isReviewd=true;
                }
            }
            imageid = 'imgsize=' + imageSize;
            if (feedSubType == FEED_SUB_TYPE_NEWS || feedSubType == FEED_SUB_TYPE_FOOTER || feedSubType == FEED_SUB_TYPE_HEADER) {
                wu = newsObj.seopath + '/articleshow/' + id + '.cms';
                if(isReviewd)
                {
                    var splittedSeo = newsObj.seopath.split("/");
                    wu = 'reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + id + '.cms';
                   
                }
            }
            else if (feedSubType == FEED_SUB_TYPE_VIDEO) {
                wu = newsObj.seopath + '/videoshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_EDITOR_SHOW) {
                wu = newsObj.seopath + '/editorschoiceshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_EDITOR_SHOW) {
                wu = newsObj.seopath + '/editorschoiceshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_REVIEW  ) {
                var splittedSeo = newsObj.seopath.split("/");
                wu = 'reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + id + '.cms';

                if (newsObj.authors) {
                    var authorId = newsObj.authors[0].id;
                    authorName = newsObj.authors[0].name;
                }    
            }
            if (newsObj.metainfo && newsObj.metainfo.CriticRating) {
                criticRating = newsObj.metainfo.CriticRating.value;
            }
            if(feedSubType == FEED_SUB_TYPE_VIDEO){
                wu = '/' + wu;
            } else {
                wu = 'https://www.gadgetsnow.com/' + wu;
            }
            sl = newsObj.seopath;

            if (newsObj.overridelink) {
                wu = newsObj.overridelink;
                if(wu.includes('/articleshow/')){
                    type = 'ARTICLE';
                } else if('/photolist/'){
                    type = 'PHOTOGALLERYSLIDESHOWSECTION';
                } else if('/videoshow/'){
                    type = 'MEDIAVIDEO';
                }
            }
            parenttitle = newsObj.parenttitle;

            if (newsObj.metainfo && this._requestparams['showmeta'] == '1') {
                allMetaInfo = {};
                Object.entries(newsObj.metainfo).forEach(function(item){
                    allMetaInfo[item[0]] = item[1].value;
                })
            }
        };

        //console.log(obj);

        return Object.assign({}, { type,sl, upd, hl, id, imageSize, parenttitle, imageid, wu, criticRating, authorName, allMetaInfo });
    }

    /**
     * Parse Item get the  detail for Navigation
     * Heading , URL , ID , ImageSize 
     * @param {contain Item Detail} obj 
     * @param {type of the article} feedSubType 
     */

    getNavItem(obj, feedSubType) {
        let id = 'id',
            imageSize = '12345';
        let navItem = this.getshortItem(obj, feedSubType);
        let newsObj = obj;
        {
            if (newsObj.thumbsize && newsObj.thumbsize !== '') {
                imageSize = newsObj.thumbsize;
            }
            imageid = 'imgsize=' + imageSize;

        }
        return Object.assign(navItem, { imageid, imageSize });
    }

    /**
     * Parse Item get the  detail for the slide  
     * Heading , URL , ID , ImageSize , type, ParentType
     * @param {contain Item Detail} obj 
     * @param {type of the article} feedSubType 
     */
    getSlideItem(obj, feedSubType, mode) {
        let slideCount, wu, slideid , summary, thumbimageid, wss;
        let slideItem = this.getmediumItem(obj, feedSubType)
        {
            if (obj.children && obj.children.length > 0) {    
                slideCount = obj.childcount
                let childObject = obj.children[0]
                if(slideItem.imageSize == '12345'){
                    thumbimageid = childObject.msid;
                    slideItem.imageSize = childObject.thumbsize;
                }
                slideid = childObject.msid;
                summary = childObject.synopsis;
                let urlMsid = obj.msid;
                let seopath = obj.seopath;
                if ((obj.metainfo && obj.metainfo.TemplateLayout && obj.metainfo.TemplateLayout.value == 'Layout1') || this._requestparams['old'] == '1') {
                    urlMsid = childObject.msid;
                    seopath = childObject.seopath;
                }
                wu = seopath + '/photolist/' + urlMsid + '.cms';
                if(this._requestparams['old'] == '1'){
                    wu = 'https://www.gadgetsnow.com/' + wu;
                } else {
                    wu = '/' + wu;
                }
                if (childObject.overridelink) {
                    wu = childObject.overridelink;
                }
                if(mode == 'webStory'){
                    wss = 'Gaming';
                    if(childObject.metainfo && childObject.metainfo.Generic && childObject.metainfo.Generic.value){
                        wss = childObject.metainfo.Generic.value;
                    }
                }
            }

        };



        return Object.assign(slideItem, { slideCount, wu, slideid, thumbimageid, wss });
    }



    /**
     * Parse Item get the  detail for the video  
     * Heading , URL , ID , ImageSize , type, ParentType
     * @param {contain Item Detail} obj 
     * @param {type of the article} feedSubType 
     */
    getvideoItem(obj, feedSubType) {
        let duration = 0;
        let videoItem = this.getmediumItem(obj, feedSubType)
        {
            if (obj.media && obj.media.durationms) {
                duration = obj.media.durationms;
            }
        };
        return Object.assign(videoItem, { duration });
    }

    getvideoDeatailItem(obj, feedSubType) {
        let summary = 0;
        let slikeId = '';
        let videoItem = this.getvideoItem(obj, FEED_SUB_TYPE_VIDEO)
        {
            if (obj.synopsis) {
                summary = obj.synopsis;
            }
            if (obj.media && obj.media.id) {
                slikeId = obj.media.id;
            }
        };
        return Object.assign(videoItem, { summary, slikeId });
    }

    parseShopGadgets(jsonobject, category, mode, utmParam) {
        let ga, utm, items = [], paramCategory;
        if (jsonobject) {
            switch (mode) {
                case FEED_SUB_TYPE_GADGETS:
                    {

                        let headerObj = jsonobject['header'];
                        if (jsonobject['response']) {
                            if (jsonobject['response'] && jsonobject['response']['params']) {
                                if (jsonobject['response']['params'].category) {
                                    paramCategory = jsonobject['response']['params'].category;
                                }
                            }
                            else if (jsonobject['header']['params'] && jsonobject['header']['params'].category)
                            {
                                paramCategory = jsonobject['header']['params'].category;
                            }
                            let datanode = (jsonobject['response']).data;
                            for (let index in datanode) {
                                items.push(new Product(this.getGnGadgetsItem(datanode[index], category)));
                            }
                        }
                        break;
                    }
                case FEED_SUB_TYPE_BRAND:
                    {
                        let paramCategory = '';
                        let headerObj = jsonobject['header'];
                        if (jsonobject['response']) {
                            if (jsonobject['response'] && jsonobject['response']['params']) {
                                if (jsonobject['response']['params'].category) {
                                    paramCategory = jsonobject['response']['params'].category;
                                }
                            }else if (jsonobject['header']['params'] && jsonobject['header']['params'].category)
                            {
                                paramCategory = jsonobject['header']['params'].category;
                            }
                            let datanode = (jsonobject['response']).data;
                            for (let index in datanode) {
                                items.push(new Item(this.getGnBrandsItem(datanode[index], category[paramCategory])));
                            }
                        }
                        break;
                    }
                case FEED_SUB_TYPE_DEAL:
                    {
                        let data = jsonobject.product;
                        for (let index in data) {
                            items.push(this.getDealGadgets(data[index], utmParam));
                        }
                        break;
                    }
                default:
                    {
                        for (let index in jsonobject) {
                            items.push(new Product(this.getGadgets(jsonobject[index], utmParam)));
                        }
                    }
            }

            if (jsonobject['ga']) {
                ga = jsonobject['ga'];
                var re = /_category_/gi;
                var action = ga.action;
                if (action && action.match(re)) {
                    var newsAction = action.replace(re, "_" + paramCategory + "_");
                    ga.action = newsAction;
                }




            }
            if (jsonobject['utm']) {
                utm = jsonobject['utm'];
            }

            if (items && items.length > 0 && jsonobject['advertisement']) {
                let advertisement = jsonobject['advertisement'];
                advertisement.map((ad) => { items.splice(ad.pos, 0, ad); });
            }
        }

        return Object.assign({}, { items, ga, utm });

    }


    /**
     * Parse Item get the  detail for the slide  
     * Heading , URL , ID , ImageSize , type, ParentType
     * @param {contain Item Detail} obj 
     * @param {type of the article} feedSubType 
     */
    parseShopGadgetsBanner(jsonobject,bannerName ,countrynm) {
        let ga, utm, items = [];

        if(jsonobject.data)
        {
           let dataContent =   jsonobject[DATA_LOADER.CONTENT];

            jsonobject = jsonobject.data;
            jsonobject[DATA_LOADER.CONTENT] = dataContent;

        }
       else if(jsonobject[DATA_LOADER.CONTENT] && jsonobject[DATA_LOADER.CONTENT]['bannerName'])
        {
         bannerName = jsonobject[DATA_LOADER.CONTENT]['bannerName'];
        }
        let bannerLink = '';
        if (jsonobject && !jsonobject[bannerName] &&jsonobject['IN'] ) {

            console.log('-------change data--');
             let dataContent =   jsonobject[DATA_LOADER.CONTENT];
            jsonobject =  JSON.parse( jsonobject['IN']);
            jsonobject[DATA_LOADER.CONTENT] = dataContent;
            countrynm = 'IN';

        }
            if (jsonobject && jsonobject[bannerName]) {
            for (let index in jsonobject[bannerName]) {
                let banner = jsonobject[bannerName][index];

                if (banner.active == 'Y') {
                    let _affurl = banner.mbannerurl.trim();
                    let _pgname ='GadgetsNow_Mweb_Home_Banner'
                    if( jsonobject[DATA_LOADER.CONTENT] && jsonobject[DATA_LOADER.CONTENT]['pgname'])
                    {
                        _pgname = jsonobject[DATA_LOADER.CONTENT]['pgname'];
                    }
                    try {
                        const url = new URL(_affurl);
                        let origin = url.origin;
                        let pageTagName = 'Home'
                        var __tag = '';


                            //////

                    //         if ( (_pgname == 'compare' && feedPgName=='compare') ||
                    //         (_pgname == 'Gadgets' && feedPgName=='gadgetlist') ||
                    //         (_pgname == 'gadgetshow' && feedPgName=='gadgetshow') ||
                    //         (_pgname == 'editorschoiceshow' && feedPgName=='topgadgets') ||
                    //         (_pgname == 'slideshows' && feedPgName=='photolist') ||
                    //         (_pgname == 'stories' && feedPgName=='gadgetsHP') ||
                    //         (_pgname == 'Home' && feedPgName=='gadgetsHP')
                    //    )
                    {
                           let paytm_utm = 'GadgetsNow_Mweb_';
                           switch (bannerName){
                               case 'compare':
                                   paytm_utm += 'Compare_Banner';
                                   pageTagName = 'compare';
                                   break;
                                case 'newsreviews':
                                    paytm_utm += 'ArticlelIst_Banner';
                                    pageTagName = 'stories';
                                    break;
                               case 'gadgetlist':
                                   paytm_utm += 'GadgetListing_Banner';
                                   pageTagName = 'Gadgets';
                                   break;
                               case 'gadgetshow':
                                   paytm_utm += 'Gadgetshow_Banner';
                                   pageTagName = 'gadgetshow';
                                   break;
                               case 'topgadgets':
                                   paytm_utm += 'TopGagdets_Banner';
                                   pageTagName = 'editorschoiceshow';
                                   break;
                               case 'photolist':
                                   paytm_utm += 'Photolist_Banner';
                                   pageTagName = 'slideshows';
                                   break;
                               case 'stories':
                                   paytm_utm += 'Articlelist_Banner';
                                   break;
                               case 'gadgetsHP':
                                   paytm_utm += 'Home_Banner';
                                   pageTagName = 'Home';
                                   break;
                           }

                           _pgname = paytm_utm;
                           if(_affurl.indexOf('amazon.') > -1 ) {
                             __tag = "mweb_"+ pageTagName.toLowerCase() +"_mrec-21";
                            if(countrynm && countrynm == 'US') {
                                __tag = 'mweb_gadgetsnow_us-20';
                            }
                            if( countrynm && countrynm == 'CA') {
                                __tag = 'mweb_gadgetsnow_canada-20';
                            }
                            if(countrynm && countrynm == 'GB') {
                                __tag = 'mweb_gadgetsnow_uk-21';
                            }
                        }
                    }
                            /////


                        let relativeUrl=_affurl;
                        if(_affurl.indexOf('flipkart.com') == -1)
                        {
                         relativeUrl = _affurl.slice(origin.length);
                         relativeUrl = encodeURIComponent(relativeUrl);
                        }
                        let price = 'banner';
                        let utm_campaign = 'mweb_home_mrec-21';
                        if(   jsonobject[DATA_LOADER.CONTENT]['utm_campaign'] )
                        {
                            utm_campaign = jsonobject[DATA_LOADER.CONTENT]['utm_campaign'];
                        }
                        if (__tag && __tag!=='')
                        {
                            utm_campaign = __tag;
                        }
                        let title = banner.bannertext;
                        let utmStr = 'utm_source=gnwap&utm_medium=Affiliate&utm_campaign=' + utm_campaign

                        if (_affurl.indexOf('amazon.') > -1) {
                            bannerLink = "/affiliate_amazon.cms?url=" + relativeUrl + "&price=" + price + "&title=" + title +
                                "&" + utmStr;
                        }
                        else if (_affurl.indexOf('tatacliq.com') > -1) {
                            bannerLink = "/affiliate_tatacliq.cms?url=" + relativeUrl + "&price=" + price + "&title=" + title +
                                "&" + utmStr;
                        }
                        else if (_affurl.indexOf('flipkart.com') > -1) {
                           let tag='flipkart_mweb_gn_'+_pgname;
                           relativeUrl =relativeUrl+'&affid=tilltd'+"&affExtParam2="+ tag;
                            relativeUrl = encodeURIComponent(relativeUrl);
                        
                            bannerLink = "https://shop.gadgetsnow.com/affiliate.cms?url=" + relativeUrl +"&tag="+tag+"&afname=flipkart&affiliate=flipkart&"+"&price=" + price + "&title=" + title +
                                "&" + utmStr;
                        }else if(title === 'Norton')
                        {
                            let tag="norton_mweb_gn_"+_pgname+"&afname=norton&utm_medium=Affiliate";
                            relativeUrl=encodeURIComponent(_affurl);
                            bannerLink = "https://shop.gadgetsnow.com/affiliate.cms?url="+relativeUrl+"&tag="+tag;
                        }
                        else {
                            let tag=title+"_mweb_gn_"+_pgname+"&afname="+title+"&utm_medium=Affiliate";
                            relativeUrl=encodeURIComponent(_affurl);
                            bannerLink = "https://shop.gadgetsnow.com/affiliate.cms?url="+relativeUrl+"&tag="+tag;
                  
                        }
                    }
                    catch (e) {
                        // Code to run if an exception occurs
                        //console.log(e);
                    }
                }
                banner = Object.assign(banner, { 'bannerLink': bannerLink });
                items.push(banner);
            }
            //  jsonobject.map((obj) => {//console.log(obj) });
            //handel ga 

            if (jsonobject['ga']) {
                ga = jsonobject['ga'];
            }
            if (jsonobject['utm']) {
                utm = jsonobject['utm'];
            }
        }

        return Object.assign({}, { items, ga, utm });

    }

    parseGadgetsUserReview(jsonobject) {

        let items =[];
        let averagerating=0;let totalcount=0;
        let totalratings=0,totalReviews=0;
        for (let index in jsonobject.data)
        {
            let userReviewEntity =jsonobject.data[index];
            if(userReviewEntity.C_T)
            {
                totalcount=totalcount+1;
                averagerating=averagerating+userReviewEntity.C_M_RA/2;
                let userReviewObj = {'rating':userReviewEntity.C_M_RA/2, 'text':userReviewEntity.C_T,'avgr':2 , 'userName':userReviewEntity.A_D_N, 'timeTxt':userReviewEntity.C_D};
                items.push(userReviewObj);
                if(userReviewEntity.C_T && userReviewEntity.C_T !=='')
                {
                    totalReviews=totalReviews+1; 
                }
                if(userReviewEntity.C_M_RA && userReviewEntity.C_M_RA/2)
                {
                    totalratings=totalratings+1;
                }
        }
        }
        if(totalcount>0)
            averagerating=averagerating/totalcount;
        let averageRating ={'averageRating':averagerating,'totalratings':totalratings,'totalreviews':totalReviews};
        //items.push(averageRatJson);  
        return Object.assign({}, { averageRating,items });

    }

    getGadgets(productInfo, utmParam) {
        let productId,
            productName,
            feature,
            price,
            mrp,
            discount,
            imageUrl,
            productUrl,
            featureList,
            specvalue,
            flag,
            imageUrlAll;

        if (productInfo) {
            productId = productInfo.PRODUCT_ID;
            productName = productInfo.PRODUCT_NAME;
            feature = productInfo.FEATURE;
            price = productInfo.PRICE;
            mrp = productInfo.MRP;
            discount = productInfo.DISCOUNT;
            imageUrl = productInfo.IMAGE_URL;
            productUrl = productInfo.PRODUCT_URL;
            if (utmParam) {
                productUrl = productUrl + '?' + utmParam;
            }
            featureList = productInfo.FEATURE_LIST;
            specvalue = productInfo.SPEC_VALUE
            flag = productInfo.FLAG;
            imageUrlAll = productInfo.IMAGE_URL_ALL;
        }


        return Object.assign({}, {
            productId,
            productName,
            feature,
            price,
            mrp,
            discount,
            imageUrl,
            productUrl,
            featureList,
            specvalue,
            flag,
            imageUrlAll
        });

    }

    /*getDealGadgets(productInfo, utmParam) {
        let productId,
            productName,
            price,
            listPrice,
            discount,
            cashback,
            imageUrl,
            productUrl,
            intermediateUrl,
            affiliate;

        if (productInfo) {
            productId = productInfo.pid;
            productName = productInfo.title;
            price = productInfo.price;
            listPrice = productInfo.listPrice;
            cashback = productInfo.cashback;
            imageUrl = productInfo.imageUrl;
            productUrl = productInfo.url;
            affiliate = productInfo.affiliate;
            if (productInfo.discount) {
                discount = discount;
            }
            if (utmParam && productUrl) {
                const amazonURLObj = new URL(productUrl);
                var pathName = amazonURLObj.pathname;
                var pathSearch = amazonURLObj.search;
                var urlParam = encodeURIComponent(pathName + pathSearch) + '&price=' + price + '&title=' + encodeURIComponent(productName);
                intermediateUrl = 'https://m.gadgetsnow.com/affiliate_amazon.cms?url=' + urlParam + utmParam;

            }
        }

        return Object.assign({}, {
            productId,
            productName,
            price,
            listPrice,
            cashback,
            imageUrl,
            intermediateUrl,
            if(discount) {
                discount
            },
            productUrl,
            affiliate
        });

    } */

    getDealGadgets(productInfo, utmParam) {
        let productId,
            productName,
            price = '',
            listPrice,
            callAction,
            discount,
            cashback,
            imageUrl,
            productUrl,
            promotionName,
            intermediateUrl,
            affiliate = 'amazon';

        if (productInfo) {
            if (productInfo.pid) {
                productId = productInfo.pid;
            }
            productName = productInfo.title;
            if (productInfo.price) {
                price = productInfo.price;
            } if (productInfo.listPrice) {
                listPrice = productInfo.listPrice;
            } if (productInfo.cashback) {
                cashback = productInfo.cashback;
            }
            if(productInfo.promotionName)
            {
                promotionName = productInfo.promotionName
            }
            imageUrl = productInfo.imageUrl;
            productUrl = productInfo.url;
            if (productInfo.affiliate) {
                affiliate = productInfo.affiliate;
            }
            if (productInfo.discount) {
                discount = productInfo.discount;
            }
            if (utmParam && productUrl) {
                const amazonURLObj = new URL(productUrl);
                var pathName = amazonURLObj.pathname;
                var pathSearch = amazonURLObj.search;
                var urlParam = encodeURIComponent(pathName + pathSearch) + '&price=' + price + '&title=' + encodeURIComponent(productName);
               
               

                if (productUrl.indexOf('amazon') > -1) {
                
                    intermediateUrl = 'https://www.gadgetsnow.com/affiliate_amazon.cms?url=' + urlParam + utmParam;
                
                }
                else if (productUrl.indexOf('flipkart.com') > -1) {
                    let tag='flipkart_mweb_gn_'+'PhotoShow';
                    let relativeUrl = productUrl;
                    relativeUrl =relativeUrl+'&affid=tilltd'+"&affExtParam2="+ tag;
                     relativeUrl = encodeURIComponent(relativeUrl);

                 
                     intermediateUrl = "https://shop.gadgetsnow.com/affiliate.cms?url=" + relativeUrl +"&tag="+tag+"&afname=flipkart&affiliate=flipkart&"+"&price=" + price + "&title=" + encodeURIComponent(productName) +
                         "&" + utmParam;
                 }
                else if(productName.indexOf('Norton') > -1)
                {
                    let tag="norton_mweb_gn_"+'PhotoShow'+"&afname=norton&utm_medium=Affiliate";
                   let relativeUrl=encodeURIComponent(productUrl);
                   intermediateUrl = "https://shop.gadgetsnow.com/affiliate.cms?url="+relativeUrl+"&tag="+tag;
                }
                else {
                    let tag=productName+"_mweb_gn_"+'PhotoShow'+"&afname="+productName+"&utm_medium=Affiliate";
                   let relativeUrl=encodeURIComponent(productUrl);
                   intermediateUrl = "https://shop.gadgetsnow.com/affiliate.cms?url="+relativeUrl+"&tag="+tag;
          
                }
            }
            if(productInfo.callAction)
            {
                callAction = productInfo.callAction;
            }
        }

        return Object.assign({}, {
            productId,
            productName,
            promotionName,
            price,
            listPrice,
            cashback,
            callAction,
            imageUrl,
            intermediateUrl,
            discount,
            productUrl,
            //affiliate
        });

    }
    getMsid(stry, requestparams) {
        let msid = "";
        let jsonobject = this._$;
        let msidroot = jsonobject['header'];
        //console.log('MSID---');
        if (jsonobject['header'] && msidroot['cacherequest']) {
            let temp = msidroot['cacherequest'];

            msid = temp['msid'];
        }
        return msid;
    }
    getWebUrl(p1, p2, p3) {
        return 'weburl';
    }
    getDomain(wu, requestparams) {
        return requestparams.dm;
    }
    getTemplate(p2, p3, requestparams) {
        switch (requestparams.feedtype) {
            case REQUEST_HOME_TYPE:
                return REQUEST_HOME_TYPE;
            default:
                return requestparams.feedsubtype;

        }

    }

    getSeoInfo(seoObject , categoryBranSeo , CategoryInfo , resultSize) {
        let uri,
            index = 'noindex',
            metaDescription,
            metaTitle,
            metaIndex,
            altTitle,
            h1Title,
            atfContent,
            btfContent,
            canonicalUrl,
            mUrl,
            ampUrl,
            keyws;
        let jsonobject = JSON.parse(seoObject);
        let responseroot = jsonobject['response'];
        let error = jsonobject['error'];
        if (jsonobject['response'] !== undefined) {
            let datanode = responseroot.data;
            if (datanode.length == 1) {
                datanode.map((obj) => {
                    uri = obj.URI;
                    index = "noindex";
                    metaDescription = obj['Meta-description'];
                    altTitle = obj['Alt-title'];
                    h1Title = obj['H1-title'];
                    atfContent = obj['Atf-content'];
                    btfContent = obj['Btf-content'];
                    canonicalUrl = obj['Canonical-url'];
                    mUrl = obj['m-url'];
                    ampUrl = obj['amp-url'];
                    keyws = obj['meta-keyws'];
                });
            }

            return Object.assign({}, {
                uri,
                index,
                metaDescription,
                metaTitle,
                metaIndex,
                altTitle,
                h1Title,
                atfContent,
                btfContent,
                canonicalUrl,
                mUrl,
                ampUrl,
                keyws
            });
        } else  {
            return getFilterDefaultSEO(this._requestparams, categoryBranSeo , CategoryInfo,resultSize);
        }
    }

    getGnGadgetsItem(productInfo, category, categoryValue, trim,utm_campaign) {
        let productId = '',
            productName,
            productUName,
            imageId,
            imageversion,
            price,
            mrp,
            brand,
            criticRating,
            avgrating,
            ratings,
            reviews,
            affArray = [],
            productUrl,
            reviewUrl,
            specificFeatures;

        if (productInfo) {

            productId = productInfo.pid;
            let urlName = productInfo.uname;
            productUName = urlName;
            productName = productInfo.name;
            price = productInfo.price;
            if (productInfo.imagemsid && productInfo.imagemsid.length > 0) {
                imageId = productInfo.imagemsid[0];
            }
            if (productInfo.criticrating) {
                criticRating = productInfo.criticrating;
            }
            if (productInfo.brand) {
                brand = productInfo.brand.name;
            }
            specificFeatures = this.getSpecificFeatures(productInfo);

            if (productInfo.imageversion) {
                imageversion = productInfo.imageversion;
            }
            //TODO: need to add avg rating after implementation from jcms
            if (productInfo.averagerating) {
                avgrating = (productInfo.averagerating / 2).toFixed(1);
            }
            if (productInfo.ratings) {
                ratings = productInfo.ratings.map((item) => { return { rating: (item.rating / 2), count: item.count } });
            }
            if (productInfo.reviews) {
                reviews = productInfo.reviews;
            }
            let categoryKey = productInfo.category;
            if (!categoryKey && categoryValue) {
                categoryKey = categoryValue;
            }
            if (trim) {
                if (categoryKey == 'laptop' && productName && productName.indexOf('(') > 0) {
                    productName = productName.substring(0, productName.indexOf('('));
                }
                //else if (categoryKey != 'laptop'&& productName && productName.length > 20) {
                //    productName = productName.substring(0, 20) + '...';
               // }
            }

            if (productInfo.reviews && productInfo.reviews.reviewdetail) {
                var reviewObj = productInfo.reviews.reviewdetail[0];
                var splittedSeo = reviewObj.seopath.split("/");
                reviewUrl = '/reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + productInfo.reviews.msid + '.cms';
            }
            
            let seoUrl = category[categoryKey].seo;
            productUrl = 'https://www.gadgetsnow.com/' + seoUrl + '/' + urlName;

            if (productInfo.affiliatesinfo) {
                affArray = this.getAffiliatesDetailNew(productInfo, categoryKey, brand,utm_campaign);
            }
        }




        return Object.assign({}, {
            productId,
            productName,
            imageId,
            imageversion,
            productUName,
            price,
            mrp,
            criticRating,
            avgrating,
            ratings,
            reviews,
            affArray,
            productUrl,
            reviewUrl,
            specificFeatures
        });

    }
    getGnBrandsItem(brandInfo, category) {
        let hl,
            wu,
            categoryName,
            imageid;
        if (brandInfo) {
            let urlName = brandInfo.uname;
            hl = brandInfo.name;
            categoryName = category.caps
            if (brandInfo.imagemsid) {
                imageid = brandInfo.imagemsid;
            }
            //TODO: need to add avg rating after implementation from jcms
            let seoUrl = category.seo;
            wu = seoUrl + '/' + urlName;
        }
        return Object.assign({}, { hl, wu, imageid, categoryName });

    }

    /**
     * Parse Item get the  detail for the slide  
     * Heading , URL , ID , ImageSize , type, ParentType
     * @param {contain Item Detail} obj 
     * @param {type of the article} feedSubType 
     */
    getReviewItem(obj, feedSubType) {
        let slideCount, wu, authorName, criticRating;
        let reviewItem = this.getmediumItem(obj, feedSubType)
        {
            var splittedSeo = obj.seopath.split("/");

            wu = '/reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + reviewItem.id + '.cms';

            if (obj.authors) {
                authorName = obj.authors[0].name;
            }
            if (obj.metainfo && obj.metainfo.CriticRating) {
                criticRating = obj.metainfo.CriticRating.value;
            }

        };


        return Object.assign(reviewItem, { authorName, wu, criticRating });
    }

    getGnGadgetsCompare(compareInfo, categoryMap) {
        let id,
            count,
            category,
            categorySeo,
            iDate,
            items = [];

        if (compareInfo) {
            id = compareInfo.id;
            category = compareInfo.category;
            count = compareInfo.count;
            iDate = compareInfo.inserteddate;
            var products = compareInfo.products
            categorySeo = categoryMap[category].seo;


            for (let index in products) {
                items.push(this.getGnGadgetsItem(products[index], categoryMap, category, true));
            }

        }
        return Object.assign({}, {
            id, count, category, categorySeo, iDate, items
        });

    }

    getGnGadgetDetail(productInfo, categoryValue, category ,utm_campaign) {
        let productId = '',
            productName,
            productUName,
            productSummary,
            modifiedDate,
            categoryName,
            imageId,
            imageversion,
            product_image,
            rumoured,
            discontinued,
            upcoming,            
            price,
            mrp,
            criticRating,
            avgrating,
            ratings,
            reviews,
            productUrl,
            brand,
            specs,
            keyFeature,
            variants,
            variantsDetail,
            affiliate,
            affLabel,
            reviewUrl,
            affArray = [],
            announceddate,
            sponsored,
            sponsoredUrl,
            sponsoredLogo,
            sponsoredLogoUrl,
            intermediateUrl;

        if (productInfo) {

            productId = productInfo.pid;
            let urlName = productInfo.uname;
            productUName = urlName;
            productName = productInfo.name;
            price = productInfo.price;
            rumoured = productInfo.rumoured;
            discontinued = productInfo.discontinued;
            upcoming = productInfo.upcoming;
            announceddate = productInfo.announceddate;
            if (productInfo.imagemsid && productInfo.imagemsid.length > 0) {
                imageId = productInfo.imagemsid[0];
                product_image = '/thumb/msid-' + imageId + ',width-218,resizemode-4,imgv-';
            }
            if (productInfo.criticrating) {
                criticRating = productInfo.criticrating;
            }
            if (productInfo.imageversion) {
                imageversion = productInfo.imageversion;
                product_image = product_image + imageversion;

            }

            product_image = product_image + '/' + productUName + '.jpg';
            //TODO: need to add avg rating after implementation from jcms
            if (productInfo.averagerating) {
                avgrating = (productInfo.averagerating / 2);
            }
            let categoryKey = productInfo.category;
            categoryName = categoryKey;
            if (!categoryKey && categoryValue) {
                categoryKey = categoryValue;
            }

            if (productInfo.averagerating) {
                avgrating = (productInfo.averagerating / 2).toFixed(1);
            }
            if (productInfo.ratings) {
                ratings = productInfo.ratings.map((item) => { return { rating: (item.rating / 2), count: item.count } });
            }
            if (productInfo.reviews) {
                reviews = productInfo.reviews;
            }
            if (productInfo.specs) {
                specs = productInfo.specs;
            }
            if (productInfo.keyfeatures) {
                keyFeature = productInfo.keyfeatures;
            }
            if (productInfo.brand) {
                brand = productInfo.brand.name;
            }

            if (productInfo.variants) {
                variants = productInfo.variants;
            }
            if (productInfo.variantsDetail) {
                variantsDetail = productInfo.variantsDetail;
            }

            if (productInfo.affiliatesinfo) {
                affArray = this.getAffiliatesDetailNew(productInfo, categoryKey, brand,utm_campaign);
                if (affArray.label) {
                    affLabel = affArray.label;
                    delete affArray['label'];
                }

            }
            if(productInfo.text)
            {
                productSummary = productInfo.text
            }
            if(productInfo.updatedate)
            {
                modifiedDate = productInfo.updatedate
            }
            if(productInfo.sponsored)
            {
                sponsored = productInfo.sponsored;
                affArray = null;
                affLabel = null;
            }
            if(productInfo.sponsoredUrl)
            {
                sponsoredUrl = productInfo.sponsoredUrl
            }

            if(productInfo.sponsoredLogo)
            {
                sponsoredLogo = productInfo.sponsoredLogo
            }

            if(productInfo.sponsoredLogoUrl)
            {
                sponsoredLogoUrl = productInfo.sponsoredLogoUrl
            }
            
            if (productInfo.reviews && productInfo.reviews.reviewdetail) {
                var reviewObj = productInfo.reviews.reviewdetail[0];
                var splittedSeo = reviewObj.seopath.split("/");
                reviewUrl = '/reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + productInfo.reviews.msid + '.cms';
            }
            let seoUrl = category[categoryKey].seo;
            productUrl = '/' + seoUrl + '/' + urlName;
        }


        return Object.assign({}, {
            productId,
            productName,
            imageId,
            imageversion,
            announceddate,
            product_image,
            modifiedDate,
            productSummary,
            productUName,
            rumoured,
            discontinued,
            upcoming,            
            price,
            mrp,
            criticRating,
            avgrating,
            ratings,
            reviews,
            productUrl, reviewUrl, brand, specs, keyFeature, variants, variantsDetail, affLabel, affArray,categoryName,sponsored,sponsoredUrl,sponsoredLogo,sponsoredLogoUrl
        });

    }


    getGnGadgetSummary(productInfo, categoryValue, category ,utm_campaign) {
        let productId = '',
            productName,
            productUName,
            productSummary,
            modifiedDate,
            categoryName,
            imageId,
            imageversion,
            product_image,
            product_imageList = [],
            rumoured,
            upcoming,
            price,
            mrp,
            criticRating,
            avgrating,
            ratings,
            reviews,
            productUrl,
            gadgetDescription,
            brand,
            specs,
            keyFeature,
            variants,
            affiliate,
            affLabel,
            reviewUrl,
            releaseDate,
            affArray = [],
            announceddate,
            intermediateUrl;

        if (productInfo) {

            productId = productInfo.pid;
            let urlName = productInfo.uname;
            productUName = urlName;
            productName = productInfo.name;
            price = productInfo.price;
            rumoured = productInfo.rumoured;
            upcoming = productInfo.upcoming;
            announceddate = productInfo.announceddate;
            if (productInfo.imagemsid && productInfo.imagemsid.length > 0) {
                imageId = productInfo.imagemsid[0];
                product_image = '/thumb/msid-' + imageId + ',width-218,resizemode-4,imgv-';
            }
            if (productInfo.imagemsid && productInfo.imagemsid.length > 0) {
                for (let index in productInfo.imagemsid) {
                    imageId = productInfo.imagemsid[index];
                    var _product_image = '/thumb/msid-' + imageId + ',width-218,resizemode-4,imgv-';
                    // Do need image URL --
                    // if (productInfo.imageversion) {
                    //     imageversion = productInfo.imageversion;
                    //     _product_image = _product_image + imageversion;
        
                    // }
        
                    // _product_image = _product_image + '/' + productUName + '.jpg';
                      
                    product_imageList.push(imageId);
                }
            }
            if (productInfo.criticrating) {
                criticRating = productInfo.criticrating;
            }
            if (productInfo.imageversion) {
                imageversion = productInfo.imageversion;
                product_image = product_image + imageversion;

            }

            product_image = product_image + '/' + productUName + '.jpg';
            //TODO: need to add avg rating after implementation from jcms
            if (productInfo.averagerating) {
                avgrating = (productInfo.averagerating / 2);
            }
            let categoryKey = productInfo.category;
            let seoUrl = category[categoryKey].seo;
            categoryName = categoryKey;
            if (!categoryKey && categoryValue) {
                categoryKey = categoryValue;
            }

            if (productInfo.averagerating) {
                avgrating = (productInfo.averagerating / 2).toFixed(1);
            }
            if (productInfo.ratings) {
                ratings = productInfo.ratings.map((item) => { return { rating: (item.rating / 2), count: item.count } });
            }
            if (productInfo.reviews) {
                reviews = productInfo.reviews;
            }
            if (productInfo.specs) {
                specs = productInfo.specs;
            }
            if (productInfo.keyfeatures) {
                keyFeature = productInfo.keyfeatures;
            }
            if (productInfo.brand) {
                brand = productInfo.brand.name;
            }
            if(productInfo.specs && productInfo.specs.general && productInfo.specs.general.launch_date){
                releaseDate = productInfo.specs.general.launch_date;
            }

            if (productInfo.variants) {
                variants = productInfo.variants;
                for (var Vkeyindex in variants) {
                    var Vkey = variants[Vkeyindex]
                    var vName = Vkey;
                    if(productInfo.variantsDetail && productInfo.variantsDetail[Vkey]){
                        vName = productInfo.variantsDetail[Vkey];
                    }
                    variants[Vkeyindex] = {'uname':Vkey, 'name':vName, 'url' : '/' + seoUrl + '/' + Vkey }
                }
            }
            if (productInfo.affiliatesinfo) {
                affArray = this.getAffiliatesDetailNew(productInfo, categoryKey, brand,utm_campaign);
                if (affArray.label) {
                    affLabel = affArray.label;
                    delete affArray['label'];
                }

            }
            if(productInfo.text)
            {
                productSummary = productInfo.text
            }
            if(productInfo.updatedate)
            {
                modifiedDate = productInfo.updatedate
            }
            
            if (productInfo.reviews && productInfo.reviews.reviewdetail) {
                var reviewObj = productInfo.reviews.reviewdetail[0];
                var splittedSeo = reviewObj.seopath.split("/");
                reviewUrl = '/reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + productInfo.reviews.msid + '.cms';
            }
            productUrl = '/' + seoUrl + '/' + urlName;
            if(productInfo.seoDetail && productInfo.seoDetail['Atf-content']){
                gadgetDescription = productInfo.seoDetail['Atf-content'];
            } else {
                let gnShowHelper = new GnJsonDataParserHelper(this._requestparams, this._$);
                gadgetDescription = gnShowHelper.getGadgetAtfContent(productInfo);
            }
        }


        return Object.assign({}, {
            productId,
            productName,
            imageId,
            imageversion,
            product_image,
            product_imageList,
            productUName,
            price,
            mrp,
            criticRating,
            avgrating,
            ratings,
            reviews,
            variants,
            releaseDate,
            productUrl, gadgetDescription, reviewUrl, brand, keyFeature, affLabel, affArray,categoryName
        });

    }

    getGnGadgetSearch(productInfo, excludePId) {
        let Product_name, uName, Product_image, Product_url;
        if (productInfo && (!excludePId || !excludePId.includes(productInfo.uname))) {

            uName = productInfo.uname;
            Product_name = productInfo.name;
            if (productInfo.imagemsid && productInfo.imagemsid.length > 0) {
                var imageId = productInfo.imagemsid[0];
                Product_image = '/thumb/msid-' + imageId + ',width-218,resizemode-4,imgv-';
            }
            if (productInfo.imageversion) {
                var imageversion = productInfo.imageversion;
                Product_image = Product_image + imageversion;
            }

            let seoUrl = category_INFO[productInfo.category].seo;
            Product_url = '/' + seoUrl + '/' + uName;

            Product_image = Product_image + '/' + uName + '.jpg';
            return Object.assign({}, { Product_name, uName, Product_image, Product_url });
        }
    }

    getAffiliatesDetailNew(productInfo, categoryKey, brand,utm_campaign){
        let gnShowHelper = new GnJsonDataParserHelper(this._requestparams, this._$);
        return gnShowHelper.getAffiliatesDetailNew(productInfo, categoryKey, brand,utm_campaign);
    }
    getAffiliatesDetail(productInfo, categoryKey, brand,utm_campaign) {
        let intermediateUrl,
            label,
            affiliate,
            affurl,
            affArray = [];
            if(!utm_campaign)
            {
                utm_campaign = "mweb_compareshow_####-21";
            }
            let utm_campaign_vijaysales = utm_campaign.substr(0, utm_campaign.split('_', 2).join('_').length);
        if (productInfo.affiliatesinfo.exactmatches && ((Object.keys(productInfo.affiliatesinfo.exactmatches)).includes('amazon') || (Object.keys(productInfo.affiliatesinfo.exactmatches)).includes('tatacliq') || (Object.keys(productInfo.affiliatesinfo.exactmatches)).includes('__paytm') || (Object.keys(productInfo.affiliatesinfo.exactmatches)).includes('vijaysales'))) {
            for (var keys in productInfo.affiliatesinfo.exactmatches) {
                var affiliates = productInfo.affiliatesinfo.exactmatches;
                var affiliat = affiliates[keys];
                label = 'BUY';
                if (affiliat && (affiliat.identifier == 'amazon' || affiliat.identifier == 'vijaysales')) {
                    var pdurl = affiliat.url
                    var listPrice, pricePercentage ,name;
                    const amazonURLObj = new URL(pdurl);
                    var pathName = amazonURLObj.pathname;
                    var pathSearch = amazonURLObj.search;
                    var lowestPrice = 0;
                    if (affiliat.lowestnewprice) {
                         lowestPrice = affiliat.lowestnewprice.amount;

                    }
                    if(affiliat.name)
                    {
                        name = affiliat.name;
                    }

                    if (affiliat.listprice) {
                        listPrice = affiliat.listprice.amount;
                        pricePercentage = (((listPrice - lowestPrice) / listPrice) * 100).toFixed(2);
                    }
                    if(utm_campaign.indexOf("####")>1 && utm_campaign.indexOf("_articleshow")>1 )
                    {
                        utm_campaign =  utm_campaign.replace("####",'buyat01');
                    }
                    else if (utm_campaign.indexOf("####")>1 && utm_campaign.indexOf("_articleshow")<0 )
                    {
                        utm_campaign =  utm_campaign.replace("####",'buyat');
                    }
                    var affName = affiliat.name;
                    var urlParam = encodeURIComponent(pathName + pathSearch) + '&price=' + lowestPrice + '&title=' + encodeURIComponent(affName);
                    if(affiliat.identifier == 'amazon'){
                        intermediateUrl = 'https://www.gadgetsnow.com/affiliate_amazon.cms?url=' + urlParam + '&utm_source=gnwap&utm_medium=Affiliate&utm_campaign='+utm_campaign;
                    }
                    if(affiliat.identifier == 'vijaysales'){
                        intermediateUrl = 'https://www.gadgetsnow.com/affiliate_vijaysales.cms?url=' + urlParam + '&utm_source=gadgetsnow&utm_medium=affiliate&utm_campaign='+utm_campaign_vijaysales;
                    }
                    affiliate = affiliat.identifier;
                    affurl = intermediateUrl;
                    //console.log(intermediateUrl);
                    affArray.push({ affiliate,affName, affurl, listPrice, lowestPrice, pricePercentage });
                }
                else if ((PAYTM_OTHER_CAT.includes(categoryKey)) || (categoryKey == 'mobile' && (PAYTM_MOBILE_BRAND.includes(brand)))) {
                    if (affiliat && affiliat.identifier == '__paytm') {
                        var pdurl = affiliat.url
                        const amazonURLObj = new URL(pdurl);
                        var pathName = amazonURLObj.pathname;
                        var pathSearch = amazonURLObj.search;
                        var listPrice, pricePercentage;
                        var lowestPrice = 0;
                        if (affiliat.lowestnewprice) {
                            lowestPrice = affiliat.lowestnewprice.amount;
   
                       }
                        if (affiliat.listprice) {
                            listPrice = affiliat.listprice.amount;
                            pricePercentage = (((listPrice - lowestPrice) / listPrice) * 100).toFixed(2);
                        }
                        var affName = affiliat.identifier;
                        var urlParam = encodeURIComponent(pathName + pathSearch) + '&price=' + lowestPrice + '&title=' + encodeURIComponent(affName);
                        intermediateUrl = 'https://www.gadgetsnow.com/affiliate_paytm.cms?url=' + urlParam + 'utm_source=gadgetsnow&utm_medium=Affiliate&utm_campaign=mweb_gadgetshow_seealso-21&putm=GadgetsNow_Mweb_Gadgetshow_mobile'
                        affiliate = affiliat.identifier;
                        affurl = intermediateUrl;
                        //console.log(intermediateUrl);
                        affArray.push({ affiliate, affurl, listPrice, lowestPrice, pricePercentage });
                    }
                }
                else if (keys == 'tatacliq' && (affiliates[keys]).products && ((affiliates[keys]).products).length > 0) {
                    var affiliat = (affiliates[keys].products)[0];
                    const amazonURLObj = new URL(pdurl);
                    var pathName = amazonURLObj.pathname;
                    var pathSearch = amazonURLObj.search;
                    var lowestPrice = affiliat.price;
                    var offer = affiliat.offer;
                    var affName = affiliat.name;
                    var urlParam = encodeURIComponent(pathName + pathSearch) + '&price=' + lowestPrice + '&title=' + encodeURIComponent(affName);
                    intermediateUrl = 'https://www.gadgetsnow.com/affiliate_tatacliq.cms?url=' + urlParam + '&utm_source=gnwap&utm_medium=Affiliate&tag=mweb_compareshow_buyat-21';
                    affiliate = affiliat.affiliate;
                    affurl = intermediateUrl;
                    //console.log(intermediateUrl);
                    affArray.push({ affiliate, affurl, offer, lowestPrice });
                }
            }

        }
        else if (productInfo.affiliatesinfo.relatedmatches && ((Object.keys(productInfo.affiliatesinfo.relatedmatches)).includes('amazon') || (Object.keys(productInfo.affiliatesinfo.relatedmatches)).includes('tatacliq') || (Object.keys(productInfo.affiliatesinfo.relatedmatches)).includes('__paytm') || (Object.keys(productInfo.affiliatesinfo.relatedmatches)).includes('vijaysales'))) {
            for (var keys in productInfo.affiliatesinfo.relatedmatches) {
                var affiliates = productInfo.affiliatesinfo.relatedmatches;
                label = 'SEE ALSO';
                // var affiliat =  affiliates[keys][0];
                if(utm_campaign.indexOf("####")>1)
                {
                    utm_campaign=  utm_campaign.replace("####",'seealso');
                }
                if (affiliates[keys][0] && (keys == 'amazon' || keys == 'vijaysales')) { 
                    
                    for(let amazonindex  in affiliates[keys])
                    {
                        if(amazonindex>0){
                            break;
                        }
                    var affiliat = affiliates[keys][amazonindex];
                    var pdurl = affiliat.url
                    const amazonURLObj = new URL(pdurl);
                    var pathName = amazonURLObj.pathname;
                    var pathSearch = amazonURLObj.search;
                    var lowestPrice = 0;
                    var name;
                    if(affiliat.lowestnewprice)
                    {
                     lowestPrice = affiliat.lowestnewprice.amount;
                    }
                    var listPrice;
                    var pricePercentage;
                    if (affiliat.listprice) {
                        listPrice = affiliat.listprice.amount;
                        pricePercentage = (((listPrice - lowestPrice) / listPrice) * 100).toFixed(2);
                    }
                    

                    var affName = affiliat.name;
                    var urlParam = encodeURIComponent(pathName + pathSearch) + '&price=' + lowestPrice + '&title=' + encodeURIComponent(affName);
                    if(keys == 'amazon'){
                        intermediateUrl = 'https://www.gadgetsnow.com/affiliate_amazon.cms?url=' + urlParam + '&utm_source=gnwap&utm_medium=Affiliate&utm_campaign='+utm_campaign;
                    }
                    if(keys == 'vijaysales'){
                        intermediateUrl = 'https://www.gadgetsnow.com/affiliate_vijaysales.cms?url=' + urlParam + '&utm_source=gadgetsnow&utm_medium=affiliate&utm_campaign='+utm_campaign_vijaysales;
                    }
                    affiliate = affiliat.identifier;
                    affurl = intermediateUrl;
                    //console.log(intermediateUrl);
                    affArray.push({ affiliate, affName, affurl, listPrice, lowestPrice, pricePercentage });

                }
            }
                else if ((PAYTM_OTHER_CAT.includes(categoryKey)) || (categoryKey == 'mobile' && (PAYTM_MOBILE_BRAND.includes(brand)))) {
                    if (affiliates[keys][0] && keys == '__paytm') {
                        var affiliat = affiliates[keys][0];
                        var pdurl = affiliat.url
                        const amazonURLObj = new URL(pdurl);
                        var pathName = amazonURLObj.pathname;
                        var pathSearch = amazonURLObj.search;
                        var lowestPrice = 0;
                        if(affiliat.lowestnewprice)
                        {
                         lowestPrice = affiliat.lowestnewprice.amount;
                        }
                        var listPrice;
                        var pricePercentage;
                        if (affiliat.listprice) {
                            listPrice = affiliat.listprice.amount;
                            pricePercentage = (((listPrice - lowestPrice) / listPrice) * 100).toFixed(2);
                        }
                        var affName = affiliat.name;
                        var urlParam = encodeURIComponent(pathName + pathSearch) + '&price=' + lowestPrice + '&title=' + encodeURIComponent(affName);
                        intermediateUrl = 'https://www.gadgetsnow.com/affiliate_paytm.cms?url=' + urlParam + 'utm_source=gadgetsnow&utm_medium=Affiliate&utm_campaign=mweb_gadgetshow_seealso-21&putm=GadgetsNow_Mweb_Gadgetshow_mobile'
                        affiliate = affiliat.identifier;
                        affurl = intermediateUrl;
                        //console.log(intermediateUrl);
                        affArray.push({ affiliate, affurl, listPrice, lowestPrice, pricePercentage });
                    }
                }
                else if (keys == 'tatacliq' && (affiliates[keys]).products && ((affiliates[keys]).products).length > 0) {
                    var affiliat = (affiliates[keys].products)[0];
                    var pdurl = affiliat.url
                    const amazonURLObj = new URL(pdurl);
                    var pathName = amazonURLObj.pathname;
                    var pathSearch = amazonURLObj.search;
                    var lowestPrice = affiliat.price;
                    var offer = affiliat.offer;
                    var affName = affiliat.title;
                    var urlParam = encodeURIComponent(pathName + pathSearch) + '&price=' + lowestPrice + '&title=' + encodeURIComponent(affName);
                    intermediateUrl = 'https://www.gadgetsnow.com/affiliate_tatacliq.cms?url=' + urlParam + '&utm_source=gnwap&utm_medium=Affiliate&tag=mweb_compareshow_buyat-21';
                    affiliate = affiliat.affiliate;
                    affurl = intermediateUrl;
                    //console.log(intermediateUrl);
                    affArray.push({ affiliate, affurl, offer, lowestPrice });

                }
            }
        }
        return Object.assign(affArray, { 'label': label });
    }

    gettopGadgetshowItem(obj, feedSubType, jsonobject ) {

       
        let id = 'id',
            type = 'type',
            hl = 'hl',
            imageid = 'imageid',
            wu = 'wu',
            parenttitle,
            imageSize = '12345',
            summary ,
            authorName,
            criticRating,
            upd = '000';
            let pdDetail = JSON.parse(jsonobject['response'].pdpResultOBject);
            let category = jsonobject['categories'];

            let gadgetDetail;
            let isReviewd = false;

        let newsObj = obj;
        {
            id = newsObj.msid;
            type = newsObj.cmstype;
            hl = obj.title;
            summary = newsObj.text
            upd = newsObj.updatedate
            if (newsObj.thumbsize && newsObj.thumbsize !== '') {
                imageSize = newsObj.thumbsize;
            }

            imageid = 'imgsize=' + imageSize;
            if(newsObj.metainfo && newsObj.metainfo.GadgetsMarkedReviewed)
            {
                if(newsObj.metainfo.GadgetsMarkedReviewed.value === "1")
                {
                    isReviewd=true;
                }
            }
            if (feedSubType == FEED_SUB_TYPE_NEWS || feedSubType == FEED_SUB_TYPE_FOOTER || feedSubType == FEED_SUB_TYPE_HEADER) {
                wu = newsObj.seopath + '/articleshow/' + id + '.cms';
                if(isReviewd)
                {
                    var splittedSeo = newsObj.seopath.split("/");
                    wu = 'reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + id + '.cms';
                   
                }
            }
            else if (feedSubType == FEED_SUB_TYPE_VIDEO) {
                wu = newsObj.seopath + '/videoshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_EDITOR_SHOW) {
                wu = newsObj.seopath + '/editorschoiceshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_EDITOR_SHOW) {
                wu = newsObj.seopath + '/editorschoiceshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_REVIEW) {
                var splittedSeo = newsObj.seopath.split("/");
                wu = 'reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + id + '.cms';

                if (newsObj.authors) {
                    var authorId = newsObj.authors[0].id;

                    authorName = newsObj.authors[0].name;

                }
                if (newsObj.metainfo && newsObj.metainfo.CriticRating) {
                    criticRating = newsObj.metainfo.CriticRating.value;
                }

            }
            wu = 'https://www.gadgetsnow.com/' + wu;
            if (newsObj.overridelink) {
                wu = newsObj.overridelink;
            }
            parenttitle = newsObj.parenttitle;
            if (newsObj.metainfo && newsObj.metainfo.GadgetsTaggedGadgets) {
               let  productName = newsObj.metainfo.GadgetsTaggedGadgets.value;
                let productInfo = pdDetail[ productName];
                gadgetDetail = this.getGnGadgetDetail(productInfo, null, category) 

            }
        };

        //console.log(obj);

        return Object.assign({}, { type, upd,summary, hl, id, imageSize, parenttitle, imageid, wu, criticRating, authorName ,gadgetDetail });
    }

    getRssfedd(obj, feedSubType, jsonobject ) {

       
        let id = 'id',
            type = 'type',
            hl = 'hl',
            imageid = 'imageid',
            wu = 'wu',
            parenttitle,
            imageSize = '12345',
            summary ,
            authorName,
            criticRating,
            upd = '000';
            let pdDetail = JSON.parse(jsonobject['response'].pdpResultOBject);
            let category = jsonobject['categories'];

            let gadgetDetail;
            let isReviewd = false;

        let newsObj = obj;
        {
            id = newsObj.msid;
            type = newsObj.cmstype;
            hl = obj.title;
            summary = newsObj.text
            upd = newsObj.updatedate
            if (newsObj.thumbsize && newsObj.thumbsize !== '') {
                imageSize = newsObj.thumbsize;
            }
            if(newsObj.metainfo && newsObj.metainfo.GadgetsMarkedReviewed)
            {
                if(newsObj.metainfo.GadgetsMarkedReviewed.value === "1")
                {
                    isReviewd=true;
                }
            }
            imageid = 'imgsize=' + imageSize;
            if (feedSubType == FEED_SUB_TYPE_NEWS || feedSubType == FEED_SUB_TYPE_FOOTER || feedSubType == FEED_SUB_TYPE_HEADER) {
                wu = newsObj.seopath + '/articleshow/' + id + '.cms';
                if(isReviewd)
                {
                    var splittedSeo = newsObj.seopath.split("/");
                    wu = 'reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + id + '.cms';
                   
                }
            }
            else if (feedSubType == FEED_SUB_TYPE_VIDEO) {
                wu = newsObj.seopath + '/videoshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_EDITOR_SHOW) {
                wu = newsObj.seopath + '/editorschoiceshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_EDITOR_SHOW) {
                wu = newsObj.seopath + '/editorschoiceshow/' + id + '.cms';
            }
            else if (feedSubType == FEED_SUB_TYPE_REVIEW) {
                var splittedSeo = newsObj.seopath.split("/");
                wu = 'reviews/' + splittedSeo[splittedSeo.length - 1] + '/articleshow/' + id + '.cms';

                if (newsObj.authors) {
                    var authorId = newsObj.authors[0].id;

                    authorName = newsObj.authors[0].name;

                }
                if (newsObj.metainfo && newsObj.metainfo.CriticRating) {
                    criticRating = newsObj.metainfo.CriticRating.value;
                }

            }
            wu = 'https://www.gadgetsnow.com/' + wu;
            if (newsObj.overridelink) {
                wu = newsObj.overridelink;
            }
            parenttitle = newsObj.parenttitle;
            if (newsObj.metainfo && newsObj.metainfo.GadgetsTaggedGadgets) {
               let  productName = newsObj.metainfo.GadgetsTaggedGadgets.value;
                let productInfo = pdDetail[ productName];
                gadgetDetail = this.getGnGadgetDetail(productInfo, null, category) 

            }
        };

        //console.log(obj);

        return Object.assign({}, { type, upd,summary, hl, id, imageSize, parenttitle, imageid, wu, criticRating, authorName ,gadgetDetail });
    }

    getSpecificFeatures(productInfo){
        var specificFeatures = {};
        switch(productInfo.category) {
            case 'mobile': {
                if(productInfo.specs.performance && productInfo.specs.performance.ram){
                    specificFeatures['ram'] = productInfo.specs.performance.ram;
                }
                if(productInfo.keyfeatures.storage){
                    specificFeatures['storage'] = productInfo.keyfeatures.storage;
                }
                if(productInfo.keyfeatures.battery){
                    specificFeatures['battery'] = productInfo.keyfeatures.battery;
                }
                break;
            }
            case 'laptop': {
                if(productInfo.keyfeatures.ram){
                    specificFeatures['ram'] = productInfo.keyfeatures.ram;
                }
                if(productInfo.keyfeatures.processor){
                    specificFeatures['processor'] = productInfo.keyfeatures.processor;
                }
                break;
            }
            case 'tablet': {
                if(productInfo.specs.performance && productInfo.specs.performance.ram){
                    specificFeatures['ram'] = productInfo.specs.performance.ram;
                }
                if(productInfo.specs.storage && productInfo.specs.storage.internal_memory){
                    specificFeatures['memory'] = productInfo.specs.storage.internal_memory;
                }
                if(productInfo.keyfeatures.battery){
                    specificFeatures['battery'] = productInfo.keyfeatures.battery;
                }
                break;
            }
            case 'camera': {
                if(productInfo.keyfeatures.resolution){
                    specificFeatures['resolution'] = productInfo.keyfeatures.resolution;
                }
                if(productInfo.keyfeatures.connectivity){
                    specificFeatures['connectivity'] = productInfo.keyfeatures.connectivity;
                }
                break;
            }
            case 'tv': {
                if(productInfo.specs.display && productInfo.specs.display.size_diagonal){
                    specificFeatures['size_diagonal'] = productInfo.specs.display.size_diagonal;
                }
                if(productInfo.specs.display && productInfo.specs.display.resolution){
                    specificFeatures['resolution'] = productInfo.specs.display.resolution;
                }
                break;
            }
            case 'powerbank': {
                if(productInfo.specs.battery_charging && productInfo.specs.battery_charging.capacity){
                    specificFeatures['capacity'] = productInfo.specs.battery_charging.capacity;
                }
                if(productInfo.specs.battery_charging && productInfo.specs.battery_charging.battery_type){
                    specificFeatures['battery_type'] = productInfo.specs.battery_charging.battery_type;
                }
                if(productInfo.specs.connectivity && productInfo.specs.connectivity.noof_output_ports){
                    specificFeatures['ports'] = productInfo.specs.connectivity.noof_output_ports;
                }
                break;
            }
            case 'smartwatch': {
                if(productInfo.specs.display && productInfo.specs.display.display_technology){
                    specificFeatures['display'] = productInfo.specs.display.display_technology;
                }
                if(productInfo.specs.battery && productInfo.specs.battery.capacitytype){
                    specificFeatures['capacity'] = productInfo.specs.battery.capacitytype;
                }
                if(productInfo.specs.connectivity && productInfo.specs.connectivity.bluetooth){
                    specificFeatures['bluetooth'] = productInfo.specs.connectivity.bluetooth;
                }
                break;
            }
            case 'ac': {
                if(productInfo.keyfeatures.capacity){
                    specificFeatures['capacity'] = productInfo.keyfeatures.capacity;
                }
                if(productInfo.keyfeatures.energy_rating){
                    specificFeatures['star_rating'] = productInfo.keyfeatures.energy_rating;
                }
                if(productInfo.keyfeatures.type){
                    specificFeatures['type'] = productInfo.keyfeatures.type;
                }
                break;
            }
            case 'washingmachine': {
                if(productInfo.specs.general && productInfo.specs.general.capacity){
                    specificFeatures['capacity'] = productInfo.specs.general.capacity;
                }
                if(productInfo.specs.general && productInfo.specs.general.type){
                    specificFeatures['type'] = productInfo.specs.general.type;
                }
                if(productInfo.specs.general && productInfo.specs.general.control){
                    specificFeatures['control'] = productInfo.specs.general.control;
                }
                break;
            }
            case 'refrigerator': {
                if(productInfo.keyfeatures.capacity){
                    specificFeatures['capacity'] = productInfo.keyfeatures.capacity;
                }
                if(productInfo.specs.general && productInfo.specs.general.energy_rating){
                    specificFeatures['energy_rating'] = productInfo.specs.general.energy_rating;
                }
                if(productInfo.specs.general && productInfo.specs.general.refrigerator_type){
                    specificFeatures['type'] = productInfo.specs.general.refrigerator_type;
                }
                break;
            }
            case 'fitnessband': {
                if(productInfo.keyfeatures.syncing){
                    specificFeatures['syncing'] = productInfo.keyfeatures.syncing;
                }
                if(productInfo.keyfeatures.features){
                    specificFeatures['features'] = productInfo.keyfeatures.features;
                }
                if(productInfo.specs.battery && productInfo.specs.battery.battery_life){
                    specificFeatures['battery'] = productInfo.specs.battery.battery_life;
                }
                break;
            }
            default: {
                specificFeatures = productInfo.keyfeatures;
               break;
            }
        }
        return specificFeatures;
    }

    getFaqGadgets(jsonobject , categoryKey )
    {
        let qnaList = [];
        var result = {'test':'we parse'};
        var categoryArr = jsonobject['categories'];
        let uname =this._requestparams[REQUEST_PARAMS_FAQ_UNAME];



        if(jsonobject && jsonobject.data && jsonobject.data.length >0 )
        {
            
            
            if(jsonobject.data[0][uname])
            {
                qnaList = jsonobject.data[0][uname]
            }
            else if (jsonobject.data[0][categoryKey])
            {
            qnaList = jsonobject.data[0][categoryKey];
            }
             let name = null;
             let productDetail = null;
             let similarobj = null;
             if(jsonobject.data[0]['productDetail'])
             {
                productDetail =  jsonobject.data[0]['productDetail'];
                name = productDetail.name;
             }
             if(jsonobject.data[0]['similarobj'])
             {
                similarobj = jsonobject.data[0]['similarobj'];
             }
             qnaList.forEach((value,index)=>{
                console.log(value);
                var que = value.question;
                if (que.indexOf('@Name of Product')>0)
                {
                    que = que.replace('@Name of Product' , name)
                }
                else if ( que.indexOf('<Name of Product>')>0 )
                {
                    que = que.replace('<Name of Product>' , name)
                    
                }
                else if ( que.indexOf('@Name')>0 )
                {
                    que = que.replace('@Name' , name)
                    
                }
                else if (que.match(new RegExp(/(@Name of Product)/gi)))
                    {
                        que= que.replace(/(@Name of Product)/gi, name);
                    }

                value.question = que;

                var ans = value.yes;
                if( (!value.yes || value.yes.length<0 )&&(value.no &&  value.no.length>2))
                {
                    ans = value.no;
                } 
                if(value.questiontype  && value.questiontype  == 'Normal' )
                {
                    if (ans.indexOf('@Name of Product')>=0)
                    {
                        ans = ans.replace('@Name of Product' , name);
                    }
                    else if ( ans.indexOf('<Name of Product>')>=0 )
                    {
                        ans = ans.replace('<Name of Product>' , name);
                    }
                    else if (ans.match(new RegExp(/(@Name of Product)/gi)))
                    {
                        ans.replace(/(@Name of Product)/gi, name);
                    }
                    value.ans = ans;

             }
                
                if( que.match(new RegExp(/(Which store has the cheapest price for)/gi) )  || que.match(new RegExp(/(Which store has the Lowest price for)/gi) ))   //que.indexOf('Which store has the cheapest price for')>-1 )
                {
                    //call affiliateDataHelper
                    let gnShowHelper = new GnJsonDataParserHelper(this._requestparams, this._$);
                    let affArray_ = [];
                    let affList = [];
                     if(value['affilate '] && value['affilate '].length>0)
                    {   
                        value['affilate '].forEach((aff,idx) => {
                            affList.push(aff);
                        });
                    }
                    let affArray =  null;// gnShowHelper.getAffiliatesDetail_Show(productDetail, null, null, jsonobject.utm);

                    if(affList && affList.length>0)
                    {
                         affArray =    gnShowHelper.getAffiliatesDetail_Show(productDetail, null, null, jsonobject.utm,affList);

                    }
                    else
                    {
                         affArray =    gnShowHelper.getAffiliatesDetail_Show(productDetail, null, null, jsonobject.utm);

                    }
                  if( value.isaffilate  &&  ( affArray.AFFILIATE && affArray.AFFILIATE['BUY'] && affArray.AFFILIATE['BUY'].length >0) )
                    {
                     affArray_ = affArray.AFFILIATE['BUY'];
                        if(affArray_ && affArray_.length >1)
                            {
                                var bylowestPrice = affArray_.slice(0);
                                bylowestPrice.sort(function(a,b) {
                                    return a.lowestPrice - b.lowestPrice;
                                });
                                affArray_ = bylowestPrice;
                            }
                    }
                    else
                    {
                        let ans = "Gadgetsnow.com has " + name +' with price ₹ '+Number(productDetail.price).toLocaleString('en-IN');
                         value.ans = ans;
                    }

                  value.affilate  = affArray_;
                  delete value['affilate '];
                }
               else
                {
                    delete value.affilate ;
                    delete value['affilate '];
                    delete value.isaffilate;
                }

                if( que.match(new RegExp(/(What is the screen size of)/gi)))   //que.indexOf('support 5G?')>-1)
                {
                    let size = '';
                    let resolution = '';
                    let ans = name + ' screen size not present';
                    if(productDetail && productDetail.specs  )
                    {
                        if(productDetail.specs.display && productDetail.specs.display.screen_size )
                        {
                            size = productDetail.specs.display.screen_size;
                            ans = name + ' features a '+ size +' display';
                            if (productDetail.specs.display.screen_resolution   )
                                {
                                    resolution = productDetail.specs.display.screen_resolution;
                                    ans=ans+ ' with a screen resolution of '+resolution+'.'
                                }
                        }
                        
                    }
                    value.ans = ans;
                }
                if( que.match(new RegExp(/(What is the RAM capacity of)/gi)))   //que.indexOf('support 5G?')>-1)
                {
                    let ram = '';
                    let storage = '';
                    let ans = name + ' ram capacity not present';
                    if(productDetail && productDetail.specs  )
                    {
                        if(productDetail.specs.performance && productDetail.specs.performance.ram )
                        {
                            ram = productDetail.specs.performance.ram;
                            ans = name + ' is available with '+ ram +' RAM ';
                            if (productDetail.specs.storage  && productDetail.specs.storage.internal_memory   )
                                {
                                    storage = productDetail.specs.storage.internal_memory ;
                                    ans=ans+ 'and '+storage +' of internal storage capacity.'
                                }
                        }
                        
                    }
                    value.ans = ans;
                }
                if( que.match(new RegExp(/(What is the price of)/gi)))   //que.indexOf('support 5G?')>-1)
                {
                    let ans = "Price of " + name +' in India is ₹ ' +   Number(productDetail.price).toLocaleString('en-IN')+" .";
                    value.ans = ans;
                }

                if( que.match(new RegExp(/(support 5G)/gi)) || que.match(new RegExp(/(support the 5G)/gi)) )   //que.indexOf('support 5G?')>-1)
                {
                    let support = 'doesn’t';
                    let logic = 'No'
                    if(productDetail && productDetail.specs  )
                    {
                        if(productDetail.specs.general && productDetail.specs.general.network )
                        {
                                if (productDetail.specs.general.network.match(new RegExp(/(5G|5g|5-G)/gi)))
                                {
                                    support = 'does';
                                    logic = 'Yes';
                                }
                        }
                        else if ( logic =='No' && productDetail.specs.network__connectivity && specs.network__connectivity.sim_1 )
                        {
                            if (productDetail.specs.network__connectivity.sim_1.match(new RegExp(/(5G|5g|5-G)/gi)))
                            {
                                support = 'does';
                                logic = 'Yes';

                            }
                        }
                    }
                    let ans = logic+', '+ name +' ' + support +' support 5G.';
                    value.ans = ans;
                }

                if(que.match(new RegExp(/(What is the display size of)/gi)))// que.indexOf('What is the display size of')>-1)
                {
                 let ans = 'Display size of '+name+' is ';
                    if(productDetail && productDetail.specs  )
                    {
                        if( productDetail.specs.display_details.display_size)
                        {
                            ans =ans + productDetail.specs.display_details.display_size;
                        }
                    }
                    value.ans = ans;
                }

                if(que.match(new RegExp(/(support Quick Charging)/gi) ))//que.indexOf('support Quick Charging')>-1)
                {
                    let logic = 'No';
                    let support = 'doesn’t';
                    if(productDetail && productDetail.specs  )
                    {
                        if(  productDetail.specs.battery && productDetail.specs.battery.quick_charging)
                        {
                           logic = 'Yes';
                           support = 'does';
                        }
                    }
                    let ans = logic +', '+ name+ ' '+ support +' supports Quick Charging.';
                    value.ans = ans;
                }

                if(  que.match(new RegExp(/(Which OS Type does)/gi)))   //que.indexOf('Which OS Type does')>-1)
                {
                 let ans = name+' supports ';
                    if(productDetail && productDetail.specs  )
                    {
                        if( productDetail.specs&&  productDetail.specs.general_information && productDetail.specs.general_information.os)
                        {
                            ans =ans + productDetail.specs.general_information.os;
                        }
                    }
                    value.ans = ans;
                }
                
                
                if(  value.questiontype && value.questiontype== 'List of Items') //que.indexOf('What are the best alternatives to')>-1)
                {
                    let  _similarobj = similarobj;
                    if(value['similarobj'])
                        {
                         _similarobj = value['similarobj'];
                        }
                    if(_similarobj && _similarobj.length >0 )
                    {
                        let similarList = [];
                        let seoUrl = categoryArr[categoryKey].seo;                       
                        _similarobj.forEach( (similarval , similaridx )=> {

                         var productUrl =  seoUrl + '/' + similarval.uname;
                
                            similarList.push(Object.assign(({'name':similarval.name, 'uname':similarval.uname,'productUrl':productUrl})))
                           
                       });

                       let ans = 'The best alternatives to '+name +' are as follows:';
                       value.ans = ans;
                       value.similarList = similarList ;                  
                    }
                }

                delete value.Manual;
                delete value.yes;
                delete value.no;
                delete value.category;
                if(value['similarobj'])
                {
                 delete value['similarobj'];

                }
 
             });
             result = qnaList;
            console.log('we look for category');

        }

        return result;
        
    }

}