<?php 
 class AffiliateUrlCreator
 {
 	public static $amazon_UTM_CAMPAIGN_buyat="mweb_gnshop_pdp_buyat-21";
 	public static $amazon_UTM_CAMPAIGN_seealso="mweb_gnshop_seealso-21";
 	public static $amazon_UTM_CAMPAIGN_masthead="mweb_gnshop_masthead-21";
 	public static $amazon_UTM_CAMPAIGN_nudge = "mweb_gnshop_sale_nudge-21";
 	public static $amazon_UTM_CAMPAIGN_atf="mweb_gnshop_atf-21";
 	public static $amazon_UTM_CAMPAIGN_btf="mweb_gnshop_btf-21";
 	public static $amazon_UTM_CAMPAIGN_recommended="mweb_gnshop_recomproduct-21";
 	public static $amazon_UTM_CAMPAIGN_sponsored="mweb_gnshop_spons-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget = "mweb_gnshop_promowidget-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget1 = "mweb_gnshop_promoted1-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget2 = "mweb_gnshop_promoted2-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget3 = "mweb_gnshop_promoted3-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget4 = "mweb_gnshop_promoted4-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget5 = "mweb_gnshop_promoted5-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget6 = "mweb_gnshop_promoted6-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget7 = "mweb_gnshop_promoted7-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget8 = "mweb_gnshop_promoted8-21";
 	public static $amazon_UTM_CAMPAIGN_promowidget9 = "mweb_gnshop_promoted9-21";
 	public static $amazon_UTM_CAMPAIGN_todayhotdeal ="mweb_gnshop_hotdeals-21";
 	public static $amazon_UTM_CAMPAIGN_todaysdeal = "mweb_gnshop_spons-21";
 	public static $amazon_UTM_CAMPAIGN_banners = "mweb_gnshop_banner-21";
 	public static $amazon_UTM_CAMPAIGN_compare = "mweb_gnshop_compare_buyat-21";
 	public static $SGN_DOMAIN = "https://shop.gadgetsnow.com/";
 	
 	
 	static function createAffiliateUrl($affiliateName,$paramArray)
 	{
 		$url= $paramArray['producturl'];
 		$affiliatefunction="gadgetsintermediate";
 		
 		if(strpos($url,"paytm") !== false)
 			$affiliatefunction="paytm";
 		elseif(strpos($url,"amazon") !== false)
 			$affiliatefunction="amazon";
 		elseif(strpos($url,"flipkart") !== false)
 			$affiliatefunction="flipkart";
 		elseif(strpos($url,"tatacliq") !== false)
 			$affiliatefunction="tatacliq";
 		elseif(strpos($url,"gadgetsnow.com") !== false || strpos($url,"/") == 0)
 			$affiliatefunction="gadgetsnow";
		elseif(strpos($url,"2gud") !== false)
			$affiliatefunction="2gud";
		elseif(strpos($url,"nnnow") !== false)
			$affiliatefunction="nnnow";
        else
		{ 		
			if(isset($affiliateName) && $affiliateName !== null && $affiliateName!== '')
				$affiliateName =$affiliateName;
			else
 				$affiliateName= "gadgetsintermediate";
			//$affiliateName= "gadgetsintermediate";
 			$paramArray['affiliatename']=strtolower($affiliateName);
        }
        $paramArray['affiliatename']=strtolower($affiliateName);
		$affiliatefunction = strtolower($affiliatefunction);	
		if($affiliateName === "2gud" || $affiliateName === "moto" || $affiliateName === "gizmobitz" || $affiliateName === "acronis")
			$url = AffiliateUrlCreator::gadgetsintermediate($paramArray);
		else
			$url =AffiliateUrlCreator::$affiliatefunction($paramArray);
 
 		
 		return $url;
 	}
 	static  function tatacliq($params)
 	{
 	    $tatacliqUrl = $params['producturl'];
 	    $productname= $params['productname'];
 	    $price=$params['price'];
 	    $pagename=$params['pagename'];
 	    $widget=$params['widget'];
 	    
 	    $nameurl = str_replace(" ","_",$productname);
 	    $intermediateUrl = $GLOBALS['general']['TATA_CLIQ_URL'];
 	    //$intermediateUrl = "https://www.gadgetsnow.com/affiliatemweb_tatacliq.cms?url=";
 	    //$tatacliqUrl = str_replace("https://www.tatacliq.com","",$tatacliqUrl);
 	    //$tatacliqUrl = str_replace("http://www.tatacliq.com","",$tatacliqUrl);
 	    $tataIntcliqUrl = "http://tatacliqaffiliates.go2cloud.org/aff_c?offer_id=2&aff_id=1509&url=";
 	    if(strpos($tatacliqUrl,'?') === false)
 	    {
 	        $tatacliqUrl=$tatacliqUrl.'?cid=af:homepage:gadgetsnow:hasoffers:15032017';
 	    }else
 	    {
 	        $tatacliqUrl=$tatacliqUrl.'&cid=af:homepage:gadgetsnow:hasoffers:15032017';
 	    }
 	    $tag="tatacliq_mweb_gnshop".($pagename!==""?"_":"").$pagename.($widget!==""?"_":"").$widget;
 	    $utmcampaign="mweb_tatacliq".($pagename!==""?"_":"").$pagename.($widget!==""?"_":"").$widget;
 	    $tatacliqUrl = $intermediateUrl.rawurlencode($tataIntcliqUrl.rawurlencode($tatacliqUrl))."&tag=".$tag."&utm_campaign=".$utmcampaign."&title=".rawurlencode($nameurl)."&price=".$price."&utm_source=mweb_gnshop&utm_medium=Affiliate";
 	    return $tatacliqUrl;
 	}
 	 static function flipkart($params)
 	{
 		$flipkartUrl = $params['producturl'];
 		$productname= $params['productname'];
 		$price=$params['price'];
 		$pagename=$params['pagename'];
 		$widget=$params['widget'];
 		
 		$tag="flipkart_mweb_gnshop".($pagename!==""?"_":"").$pagename.($widget!==""?"_":"").$widget;
 		if(strpos($flipkartUrl,'?') === false)
 		{
 			$flipkartUrl = rawurlencode($flipkartUrl."?affid=tilltd"."&affExtParam2=".$tag)."&tag=".$tag."&affExtParam2=".$tag."&afname=flipkart&affiliate=flipkart&utm_source=mweb_gnshop&utm_medium=Affiliate&title=".rawurlencode($productname)."&price=".rawurlencode($price);
 		}else 
 		{
 			$flipkartUrl = rawurlencode($flipkartUrl."&affid=tilltd"."&affExtParam2=".$tag)."&tag=".$tag."&affExtParam2=".$tag."&afname=flipkart&affiliate=flipkart&utm_source=mweb_gnshop&utm_medium=Affiliate&title=".rawurlencode($productname)."&price=".rawurlencode($price);
 		}
 		$flipkartUrl=$GLOBALS['general']['AFFILIATE_INTERMEDIATE_URL']."?url=".$flipkartUrl;
 		
 		return $flipkartUrl;
 	}
 	 static function amazon($params)
 	{
 		$amazonUrl = $params['producturl'];
 		$productname= $params['productname'];
 		$tagname=$params['tagname'];
 		$price=$params['price'];
 		$pagename=$params['pagename'];
 		$widget=$params['widget'];
		$subafname=$params['affiliatename'];
		$tag='';
		$amazonUrl = str_replace("https://www.amazon.in","",$amazonUrl);
 		if(isset($productname))
 		{
 			$productname = str_replace("%","",$productname);
 		}
 		$amazonUrl = str_replace("http://www.amazon.in","",$amazonUrl);
 		if(strcasecmp($subafname,"uag")===0)
 		{
 			$intermediateUrl = $GLOBALS['general']['GADGETS_INTERMEDIATE_URL'];
 			$amazonUrl = $intermediateUrl."?url=".rawurlencode($amazonUrl);
 		}else {
	 		if(strpos($amazonUrl,"?") === false)
	 		{
	 			$amazonUrl =$amazonUrl."?tag=timofind-21";
	 		}else
	 		{
	 			$amazonUrl =$amazonUrl."&tag=timofind-21";
	 		}
	 		$intermediateUrl = $GLOBALS['general']['GADGETS_INTERMEDIATE_URL'];
	 		if($pagename === "articlepage")
	 			$utmcampaign="mweb_gnshop_bestdeals-21";
	 		else
	 		{
	 			if(isset(AffiliateUrlCreator::${"amazon_UTM_CAMPAIGN_".$widget}))
	 			{
	 				$utmcampaign = AffiliateUrlCreator::${"amazon_UTM_CAMPAIGN_".$widget};
	 				$tag=AffiliateUrlCreator::${"amazon_UTM_CAMPAIGN_".$widget};
	 			}
	 			else
	 			{
	 				$utmcampaign=AffiliateUrlCreator::$amazon_UTM_CAMPAIGN_promowidget;
	 			}
	 		}
	 		if(strcasecmp($tagname,'Amazon OnePlus Nord CE2 Affiliate CPC Feb')===0)
	 		{
	 		    $amazonUrl = $params['producturl'];
	 		    $amazonUrl=str_replace("https://www.amazon.in","",$amazonUrl);
	 		    if(strpos($amazonUrl,"?") === false)
	 		    {
	 		        $amazonUrl =$amazonUrl."?tag=gadgetsnow_amazon_cpc_oneplus-21";
	 		    }else
	 		    {
	 		        $amazonUrl =$amazonUrl."&tag=gadgetsnow_amazon_cpc_oneplus-21";
	 		    }
	 		    $amazonUrl = $intermediateUrl."?url=".rawurlencode($amazonUrl);
	 		}else {
	 		    $amazonUrl = $intermediateUrl."?url=".rawurlencode($amazonUrl)."&tag=timofindia-21"."&utm_campaign=".$utmcampaign."&title=".rawurlencode($productname)."&price=".$price."&utm_source=mweb_gnshop&utm_medium=Affiliate";
	 		}
 		}
 		return $amazonUrl;	
 	}
 	
 	static function paytm($params)
 	{
 		$paytmUrl = $params['producturl'];
 		$productname= $params['productname'];
 		$price=$params['price'];
 		$pagename=$params['pagename'];
 		$widget=$params['widget'];
 		
 		if(strpos($paytmUrl,"-pdp")!==false)
 		{
 			$paytmUrl = str_replace("-pdp","",$paytmUrl);
 			$paytmUrl = str_replace("https://paytmmall.com","https://paytm.com/shop/p", $paytmUrl);
 		}else
 		{
 			$paytmUrl = str_replace("https://paytmmall.com","", $paytmUrl);
 		}
 		if(strpos($paytmUrl,"?") !== false)
 		{
 			$paytmUrl = $paytmUrl."&";
 		}else {
 			$paytmUrl = $paytmUrl."?";
 		}
 		//$paytmUrl = rawurlencode($paytmUrl);
 		$putm="GadgetsNowShop_mweb".($pagename!==""?"_":"").$pagename.($widget!==""?"_":"").$widget;
 		$utmcampaign="gadgetsnow_feeds";
 		//"GadgetsNowShop_mweb".($pagename!==""?"_":"").$pagename.($widget!==""?"_":"").$widget;;
 		$content=$pagename !== ""?$pagename:($widget!==""?$widget:"pdp");
 		$utmkeword="GadgetsNowShop_mweb".($pagename!==""?"_":"").$pagename.($widget!==""?"_":"").$widget;;
 		$paytmUrl = $paytmUrl."&utm_name=cpc&utm_content=".$content."&utm_campaign=gadgetsnow_feed&utm_term=".
 		$utmkeword."&utm_source=Gadgetsnow&utm_medium=Affiliate&title=".rawurlencode($productname);
 		$paytmUrl = $GLOBALS['general']['PAYTM_INTERMEDIATE_URL']."?url=".rawurlencode($paytmUrl)."&putm=gadgetsnow_feeds";
 			
 	 	return $paytmUrl;
 	}
 	
 	static function samsung($params)
 	{
 		$samsungUrl = $params['producturl'];
 		$productname= $params['productname'];
 		$price=$params['price'];
 		$pagename=$params['pagename'];
 		$widget=$params['widget'];
 		
 		$nameurl = str_replace(" ","_",$productname);
 		$intermediateUrl = $GLOBALS['general']['AFFILIATE_INTERMEDIATE_URL']."?url=";
 		$tag="samsung_mweb_gnshop_".($pagename!==""?"_":"").$pagename.($widget!==""?"_":"").$widget;
 		$samsungUrl = $intermediateUrl.rawurlencode($samsungUrl)."&tag=".$tag."&afname=samsung&utm_medium=Affiliate"."&title=".rawurlencode($nameurl)."&price=".$price;
 		return $samsungUrl;	
 	}
 	static function gadgetsnow($params)
 	{
 		$gnurl = $params['producturl'];
 		return $gnurl;
 	}
 	static function saregama($params)
 	{
 		$url = $params['producturl'];
 		$affiliatename=$params['affiliatename'];
 		$productname= $params['productname'];
 		$price=$params['price'];
 		$pagename=$params['pagename'];
 		$widget=$params['widget'];
 		$intermediateUrl = $GLOBALS['general']['AFFILIATE_INTERMEDIATE_URL']."?url=";
 		$tag=$affiliatename."_mweb_gnshop_".($pagename!==""?"_":"").$pagename.($widget!==""?"_":"").$widget;
 		$finalurl = $intermediateUrl.rawurlencode($url)."&tag=".$tag."&afname=".$affiliatename."&utm_medium=Affiliate"."&title=".rawurlencode($productname)."&price=".$price;
 		return $finalurl;
 	}



 	static function nnnow($params)
 	{
 		$url = $params['producturl'];
 		$affiliatename=$params['affiliatename'];
 		if(strpos($url,"nnnow") !== false)
 		{
 			if(strpos($url,"?") !== false)
 				$url=$url."&affid=gadgetsnow&utm_source=affiliate&utm_medium=til&utm_campaign=gadgetsnowshop";
 			else
 				$url = $url ."?affid=gadgetsnow&utm_source=affiliate&utm_medium=til&utm_campaign=gadgetsnowshop";
 			$affiliatename='nnnow';
 		}
 		$productname= $params['productname'];
 		$price=$params['price'];
 		$pagename=$params['pagename'];
 		$widget=$params['widget'];
 		$intermediateUrl = $GLOBALS['general']['AFFILIATE_INTERMEDIATE_URL']."?url=";
 		$tag=$affiliatename."mweb_gnshop".($pagename!==""?"":"").$pagename.($widget!==""?"":"").$widget;
 		$finalurl = $intermediateUrl.rawurlencode($url)."&tag=".$tag."&afname=".$affiliatename."&utm_medium=Affiliate"."&title=".rawurlencode($productname)."&price=".$price;
 		return $finalurl;
 	}

	static function gadgetsintermediate($params)
 	{
 		$requrl=AffiliateUrlCreator::$SGN_DOMAIN.(isset($_REQUEST['_url'])?$_REQUEST['_url']:"/");
 		$url = $params['producturl'];
 		$affiliatename=$params['affiliatename'];
 		if(strpos($url,"2gud") !== false)
 		{
 			if(strpos($url,"?") !== false)
 				$url=$url."&affid=gadgetsnow";
 			else 
 				$url = $url ."?affid=gadgetsnow";
 			$affiliatename='2gud';
 			$tag=$affiliatename."_mweb_gnshop_".($pagename!==""?"":"").$pagename.($widget!==""?"":"").$widget;
 		}else if ($affiliatename === "acronis")
 		{
 			$pagename='pdp';$widget='buy_at';
 			if(isset($params['pagename']))
 				$pagename=$params['pagename'];
 			if(isset($params['widget']))
 				$widget=$params['widget'];
 			if(strpos($url,"?") !== false)
 				$url=$url."&tag=".$pagename.$widget;
 			else
 				$url = $url ."?tag=".$pagename.$widget;
 			$tag=$affiliatename."_mweb_gnshop_".($pagename!==""?"":"").$pagename.($widget!==""?"":"").$widget;
 		}else if ($affiliatename === "norton")
 		{
 			$widget=$params['widget'];
 			$url = $url ."?tag="."Norton_".$widget."_mweb_gnshop_recomproduct-21";
 			$tag="Norton_".$widget."_mweb_gnshop_recomproduct-21";
 		}else {
 			if(isset($params['pagename']))
 				$pagename=$params['pagename'];
 			if(isset($params['widget']))
 				$widget=$params['widget'];
 			$tag=$affiliatename."_mweb_gnshop_".(isset($pagename)?$pagename:"").'_'.(isset($widget)?$widget:"");
 		}
 		$productname= $params['productname'];
 		$price=$params['price'];
 		$pagename=$params['pagename'];
 		$widget=$params['widget'];
 		$intermediateUrl = $GLOBALS['general']['AFFILIATE_INTERMEDIATE_URL']."?url=";
 		
 		$finalurl = $intermediateUrl.rawurlencode($url)."&tag=".$tag."&afname=".$affiliatename."&utm_medium=Affiliate"."&title=".rawurlencode($productname)."&price=".$price;
 		return $finalurl;	
 	}




	
        static function gud($params)
        {
                $url = $params['producturl'];
                $affiliatename=$params['affiliatename'];
                $productname= $params['productname'];
                $price=$params['price'];
                $pagename=$params['pagename'];
                $widget=$params['widget'];
                $intermediateUrl = $GLOBALS['general']['AFFILIATE_INTERMEDIATE_URL']."?url=";
                $tag=$affiliatename."_mweb_gnshop_".($pagename!==""?"_":"").$pagename.($widget!==""?"_":"").$widget;
                $finalurl = $intermediateUrl.rawurlencode($url)."&tag=".$tag."&afname=".$affiliatename."&utm_medium=Affiliate"."&title=".rawurlencode($productname)."&price=".$price;
                return $finalurl;
        }

 }

?>
