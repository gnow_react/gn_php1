<?php
function issetFunc($value){
	return isset($value);
}
function handleStr($value){
	$result = "";
	if (isset($value) && gettype($value)=== "string" && strlen($value)>0){
		$result = $value;	
	}
	return $result;
}
function handleStrAndDefault($value, $defaultStr){
	$result = $defaultStr;
	if (isset($value) && gettype($value)=== "string" && strlen($value)>0){
		$result = $value;
	}
	return $result;
}

global $general;
$general = parse_ini_file("General.ini");

global $orderProp;
$orderProp = parse_ini_file("order.properties");

global $categoryMap;
$categoryMap = parse_ini_file("categoryMap.properties");

global $bankEmiProperties;
$bankEmiProperties = parse_ini_file("bankemi.properties");


global $gncategory;
$gncategory = parse_ini_file("GNCategory.properties");

global $configPriceFilter;
$configPriceFilter = parse_ini_file("/opt/categoryPriceurl.properties");
?>
