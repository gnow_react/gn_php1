<?php
class Utils {
	public static $_CONNECT_TIMEOUT = 5;
	public static $_RETURN_TRANSFER = 1;
	public static $_CONTENT_TYPE = Array (
			'Content-Type: application/json' 
	);
	protected function Initialize() {
		// self::$_CONNECTTIMEOUT = 5;
		// self::$_RETURNTRANSFER = 1;
	}
	public static  function  getXMLResponse($link)
	{
		try
		{
			$curl = curl_init ();
			curl_setopt($curl, CURLOPT_URL, $link);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt ( $curl, CURLOPT_TIMEOUT, 60 );
				
	
			$data = curl_exec ($curl);
			$code = curl_getinfo ( $curl, CURLINFO_HTTP_CODE );
			curl_close($curl);
			$data = simplexml_load_string($data);
			//	print_r( );die;
		}catch(Exception $ex)
		{
			$code="500";
			$data="Error occurred";
		}
		return $data;
	
	}
	public static function putMemcacheValue($key,$value)
	{
		$mem_var = new Memcache();
		$memcacheIp = $GLOBALS ['general'] ['MEMCACHE_IP'];
		$memcachePort = $GLOBALS ['general'] ['MEMCACHE_PORT'];
		$mem_var->addServer($memcacheIp, $memcachePort);
		$response = $mem_var->set($key,$value);
		
	}
	
	public static function getMemcaheValue($key)
	{
		$mem_var = new Memcache();
		$memcacheIp = $GLOBALS ['general'] ['MEMCACHE_IP'];
		$memcachePort = $GLOBALS ['general'] ['MEMCACHE_PORT'];
		$mem_var->addServer($memcacheIp, $memcachePort);
		$response = $mem_var->get($key);
		return $response;
	}
	
	public static function getNonSplStr($str) {
		$tempStr = $str;
		if (! isset ( $tempStr )) {
			return $tempStr;
		}
		$cleanStr = "";
		$pattern = "/^[ .a-zA-Z0-9]+$/";
		for($i = 0; $i < strlen ( $tempStr ); $i ++) {
			$charAt = substr ( $tempStr, $i, 1 ) . "";
			try {
				if (preg_match ( $pattern, $charAt )) {
					$cleanStr .= $charAt;
				}
			} catch ( \Exception $e ) {
				echo $e->getMessage ();
			}
		}
		$cleanStr = str_replace ( " ", "_", $cleanStr, $repCount );
		return $cleanStr;
	}
	public static function getApiResponse($url) {
		$result;
		try {
			$result = Utils::getPostCurl ( $url, "", Utils::$_CONTENT_TYPE, "", "", "", "" );
		} catch ( \Exception $e ) {
			echo "Error " . $e->getMessage () . " while getting data for: " . htmlentities($url);
		}
		return $result;
	}
	public static function getApiResponseWithinTime($url,$connectTimeout,$executeTimeout) {
		$result;
		try {
			$result = Utils::getPostCurl ( $url, "", Utils::$_CONTENT_TYPE, "", "",$connectTimeout,$executeTimeout );
		} catch ( \Exception $e ) {
			echo "Error " . $e->getMessage () . " while getting data for: " . htmlentities($url);
		}
		return $result;
	}
	
	public static function getApiSSLResponse($url) {
		$result;
		try {
			$result = Utils::getPostSSLCurl ( $url, "", Utils::$_CONTENT_TYPE, "", "" );
		} catch ( \Exception $e ) {
			echo "Error " . $e->getMessage () . " while getting data for: " . htmlentities($url);
		}
		return $result;
	}
	public static function getProtectedApiResponse($url, $requestType, $contetType, $payload, $usrPwd) {
		$result;
		try {
			$result = Utils::getPostCurl ( $url, $requestType, $contetType, $payload, $usrPwd, "", "" );
		} catch ( \Exception $e ) {
			echo "Error " . $e->getMessage() . " while getting data for: " . htmlentities($url);
		}
		return $result;
	}
	public static function getPostCurl($url, $requestType, $contetType, $payload, $usrPwd, $connectTimeout, $executeTimeout) {
		$data;
		$code;
		$curl = null;
		try {
			$curl = curl_init ();
			if (isset ( $requestType ) && $requestType === CURLOPT_POST) {
				curl_setopt ( $curl, CURLOPT_POST, 1 );
			}
			//file_put_contents("/opt/mfrontend/PHP_LOG.txt", "\n UTil ------ ".$url.$requestType,FILE_APPEND);
			curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
			if (! isset ( $contetType )) {
				$contetType = Utils::$_CONTENT_TYPE;
			}
			curl_setopt ( $curl, CURLOPT_HTTPHEADER, $contetType );
			if (isset ( $payload ) && $payload !== "") {
				curl_setopt ( $curl, CURLOPT_POSTFIELDS, $payload );
			}
			if (isset ( $usrPwd ) && $usrPwd !== "") {
				curl_setopt ( $curl, CURLOPT_USERPWD, $usrPwd );
			}
			curl_setopt ( $curl, CURLOPT_URL, $url );
			if (isset ( $connectTimeout ) && $connectTimeout !== "") {
				curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, $connectTimeout );
			}else{
				curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 0 );
			}
			
			if (isset ( $executeTimeout ) && $executeTimeout !== "") {
				curl_setopt ( $curl, CURLOPT_TIMEOUT, $executeTimeout );
			}else{
				curl_setopt ( $curl, CURLOPT_TIMEOUT, 60 );
			}			
			$data = curl_exec ( $curl );
//file_put_contents("/opt/mfrontend/PHP_LOG.txt", "\n UTil ------ ".$data,FILE_APPEND);
                        //			 echo "<br/>Data: ".$data;
			$code = curl_getinfo ( $curl, CURLINFO_HTTP_CODE );
			curl_close ( $curl );
		} catch ( \Exception $e ) {
			echo $e->getMessage();
		}
		return array (
				"code" => $code,
				"data" => $data 
		);
	}
	public static function executeMultipartRequest($url,$requestType,$payload)
	{
		$curl = curl_init ();
		curl_setopt ( $curl, CURLOPT_POST, 1 );
			
		$contetType = 	Array (
				'Content-Type: multipart/form-data'
		);
		curl_setopt ( $curl, CURLOPT_HTTPHEADER, $contetType );
		
		curl_setopt ( $curl, CURLOPT_POSTFIELDS, $payload);
		
			
		curl_setopt ( $curl, CURLOPT_URL, $url );
		curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 0 );
		curl_setopt ( $curl, CURLOPT_TIMEOUT, 60 );
		curl_setopt($curl, CURLOPT_FAILONERROR, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		
		$data = curl_exec ( $curl );
		$code = curl_getinfo ( $curl, CURLINFO_HTTP_CODE );
		curl_close ( $curl );
		return array (
				"code" => $code,
				"data" => $data
		);
	}
	public static function getPostSSLCurl($url, $requestType, $contetType, $payload, $usrPwd) {
		$data;
		$code;
		$curl = null;
		try {
			$curl = curl_init ();
			if (isset ( $requestType ) && $requestType === CURLOPT_POST) {
				curl_setopt ( $curl, CURLOPT_POST, 1 );
			}
			
			curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
			if (! isset ( $contetType )) {
				$contetType = Utils::$_CONTENT_TYPE;
			}
			curl_setopt ( $curl, CURLOPT_HTTPHEADER, $contetType );
			if (isset ( $payload ) && $payload !== "") {
				curl_setopt ( $curl, CURLOPT_POSTFIELDS, $payload );
			}
			if (isset ( $usrPwd ) && $usrPwd !== "") {
				curl_setopt ( $curl, CURLOPT_USERPWD, $usrPwd );
			}
			curl_setopt ( $curl, CURLOPT_URL, $url );
			curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 0 );
			curl_setopt ( $curl, CURLOPT_TIMEOUT, 60 );
			curl_setopt($curl, CURLOPT_FAILONERROR, true);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				
			$data = curl_exec ( $curl );
	//		echo "<br/>Data: ".$data;
			$code = curl_getinfo ( $curl, CURLINFO_HTTP_CODE );
			curl_close ( $curl );
		} catch ( \Exception $e ) {
			echo $e->getMessage();
		}
		return array (
				"code" => $code,
				"data" => $data
		);
	}
	
	public static function Cast(&$destination, stdClass $source) {
		$sourceReflection = new \ReflectionObject($source);
		$sourceProperties = $sourceReflection->getProperties();
		foreach ($sourceProperties as $sourceProperty) {
			$name = $sourceProperty->getName();
			if (gettype($destination->{$name}) == "object" && get_class($source->$name)=='stdClass') {
				self::Cast($destination->{$name}, $source->$name);
			} else {
				$destination->{$name} = $source->$name;
			}
		}
	}
	
	public static function RemoveDQoutes($tempVal) {
		if ($tempVal != null) {
			$tempVal = str_replace ( "\"", "", $tempVal );
			$tempVal = str_replace ( "%22", "", $tempVal );
			$tempVal = urldecode($tempVal);
		}
		return $tempVal;
	}
	
	public static function secureUrls($associativeArr) {
		if ($associativeArr != null) {
			foreach ( $associativeArr as $key => $productImgUrl ) {
				$tempImgUrl = str_replace( "http://", "https://", $productImgUrl );
				$associativeArr [$key] = $tempImgUrl;
}
		}
		return $associativeArr;
	}
	public static function secureStrUrls($strToBeSecure) {
		if ($strToBeSecure != null) {
				$tempImgUrl = str_replace( "http://", "https://", $strToBeSecure );
				return $tempImgUrl;
		}
		return $strToBeSecure;
	}
	
	public static function getSecuritytoken()
	{
		$milliseconds = round(microtime(true) * 1000);
		$stagger = intval (round(rand() * $milliseconds) ) ;
		return $stagger;
	}
	
	public static function RemoveBS($Str) {
		$StrArr = str_split($Str); $NewStr = '';
		foreach ($StrArr as $Char) {
			$CharNo = ord($Char);
			if ($CharNo == 163) { $NewStr .= $Char; continue; } // keep �
			if ($CharNo > 31 && $CharNo < 127) {
				$NewStr .= $Char;
			}
		}
		return $NewStr;
	}
	public static function multiRequest($data, $options = array()) {
		// array of curl handles
		$curly = array();
		// data to be returned
		$result = array();
		try{	  
			// multi handle
			$mh = curl_multi_init();
		
			// loop through $data and create curl handles
			// then add them to the multi-handle
			foreach ($data as $id => $d) {
		
			$curly[$id] = curl_init();
		
			$url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
			curl_setopt($curly[$id], CURLOPT_URL,            $url);
			curl_setopt($curly[$id], CURLOPT_HEADER,         0);
			curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
		
			// post?
			if (is_array($d)) {
				if (!empty($d['post'])) {
				curl_setopt($curly[$id], CURLOPT_POST,       1);
				curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
				}
			}
		
			// extra options?
			if (!empty($options)) {
				curl_setopt_array($curly[$id], $options);
			}
		
			curl_multi_add_handle($mh, $curly[$id]);
			}
		
			// execute the handles
			$running = null;
			do {
			curl_multi_exec($mh, $running);
			} while($running > 0);
		
		
			// get content and remove handles
			foreach($curly as $id => $c) {
			$result[$id] = json_decode(curl_multi_getcontent($c), true);
			curl_multi_remove_handle($mh, $c);
			}	  
			// all done
			curl_multi_close($mh);	  
		} catch ( \Exception $e ) {
			echo "Error " . $e->getMessage() . " while getting data for: " . htmlentities($url);
		}
		return $result;
	  }
	  
	  public static function getGadgetCategory($categorySeo) {
		$gadgetCategories = [
			"mobile-phones" => [
			  'id' => 'mobile',
			  'name'=> 'Mobile Phones',
			  'seoName'=> 'mobile-phones',
			],
			"laptops" => [
			  'id'=> 'laptop',
			  'name' => 'Laptops',
			  'seoName' => 'laptops',
			],
			"tablets" => [
				'id' => 'tablet',
				'name'=> 'Tablet',
				'seoName'=> 'tablets',
			],
			"cameras" => [
				'id'=> 'camera',
				'name' => 'Cameras',
				'seoName' => 'cameras',
			],
			"television" => [
				'id'=> 'tv',
				'name' => 'Televisions',
				'seoName' => 'television',
			],
			"powerbank" => [
				'id' => 'powerbank',
				'name'=> 'Powerbanks',
				'seoName'=> 'powerbank',
			],
			"smartwatch" => [
				'id'=> 'smartwatch',
				'name' => 'Smartwatchs',
				'seoName' => 'smartwatch',
			],
			"ac" => [
				'id'=> 'ac',
				'name' => 'Air Conditioners',
				'seoName' => 'ac',
			],
			"washingmachine" => [
				'id'=> 'washingmachine',
				'name' => 'Washing Machines',
				'seoName' => 'washingmachine',
			],
			"refrigerator" => [
				'id' => 'refrigerator',
				'name'=> 'Refrigerator',
				'seoName'=> 'refrigerator',
			],
			"fitnessband" => [
				'id'=> 'fitnessband',
				'name' => 'Fitnessbands',
				'seoName' => 'fitnessband',
			]
		];
		if ( isset($gadgetCategories[$categorySeo]) ){
			return $gadgetCategories[$categorySeo];
		}
		return $gadgetCategories;
	  }
	  
}
?>
