<?php
$router = new \Phalcon\Mvc\Router ();


$router->add ( "/([a-zA-Z0-9-+_()'.]*)/([a-zA-Z0-9-]*)/alternatives", array (
    "controller" => "gadgetsnow",
    "action" => "relatedpages",
    "productId" => 2,
    "category" => 1
) );
$router->add ( "/((?!control|info|gadgetsBrand|compare)[a-zA-Z0-9-+_()'.]*)/([a-zA-Z0-9-]*)", array (
    "controller" => "gadgetsnow",
    "action" => "gadgetshow",
    "productId" => 2,
    "category" => 1
) );

$router->add ( "/affiliate/([a-zA-Z0-9-+_()'.]*)/([a-zA-Z0-9-+_()'.]*)/([a-zA-Z0-9-]*)", array (
    "controller" => "gadgetsnow",
    "action" => "gadgetsaffiliate",
    "productId" => 3,
    "category" => 2,
	"widgettype" => 1,
) );

$router->add ( "/sso.cms", array (
    "controller" => "gadgetsnow",
    "action" => "sso"
));
$router->add ( "/healthcheck", array (
    "controller" => "gadgetsnow",
    "action" => "healthcheck"
));
$router->add ( "/affiliate/COUNTER/([a-zA-Z0-9-+_()'.]*)/([a-zA-Z0-9-]*)", array (
    "controller" => "gadgetsnow",
    "action" => "counter",
    "productId" => 3,
    "category" => 2,
	"widgettype" => 1,
) );
//  $router->add ( "/brand-store", array (
//  		"controller" => "miscellaneous",
//  		"action" => "brandstore",
//  		"pagename"=>"brandstore"
//  ));
return $router;

