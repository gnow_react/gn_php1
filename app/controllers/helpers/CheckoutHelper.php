<?php
// include __DIR__ . "/../../config/Utils.php";
class CheckoutHelper {
	public static function checkout($thisObj) 
	{
		$thisObj->view->setVar("leafTitle","checkout");
		if (! isset ( $_SESSION ))
			session_start ();
		
		$grci = "";
		if (isset($_SESSION['guestCheckedIn'])){$grci = $_SESSION['guestCheckedIn']; }
		
		if (isset ( $_SESSION ['userLogin'] ) && (($_SESSION ['userLogin']->getUserName()!='Guest') || ($grci === 'guestReCheckedIn'))) {
			unset($_SESSION['guestCheckedIn']);
			if (isset ( $_SESSION ['shoppingCart'] ) && sizeof ( $_SESSION ['shoppingCart']->getShoppingList()->getShoppingListItems () ) > 0) {
				$responsearray=CheckoutHelper::inventoryCheckOnCart($_SESSION ['shoppingCart']);
				$redirect = $responsearray['redirect'];
				$message = $responsearray['message'];
				if(isset($message) && $message !=""){
					$_SESSION['priceChangeMessage']=$message;
				}
				$thisObj->view->setVar ( "priceChangeMessage", $message );
				if($redirect!=""){
					header ( "location:".$redirect );exit();
				}else{
					if($_SESSION ['userLogin']->getUserName()!='Guest'){
						$partyId = $_SESSION ['userLogin']->getPartyId ();
						$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
						$url = $serverUrl . "/rest/party/address/view?partyId=" . $partyId;
						$result = Utils::getProtectedApiResponse ( $url, null, null, null, null );
						if (issetFunc ( $result )) {
							$addressJson = json_decode ( $result ['data'] );
							$thisObj->view->setVar ( "addressArray", $addressJson );
						}
					}
				}
			} else {
				header ( "location:/control/showcart" );
			}
		} else {
			header ( "location:/control/login" );
		}
	}
	
	private static function inventoryCheckOnCart($cart){
		$redirect="";
		$isOOS="NO";
		$message = "";
		$data="{'shoppingCart':".json_encode($cart)."}";
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$url = $serverUrl . "/rest/inventory/checkInventoryForCart";
//		print_r("URL::".$url."==params==".$data);
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		
		if(isset($result)){
			$invJson = json_decode($result['data']);
			if(isset($invJson->responseMap->priceChangeProducts))
			{
				$message = $invJson->responseMap->priceChangeProducts;
			}
			/*Cart fromserver comes only if price is changed
			 *  overwrite cart
			 */  
			if(isset($invJson->responseMap->cart))
			{
				$newcart = $invJson->responseMap->cart;
				$stdobject_ = (object)$newcart;
				
				$userCart=new ShoppingCart();
				Utils::Cast($userCart, $stdobject_);
				$shoppingList = $userCart->getShoppingList();
				$cart = $_SESSION['shoppingCart'];
				$cart->setShoppingList($shoppingList);
				$_SESSION['shoppingCart'] = $cart;
			} 
			if(isset($invJson->responseMap->status) && $invJson->responseMap->status==='success' && $invJson->responseMap->flag==="false")
			{
				$_SESSION['error']=implode ( "<br>", $invJson->responseMap->errorLog );
				//$redirect="/control/showcart";
			}else if(isset($invJson->responseMap->status) && $invJson->responseMap->status==='error'){
				$_SESSION['error']=$invJson->responseMap->message;
				//$redirect="/control/showcart";
			}
		}
		return array (
				"redirect" => $redirect,
				"message" => $message,
		);
	}
	
	public static function createCustomerCheckout() {
		
		$shipAddress = array ();
		$redirect = "";
		$shippingContactMechId="";
		if (! isset ( $_SESSION ))
			session_start ();
		$allowedpayMentOption = array();
		if (! isset ( $_SESSION ['userLogin'] )) {
			$redirect = "/control/login";
		} else if (!isset($_SESSION['shoppingCart'])) {
			$redirect = "/control/showcart";
			
		}else if (isset ( $_POST ['address'] ) && $_POST ['address'] != "") { 
			$productId = array ();
			$servicableError = "";
			$ServiceResult = array ();
			$addressLine=$_POST['address'];
			$remove_character = array("\n", "\r\n", "\r");
			$addressLine = str_replace($remove_character , ' ', $addressLine);
			$shipAddress = array (
					"toName" => $_POST ['name'],
					"address1" => $addressLine,
					"city" => $_POST ['city'],
					"state" => $_POST ['state'],
					"postalCode" => $_POST ['pincode'],
					"contactMobile" => $_POST ['contactNo'] 
			);
//print_r($shipAddress);die;
			if (isset ( $_POST ['shiptoadd'] ) && $_POST ['shiptoadd'] != "") {
				$shippingContactMechId=$_POST ['shiptoadd'];
				$shipAddress ['contactMechId'] = $shippingContactMechId;
				$shipAddress ['shippingContactMechId'] = $shippingContactMechId;
				$_SESSION ['shoppingCart']->setShippingAddress( $shipAddress );
				$_SESSION ['shoppingCart']->setShippingContactMechId($shippingContactMechId);
				$redirect = "/control/checkoutPayment";
			
			} else { 
				$data="";
				$attname="";
					
				$attnName1="";
				$attnname = "guest";
				if($_SESSION["userLogin"]->getUserName()=='Guest'){
					if(isset ( $_POST ['name'] ) && $_POST ['name'] != ""){
						$attname=$_POST ['name'];
					}else{
						$attname=$_POST ['yourname'];
					}
					$attnName1="guest";
					$data='"YOUR_NAME":"' . $_POST ['yourname'] . '",';
				}else{
					$attnName1=$_POST['attnname'];
					$attname=$_POST ['name'];
				}
				/* Validating GC and servicebility before address creation, as discussed */
						$isValidGc=true;
						
								$redirect = "/control/checkoutPayment";
								$data = $data . '"partyId":"' . $_SESSION ['userLogin']->getPartyId () . '","contactMechTypeId":"POSTAL_ADDRESS",
								"TO_NAME":"' . $attname . '" ,"ATTN_NAME":"' . $attnName1 . '","ADDRESS1":"' . $addressLine . '",
								"CITY":"' . $_POST ['city'] . '", "STATE_PROVINCE_GEO_ID":"' . $_POST ['state'] . '" ,"COUNTRY_GEO_ID": "IND",
								"POSTAL_CODE":"' . $_POST ['pincode'] . '","CONTACT_MOBILE":"' . $_POST ['mobileno'] . '"';
								
								//DSR-29561 , 'addredit' will be set only when address is edited DSR-29561
								if(isset($_POST ['addredit']) && strlen($_POST ['addredit']) > 0){
									$data = $data . ',"curContactMechId":"' . $_POST ['addredit'] . '"';
									$data = "{" . $data . "}";
									$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
									$url = $serverUrl . "/rest/party/address/edit";
									$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );	
									if (isset( $result )) {
										$addressJson = json_decode ( $result ['data'] );
										if (isset ($addressJson->responseMap->status ) && $addressJson->responseMap->status === "success") {
											$shippingContactMechId = $addressJson->responseMap->contactMechId;
											$_SESSION ['shoppingCart']->setShippingContactMechId ( $shippingContactMechId );
											$shipAddress ['contactMechId'] = $shippingContactMechId;
											$shipAddress ['shippingContactMechId'] = $shippingContactMechId;
											$_SESSION ['shoppingCart']->setShippingAddress ( $shipAddress );
										} else {
											$redirect = "/control/checkout";
											if(isset($addressJson->responseMap->message)){
												$addressError = trim($addressJson->responseMap->message);
												if (strpos($addressError, "ADDRESS NOT UPDATED SUCCESSFULLY: ADDRESS IS SAME AS OLD ADDRESS" ) !== false || strcmp ( $addressError, "ADDRESS NOT UPDATED SUCCESSFULLY: ADDRESS IS SAME AS OLD ADDRESS " ) !== false) {
													$redirect = "/control/checkoutPayment";
												}else{
													$_SESSION ['error'] = 'Error: ' .$addressError;
												}
											}else{
												$_SESSION ['error'] = 'Some error has occured. Please try again.';
											}
										}
									}
										
									
								} else {
									$data = "{" . $data . "}";
									$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
									$url = $serverUrl . "/rest/party/address/create";
									$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
									//print_r($url);print_r($data);print_r($result);die;
									if (issetFunc ( $result )) {
										$addressJson = json_decode ( $result ['data'] );
										if (isset ( $addressJson->responseMap->status ) && $addressJson->responseMap->status === "success") {
											$shippingContactMechId = $addressJson->responseMap->contactMechId;
											$_SESSION ['shoppingCart']->setShippingContactMechId ( $shippingContactMechId );
								
											$shipAddress ['contactMechId'] = $shippingContactMechId;
											$shipAddress ['shippingContactMechId'] = $shippingContactMechId;
								
											$_SESSION ['shoppingCart']->setShippingAddress ( $shipAddress );
										} else {
											$_SESSION ['error'] = 'Error: ';
											$addressError ='';
											if (isset ( $addressJson->responseMap->message )) {
												$addressError = trim($addressJson->responseMap->message);
												if (strpos ( $addressError, "ATTN_NAME should be unique" ) !== false || strcmp ( $addressError, "ATTN_NAME should be unique" ) !== false) {
													$addressError= 'Duplicate Address';
												}
												$_SESSION ['error'] = 'Error: ' .$addressError;
											}
											$redirect = "/control/showcart";
										}
									}
								}
						//	}
						//}
			}
		} else {
			$redirect = "/control/showcart";
			
		} 	
		if($redirect === "/control/checkoutPayment" )
		{
			$tempemail="";
			$isGuest="NO";
			if(isset($_SESSION ['userLogin']))
			{
				$tempemail=$_SESSION ['userLogin']->getUserLoginId();
			}
			if($_SESSION["userLogin"]->getUserName()=='Guest'){
				$tempemail = $_SESSION["userLogin"]->getTemp();
				$_SESSION ['shoppingCart']->getUserLogin()->setTemp($tempemail);
				$isGuest="YES";
			}
			$shoppingCart=$_SESSION ['shoppingCart'];
		 	$shipAddress= $shoppingCart->getShippingAddress();
			 $mobileNo="";
			 if(isset($shipAddress) && sizeof($shipAddress)>0)
			{
				$mobileNo = $shipAddress['contactMobile'];
			} 
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			
			$isValidGc=true;
			if(!$_SESSION ['shoppingCart']->getAutoGiftCouponUsed() && $_SESSION ['shoppingCart']->getGiftCoupons()!=null && $_SESSION ['shoppingCart']->getGiftCoupons()!=""){
				$isValidGc=CheckoutHelper::gcValidation($_SESSION ['shoppingCart']);
				if(!$isValidGc){
					$redirect = "/control/checkout";
				}
			}
			if($isValidGc){
				$ServiceResult = CheckoutHelper::checkCartDeliveryLocation ( $_POST ['pincode'] );
				if (isset ( $ServiceResult ) && isset ( $ServiceResult ['message'] )) {
					$servicableError = $ServiceResult ['message'];
				}
				if (isset ( $ServiceResult ) && isset ( $ServiceResult ['productId'] )) {
					$productId = $ServiceResult ['productId'];
				}
					
				if ($servicableError != "") {
					$redirect = "/control/checkout";
					$_SESSION ['error'] = $servicableError;
				} else {
					$redirect = "/control/checkoutPayment";
				}
			}
			$allowedpayMentOption =	CheckoutHelper::getPaymentOptionCheckForShoppingCart($productId,$_POST ['pincode'],$_SESSION ['shoppingCart']->getAllowedZipCodeMode(),$tempemail,$isGuest,$mobileNo);
			$Searchkey = array_search('EXT_COD', $allowedpayMentOption);
			
			$shoppingCart=$_SESSION ['shoppingCart'];
			
			$paymodesFromGC = $shoppingCart->getPaymentModes();
				
			$gcPaymodesKey = array_search('cod', $paymodesFromGC);
			
			if((isset($paymodesFromGC) && count($paymodesFromGC)) && (!isset($gcPaymodesKey) || $gcPaymodesKey == ""))
			{
				$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD"]);
				$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD_CARD_WALLET"]);
				$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD_WALLET"]);
				$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD_CARD"]);
				
			} else
			{
				$Searchkey = array_search('EXT_COD', $allowedpayMentOption);
			}
			$_SESSION ['allowedpayMentOption'] = $allowedpayMentOption; 
		} 
		header ( "location:" . $redirect );
	}
	private static function checkCartDeliveryLocation($pincode) { 
		$returnArray = array ();
		$ServiceResult = array ();
		$productId = array();
		$message = "";
		$grandTotal = 0;
		$productCodCharges = array();
		$totalCodCharges = 0;
		$amountTocollect = 0;
		if (! isset ( $_SESSION ))
			session_start ();
		
		if (isset ( $_SESSION ['shoppingCart'] )) {
			$shoppingCart = $_SESSION ['shoppingCart'];
			$userLogin = $_SESSION ['userLogin'];
			
			$data = '{ 
				"userLoginId":"' . $userLogin->getUserLoginId () . '",
				"shoppingCart":'.json_encode($shoppingCart).'}';
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$url = $serverUrl. "/rest/pmt/calculateCod";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
			if (isset( $result ) &&  isset($result['code']) && $result['code']===200 ) {
				$data=json_decode($result['data'],true);
				if(isset($data['responseMap']['status']) && "success"===$data['responseMap']['status'] && isset($data['responseMap']['COD_CALCULATION']))
				{
					
					$productCodCharges = $data['responseMap']['COD_CALCULATION'];
				}
			}
			$shoppingCartItems = $shoppingCart->getShoppingList()->getShoppingListItems ();
			if (sizeof ( $shoppingCartItems ) > 0) {
				$productInfo = array ();
				$cartProduct = array ();
				$zipCodePermit = array ();
				foreach ( $shoppingCartItems as $key => $productObj ) {
					$item=new ShoppingListItem();
					if (get_class($productObj)=='stdClass') {
						Utils::Cast($item, $productObj);
					}else{
						$item=$productObj;
					}	
						
					if ($item->getProductId () != "") {
						$codCharges = 0;
						if(isset($productCodCharges[$item->getProductId ()]))
						{
							$codCharges = $productCodCharges[$item->getProductId ()];
							$item->setCodCharges($codCharges);
							$totalCodCharges = $totalCodCharges+$codCharges;
						}
						$productInfo [$key] = "{'productId':'" . $item->getProductId () . "','quantity':'" . $item->getQuantity () . "','codCarges':'".$codCharges."'}";
						array_push ( $productId, $item->getProductId() );
						$cartProduct [$item->getProductId ()] = $item->getProductName();
						if ($item->getBasePrice () > 0) {
							$priceToCalc = $item->getBasePrice ();
						} else {
							$priceToCalc = $item->getListPrice ();
						}
						$itemAmount = ($priceToCalc + $item->getShippingPrice ()) * $item->getQuantity ();
						$grandTotal = $grandTotal + $itemAmount;
						if (null!==$item->getGcDiscount()) {
							$itemAmount = $itemAmount - $item->getGcDiscount();
						}
						$amountTocollect = $amountTocollect+$itemAmount;
					}
				}
				$shoppingCart->setAmountToCollect($amountTocollect );
				$shoppingCart->setGrandTotal($grandTotal );
				//$shoppingCart->setCodCharges($totalCodCharges );
				$shoppingCart->setCodCharges(0 );
									
				if (sizeof ( $productInfo ) > 0) {
					$data = "{'pinNumber':'" . trim ( $pincode ) . "','productInfo':[" . implode ( ",", $productInfo ) . "]}";
					$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
					$url = $serverUrl . "/rest/product/checkDeliveryLocation";
					$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
					if (isset ( $result )) {
						$json = json_decode ( $result ['data'] );
						$commonZipcodes=array();
						// Oopss!! Disabled Item(s) cannot be shipped to your specified shipping address. Remove Disabled item(s) or Change your shipping address.
						if (isset ( $json->status ) && $json->status == "SUCCESS") {
							
							$zipCodePermit = array ("POSTPAID","PREPAID");
							$zipArray;
							$expressFlag = $json->isExpressService;
							$_SESSION['expressFlag'] = $expressFlag;
							foreach ( $json->productInfo as $a ) {
								if ($a->message == "Not Servicable" || $a->message == "") {
									$prodName = substr ( $cartProduct [$a->productId], 0, 20 ) . "...";
									$message = "We can't ship the product ".$prodName." to the pincode ".$pincode." you've entered. Please try again from a different location.";
								} elseif (isset($a->maxPermitQuantity) && $a->maxPermitQuantity < $a->quantity) {	
									$prodName = substr ( $cartProduct [$a->productId], 0, 20 ) . "...";
									$message = "We can't ship the product ".$prodName." to the pincode ".$pincode." you've entered. Please try again from a different location.";
								} elseif(isset($a->zipCodeTypeList) && (in_array("POSTPAID",$a->zipCodeTypeList) && count($a->zipCodeTypeList) === 1 )) {
									
									$prodName = substr ( $cartProduct [$a->productId], 0, 20 ) . "...";
									$message = "We can't ship the product ".$prodName." to the pincode ".$pincode." you've entered. Please try again from a different location. PREPAID servicebility not present";
								}
								elseif(isset($a->zipCodeTypeList) && ( in_array("POSTPAID",$a->zipCodeTypeList) &&  count($a->zipCodeTypeList) === 1 )) {
									
									$prodName = substr ( $cartProduct [$a->productId], 0, 20 ) . "...";
									$message = "We can't ship the product ".$prodName." to the pincode ".$pincode." you've entered. Please try again from a different location. PREPAID servicebility not present";
								}
								print_r('chhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh');
								print_r($a->zipCodeTypeList);
								print_r($a);
								//die;
								$zipArray = null;
								array_push($zipArray,$a->zipCodeTypeList);
								$tempPrevious = null;
								$temp =$a->zipCodeTypeList;
								$commonZipcodes=$temp;//$zipArray[$i];
								//$temp = $zipArray[$i];
								if(isset($tempPrevious))
								{
									$commonZipcodes = array_intersect($tempPrevious,$temp);
								}
								$tempPrevious = $temp;
							}
							}
						} else {
							$message = "Error: Please Try Again!!";
						}
				}
			 $_SESSION ['shoppingCart']->setAllowedZipCodeMode($commonZipcodes);
			}
		}
		$returnArray = array (
				'message' => $message,
				'productId' => $productId 
		);
		return $returnArray;
	}
	
	private static function gcValidation($cart) {
		if (! isset ( $_SESSION )) session_start ();
		$shipAddress=$cart->getShippingAddress();
		$shippingContactMechId=$cart->getShippingContactMechId();
		$isValidGc=false;
		$data=json_encode($cart);
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$url = $serverUrl . "/rest/gcws/validateGC";
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		
		if(isset($result)){
			$gcValidation=json_decode($result['data']);
			if(isset($gcValidation->status) && $gcValidation->status=='success'){
				$isValidGc=true;
				$gcValues=array();
				$newCart=$gcValidation->cart;
				$stdobject_ = (object)$newCart;
					
				$gcCart=new ShoppingCart();
				if (get_class($stdobject_)=='stdClass') {
					Utils::Cast($gcCart, $stdobject_);
				}else{
					$gcCart=$stdobject_;
				}
				$gcCart->setShippingAddress($shipAddress);
				$gcCart->setShippingContactMechId($shippingContactMechId);
				if(isset($_SESSION ['userLogin']) && $_SESSION ['userLogin']->getUserName()=='Guest') { $gcCart->setUserLogin($_SESSION ['userLogin']); }
				if($gcCart->getOrderId()!=null && $gcCart->getOrderId()!=""){$gcCart->setOrderId("");}
				$_SESSION ['shoppingCart']=$gcCart;
			}else{
				$_SESSION ['error']=$gcValidation->message;

				CheckoutHelper::ClearGc($cart);
			}
		}else{
			$_SESSION ['error']="Error: Please check the coupon code ".$cart->getGiftCoupons();
			CheckoutHelper::ClearGc($cart);
		}
		return $isValidGc;
	}
	
	public static function getPaymentOptionCheckForShoppingCart($productId,$pincode,$allowedPayMode,$userId,$isGuest,$mobileNo)
	{
		$allowedpayMentOption = array();
		if (sizeof ( $productId ) > 0) {
			$data = "{'pinNumber':'" . trim ( $pincode ) . "','productIdList':[" . implode ( ",", $productId ) . "],'allowedPaymode':[" . implode ( ",", $allowedPayMode ) . "],'userId':'".$userId."','isGuest':'".$isGuest."' ,'mobileNo':'".$mobileNo."'}";
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$oldServerUrl   = $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
			$url = $serverUrl . "/rest/product/paymentOptionCheckForShoppingCart";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
			print_r($data);print_r($url);print_r($result);
			if (isset ( $result )) {
				$json = json_decode ( $result ['data'] );
				if (isset ( $json->status ) && $json->status == "SUCCESS") {
					foreach ( $json->paymentInfo  as $key =>$Payitem ) {
						if($key === 0)
						{
							$allowedpayMentOption = $Payitem->payMentOption;
						}else {
							$allowedpayMentOption = array_intersect ( $allowedpayMentOption,$Payitem->payMentOption);
						}
					}
					if(isset($json->SHOW_OTP))
					{
						$allowedpayMentOption['SHOW_OTP']=$json->SHOW_OTP;
						$allowedpayMentOption['SHOW_COD']=$json->SHOW_COD;
					}
				}
			
			}

			//logic to exclude COD from allowed payment options for Registration campaign REG500
			$gcapplied= "";
			$gcapplied = $_SESSION ['shoppingCart']->getGiftCoupons();
			if($gcapplied == 'REG500'){
				if(false !== array_search("EXT_COD",$allowedpayMentOption)) {
					$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD"]);
				}
			}	
			
			if(false !== array_search("EXT_COD",$allowedpayMentOption)) {
			$url = $serverUrl . "/rest/product/filterByShoppingCartCODRules";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
echo"<BR><BR>";			print_r($data);
			if (isset ( $result )) {
				$json =  json_decode($result ['data']) ;
				if (isset ( $json->status ) && $json->status == "ERROR" && ($json->TIL_CREDIT_LIMIT_ERROR || $json->MERCHANT_CREDIT_LIMIT_ERROR)) {
					$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD"]);
					$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD_WALLET"]);
					$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD_CARD_WALLET"]);
					$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD_CARD"]);
				}
				
			}
			}
			
		}
		print_r($url);print_r($result);
		print_r($allowedpayMentOption);
//		die;
		return $allowedpayMentOption;
	}
	
	public static function getPaymentGatewayDetail($userId)
	{
		$allowedpaymentGateway = "";
			$data = "{'userId':'" . $userId . "'}";
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$oldServerUrl   = $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
			$url = $serverUrl . "/rest/pmt/getPaymentGatewayConfig";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
			if (isset ( $result )) {
				$json = json_decode ( $result ['data'] );
				if (isset ( $json->status ) && $json->status == "success") {
					
					if(isset($json->paymentGateway))
					{
						$allowedpaymentGateway=$json->paymentGateway;
					}
				}
					
			}
	
		return $allowedpaymentGateway;
	
	}
	
	public static function getCityStateFromZipCode() {
		$data = "{'zipCode':'" . $_GET ['postalCode'] . "'}";
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$url = $serverUrl . "/rest/party/address/getCityStateFromZipCode";
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		print_r ( $result ['data'] );
	}
	public static function checkoutPayment($thisObj) {
		$thisObj->view->setVar("leafTitle","payment");
		$staticUrl = $GLOBALS ['general'] ['STATIC_IMG_PROTOCOL_LESS_URL'];
		$protocol = $GLOBALS ['general'] ['SECURE_PROTOCOL'];
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		
		
		
		$thisObj->view->setVar ( "staticUrl", $protocol.$staticUrl );
		if (! isset ( $_SESSION ))
			session_start ();
		if(!isset( $_SESSION['shoppingCart']) || !isset( $_SESSION['userLogin'])){
			header ( "location:/control/showcart" );
		}else if(sizeof($_SESSION['shoppingCart']->getShippingAddress())===0){
			header ( "location:/control/showcart" );
		}else{
			
			$responsearray = CheckoutHelper::inventoryCheckOnCart($_SESSION ['shoppingCart']);			
			$redirect = $responsearray['redirect'];
			$message = $responsearray['message'];
			$allowedpaymentGateway	=	CheckoutHelper::getPaymentGatewayDetail($_SESSION['userLogin']->getUserLoginId ());
			
			error_log(" allowedpaymentGateway ".$allowedpaymentGateway , 0);
			$thisObj->view->setVar ( "paymentGateway", $allowedpaymentGateway );
				
			if(isset($_SESSION['priceChangeMessage']) && $_SESSION['priceChangeMessage'] != "")
			{
					$thisObj->view->setVar ( "priceChangeMessage", $_SESSION['priceChangeMessage'] );
					$_SESSION['priceChangeMessage']=$message;
			}else{
			$message = $responsearray['message'];
			$thisObj->view->setVar ( "priceChangeMessage", $message );
			}
			if($redirect!=""){
				header ( "location:".$redirect );exit();
			}else if($_SESSION ['shoppingCart']->getAmountToCollect()>=3000){
				if($_SESSION ['shoppingCart']->getOrderId()!=null && $_SESSION ['shoppingCart']->getOrderId()!=""){
					$_SESSION ['shoppingCart']->setOrderId('');
				}
				$url = $serverUrl. "/rest/pmt/emiCalculator";
				$data =  '{"shoppingCart":'.json_encode($_SESSION['shoppingCart']).',"userLoginId": "'.$_SESSION['userLogin']->getUserLoginId ().'"}';
				$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
				if(isset($result))
				{
					$respJson = json_decode ( $result ['data'] );
					if (isset ($respJson->status) && $respJson->status=='success') {
						
						$thisObj->view->setVar ('emiResult',$respJson->CreditCardEMI);
					}
				}
			}
		}
		$orderUrl = "";
	}
	
	public static function getcaptcha() {
		$serverUrl   = $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
		$captchaUrl   =  $GLOBALS['general']['CAPTCHA_URL'];
		$url = $serverUrl . "/control/getCaptchaJson?sitId=WAP_SITE";
		$result = Utils::getApiResponse( $url );
		$captchaObj = array();
		$captchaImgObj = array();
		if (isset ( $result )) {
			$json = json_decode ( $result ['data'] );
			if (isset ( $json->ID_KEY ) && $json->ID_KEY !== "") {
					$key = $json->ID_KEY;
					$captchaFileName = $json->captchaFileName;
					$captchaImage= $captchaUrl.$captchaFileName;
					$captchaObj= array (
					"captchakey" => $key,
					"captchaImage" => $captchaImage 
				);
					
					$captchaImgObj= array (
							"captchaImage" => $captchaImage
					);
						
			}
		}
		$_SESSION ['captchaObj'] = $captchaObj;
		return $captchaImgObj;
	}
	
	public static function captchaValidate() {
		
		if(isset($_SESSION ['captchaObj']))
		{
			$captchaObj = $_SESSION ['captchaObj'];
		}
		
		if(isset($_POST['input']) &&   isset($captchaObj))
		{
			$userInput = $_POST['input'];
			$captchaCode = $captchaObj['captchakey'];
			
			if($userInput === $captchaCode)
			{
				$_SESSION ['COD_OTP_BYPASS_CHECK'] = 'Y';
				return  "true";
			}else if($userInput === '64860486262272'){ //for test automation scripts
				$_SESSION ['COD_OTP_BYPASS_CHECK'] = 'Y';
				return  "true";
			}
		}
		
 		return "false";
	
	}
	public static function validateCODOTP()
	{
		$isExpired = false;
		$codOTP="";
		$isOtpExpired="N";
		$codOtpTime="";
		$userInput="";
		$codotpTimeout = $GLOBALS['general']['COD_OTP_TIMEOUT'];
		
		if(isset($_SESSION ['COD_OTP']))
		{
			$codOTP = $_SESSION ['COD_OTP'];
			
		}
		if(isset($_SESSION['COD_OTP_TIME']))
		{
			$codOtpTime = $_SESSION['COD_OTP_TIME'];
		}
		
		if(time() - $_SESSION['COD_OTP_TIME'] > $codotpTimeout)
		{
			$isOtpExpired="Y";
			$isExpired=true;
		}
		if(isset($_GET['inputOtp']) && isset($codOTP))
		{
			$userInput = $_GET['inputOtp'];
				
			if($userInput === $codOTP)
			{
				if($isExpired)
				{
					return array('isOtpCorrect'=>'Y','_EVENT_MESSAGE_'=>'OTP expired','response'=>'success','isOtpExpired'=>$isOtpExpired,'test'=>$codOTP,'123e'=>$userInput);
				}else 
				{
					$_SESSION ['COD_OTP_BYPASS_CHECK'] = 'Y';
					return array('isOtpCorrect'=>'Y','_EVENT_MESSAGE_'=>'Code verified.','response'=>'success','isOtpExpired'=>$isOtpExpired,'test'=>$codOTP,'123e'=>$userInput);
				}
			}else 
			{
				 return array('isOtpCorrect'=>'N','_EVENT_MESSAGE_'=>'Verification code is not correct.','response'=>'success','isOtpExpired'=>$isOtpExpired,'test'=>$codOTP,'123e'=>$userInput);
			}
		}else {
			return array('isOtpCorrect'=>'N','_EVENT_MESSAGE_'=>'Verification code is not correct.','response'=>'success','isOtpExpired'=>$isOtpExpired,'test'=>$codOTP,'123e'=>$userInput);
		}
	}

	public static function sendCODOTP()
	{
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$url = $serverUrl . '/rest/pmt/cod/getOTP';
		$cart=$_SESSION ['shoppingCart'];
		$shipAddress= $cart->getShippingAddress();
		$mobileNo="";
		$codOTPAttempts=0;
		$data="";
		$oldOtp="";
		if(isset($shipAddress) && sizeof($shipAddress)>0)
		{
			$mobileNo =$shipAddress['contactMobile'];
		}
		if(isset($_GET['isResendClicked']) && $_GET['isResendClicked'] == "fromResend")
		{
			$oldOtp= $_SESSION ['COD_OTP'];
		} 
		if($oldOtp != "")
		{
			$data='{"partyId":"'.$cart->getPartyId().'", "mobileNo":"'.$mobileNo.'", "otp": "'.$oldOtp.'","otp_type":"COD_OTP"}';
		}else {
			$data='{"partyId":"'.$cart->getPartyId().'", "mobileNo":"'.$mobileNo.'","otp_type":"COD_OTP"}';
		}
		if(isset($_SESSION['mobileNo'.$mobileNo]))
		{
			$codOTPAttempts = $_SESSION['mobileNo'.$mobileNo];
		}
		if($codOTPAttempts  < 2 )
		{
			$showResend = 'true';
		}else {
			$showResend = 'false';
		}
		$codOTPAttempts=$codOTPAttempts+1;
		
		$_SESSION['mobileNo'.$mobileNo] = $codOTPAttempts;
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
						
		 if(isset($result))
		{
			$resultJson = json_decode ( $result ['data'] );
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				$otp=$resultJson->codOtp;
				
				$_SESSION['COD_OTP']=$otp;
				$_SESSION['COD_OTP_TIME'] = time();
				return array('show_resend'=>$showResend,'response'=>'success','message'=>'OTP Sent','mobileNo'=>$mobileNo,'attemps'=>$codOTPAttempts);
			}else 
			{
				return array('show_resend'=>$showResend,'response'=>'success','message'=>'OTP not Sent','mobileNo'=>$mobileNo);
			}
		} else 
		{
			
		    return array('show_resend'=>'false','response'=>'error','message'=>'OTP not Sent','mobileNo'=>$mobileNo,'attempts'=>$codOTPAttempts);
		}
	}
	
	public static function gcCalculateForCheckout(){
		$redirect="";
		if (! isset ( $_SESSION ))
			session_start ();
		$allowedpayMentOption = array();
		if (! isset ( $_SESSION ['userLogin'] )) {
			$redirect = "/control/login";
		} else if (isset($_POST['gc']) && trim($_POST ['gc'])!="") {
			$isGuest="NO";
			if($_SESSION["userLogin"]->getUserName()=='Guest'){
				$tempemail = $_SESSION["userLogin"]->getTemp();
				$_SESSION ['shoppingCart']->getUserLogin()->setTemp($tempemail);
				$isGuest="YES";
			}
			$productId = array ();
			$servicableError = "";
			$ServiceResult = array ();
			$gc=trim($_POST ['gc']);
			$cart=$_SESSION ['shoppingCart'];
			if(isset($_SESSION['gcValues'])) unset($_SESSION ['gcValues']);
			if($_POST['action']==='clearGc'){
				CheckoutHelper::ClearGc($cart);
			}else{
				$cart->setGiftCoupons($gc);
				$isValidGc=CheckoutHelper::gcValidation($cart);
				if($isValidGc) $_SESSION ['gcValues']=array('coupon'=>$gc,'status'=>'success');
			}
			$redirect = "/control/checkoutPayment";
			
			$address=$cart->getShippingAddress();
			$ServiceResult = CheckoutHelper::checkCartDeliveryLocation ( $address["postalCode"] );
			if (isset ( $ServiceResult ) && isset ( $ServiceResult ['productId'] )) {
				$productId = $ServiceResult ['productId'];
			}
			$shoppingCart=$_SESSION ['shoppingCart'];
			$shipAddress= $shoppingCart->getShippingAddress();
			$mobileNo="";
			if(isset($shipAddress) && sizeof($shipAddress)>0)
			{
				$mobileNo = $shipAddress['contactMobile'];
			}
			$allowedpayMentOption =	CheckoutHelper::getPaymentOptionCheckForShoppingCart($productId,$address["postalCode"],$_SESSION ['shoppingCart']->getAllowedZipCodeMode(),$_SESSION ['userLogin']->getUserLoginId(),$isGuest,$mobileNo);
			$paymodesFromGC = $shoppingCart->getPaymentModes();
			
			$gcPaymodesKey = array_search('cod', $paymodesFromGC);
			if((isset($paymodesFromGC) && count($paymodesFromGC)) && (!isset($gcPaymodesKey) || $gcPaymodesKey == ""))
			{
				$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD"]);
				$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD_WALLET"]);
				$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD_CARD_WALLET"]);
				$allowedpayMentOption = array_diff($allowedpayMentOption,["EXT_COD_CARD"]);
				
			} else 
			{
				$Searchkey = array_search('EXT_COD', $allowedpayMentOption);
				
			}
			$_SESSION ['allowedpayMentOption'] = $allowedpayMentOption;
		} else 
		{
			$redirect = "/control/showcart";
		}
		header ( "location:" . $redirect );exit();
	}
	private static function getCODOTPMemcache($userId,$mobileNo)
	{
		$key=$userId."#".$mobileNo."#"."WAP";
	}
	private static function ClearGc($cart) {
		if (! isset ( $_SESSION )) session_start ();
		$shipAddress=$cart->getShippingAddress();
		$shippingContactMechId=$cart->getShippingContactMechId();
		$userLoginDetails = $cart->getUserLogin();
		if(isset($_SESSION ['userLogin'])){
			$userLogin=$_SESSION ['userLogin'];
		}
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$url = $serverUrl . '/rest/sc/getShoppingCart?PARTY_ID=' . $cart->getPartyId() . '&SHOPPING_LIST_TYPE=SLT_SPEC_PURP';
		$result = Utils::getProtectedApiResponse ( $url, null, null, null, null );
		if(isset($result)){
			$json=json_decode($result['data']);
			if(isset($json->status) && $json->status=='success'){
				$decodedCart=json_decode ($result ['data'],true);
				if($decodedCart ['CART']['shoppingList']['shoppingListItems'][0]['productId']!=null){
					$newCart = $json->CART;
					$stdobject_ = (object)$newCart;
				
					$userCart=new ShoppingCart();
					if (get_class($stdobject_)=='stdClass') {
						Utils::Cast($userCart, $stdobject_);
					}else{
						$userCart=$stdobject_;
					}
					if($userCart->getUserLogin()==null && isset($userLogin) && $userLogin->getUserName()==='Guest'){
						$userCart->setUserLogin($userLoginDetails);
					}
					$userCart->setShippingAddress($shipAddress);
					$userCart->setShippingContactMechId($shippingContactMechId);
					$_SESSION ['shoppingCart']=$userCart;
				}
			}
		}
	}
}
?>
