<?php

class ArticleHelper {
	
	public static function submitQuestion($this)
	{
	
		$productQuestion= 'NA';
		$productId='NA';
		$userLoginId='';
		//Skip Security test to fix PDP Cache issue
		$securityTokenresult='PASS';
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$responseCode = 'error';
		$status = 'error';
		$responseMsg = 'Sorry your question cannot be post due to some issue';
		 
		error_log("  SecurityToken pass  ".$securityTokenresult , 0);
	
		if (isset( $_POST ['askQuestion'] ))
		{
			$productQuestion=$_POST['askQuestion'];
		}
	
		if (isset( $_POST ['articleId'] ))
		{
			$productId=$_POST['articleId'];
		}
	
		if (isset ( $_SESSION ['userLogin'] ) && (($_SESSION ['userLogin']->getUserName()!='Guest')))
		{
			$userLoginId =  $_SESSION ['userLogin']->getUserLoginId();
		}
	
		if($productQuestion!='NA' && $productId!='NA'  && $securityTokenresult!='FAIL')
		{
			$productQuestion =    htmlentities($productQuestion);
			$data = "{\"productId\":\"".$productId."\",\"userLoginId\":\"".$userLoginId."\",\"productQuestion\":\"".$productQuestion."\"}";
			$url = $serverUrl."/rest/product/submitProductQue";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		}
		if(isset($result)  && $securityTokenresult!='FAIL')
		{
			$resultJson = json_decode ( $result ['data'] );
			error_log(" submitProductQue Response  ".$result ['data'] , 0);
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				$responseCode = 'success';
				$status = $resultJson->status;
				$responseMsg = 'Thank you for your question. It will appear here in a while.';
	
				if(isset($_SESSION ['securityTokenQue']))
				{
					unset($_SESSION ['securityTokenQue']);
				}
			}
			else
			{
				$responseCode = 'error';
				$responseMsg = $resultJson->message;
			}
		}
		return (array (
				"status" => $status,
				"responseCode" => $responseCode,
				"responseMsg" => $responseMsg,
		));
	}
	
	public static function submitAnswer($this)
	{
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$productAnswer= 'NA';
		$productQuestionId='NA';
		$userLoginId='';
		$securityTokenresult='PASS';
		if (isset( $_POST ['answer'] ))
		{
			$productAnswer=$_POST['answer'];
		}
		if (isset( $_POST ['questionId'] ))
		{
			$productQuestionId=$_POST['questionId'];
		}
		$responseCode = 'error';
		$status = 'error';
		$responseMsg = 'Sorry your answer cannot be post due to some issue';
		 
		error_log("  SecurityToken pass  ".$securityTokenresult , 0);
	
		if (isset ( $_SESSION ['userLogin'] ) && (($_SESSION ['userLogin']->getUserName()!='Guest')))
		{
			$userLoginId =  $_SESSION ['userLogin']->getUserLoginId();
		}
		if($productQuestionId!='NA' && $productAnswer!='NA' && $securityTokenresult!='FAIL' )
		{
				
			$productAnswer =    htmlentities($productAnswer);
			$data = "{\"productQuestionId\":\"".$productQuestionId."\",\"userLoginId\":\"".$userLoginId."\",\"productAnswer\":\"".$productAnswer."\"}";
			$url = $serverUrl."/rest/product/submitProductAns";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		}
	
		if(isset($result) && $securityTokenresult!='FAIL')
		{
			$resultJson = json_decode ( $result ['data'] );
			error_log(" submitProductAns Response  ".$result ['data'] , 0);
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				$status = $resultJson->status;
				$responseCode = 'success';
				$responseMsg = 'Thank you for your answer. It will appear here in a while.';
				if(isset($_SESSION ['securityToken'.$productQuestionId]))
				{
					unset($_SESSION ['securityToken'.$productQuestionId]);
				}
			}
			else
			{
				$responseCode = 'error';
				$responseMsg = $resultJson->message;
			}
		}
		return (array (
				"status" => $status,
				"responseCode" => $responseCode,
				"responseMsg" => $responseMsg,
	
		));
	}
	
	public static function quetsionAnswersforArticle($articleId)
	{
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$queAnsUrl=$serverUrl."/rest/product/getProductQuesAns";
		$revData = "{'productId':'".$articleId."'}";
		$queAnsresult = Utils::getProtectedApiResponse($queAnsUrl, CURLOPT_POST, null, $revData, null);
		$securityTokenQue =	Utils::getSecuritytoken();
		error_log("  securityTokenQue  ".$securityTokenQue , 0);
		$_SESSION['securityTokenQue']=$securityTokenQue;
		//$thisobj->view->setVar("clientTokenQue",$securityTokenQue);
		
		$secTokenArray = [];
		
		if(isset($queAnsresult))
		{
			$queAnsJson = json_decode($queAnsresult['data'],true);
			$queAnsList = $queAnsJson['responseMap']['QuesAns'];
			foreach($queAnsList as $value)
			{
				$securityToken =	Utils::getSecuritytoken();
				error_log("securityToken".$value['productQuestionId']." :: ".$securityToken , 0);
				$_SESSION["securityToken".$value['productQuestionId']]=$securityToken;
				$secTokenArray[$value['productQuestionId']]=$securityToken;
			}
			//$thisobj->view->setVar("secTokenArray",$secTokenArray);
			return $queAnsList;
		}
	}
	
	public static function submitRating($this)
	{

		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$userLoginId=$_POST['userLoginId'];
		$articleRating=$_POST['articleRating'];
		$articleId=$_POST['articleId'];
		
		if (isset ( $_SESSION ['userLogin'] ) && (($_SESSION ['userLogin']->getUserName()!='Guest')))
		{
			$userLoginId =  $_SESSION ['userLogin']->getUserLoginId();
		}/* else
		{
			$_SESSION['lastRegisterPage'] =$referer;
			header("location:"."/control/login");
		} */
		$data = "{'productId':'".$articleId."','userLoginId':'".$userLoginId."','productRating':'".$articleRating."','RnRType':'RRA'}";
		$url = $serverUrl."/rest/product/submitProductReviews";
		
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		if(isset($result))
		{
			$resultJson = json_decode ( $result ['data'] );
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				print_r("sucess".$referer);
			}
		}
	}
	
	
	public static function reviewAndRatingsForArticle($articleId)
	{
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$reviewUrl=$serverUrl."/rest/product/getProductReviews";
		$revData = "{'ProductId':'".$articleId."'}";
		$revResult = Utils::getProtectedApiResponse($reviewUrl, CURLOPT_POST, null, $revData, null);
		if(isset($revResult))
		{
			$revJson = json_decode($revResult['data'],true);
			$reviews = $revJson['responseMap']['Reviews'];
			return $reviews;
		}
	}
	
	public static function submitComments($this)
	{
	
		$productQuestion= 'NA';
		$productId='NA';
		$userLoginId='ANONYMOUS';
		//Skip Security test to fix PDP Cache issue
		$securityTokenresult='PASS';
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$responseCode = 'error';
		$status = 'error';
		$responseMsg = 'Sorry your comment cannot be post due to some issue';
			
		error_log("  SecurityToken pass  ".$securityTokenresult , 0);
	
		if (isset( $_POST ['askQuestion'] ))
		{
			$productQuestion=$_POST['askQuestion'];
		}
	
		if (isset( $_POST ['articleId'] ))
		{
			$productId=$_POST['articleId'];
		}
	
		if (isset ( $_SESSION ['userLogin'] ) && (($_SESSION ['userLogin']->getUserName()!='Guest')))
		{
			$userLoginId =  $_SESSION ['userLogin']->getUserLoginId();
		}
	 
		if($productQuestion!='NA' && $productId!='NA'  && $securityTokenresult!='FAIL')
		{
			$productQuestion =    htmlentities($productQuestion);
			$data = "{\"productId\":\"".$productId."\",\"userLoginId\":\"".$userLoginId."\",\"productQuestion\":\"".$productQuestion."\",\"type\":\"COMMENT\"}";
			$url = $serverUrl."/rest/product/submitProductQue";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		}
		if(isset($result)  && $securityTokenresult!='FAIL')
		{
			$resultJson = json_decode ( $result ['data'] );
			error_log(" submitProductQue Response  ".$result ['data'] , 0);
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				$responseCode = 'success';
				$status = $resultJson->status;
				$responseMsg = 'Thank you for your comment. It will appear here in a while.';
	
				if(isset($_SESSION ['securityTokenQue']))
				{
					unset($_SESSION ['securityTokenQue']);
				}
			}
			else
			{
				$responseCode = 'error';
				$responseMsg = $resultJson->message;
			}
		}
		return (array (
				"status" => $status,
				"responseCode" => $responseCode,
				"responseMsg" => $responseMsg,
		));
	}
	public static function submitCommentAnswer($this)
	{
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$productAnswer= 'NA';
		$productQuestionId='NA';
		$userLoginId='';
		$securityTokenresult='PASS';
		if (isset( $_POST ['answer'] ))
		{
			$productAnswer=$_POST['answer'];
		}
		if (isset( $_POST ['questionId'] ))
		{
			$productQuestionId=$_POST['questionId'];
		}
		$responseCode = 'error';
		$status = 'error';
		$responseMsg = 'Sorry your reply cannot be post due to some issue';
			
		error_log("  SecurityToken pass  ".$securityTokenresult , 0);
	
		if (isset ( $_SESSION ['userLogin'] ) && (($_SESSION ['userLogin']->getUserName()!='Guest')))
		{
			$userLoginId =  $_SESSION ['userLogin']->getUserLoginId();
		}
		if($productQuestionId!='NA' && $productAnswer!='NA' && $securityTokenresult!='FAIL' )
		{
	
			$productAnswer =    htmlentities($productAnswer);
			$data = "{\"productQuestionId\":\"".$productQuestionId."\",\"userLoginId\":\"".$userLoginId."\",\"productAnswer\":\"".$productAnswer."\"}";
			$url = $serverUrl."/rest/product/submitProductAns";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
			 
		}
	
		if(isset($result) && $securityTokenresult!='FAIL')
		{
			$resultJson = json_decode ( $result ['data'] );
			error_log(" submitProductAns Response  ".$result ['data'] , 0);
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				$status = $resultJson->status;
				$responseCode = 'success';
				$responseMsg = 'Thank you for your reply. It will appear here in a while.';
				if(isset($_SESSION ['securityToken'.$productQuestionId]))
				{
					unset($_SESSION ['securityToken'.$productQuestionId]);
				}
			}
			else
			{
				$responseCode = 'error';
				$responseMsg = $resultJson->message;
			}
		}
		return (array (
				"status" => $status,
				"responseCode" => $responseCode,
				"msg" => $responseMsg,
	
		));
	}
	
	public static function commentsforArticle($commentsforArticles)
	{ 
		$commentsList =array();
		foreach ($commentsforArticles as $comment){
			if(isset($comment['type']) && strcasecmp($comment['type'],'COMMENT')== 0)
			{
				array_push($commentsList,$comment);
			}
			
		}
		return $commentsList;
	}
	public static function filterQuesforArticle($questionsforArticles)
	{
		$quesList =array();
		foreach ($questionsforArticles as $comment){
			if(!isset($comment['type']) || strcasecmp($comment['type'],'COMMENT')!= 0)
			{
				array_push($quesList,$comment);
			}
				
		}
		return $quesList;
	}
}
?>