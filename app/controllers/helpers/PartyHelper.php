<?php
// include __DIR__ . "/../../config/Utils.php";

class PartyHelper {

	public static function userProfile($thisObj) {
		
		$partyId ="";
		$lastPage ="";
		if(isset($_POST['lp']))
		{
			$lastPage = $_POST['lp'];
		}
		if(!isset($_SESSION['userLogin'])){
			$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
			header ("location:/control/login");
		} else {
			$userName = $_SESSION['userLogin']->getUserName();
			$partyId = $_SESSION['userLogin']->getPartyId();
			if($userName == "Guest" || strlen($partyId) == 0){
				$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
				header ("location:/control/login");
			}
		}
		
		$thisObj->view->setVar("leafTitle","profile");
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
    	$url = $serverUrl."/rest/party/profile/userProfile?partyId=".$partyId;
		$result = Utils::getProtectedApiResponse($url, null, null, null, null);

		//print_r($result);
		if(isset($result)){
			$jsonResult = json_decode($result['data']);
			if(isset($jsonResult->status) && $jsonResult->status === "success" && $jsonResult->code == 100){
				
				if(isset($_SESSION['personProfile'])){
					unset($_SESSION['personProfile']);
					$person = new Person($jsonResult->USER_LOGIN_ID, $jsonResult->FIRST_NAME,$jsonResult->LAST_NAME,$partyId);
					$_SESSION['personProfile'] = $person;
				}
				
// 				if(isset($jsonResult->USER_EMAIL_ID)){
// 					$thisObj->view->setVar("email",$jsonResult->USER_EMAIL_ID);
// 				}else {
// 					$thisObj->view->setVar("email","");
// 				}
				
				$userId = $_SESSION['userLogin']->getUserLoginId();
 				if(isset($userId)){
 					$thisObj->view->setVar("email",$_SESSION['userLogin']->getUserLoginId());
 				}else {
 					$thisObj->view->setVar("email"," ");
 				}
				
				if(isset($jsonResult->CONTACT_MOBILE)){
					$thisObj->view->setVar("mobileNo",$jsonResult->CONTACT_MOBILE);
				}else {
					$thisObj->view->setVar("mobileNo","");
				}
				
				if(isset($jsonResult->FIRST_NAME)){
					$fn= $jsonResult->FIRST_NAME;
					$fn= ucfirst($fn);
					$thisObj->view->setVar("firstName",$fn);
				}else {
					$thisObj->view->setVar("firstName","");
				}

				if(isset($jsonResult->LAST_NAME)){
					$thisObj->view->setVar("lastName",$jsonResult->LAST_NAME);
				}else {	
					$thisObj->view->setVar("lastName","");
				}
				
				if(isset($jsonResult->GENDER)){
					$thisObj->view->setVar("gender",$jsonResult->GENDER);
				}else {
					$thisObj->view->setVar("gender","");
				}

				if(isset($jsonResult->BIRTH_DATE)){
					$thisObj->view->setVar("dob",$jsonResult->BIRTH_DATE);
				}else{
					$thisObj->view->setVar("dob","");
				}
				
				if(isset($jsonResult->RELATIONSHIP_STATUS)){
					$thisObj->view->setVar("relationship",$jsonResult->RELATIONSHIP_STATUS);
				}else {
					$thisObj->view->setVar("relationship","");
				}

// 				if(isset($jsonResult->EMPLOYMENT)){
// 					$thisObj->view->setVar("employment",$jsonResult->EMPLOYMENT);
// 				}else{
// 					$thisObj->view->setVar("employment","");
// 				}

// 				if(isset($jsonResult->EDUCATION)){
// 					$thisObj->view->setVar("education",$jsonResult->EDUCATION);
// 				}else {
// 					$thisObj->view->setVar("education","");
// 				}
				
// 				if(isset($jsonResult->ANNUAL_INCOME)){	
// 					$thisObj->view->setVar("income",$jsonResult->ANNUAL_INCOME);
// 				}else {
// 					$thisObj->view->setVar("income","");
// 				}	

// 				if(isset($jsonResult->BEST_DESCRIBE)){
// 					$thisObj->view->setVar("nickname",$jsonResult->BEST_DESCRIBE);
// 				}else {
// 					$thisObj->view->setVar("nickname","");
// 				}

				if(isset($jsonResult->addressList)){
					$thisObj->view->setVar ("addressArray", $jsonResult->addressList);
				}
				
		     }else{
		     	if (isset ($jsonResult->message)) {
		     		$_SESSION['error']=$jsonResult->message;
		     	}
		     }	
	     }
      }
      
	public static function userGC($thisObj){

//		if(!isset($_SESSION)) session_start();

		if(!isset($_SESSION['userLogin'])){
			$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
			header ("location:/control/login");
		} else {
			$userName = $_SESSION['userLogin']->getUserName();
			$partyId = $_SESSION['userLogin']->getPartyId();
			if($userName == "Guest" || strlen($partyId) == 0){
				$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
				header ("location:/control/login");
			}
		}
		
		$data  = "{'userId':'".$userName."'}";
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
    	$url = $serverUrl."/rest/party/giftCoupon";
		$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);

		if(isset($result)){
			$jsonResult = json_decode($result['data']);
			if(isset($jsonResult->status) && $jsonResult->status === "success"){
				if(strlen($jsonResult->gc) > 0){
					$thisObj->view->setVar("gcArray",$jsonResult->gc);
				}
		    }else{
		     	if (isset ($jsonResult->message)) {
		     		$_SESSION['error']=$jsonResult->message;
		     	}
		    }		
	    }
	}

	public static function updateProfileBasic(){
		
		if(!isset($_SESSION['userLogin'])){
			$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
			header ("location:/control/login");
		} else {
			$userName = $_SESSION['userLogin']->getUserName();
			$partyId = $_SESSION['userLogin']->getPartyId();
			if($userName == "Guest" || strlen($partyId) == 0){
				$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
				header ("location:/control/login");
			}
		}
		
		$fName = $_POST ['firstName'];
		$lName = $_POST ['lastName'];
		$pNumber = $_POST ['phoneNumbers'];
		
		if(!isset($fName) || strlen($fName) == 0 ){
			$_SESSION ['error'] = 'First name cannot be empty.';
			header ("location:/control/profile?tab=basicinfo");
		}
		
		$pattern = "/^[789]\d{9}$/";
	
		if(!isset($pNumber) || strlen($pNumber) == 0 ){
			$_SESSION ['error'] = 'Mobile No. cannot be empty.';
			header ("location:/control/profile?tab=basicinfo");
		} elseif (strlen($pNumber) != 10){
			$_SESSION ['error'] = 'Mobile No. should be of 10 digits.';
			header ("location:/control/profile?tab=basicinfo");
		} /* elseif (preg_match($pattern, $phnEntered, $matches, PREG_OFFSET_CAPTURE)){
			$_SESSION ['error'] = 'Please enter the correct Mobile No.';
			header ("location:/control/profile");
		} */
		
		
		$data = "{'userId':'" . $userName . "','firstName':'" . $fName . "','lastName':'" . $lName . "','mobileNo':'" . $pNumber . "','partyId':'" . $partyId . "','basicInfo':'true'}";
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$url = $serverUrl . "/rest/party/profile/edit";
		$result = Utils::getProtectedApiResponse ($url, CURLOPT_POST, null, $data, null);
		if (isset ($result)) {
			$jsonResult = json_decode ($result ['data']);
			if (isset ( $jsonResult->status ) && $jsonResult->status === "success") {
				if (isset ($jsonResult->message)) {
					$_SESSION['successMsg']=$jsonResult->message;
				}
			} else {
				if (isset ($jsonResult->message)) {
					$_SESSION['error']=$jsonResult->message;
				}
			}
		}
		header ("location:/control/profile?tab=basicinfo");
	}

	public static function updateProfileOther(){
	
		if(!isset($_SESSION['userLogin'])){
			$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
			header ("location:/control/login");
		} else {
			$userName = $_SESSION['userLogin']->getUserName();
			$partyId = $_SESSION['userLogin']->getPartyId();
			if($userName == "Guest" || strlen($partyId) == 0){
				$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
				header ("location:/control/login");
			}
		}
	
		$gender = $_POST['gender'];
		$dob = $_POST['showBirthDate'];
		$dob = urlencode($dob);
		$mStatus= $_POST['maritalStatus'];
// 		$employmentStatus= $_POST['employmentStatusEnumId'];
// 		$educationStatus = $_POST['educationStatusEnumId'];
// 		$incomeStatus = $_POST['incomeStatusEnumId'];
// 		$nickname = $_POST['nickname'];
				
		
		if(!isset($gender) || strlen($gender) == 0 ){
			$_SESSION ['error'] = 'Please select a gender.';
			header ("location:/control/profile?tab=otherinfo");
		}
		if(!isset($dob) || strlen($dob) == 0 ){
			$_SESSION ['error'] = 'Please select a Date-Of-Birth.';
			header ("location:/control/profile?tab=otherinfo");
		}
		if(!isset($mStatus) || strlen($mStatus) == 0 ){
			$_SESSION ['error'] = 'Please select a Marital Status.';
			header ("location:/control/profile?tab=otherinfo");
		}
// 		if(!isset($employmentStatus) || strlen($employmentStatus) == 0 ){
// 			$_SESSION ['error'] = 'Please select a Employment Status.';
// 			header ("location:/control/profile?tab=otherinfo");
// 		}
// 		if(!isset($educationStatus) || strlen($educationStatus) == 0 ){
// 			$_SESSION ['error'] = 'Please select a Education Status.';
// 			header ("location:/control/profile?tab=otherinfo");
// 		}
// 		if(!isset($incomeStatus) || strlen($incomeStatus) == 0 ){
// 			$_SESSION ['error'] = 'Please select a Income Status.';
// 			header ("location:/control/profile?tab=otherinfo");
// 		}
// 		if(!isset($nickname) || strlen($nickname) == 0 ){
// 			$_SESSION ['error'] = 'Please select a nickname.';
// 			header ("location:/control/profile?tab=otherinfo");
// 		}
		
//		$data = "{'userId':'" . $userName . "','partyId':'" . $partyId . "','gender':'" . $gender . "','dob':'" . $dob . "','mStatus':'" . $mStatus . "','empStatus':'" . $employmentStatus . "','eduStatus':'" . $educationStatus . "','incStatus':'" . $incomeStatus . "','nickname':'" . $nickname . "','otherInfo':'true'}";
		$data = "{'userId':'" . $userName . "','partyId':'" . $partyId . "','gender':'" . $gender . "','dob':'" . $dob . "','mStatus':'" . $mStatus . "','otherInfo':'true'}";
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$url = $serverUrl . "/rest/party/profile/edit";
		$result = Utils::getProtectedApiResponse ($url, CURLOPT_POST, null, $data, null);
		
		if (isset ($result)) {
			$jsonResult = json_decode ($result ['data']);
			if (isset ( $jsonResult->status ) && $jsonResult->status === "success") {
				if (isset ($jsonResult->message)) {
					$_SESSION['successMsg']=$jsonResult->message;
				}
			} else {
				if (isset ($jsonResult->message)) {
					$_SESSION['error']=$jsonResult->message;
				}
			}
		}

	header ("location:/control/profile?tab=otherinfo");
	}
	
	public static function updateProfileAddress(){
		
		PartyHelper::checklogin();
		$addressLine=$_POST['address'];
		
		if(!isset($addressLine) || strlen($addressLine) == 0 ){
			$_SESSION ['error'] = 'Please Enter Shipping Adddress.';
			header ("location:/control/profile?tab=address");
		}
		$remove_character = array("\n", "\r\n", "\r");
		$addressLine = str_replace($remove_character , ' ', $addressLine);

		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$data = '{"partyId":"' . $_SESSION ['userLogin']->getPartyId () . '","contactMechTypeId":"POSTAL_ADDRESS",
					"TO_NAME":"' . $_POST ['name'] . '" ,"ATTN_NAME":"","ADDRESS1":"' . $addressLine . '",
					"CITY":"' . $_POST ['city'] . '", "STATE_PROVINCE_GEO_ID":"' . $_POST ['state'] . '" ,"COUNTRY_GEO_ID": "IND",
					"POSTAL_CODE":"' . $_POST ['pincode'] . '","CONTACT_MOBILE":"' . $_POST ['mobileno'] . '"';
		
		
		if(isset($_POST ['addredit']) && strlen($_POST ['addredit']) > 0){ 				//Address editing Service
			
			$data = $data .',"curContactMechId":"' . $_POST ['addredit'] . '"}';
			$url = $serverUrl . "/rest/party/address/edit";
		
		}else {							//Address creation Service
			
			$data = $data .'}';
			$url = $serverUrl . "/rest/party/address/create";
		}
		
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		if (isset($result)) {
			$addressJson = json_decode ($result ['data']);
			if (isset ($addressJson->responseMap->status) && $addressJson->responseMap->status === "success") {
				if (isset ($addressJson->responseMap->message)) {
					$_SESSION['successMsg']=$addressJson->responseMap->message;
				}
			} else {
				if (isset ($addressJson->responseMap->message)) {
					$_SESSION['error']=$addressJson->responseMap->message;
				}
			}
		}
		
	header ("location:/control/profile?tab=address");
	}
	
	public static function updateProfilePassword(){
		
		if(!isset($_SESSION['userLogin'])){
			$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
			header ("location:/control/login");
		} else {
			$userName = $_SESSION['userLogin']->getUserName();
			$partyId = $_SESSION['userLogin']->getPartyId();
			if($userName == "Guest" || strlen($partyId) == 0){
				$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
				header ("location:/control/login");
			}
		}
		
		$oldPswd = $_POST ['currentPassword'];
		$newPswd = $_POST ['newPassword'];
		if(strlen($newPswd) > 5 && strlen($newPswd) < 15){
			if($oldPswd != $newPswd){
				$data = "{'userId':'" . $userName . "','oldPassword':'" . $oldPswd . "','newPassword':'" . $newPswd . "'}";
				$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
				$url = $serverUrl . "/rest/changeOldPassword";
				$result = Utils::getProtectedApiResponse ($url, CURLOPT_POST, null, $data, null);
				
				if (isset ($result)) {
					$jsonResult = json_decode ($result ['data']);
					if (isset ( $jsonResult->status ) && $jsonResult->status === "success") {
						if (isset ($jsonResult->message)) {
							$_SESSION['successMsg']=$jsonResult->message;
						}
					} else {
						if (isset ($jsonResult->message)) {
							$_SESSION['error']=$jsonResult->message;
						}
					}
				}
			} else {	
				$_SESSION ['error'] = 'New Password should be different than Old password. Please choose a different password.';
			}			
		} else {
			$_SESSION ['error'] = 'Password length should be between 6 and 14.';
		}
		
	header ("location:/control/profile?tab=changepassword");
	}
		
	public static function checkLogin(){
		
		if(!isset($_SESSION['userLogin'])){
			$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
			header ("location:/control/login");
		} else {
			$userName = $_SESSION['userLogin']->getUserName();
			$partyId = $_SESSION['userLogin']->getPartyId();
			if($userName == "Guest" || strlen($partyId) == 0){
				$_SESSION ['error'] = 'Kindly login to See/Edit your profile';
				header ("location:/control/login");
			}
		}
	}

}

?>

