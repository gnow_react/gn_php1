 <?php
 
class PaymentHelper {
	public static function processPayment($thisObj) {
 
		$channelValue='';
		$FRONTEND_URL= $GLOBALS ['general'] ['FRONT_END_SERVER_URL'];
		
		if (! isset ( $_SESSION ))
			session_start ();
			$userLogin = $_SESSION ['userLogin'];
			$redirect="/";
	  
		if (isset ($userLogin)) {
			$redirect="/control/checkoutPayment";
			
			if (isset ( $_SESSION ['shoppingCart'] ) && sizeof ( $_SESSION ['shoppingCart']->getShoppingList()->getShoppingListItems () ) > 0) {
				$cart = $_SESSION ['shoppingCart'];
				$cart->setUserLogin($userLogin);
				if($cart->getShippingContactMechId()==null || $cart->getShippingContactMechId()==""){

					$_SESSION ['error'] = "There is some problem while placing your order. Try again.";
					header('location:'.htmlentities($redirect));exit();
				}
				
				$payment_mode = $_POST['mode'];
				$payment_method_type_id="";
				if(isset($_POST['cod_card']))
				{
					$payment_method_type_id=$_POST['cod_card'];
				}
				if(isset($_POST['cod_wallet']))
				{
					if($payment_method_type_id == "")
					    $payment_method_type_id=$_POST['cod_wallet'];
					else
						$payment_method_type_id=$payment_method_type_id.','.$_POST['cod_wallet'];
						
				}if(isset($_POST['cod']))
				{
					if($payment_method_type_id == "")
						$payment_method_type_id=$_POST['cod'];
					else
						$payment_method_type_id=$payment_method_type_id.','.$_POST['cod'];
					
				}

				if(isset($_POST['codOtpVerified']))
				{
					$codOtpVerfied = $_POST['codOtpVerified'];
					$_SESSION['codOtpVerified']=$codOtpVerfied;
				}
				//Code to check COD OTP bypass
		 	/*	if(isset($payment_mode) && ($payment_mode == "EXT_COD" || $payment_mode == "ext_cod")){
					if(!isset($_SESSION ['COD_OTP_BYPASS_CHECK']) || $_SESSION ['COD_OTP_BYPASS_CHECK'] != 'Y'){
						header('location:/control/logout');exit;
					}
				}*/
				if(isset($_SESSION ['COD_OTP_BYPASS_CHECK'])) 
						unset($_SESSION ['COD_OTP_BYPASS_CHECK']);
				//
				
				if($payment_mode === "EXT_COD" || $payment_mode==="PAYZAPP")
				{
					
				$data = '{
						"GCID": "' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"userLoginId":"' . $userLogin->getUserLoginId () . '",
						"shoppingCart":'.json_encode($cart).',
						"payment_mode": "'.$payment_mode.'",
						"paymentMethodId":"'.$payment_method_type_id.'",
						"successURL": "https://'.$FRONTEND_URL.'/control/paymentVerify",
						"tab": "EXT_COD",
						"clientIp":"' .PaymentHelper::get_client_ip(). '",
						"userid": "'.$userLogin->getUserLoginId().'",
						"checkoutpage": "payment",
						"gcno":"' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'"
					}';
				}
				else if($payment_mode === "CREDIT_CARD")
				{
				$data = '{
						"GCID": "' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"userLoginId":"' . $userLogin->getUserLoginId () . '",
						"shoppingCart":'.json_encode($cart).',
						"successURL": "https://'.$FRONTEND_URL.'/control/paymentVerify",
						"payment_mode": "'.$payment_mode.'",
						"userid": "'.$userLogin->getUserLoginId().'",
						"cardCVV" : "'.(isset($_POST['cardCVV'])?$_POST['cardCVV']:null).'",
						"cardNumber" : "'.(isset($_POST['cardNumber'])?$_POST['cardNumber']:null).'",
						"cardType" : "'.(isset($_POST['cardType'])?$_POST['cardType']:null).'",
						"ccName" : "'.(isset($_POST['ccName'])?$_POST['ccName']:null).'",
						"cref" : "'.(isset($_POST['cref'])?$_POST['cref']:null).'",				
						"selectMonth" : "'.(isset($_POST['selectMonth'])?$_POST['selectMonth']:null).'",
						"selectYear" : "'.(isset($_POST['selectYear'])?$_POST['selectYear']:null).'",
						"clientIp":"' .PaymentHelper::get_client_ip(). '",
						"binNumber" : "'.(isset($_POST['binNumber'])?$_POST['binNumber']:null).'",		
						"gcno":"' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'"
					}';
				}
				else if($payment_mode === "DEBIT_CARD")
				{
				$data = '{
						"GCID": "' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"userLoginId":"' . $userLogin->getUserLoginId () . '",
						"shoppingCart":'.json_encode($cart).',
						"successURL": "https://'.$FRONTEND_URL.'/control/paymentVerify",
						"payment_mode": "'.$payment_mode.'",
						"userid": "'.$userLogin->getUserLoginId().'",
						"cardCVV" : "'.(isset($_POST['cardCVV'])?$_POST['cardCVV']:null).'",
						"cardNumber" : "'.(isset($_POST['cardNumber'])?$_POST['cardNumber']:null).'",
						"cardType" : "'.(isset($_POST['cardType'])?$_POST['cardType']:null).'",
						"dcName" : "'.(isset($_POST['dcName'])?$_POST['dcName']:null).'",
						"cref" : "'.(isset($_POST['cref'])?$_POST['cref']:null).'",				
						"dcselectMonth" : "'.(isset($_POST['dcselectMonth'])?$_POST['dcselectMonth']:null).'",
						"dcselectYear" : "'.(isset($_POST['dcselectYear'])?$_POST['dcselectYear']:null).'",
						"clientIp":"' .PaymentHelper::get_client_ip(). '",
						"binNumber" : "'.(isset($_POST['binNumber'])?$_POST['binNumber']:null).'",		
						"gcno":"' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'"
					}';
				}
				else if($payment_mode === "CREDIT_CARD_EMI")
				{
					$data = '{
						"GCID": "' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"userLoginId":"' . $userLogin->getUserLoginId () . '",
						"shoppingCart":'.json_encode($cart).',
						"successURL": "https://'.$FRONTEND_URL.'/control/paymentVerify",
						"payment_mode": "'.$_POST['radioemi'].'",
						"userid": "'.$userLogin->getUserLoginId().'",
						"cardCVV" : "'.(isset($_POST['cardCVV'])?$_POST['cardCVV']:null).'",
						"cardNumber" : "'.(isset($_POST['cardNumber'])?$_POST['cardNumber']:null).'",
						"cardType" : "'.(isset($_POST['cardType'])?$_POST['cardType']:null).'",
						"ccNameemi" : "'.(isset($_POST['ccNameemi'])?$_POST['ccNameemi']:null).'",
						"cref" : "'.(isset($_POST['cref'])?$_POST['cref']:null).'",
						"selectMonthemi" : "'.(isset($_POST['selectMonthemi'])?$_POST['selectMonthemi']:null).'",
						"selectYearemi" : "'.(isset($_POST['selectYearemi'])?$_POST['selectYearemi']:null).'",
						"gcno":"' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"clientIp":"' .PaymentHelper::get_client_ip(). '",
						"binNumber" : "'.(isset($_POST['binNumber'])?$_POST['binNumber']:null).'",		
						"oneclickcheckoutpay":"N"		
					}';
					$payment_mode = $_POST['radioemi'];
				}
				else if($payment_mode === "NETBANKING")
				{
				$data = '{
						"GCID": "' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"userLoginId":"' . $userLogin->getUserLoginId () . '",
						"shoppingCart":'.json_encode($cart).',
						"payment_mode": "'.$payment_mode.'",
						"selectNetBanking":  "'.(isset($_POST['netBankSelect'])?$_POST['netBankSelect']:null).'",	
						"successURL": "https://'.$FRONTEND_URL.'/control/paymentVerify",
						"tab": "NETBANKING",
						"userid": "'.$userLogin->getUserLoginId().'",
						"checkoutpage": "payment",
						"clientIp":"' .PaymentHelper::get_client_ip(). '",
						"gcno":"' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'"
					}';
				}
				else if($payment_mode === "GIFT_CARD")
				{
				$data = '{
						"GCID": "' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"userLoginId":"' . $userLogin->getUserLoginId () . '",
						"shoppingCart":'.json_encode($cart).',
						"payment_mode": "'.$payment_mode.'",
						"successURL": "https://'.$FRONTEND_URL.'/control/paymentVerify",
						"tab": "GIFT_CARD",
						"userid": "'.$userLogin->getUserLoginId().'",
						"checkoutpage": "payment",
						"clientIp":"' .PaymentHelper::get_client_ip(). '",
						"gcno":"' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'"
					}';
				}else if($payment_mode === "PAYMATE")
				{
				$data = '{
						"GCID": "' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"userLoginId":"' . $userLogin->getUserLoginId () . '",
						"shoppingCart":'.json_encode($cart).',
						"payment_mode": "PAYMATE",
						"selectNetBanking":  "'.(isset($_POST['netBankSelect'])?$_POST['netBankSelect']:null).'",	
						"successURL": "https://'.$FRONTEND_URL.'/control/paymentVerify",
						"tab": "PAYMATE",
						"userid": "'.$userLogin->getUserLoginId().'",
						"checkoutpage": "payment",
						"clientIp":"' .PaymentHelper::get_client_ip(). '",
						"gcno":"' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"paymatemobile" : "'.$_POST['paymatemobile'].'"
					}';
				}else if($payment_mode === "PAYTM" || $payment_mode === "OXIGEN" || $payment_mode === "AMEXEZ" || $payment_mode === "MOBIKWIK" || $payment_mode === "JIOWALLET")
				{$data = '{
						"GCID": "' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'",
						"userLoginId":"' . $userLogin->getUserLoginId () . '",
						"shoppingCart":'.json_encode($cart).',
						"payment_mode": "'.$payment_mode.'",
						"successURL": "https://'.$FRONTEND_URL.'/control/paymentVerify",
						"tab": "WALLET",
						"userid": "'.$userLogin->getUserLoginId().'",
						"checkoutpage": "payment",
						"clientIp":"' .PaymentHelper::get_client_ip(). '",
						"gcno":"' .($cart->getGiftCoupons ()?$cart->getGiftCoupons ():null).'"
					}';
				}
				
				$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
				$cartPayMode=$cart->getPaymentInfo();
                if($cart->getOrderId()!=null && $cart->getOrderId()!="" && $cartPayMode==$payment_mode && ($payment_mode === 'EXT_COD' || $payment_mode === 'GIFT_CARD'))
			
				{
					$paymentParams = $cart->getPaymentParams();
					if($payment_mode === "PAYZAPP")
					{
						include __DIR__ ."/../../views/control/submitPayzapp.phtml";
					}else{
						include __DIR__ . "/../../views/control/submitPayment.phtml";
					}
					die;
				}else {							
					$url = $serverUrl . "/rest/pmt/submitPayment";

				
					if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "com.shopping")
					{
						$channelValue = 'MOBILE_CHANNEL';
						$cart->setChannel($channelValue);
					}
					$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
						if (isset( $result ) &&  isset($result['code']) && $result['code']===200 ) {
						$data=json_decode($result['data'],true);
						
						if(isset($data['responseMap']['status']) && "success"===$data['responseMap']['status'])
						{
							 $paymentParams = $data['responseMap']['paymentParams'];
							 if(isset($paymentParams['cartOrderId'])){
							 $cart->setOrderId($paymentParams['cartOrderId']);
							 }	 if(isset($paymentParams['cartPoNumber'])){
							 $cart->setPoNumber($paymentParams['cartPoNumber']);
							 } 	 if(isset($paymentParams['cartGcPaymentId'])){
							 $cart->setGcPaymentId($paymentParams['cartGcPaymentId']);
							 }if(isset($paymentParams['cartGcFulfillmentId'])){
							 $cart->setGcFulfillemntId($paymentParams['cartGcFulfillmentId']);
							 }
							 $paymentInfo=array($payment_mode);
							 $cart->setPaymentInfo($paymentInfo);
							 if($payment_mode === "PAYZAPP"){
							
							 
							 	include __DIR__ ."/../../views/control/submitPayzapp.phtml";
							 }else {						
								include __DIR__ . "/../../views/control/submitPayment.phtml";
							 }
							die;
						}else if(isset($data['responseMap']['status']) && "error"===$data['responseMap']['status']){						
							$expMsg=$data['responseMap']['message'];
							if(isset($expMsg))
								$_SESSION ['error'] = $expMsg;
							else 
								$_SESSION ['error'] = "There was an error in creating your order. Please try again.";

						}else{

						}
					}
				}
			}
			else if(isset($_SESSION['orderId'])){
				if(isset($_SESSION['shoppingCart'])){
					PaymentHelper::cleanCart();
					unset($_SESSION['shoppingCart']);
					unset($_SESSION ['error']);
					PaymentHelper::sendOrderCreationMail($_SESSION['orderId']);
				}
				$redirect="/control/ordercomplete?orderId=".$_SESSION['orderId'];
			}
		}
		//print_r($redirect);die;
		header('location:'.htmlentities($redirect));	
	}
	
	public static function processPaymate($thisObj)
	{
	   $flag=false;
		if(isset($_GET['mobileNo']) && trim($_GET['mobileNo']!="")){
			$data="{'mobileno':'".$_GET['mobileNo']."'}";
			$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
			$mobileNO = $_GET['mobileNo'];
			$url = $serverUrl."/rest/pmt/validatePaymate?mobileno=".$mobileNO;
		
			$result = Utils::getProtectedApiResponse($url, null, null,null, null);
		
			if(isset($result['data'])){
				$flag=$result['data'];
				}
			}
		
		return $flag;
		}
		
		
	public static function get_client_ip() {
		
    	$ipaddress = '';
    
    if (getenv('X-Forwarded-For'))
    	return getenv('X-Forwarded-For');
    
    else if (getenv('Proxy-Client-IP'))
        return getenv('Proxy-Client-IP');
    
    else if(getenv('WL-Proxy-Client-IP'))
        return getenv('WL-Proxy-Client-IP');
    
    else if (getenv('HTTP_CLIENT_IP'))
    	return getenv('HTTP_CLIENT_IP');
    
    else if (getenv('HTTP_X_CLUSTER_CLIENT_IP'))
    	return getenv('HTTP_X_CLUSTER_CLIENT_IP');
    
    else if (getenv('HTTP_VIA'))
    	return getenv('HTTP_VIA');
        
    else if(getenv('HTTP_X_FORWARDED_FOR'))
    	return getenv('HTTP_X_FORWARDED_FOR');
    
    else if(getenv('HTTP_X_FORWARDED'))
        return getenv('HTTP_X_FORWARDED');
    
    else if(getenv('HTTP_FORWARDED_FOR'))
        return getenv('HTTP_FORWARDED_FOR');
    
    else if(getenv('HTTP_FORWARDED'))
       return getenv('HTTP_FORWARDED');
    
    else if(getenv('REMOTE_ADDR'))
        return getenv('REMOTE_ADDR');
    
    else
        $ipaddress = 'UNKNOWN';
    	return $ipaddress;
	}
		
	public static function processPaymentSuccess($thisObj) {
		if (! isset ( $_SESSION ))
			session_start ();
		$redirect ="/";
		if (isset ( $_SESSION ['userLogin'] )) {
			if (isset ( $_SESSION ['shoppingCart'] ) && sizeof ( $_SESSION ['shoppingCart']->getShoppingList()->getShoppingListItems () ) > 0) {
				$oid=$_GET['orderId'];
				$cart = $_SESSION ['shoppingCart'];
				$userLoginObject = $_SESSION['userLogin'];
				$cart->setUserLogin($userLoginObject);
				$userLoginId='';
				if (isset($_SESSION ['userLogin'])){
					$userLoginId=$_SESSION ['userLogin']->getUserLoginId ();
				}
					 /*$_SESSION['orderId']=$oid;
                                                PaymentHelper::cleanCart();
                                                unset($_SESSION['shoppingCart']);
                                                unset($_SESSION ['error']);
                                                PaymentHelper::sendOrderCreationMail($oid);
                                                $redirect="/control/ordercomplete?orderId=".$oid;*/
					
				$serverUrl =  $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
				$url = $serverUrl . "/crmsfa/control/processPaymentJsonRequest?orderId=".$oid."&userLoginId=".$userLoginId."&productStoreId=9000&orderTotal=".$cart->getAmountToCollect()."&faceToFace=n&manualHold=n";
				$result = Utils::getProtectedApiResponse($url, null, null, null, null);
				$redirect = "/control/checkoutPayment";
				$_SESSION ['error'] = "There is some Problem for Order Number:".$oid."<BR> Kindly reach our Customer Care at <b>0141-3921081</b> or <b><a href='mailto:csgadgetsnow@timesinternet.in'>csgadgetsnow@timesinternet.in</a></b> ";
				if (isset( $result ) &&  isset($result['code']) && $result['code']===200 ) {	
					$data = $result['data'];
					$paymentProcessResult=json_decode($result['data'],true);
		
					if(isset($paymentProcessResult['STATUS']) && "SUCCESS"===$paymentProcessResult['STATUS'])
					{
						$_SESSION['orderId']=$oid;
						PaymentHelper::cleanCart();
						unset($_SESSION['shoppingCart']);
						unset($_SESSION ['error']);
						PaymentHelper::sendOrderCreationMail($oid);
					 	$redirect="/control/ordercomplete?orderId=".$oid;
					}
				}
			}
		}
		header('location:'.htmlentities($redirect),false);
	}
	
	/*public static function json_encode_objs($item){
		
		if(is_object($item))
		{print_r('ok');
		PaymentHelper::my_get_object_vars($item);
		foreach($item as  $attribute => $value){
			print_r($attribute);
			
		}
			print_r(get_object_vars ($item));
		}
		if(!is_array($item) && !is_object($item)){
			print_r('not an object or array');
			return json_encode($item);
		}else{
			$pieces = array();
		
			foreach($item as $k=>$v){
				print_r($k);
				$pieces[] = "\"$k\":".json_encode_objs($v);
			}
			return '{'.implode(',',$pieces).'}';
		}
	}
	
	
	static function  my_get_object_vars($obj) {
		$ref = new ReflectionObject($obj);
		$properties = $ref -> getProperties(ReflectionProperty::IS_PRIVATE);
		$result = array();
		foreach ($properties as $property)
		{
			$property->setAccessible(true);
			print_r($property);
			$result[$property -> getName()] = $property -> getValue($obj);
		}
		
	
		print_r($result);
	}*/
	
	
	static function sendOrderCreationMail($orderId)
	{
		$serverUrl =  $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
		$url = $serverUrl . "/control/sendOrderCreationEmailSMS?orderId=".$orderId;
		$result = Utils::getProtectedApiResponse($url, null, null, null, null);
		if (isset( $result )) {
			$jsonresult = json_decode($result['data']);
			if(isset($jsonresult->status) && $jsonresult->status==='success')
			{
				print_r($jsonresult->messgae);
			}
			
		}
	}
	
	static function submitCCAvenue($thisObj)
	{
		
		$userLogin = $_SESSION ['userLogin'];
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$gcId = '';
		$isOk = false;
		if (isset ($userLogin)) 
		{
			if (isset ( $_SESSION ['shoppingCart'] ) && sizeof ( $_SESSION ['shoppingCart']->getShoppingList()->getShoppingListItems () ) > 0) 
			{
				$cart = $_SESSION ['shoppingCart'];
				$cart->setUserLogin($userLogin);
				if(isset($_SESSION['gcValues']) && isset($_SESSION['gcValues']['coupon']))
				{ 
							$gcId =$_SESSION['gcValues']['coupon']; 
				}
				$data = '{
							"GCID": "'.$gcId.'",
							"userLoginId":"' . $userLogin->getUserLoginId () . '",
							"shoppingCart":'.json_encode($cart).',
							"clientIp":"' .PaymentHelper::get_client_ip(). '",
							"userid": "'.$userLogin->getUserLoginId().'",
							"paymentGateWay": "CCAVENUE",
							"gcno":"' .($cart->getGiftCoupons()?$cart->getGiftCoupons ():null).'"
							}';
				$tempOrderUrl = $serverUrl . "/rest/pmt/submitCCAvenue";
				$result = Utils::getProtectedApiResponse($tempOrderUrl, null, null, $data, null);
				//print_r($tempOrderUrl);print_r($data);
				//print_r($result);die;
				if (isset( $result )) {
					$jsonresult = json_decode($result['data']);
					if(isset($jsonresult->status) && $jsonresult->status==='success')
					{
						$tempOrderId = $jsonresult->orderId;
						$thisObj->view->setVar("tempOrderId",$tempOrderId); 
						$cart->setOrderId($tempOrderId);
						$_SESSION['shoppingCart'] = $cart;
					}
				}
				$isOk=true;
				$jsonConfigresult = PaymentHelper::getPaymentGatewayConfig('CCAVENUE');
				
				if (isset( $jsonConfigresult )) {
						
					$ccAvenueMrchId = $jsonConfigresult->CCAVENUE_WAP_MID;
					echo $ccAvenueMrchId;
					$thisObj->view->setVar("CCAVENUE_MERCHANT_ID",$ccAvenueMrchId);
					$ccAvenueWorkingKey = $jsonConfigresult->CCAVENUE_WAP_WKEY;
					$thisObj->view->setVar("CCAVENUE_WORKING_KEY",$ccAvenueWorkingKey);
					$ccAvenueAccessCode = $jsonConfigresult->CCAVENUE_WAP_ACODE;
					$thisObj->view->setVar("CCAVENUE_ACCESS_CODE",$ccAvenueAccessCode);
				
				}
				
			}
		}
		if(!$isOk) {
			$_SESSION['error'] = "Please try to place the order again. Inconvenience is regretted.";
			header('location:'.'/control/showcart',false);
			
		}
	}
	
	
	
	static function cleanCart()
	{
		if (! isset ( $_SESSION ))
			session_start ();
		$cart = $_SESSION ['shoppingCart'];
		if (isset ( $_SESSION ['shoppingCart'] ) && sizeof ( $_SESSION ['shoppingCart']->getShoppingList()->getShoppingListItems () ) > 0) {
			$shoppingCartItems = $cart->getShoppingList()->getShoppingListItems ();
			if (sizeof ( $shoppingCartItems ) > 0) {
				foreach ( $shoppingCartItems as $key => $productObj ) {
					$item=new ShoppingListItem();
					if (get_class($productObj)=='stdClass') {
						Utils::Cast($item, $productObj);
					}else{
						$item=$productObj;
					}
						
					$data = "{'productId':'" . $item->getProductId () . "','shoppingListId':'" . $cart->getShoppingList()->getShoppingListId() . "','shoppingListType':'SLT_SPEC_PURP'}";
					$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
					$url = $serverUrl . "/rest/sc/itemRemoveFromShoppinglist";
					$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
					$_SESSION ['shoppingCart']='';
				}
			}
		}		// delete from cart
	}
	public static function orderNewPage($thisObj)
	{
		
	}
	
	public static function paymentverifyccavenue($thisObj)
	{
		file_put_contents("/opt/frontend/PHP_LOG.txt","\n HEY THERE",FILE_APPEND);
		print_r('here');
		$jsonConfigresult = PaymentHelper::getPaymentGatewayConfig('CCAVENUE');
		$working_key="";
		if (isset( $jsonConfigresult )) {
		
			$working_key = $jsonConfigresult->CCAVENUE_WORKING_KEY;
			//$working_key='0DD3CACE0DEC77E573B3D7F5FD90BBC2';
		
		}
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$paymode = "";
		$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
		$rcvdString=CCAvenueCrypto::decrypt($encResponse,$working_key);		//Crypto Decryption used as per the specified working key.
		$transactionFinalStatus="";
		$decryptValues=explode('&', $rcvdString);
		$trackingId = "";
		$bankRefNo = "";
		$responseCode="";
		$order_id="";
		$error="";
		$amount="";$bankName="";
		$dataSize=sizeof($decryptValues);
		$temp="hey";
		print_r("BEFORE LOOP ");
		for($i = 0; $i < $dataSize; $i++)
		{
			print_r($dataSize);
			$information=explode('=',$decryptValues[$i]);
			if($information[0] === 'order_status')	$transactionFinalStatus=$information[1] ;
			if($information[0] === 'tracking_id')	$trackingId=$information[1];
			if($information[0] === 'bank_ref_no')	$bankRefNo=$information[1];
			if($information[0] === 'response_code')	$responseCode=$information[1];
			if($information[0] === 'amount')	$amount=$information[1];
			if($information[0] === 'order_id')	$order_id=$information[1];
			if($information[0] === 'card_name')	$bankName=$information[1];
			if($information[0] === 'payment_mode')	$paymode=$information[1];
			if($information[0] === 'failure_message')	$error=$information[1];
			print_r('status---');print_r($information);print_r($transactionFinalStatus);	
			$temp=$temp.$information[0];
			
		}
		file_put_contents("/opt/frontend/PHP_LOG.txt","PRINT IT ----".$trackingId.$transactionFinalStatus,FILE_APPEND);	
		//print_r("PAYMENT STATUS ----------"); die; 
		$redirect ="/";
	//	if (! isset ( $_SESSION )) session_start ();
			$userLogin = $_SESSION ['userLogin'];
		if (!isset ( $userLogin )) {
                    header('location:'.'/control/orderError',false);
                }

	//print_r('-----------------------'.$userLogin);	
		if (isset ( $userLogin )) {error_log("IAMHERE");
			if (isset ( $_SESSION ['shoppingCart'] )) {
				$cart = $_SESSION ['shoppingCart'];
				$cart->setUserLogin($userLogin);
				$gcId = '';
				if(isset($_SESSION['gcValues']['coupon'])){ $gcId =$_SESSION['gcValues']['coupon']; }
				// data for order creation
				$data = '{
						"GCID": "'.$gcId.'",
						"userLoginId":"' . $userLogin->getUserLoginId () . '",
						"shoppingCart":'.json_encode($cart).',
						"payment_mode":"'.$paymode.'",
						"orderId":'.$cart->getOrderId().',
						"selectNetBanking":"'.$bankName.'",
						"userid": "'.$userLogin->getUserLoginId().'",
						"clientIp":"' .PaymentHelper::get_client_ip(). '",
						"gcno":"' .($cart->getGiftCoupons()?$cart->getGiftCoupons():null).'"
					}';
				$url =  $serverUrl . "/rest/pmt/completeCCAvenueOrder";
				$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
	                        //print_r($url);print_r(''.'<BR>'.$result);die;	
				if (isset( $result ) &&  isset($result['code']) && $result['code']===200 ) {
					$data=json_decode($result['data'],true);
					$paymode = $data['paymentMode'];
					$responseCode = $data['pgResponseCode'];
					$trackingId =$data['ccAvenueTransactionId'];
					$amount= $data['pgamount'];
					$bankRefNo =$data['bankTransactionNumber'];
					$transactionFinalStatus=$responseCode;
					$order_id=$data['orderId'];
					PaymentHelper::ccavenuePaymentcomplete($trackingId,$order_id,$transactionFinalStatus,$bankRefNo,$responseCode,$bankName,$amount,$order_id,$paymode);
				}else {
					$_SESSION ['error'] = "Error: Some error occurred while creating order";
					header('location:'.'/control/checkoutPayment',false);
				}
			}
		}else
{
	 $_SESSION ['error'] = "Error: Session Has timedout";
         header('location:'.'/control/login',false);
}
	}
	// For CCAvenue 
	public static function ccavenuePaymentcomplete($trackingId,$order_id,$transactionFinalStatus,$bankRefNo,$responseCode,$bankName,$amount,$orderId,$paymode)
	{

		/* $trackingId='106292675865';
		$transactionFinalStatus='SUCCESS';
		$bankRefNo='173008253847';
		$bankName='HDFC';
		$amount='1';
		$orderId='SGN6000068012';
		$cart->setOrderId($orderId);
		$paymode='NETBANKING';
		$responseCode='SUCCESS' ; */
		$redirect ="/";
		if (! isset ( $_SESSION )) session_start ();
		$userLogin = $_SESSION ['userLogin'];
		if (isset ( $userLogin )) {
			if (isset ( $_SESSION ['shoppingCart'] )) {
				$cart = $_SESSION ['shoppingCart'];
				
				$cart->setUserLogin($userLogin);
				$data = '{
				"userLoginId":"' . $userLogin->getUserLoginId () . '",
				"shoppingCart":'.json_encode($cart).',
				"IssuerRecipientId" : "'.(isset($responseCode)?$responseCode:null).'",
				"amount" : "'.(isset($amount)?$amount:null).'",
				"providerTransactionId" : "'.(isset($bankRefNo)?$bankRefNo:null).'",
				"hermesTransactionId" : "'.(isset($trackingId)?$trackingId:null).'",
				"transactionFinalStatus" : "'.$transactionFinalStatus.'",
				"provider":"'.$bankName.'",
				"Error":"",
				"paymode":"'.$paymode.'",
				"orderId" : "'.(isset($orderId)?$orderId:null).'",
				"currency" : "INR"
				}'; 
				$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
				$url = $serverUrl . "/rest/pmt/ccavenuePaymentcomplete";
				$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
				if (isset( $result ) &&  isset($result['code']) && $result['code']===200 ) {
					$paymentverifyResult=json_decode($result['data'],true);
						
					$transactionFinalStatus = (isset($transactionFinalStatus) && $transactionFinalStatus!=null?$transactionFinalStatus:'Failure');
					$verificationFinalStatus = ( (isset($paymentverifyResult['responseCode']) )?$paymentverifyResult['responseCode']:'FAILURE');
					//print_r($transactionFinalStatus);print_r($verificationFinalStatus);die;
					if($transactionFinalStatus==='SUCCESS' && $verificationFinalStatus=== 'SUCCESS' ){
						$redirect="/control/processPaymentSuccess?orderId=".$orderId;
					}else {
						$_SESSION ['pgError'] = "Error:".$error.$order_id;
						if($transactionFinalStatus === 'Failure' || $transactionFinalStatus === 'Aborted'){
							$redirect="/control/processPaymentFailure";
						}else{
							$redirect="/control/checkoutPayment";
						}
					}
				}else{
					$_SESSION["error"]="Error in creating Order. Please try again!!";
				}
			}
		}else
		{
			$_SESSION["error"]="You are not logged in. Please login to proceed!!";
			$redirect = "/control/login";
		}
		
		header('location:'.htmlentities($redirect),false);
		
	}
	
	
	
	public static function paymentVerify($thisObj) {
		//print_r($_REQUEST);		
		$redirect ="/";
		$transactionFinalStatus = (isset($_REQUEST['TransactionFinalStatus'])?$_REQUEST['TransactionFinalStatus']:null);
		$error = (isset($_REQUEST['Error'])?$_REQUEST['Error']:null);
		if (! isset ( $_SESSION )) session_start ();
		$userLogin = $_SESSION ['userLogin'];
		$codOtpVerfied="";
		
		if(isset($_SESSION['codOtpVerified']))
		{
			$codOtpVerfied = $_SESSION['codOtpVerified'];
		}
		if (isset ( $userLogin )) {
			if (isset ( $_SESSION ['shoppingCart'] )) {
				$cart = $_SESSION ['shoppingCart'];		
				$cart->setUserLogin($userLogin);			
				$data = '{
				"userLoginId":"' . $userLogin->getUserLoginId () . '",
				"emailAddressGuest":"'.$userLogin->getTemp().'",
				"shoppingCart":'.json_encode($cart).',
				"IssuerRecipientId" : "'.(isset($_REQUEST['IssuerRecipientId'])?$_REQUEST['IssuerRecipientId']:null).'",
				"amount" : "'.(isset($_REQUEST['amount'])?$_REQUEST['amount']:null).'",	
				"providerTransactionId" : "'.(isset($_REQUEST['providerTransactionId'])?$_REQUEST['providerTransactionId']:null).'",	
				"token" : "'.(isset($_REQUEST['token'])?$_REQUEST['token']:null).'",
				"hermesTransactionId" : "'.(isset($_REQUEST['hermesTransactionId'])?$_REQUEST['hermesTransactionId']:null).'",
				"transactionFinalStatus" : "'.$transactionFinalStatus.'",	
				"provider" : "'.(isset($_REQUEST['provider'])?$_REQUEST['provider']:null).'",	
				"Error" : "'.(isset($_REQUEST['Error'])?$_REQUEST['Error']:null).'",
				"orderId" : "'.(isset($_REQUEST['orderId'])?$_REQUEST['orderId']:null).'",
				"codOtpVerified" : "'.$codOtpVerfied.'",		
				"currency" : "'.(isset($_REQUEST['currency'])?$_REQUEST['currency']:null).'"	
				}';
				$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
				$url = $serverUrl . "/rest/pmt/paymentVerify";	
				//$url = "http:/localhost:8090/shopping/rest/pmt/paymentVerify";	
				
				
				$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
							
				if (isset( $result ) &&  isset($result['code']) && $result['code']===200 ) {
							$paymentverifyResult=json_decode($result['data'],true);
					
					$transactionFinalStatus = (isset($transactionFinalStatus) && $transactionFinalStatus!=null?$transactionFinalStatus:'FAILURE');
					
						$verificationFinalStatus = ( (isset($paymentverifyResult['responseMap']) && isset($paymentverifyResult['responseMap']['status']) && $paymentverifyResult['responseMap']['status']!=null)?$paymentverifyResult['responseMap']['status']:'FAILURE');

					if($transactionFinalStatus==='SUCCESS' && $verificationFinalStatus=== 'success' ){
						$redirect="/control/processPaymentSuccess?orderId=".$cart->getOrderId();
					}else {
						$_SESSION ['pgError'] = "Error:".$error;
						if($transactionFinalStatus === 'FAILURE' || $transactionFinalStatus === 'RECEIVED' || $transactionFinalStatus === 'ABORTED'){
							$redirect="/control/processPaymentFailure";
						}else{
							$redirect="/control/checkoutPayment";
						}
					}
				}else{
					$_SESSION["error"]="Error in creating Order. Please try again!!";
				}
			}
		}
		 file_put_contents("/opt/frontend/PHP_LOG.txt", "\n PAYMENTVERIFY REDIRECTING TO ------ ".$redirect,FILE_APPEND);
		header('location:'.htmlentities($redirect),false);
	}
	
	
	public static function processPaymentFailure($thisObj) {
		if (! isset ( $_SESSION ))
			session_start ();
		$redirect ="/";
		if (isset ( $_SESSION ['userLogin'] )) {
			if (isset ( $_SESSION ['shoppingCart'] ) && sizeof ( $_SESSION ['shoppingCart']->getShoppingList()->getShoppingListItems () ) > 0) {
				$cart = $_SESSION ['shoppingCart'];
				$userLoginObject = $_SESSION['userLogin'];
				$cart->setUserLogin($userLoginObject);
				$userLoginId='';
				if (isset($_SESSION ['userLogin'])){
					$userLoginId=$_SESSION ['userLogin']->getUserLoginId ();
				}
				
				$data="{'transactionFinalStatus':'FAILURE','currency':'INR','userLoginId':'".$userLoginId."',shoppingCart':".json_encode($cart)."}";
				
				$serverUrl =  $GLOBALS['general']['BACKEND_SERVER_URL'];
				$url = $serverUrl . "/rest/pmt/paymentFailed";
				$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
				$redirect="/control/checkoutPayment";
				if (isset( $result )) {
					$paymentProcessResult=json_decode($result['data']);
					if(isset($paymentProcessResult->responseMap) && "success"===$paymentProcessResult->responseMap->status)
					{
						$_SESSION['pgError']=$paymentProcessResult->responseMap->message;
					}
				}
			}
		}
		header('location:'.htmlentities($redirect),false);
	}
	
	public static function getCardBinInfo(){
		$response = array();
		if(isset($_GET['cardBin']) && trim($_GET['cardBin']!="")){
			$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
			$url = $serverUrl."/rest/pmt/getCardBinInfo?cardBin=".$_GET['cardBin'];
			$result = Utils::getApiResponse($url);
		
			if( isset($result['code']) && $result['code']===200  && isset($result['data'])){
				$resJson = json_decode($result['data']);
				if(isset($resJson->responseMap->status) && $resJson->responseMap->status==='SUCCESS'){
					
					$response = $resJson->responseMap ;
				}
			}
		}
		return $response;
	}
	
	public static function processPayZapp()
	{
		if(isset($_POST['wibmoTxnId']))
		{
			$wibmoTxnId = $_POST['wibmoTxnId'];
		}
		if(isset($_POST['resCode']))
		{
			$wibmoResCode = $_POST['resCode'];
		}
		if(isset($_POST['resDesc']))
		{
			$wibmoResDesc = $_POST['resDesc'];
		}
		if(isset($_POST['msgHash']))
		{
			$wibmoMsgHash = $_POST['msgHash'];
		}
		if(isset($_POST['dataPickUpCode']))
		{
			$wibmoDataPickUpCode = $_POST['dataPickUpCode'];
		}
		$cart = $_SESSION ['shoppingCart'];
		$oId = $cart->getOrderId();
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$url = $serverUrl . "/rest/pmt/paymentVerify";
		//$url = "http:/localhost:8090/shopping/rest/pmt/paymentVerify";
		//die;
		
		//$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );//print_r($result);die;
		
		
		//print_r($result);die;
		$redirect ="/";
		$transactionFinalStatus = "success";
		$error = (isset($_REQUEST['Error'])?$_REQUEST['Error']:null);
		if (! isset ( $_SESSION )) session_start ();
		$userLogin = $_SESSION ['userLogin'];
			
		if (isset ( $userLogin )) {
			if (isset ( $_SESSION ['shoppingCart'] )) {
				$cart = $_SESSION ['shoppingCart'];
				$data = '{
				"userLoginId":"' . $userLogin->getUserLoginId () . '",
				"shoppingCart":'.json_encode($cart).',
				"IssuerRecipientId" : "",
				"amount" : "",
				"providerTransactionId" : "'.$wibmoDataPickUpCode.'",
				"token" : "'.$wibmoResCode.'",
				"hermesTransactionId" : "'.($wibmoTxnId).'",
				"transactionFinalStatus" : "'.$wibmoResCode.'",
				"provider" : "provider",
				"Error" : "NA",
				"orderId" : "'.$cart->getOrderId().'",
				"currency" : "INR",
				"wibmoTxnId":"'.$wibmoTxnId.'",
				"resCode":"'.$wibmoResCode.'",
				"resDesc":"'.$wibmoResDesc.'",
				"msgHash":"'.$wibmoMsgHash.'",
				"dataPickUpCode":"'.$wibmoDataPickUpCode.'",
				"merTxnId":"'.$cart->getOrderId().'"
				}"';
				$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
				$url = $serverUrl . "/rest/pmt/paymentPayzappVerify";
			
				if($wibmoResCode=== "000")
				{
				  $result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
				   if (isset( $result ) &&  isset($result['code']) && $result['code']===200 ) {
					    $redirect="/control/processPaymentSuccess?orderId=".$cart->getOrderId();
					}	
				}else
				{
				   $redirect="/control/processPaymentFailure";
				}	
	
				/*$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );//print_r($data);die;
				if (isset( $result ) &&  isset($result['code']) && $result['code']===200 ) {

					 $redirect="/control/processPaymentSuccess?orderId=".$cart->getOrderId();
//print_r("hereeeeeeeeeeeeeeee");die;
/*
					$transactionFinalStatus = (isset($transactionFinalStatus) && $transactionFinalStatus!=null?$transactionFinalStatus:'FAILURE');
					if($transactionFinalStatus==='SUCCESS'){
						$redirect="/control/processPaymentSuccess?orderId=".$cart->getOrderId();
					}else {
						$_SESSION ['pgError'] = "Error:".$error;
						if($transactionFinalStatus === 'FAILURE' || $transactionFinalStatus === 'RECEIVED' || $transactionFinalStatus === 'REJECTED'){
							$redirect="/control/processPaymentFailure";
						}else{
							$redirect="/control/checkoutPayment";
						}
					}
				}else{
					$_SESSION["error"]="Error in creating Order. Please try again!!";
				}*/
			}
		}
		
		header('location:'.htmlentities($redirect),false);
		
	}
	
	
		
			public static function getPaymentGatewayConfig($paymentGateway)
			{
				$data = "{'paymentGateway':'" . $paymentGateway . "'}";
				$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
				$oldServerUrl   = $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
				$url = $serverUrl . "/rest/pmt/getPaymentGatewayConfig";
				$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
				
				if (isset ( $result )) {
					$json = json_decode ( $result ['data'] );
					if (isset ( $json->status ) && $json->status == "success") {
							
						return $json;
					}
						
				}
			
				return $data;
			
			}	
}
?>
