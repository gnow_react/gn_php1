<?php

class GPlusHelper {
	static $clientId;
	static $clientSecretId;
	static $gplusAuthUrl;
	static $gplusAuthCallbackUrl;
	static $fbGetAccessTokenUrl;
	static $accessToken;
	static $fbGetUserInfoUrl;
	function initialize() {
		self::$clientId = $GLOBALS ['general'] ['GPLUS_CLIENT_ID'];
		self::$clientSecretId = $GLOBALS ['general'] ['GPLUS_CLIENT_SECRET_ID'];
		self::$gplusAuthUrl = $GLOBALS ['general'] ['FB_AUTHORIZE_URL'];
		self::$gplusAuthCallbackUrl = $GLOBALS ['general'] ['GPLUS_CALLBACK_URL'];
		self::$gplusGetAccessTokenUrl = $GLOBALS ['general'] ['FB_GET_ACCESS_TOKEN_URL'];
		self::$fbGetUserInfoUrl = $GLOBALS ['general'] ['FB_GET_USER_INFO_URL'];
	}
	public static function getUserInfo($accessToken) {
		$retrieveUserInfoUrl = $GLOBALS ['general'] ['GPLUS_RETRIEVE_USER_INFO_URL'];
		
		$gplusGetAccessTokenUrl = $GLOBALS ['general'] ['GPLUS_RETRIEVE_TOKEN_URL'];
		
		$headr [] = 'Authorization: Bearer ' . $accessToken;
		
		$curl = curl_init ( $retrieveUserInfoUrl );
		
		// curl_setopt($curl, CURLOPT_GET, true);
		curl_setopt ( $curl, CURLOPT_HTTPHEADER, $headr );
		curl_setopt ( $curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
		// print_r($retrieveUserInfoUrl);
		$json_response = curl_exec ( $curl );
		curl_close ( $curl );
		// print_r('***********************************************');
		
		$userInfo = json_decode ( $json_response );
		return $userInfo;
	}
	public static function retrieveAccessToken($code) {
		$retrieveTokenUrl = $GLOBALS ['general'] ['GPLUS_RETRIEVE_TOKEN_URL'];
		$gplusAuthCallbackUrl = $GLOBALS ['general'] ['GPLUS_CALLBACK_URL'];
		
		$clientId = $GLOBALS ['general'] ['GPLUS_CLIENT_ID'];
		$clientSecretId = $GLOBALS ['general'] ['GPLUS_CLIENT_SECRET_ID'];
		$gplusGetAccessTokenUrl = $GLOBALS ['general'] ['GPLUS_RETRIEVE_TOKEN_URL'];
		
		$clienttoken_post = array (
				"code" => $code,
				"client_id" => $clientId,
				"client_secret" => $clientSecretId,
				"redirect_uri" => $gplusAuthCallbackUrl,
				"grant_type" => "authorization_code" 
		);
		
		$curl = curl_init ( $gplusGetAccessTokenUrl );
		
		curl_setopt ( $curl, CURLOPT_POST, true );
		curl_setopt ( $curl, CURLOPT_POSTFIELDS, $clienttoken_post );
		curl_setopt ( $curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
		
		$json_response = curl_exec ( $curl );
		curl_close ( $curl );
		$authObj = json_decode ( $json_response );
		$accessToken = "";
		if (isset ( $authObj->refresh_token )) {
			// refresh token only granted on first authorization for offline access
			// save to db for future use (db saving not included in example)
			global $refreshToken;
			$refreshToken = $authObj->refresh_token;
		}
		if (isset ( $authObj->access_token )) {
			$accessToken = $authObj->access_token;
		}
		print_r ( $json_response );
		return $accessToken;
	}
	public static function login() {
		if (isset ( $_GET ['code'] )) {
			$code = $_GET ['code'];
			$accessToken = GPlusHelper::retrieveAccessToken ( $code );
			if (isset ( $accessToken ) && $accessToken !== "" ) {
				
				$userInfo = GPlusHelper::getUserInfo ( $accessToken );
				// print_r("---".$userInfo->email."----".$userInfo->given_name."");
				
				$siteId = $GLOBALS ['general'] ['jsso_siteId'];
				$jssoURL = $GLOBALS ['general'] ['jsso_oauth_url'];
				$hostName = "shop.gadgetsnow.com"; // $_SERVER['HTTP_HOST'];
				$userid = $userInfo->email;
				$id = $userInfo->id;
				$firstname = $userInfo->given_name;
				$lastname = $userInfo->family_name;
				$username = $userInfo->name;
				$URL = $jssoURL . "oauthid=" . $id . "&oauthsiteid=googleplus&securitykey=" . $accessToken . "&siteid=" . $siteId . "&ru=https://" . $hostName . "&nru=https://" . $hostName;
				$ru = "https://shop.gadgetsnow.com/control/showcart";
				$nru = "https://shop.gadgetsnow.com";
				$ur = GPlusHelper::gplusLogin ( $userid, $username, $firstname, $lastname, $accessToken, $id, "googleplus" );
				$URL = $jssoURL . "oauthid=" . $id . "&oauthsiteid=googleplus&securitykey=" . $accessToken . "&siteid=" . $siteId . "&ru=" . $ru . "&nru=" . $nru;
				$redirect = "/control/test?url=" . rawurlencode ( $URL );
			} else {
				$_SESSION ['error'] = 'Error: ERROR IN LOGIN';
				$redirect = "/control/login";
			}
		} else {
			$_SESSION ['error'] = 'Error: ERROR IN LOGIN';
			$redirect = "/control/login";
		}
		if (isset ( $_COOKIE ['nrtred'] )) {
			$redirect = "https://www.jdoqocy.com/click-100112161-13154027?sid=";
			if (isset ( $_SESSION ['userLogin'] )) {
				$userLogin = $_SESSION ['userLogin'];
				$ssoId = $userLogin->getSsoId ();
			}
			$redirect = $redirect . $ssoId;
		}
		header ( 'location:' . $redirect );
		exit ();
	}
	public static function gplusLogin($userid, $username, $firstname, $lastname, $access_token, $oauthid, $socialSiteId) {
		if (! isset ( $_SESSION ))
			session_start ();
		if (isset ( $_SESSION ['userLogin'] )) {
			$redirect = "/";
		} else {
			$payload = "{'userid':'" . $userid . "','firstname':'" . $firstname . "','lastname':'" . $lastname . "','access_token':'" . $access_token . "','oauthid':'" . $oauthid . "','socialsiteid':'" . $socialSiteId . "'}";
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$url = $serverUrl . "/rest/loginSocialSite";
			
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $payload, null );
			
			$redirect = "/control/login";
			if (isset ( $_POST ['lp'] ) && strpos ( $_POST ['lp'], 'showcart' ) !== false) {
				$_SESSION ['lastRegisterPage'] = $_POST ['lp'];
			}
			if (isset ( $result )) {
				$jsonResult = json_decode ( $result ['data'] );
				if (isset ( $jsonResult->status ) && $jsonResult->status === "success" && isset ( $jsonResult->USER_LOGIN )) {
					if (! isset ( $_SESSION ))
						session_start ();
					$userName = $jsonResult->USER_LOGIN->userLoginId;
					$ssoId = $jsonResult->USER_LOGIN->ssoId;
					$userLogin = new UserLogin ( $userName, $jsonResult->USER_LOGIN->partyId, $userName, "", $jsonResult->USER_LOGIN->isPrime, $jsonResult->USER_LOGIN->ssoId );
					$person = new Person ( $username, $jsonResult->PERSON_PROFILE->FIRST_NAME, $jsonResult->PERSON_PROFILE->LAST_NAME, $jsonResult->USER_LOGIN->partyId );
					$_SESSION ['personProfile'] = $person;
					$_SESSION ['userLogin'] = $userLogin;
					
					LoginHelper::loginCartHelper ( $jsonResult->USER_LOGIN->partyId, $userid );
					
					$redirect = "/";
					if (isset ( $_SESSION ['lastRegisterPage'] )) {
						$lPage = $_SESSION ['lastRegisterPage'];
					}
					// code to redirect it to address page or PDP (in case of addtowishlist)in case of last page as showcart
					if (isset ( $lPage )) {
						if (strpos ( $lPage, 'p_' ) !== false) {
							ProductHelper::addInWishlistFromPDP ();
							$redirect = $lPage;
						} else {
							list ( $junk, $cFlow ) = explode ( "/control/", $lPage );
							if (isset ( $cFlow )) {
								if ($cFlow === "showcart") {
									$cFlow = "checkout";
								}
								$lPage = "/control/" . $cFlow;
								$redirect = $lPage;
							}
						}
					}
				} else {
					if (! isset ( $_SESSION ))
						session_start ();
					$_SESSION ['error'] = 'Error: ' . (isset ( $jsonResult->message ) && $jsonResult->message != "" ? $jsonResult->message : "Please try again!");
					$redirect = $redirect . "?isExistingUserDiv";
				}
			} else {
				echo "Result: " . gettype ( $result );
			}
		}
		return $redirect;
	}
	public static function loginCampaign() {
		// echo "shivam";
		// echo "----".$_GET['code'];die;
		if (isset ( $_GET ['code'] )) {
			$code = $_GET ['code'];
			$accessToken = GPlusHelper::retrieveAccessToken ( $code );
			$userInfo = GPlusHelper::getUserInfo ( $accessToken );
			$userid = $userInfo->email;
			$id = $userInfo->id;
			$firstname = $userInfo->given_name;
			$lastname = $userInfo->family_name;
			$username = $userInfo->name;
			$gender = $userInfo->gender;
			GPlusHelper::gplusLoginCampaign ( $userid, $username, $firstname, $lastname, $gender, $accessToken, $id, "google" );
		} else {
			$_SESSION ['error'] = 'Error: ERROR IN LOGIN';
			header ( 'location:/control/login' );
			exit ();
		}
	}
	public static function gplusLoginCampaign($userid, $username, $firstname, $lastname, $gender, $access_token, $oauthid, $socialSiteId) {
		if (! isset ( $_SESSION ))
			session_start ();
		$redirect = "/control/registersuccess";
		
		if (isset ( $_SESSION ['userLogin'] )) {
		} else {
			$payload = "{'userid':'" . $userid . "','firstname':'" . $firstname . "','lastname':'" . $lastname . "','gender':'" . $gender . "','access_token':'" . $access_token . "','oauthid':'" . $oauthid . "','socialsiteid':'" . $socialSiteId . "','registrationCampaign':'campaign'}";
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$url = $serverUrl . "/rest/loginSocialSite";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $payload, null );
			
			if (isset ( $result )) {
				$jsonResult = json_decode ( $result ['data'] );
				if (isset ( $jsonResult->status ) && $jsonResult->status === "success") {
					if (isset ( $jsonResult->message )) {
						// $_SESSION['successMsg']=$jsonResult->message;
						$_SESSION ['campaignRegisterRsltMsg'] = $jsonResult->message;
					}
				} else {
					if (isset ( $jsonResult->message )) {
						// $_SESSION['error']=$jsonResult->message;
						$_SESSION ['campaignRegisterRsltMsg'] = $jsonResult->message;
					}
				}
			}
		}
		header ( 'location:' . htmlentities ( $redirect ) );
		exit ();
	}
}
?>
