<?php 

class AMPProductHelper
{
	public function ampIndexAction()
	{

    	$categoryIdNonmobile= array('10041','10016','10026','10027','10028','10029','10030','10036','10038','10039','10042',
    							'10043','10044','10045','10046','10047','10421','10422','10425');
    	$showAmazonProduct = false;
    	$FRONTEND_URL= $GLOBALS ['general'] ['FRONT_END_SERVER_URL'];
    	$requestUri = $_SERVER['REQUEST_URI'];
    	$_SESSION['lastRegisterPage']=$requestUri;
    	
    	$this->view->setVar('leafTitle','product');
    	$productId = $this->dispatcher->getparam("productId");
	    $staticImgUrl = $GLOBALS['general']['STATIC_IMG_URL'];
	    $serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
	   	$oldServerUrl = $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
	   	$RecommendationServerUrl = $GLOBALS['general']['BACKEND_RECOMMENDATION_SERVER_URL'];
	   	
		$authInput = $GLOBALS['general']['AUTH_INPUT'];
		$title = "Product";
		$amazonSponsoredUrl="";
		$affiliateExists=false;
    	$product = new Product();
	   	if(isset($productId)){
	    	$data  = "{'productId':'".$productId."'}"; 
			$productBasicInfoUrl = $serverUrl."/rest/product/getProductBasicInfo/";
			$result = Utils::getProtectedApiResponse($productBasicInfoUrl, CURLOPT_POST, null, $data, null);
    	}
    	if(isset($result)){
			$jsonResult = json_decode($result['data']);
			//$this->view->setVar("all", $jsonResult);
			if(isset($jsonResult->productId)){
				$aprice=0;$sprice=0;$tprice=0;
				$product->setProductId($jsonResult->productId);
				$product->setProductName($jsonResult->productName);
				$pname = $jsonResult->productName;
				$product->setImageUrl(str_replace("/original/","/large/",$staticImgUrl.$jsonResult->originalImageUrl));
				$product->setProductImageUrl(str_replace("/original/","/medium/",$staticImgUrl.$jsonResult->originalImageUrl));
				$product->setBrandObj($jsonResult->brandObj);
				$brandname=$jsonResult->brandObj->brandName;
				$product->setBrandName($jsonResult->brandObj->brandName);
				$product->setManufacturerAddress($jsonResult->manufacturerAddress);
				$product->setImporterAddress($jsonResult->importerAddress);
				$isBidPid = $jsonResult->isBidPid;
				if($isBidPid)
					$this->view->setVar('dealType','Bid');
				$showFirst="SGN";
				if(isset($jsonResult->affiliateProducts))
				{
				$affiliateProducts = $jsonResult->affiliateProducts;
				$affiliateList="";
				$priceArray = array();
				$dummy = $affiliateProducts[0]->AFFILIATE_NAME;
				if(strcasecmp($dummy,"dummy") == 0)
				{
					$this->view->setVar("dummy",$dummy);
				}
				 else 
				 { 
				 	$priceAfiiliateArray=array();
				 foreach($affiliateProducts as $affp)
					{ 
						$affiliateExists=true;
						$affiliateArray = array();
						$aff_name= $affp->AFFILIATE_NAME;
						if($affiliateList != "")
							$affiliateList = $affiliateList.','.$aff_name;
						else 
							$affiliateList = $aff_name;
						$aff_pname=$affp->AFFILIATE_PRODUCT_NAME;
						$aff_url=$affp->AFFILIATE_PRODUCT_URL;
						$aff_price = $affp->PRICE;
						$offer="";
						if(isset($affp->OFFER))
						{
							$offer=$affp->OFFER;
						}
						if($aff_name == "TATACLIQ")
						{
							$this->view->setVar($aff_name."AFF_PROD_NAME",$aff_pname);
							$this->view->setVar($aff_name."AFF_PRICE",$aff_price);
							$this->view->setVar($aff_name."OFFER",$offer);
							$tprice=$aff_price;
							$tatacliqIntermediateurl=$GLOBALS['general']['TATA_CLIQ_URL'];
							$tatacliqUrl = str_replace("https://www.tatacliq.com","",$aff_url);
							$nameurl = str_replace(" ","_",$aff_pname);
							$tatacliqUrl = rawurlencode($tatacliqUrl)."&tag=mweb_gnshop_pdp_buynow&utm_source=mweb_gnshop&utm_medium=Affiliate&title=".rawurlencode($nameurl)."&price=".$aff_price;
							$tatacliqUrl=$tatacliqIntermediateurl."?url=".$tatacliqUrl;
							$imgurl=$tatacliqUrl;
							
							$this->view->setVar($aff_name."AFF_URL",$tatacliqUrl); 
							$affiliateArray = array("name"=>$aff_name,"url"=>$tatacliqUrl,"price"=>$aff_price);
							array_push($priceAfiiliateArray,array("TATACLIQ"=>$affiliateArray));
							array_push($priceArray,array($aff_price=>"TATACLIQ"));
							
						}elseif($aff_name == "AMAZON") 
						{  
							$aprice=$aff_price;
							$this->view->setVar($aff_name."AFF_PROD_NAME",$aff_pname);	
							$this->view->setVar($aff_name."AFF_PRICE",$aff_price);
							$this->view->setVar($aff_name."OFFER",$offer);
							
							$this->view->setVar("amazonProductName",$jsonResult->amazonProductName);
							$this->view->setVar("amazonPrice",$jsonResult->amazonPrice);
	
							$amazonUrl = $aff_url;
							$amazonUrl = str_replace("https://www.amazon.in","",$amazonUrl);
							$amazonUrl = str_replace("http://www.amazon.in","",$amazonUrl);
							if(strpos($amazonUrl,"?") == false)
							{
								$amazonUrl =$amazonUrl."?tag=timofind-21";
							}else 
							{
								$amazonUrl =$amazonUrl."&tag=timofind-21";
							}
							$intermediateUrl = "http://m.gadgetsnow.com/affiliate_amazon.cms?url=";
							$amazonUrl = $intermediateUrl.rawurlencode($amazonUrl)."&utm_campaign=mweb_gnshop_pdp_buyat-21"."&title=".rawurlencode($aff_pname)."&price=".$aff_price."&utm_source=mweb_gnshop&utm_medium=Affiliate";
							$product->setAmazonUrl($amazonUrl);
							$showAmazonProduct = true;
							$this->view->setVar("amazonUrl",$amazonUrl);	
							$this->view->setVar($aff_name."AFF_URL",$amazonUrl);
							$affiliateArray = array("afpname"=>$aff_pname,"name"=>$aff_name,"url"=>$amazonUrl,"price"=>$aff_price);
							$priceAfiiliateArray["AMAZON"]=$affiliateArray;
							$priceArray[$aff_price]="AMAZON";
							
								
							//array_push($priceAfiiliateArray,array("AMAZON"=>$affiliateArray));
							
						} elseif($aff_name == "SAMSUNG") 
						{  
							
							$sprice=$aff_price;
							$this->view->setVar($aff_name."AFF_PROD_NAME",$aff_pname);	
							$this->view->setVar($aff_name."AFF_PRICE",$aff_price);
							$this->view->setVar($aff_name."OFFER",$offer);
	
							$samsungUrl = $aff_url;
							$nameurl = str_replace(" ","_",$aff_pname);
							$intermediateUrl = "https://m.shop.gadgetsnow.com/affiliate.cms?url=";
							$samsungUrl = $intermediateUrl.rawurlencode($samsungUrl)."&tag=samsung_mweb_gnshop_pdp_buynow&afname=".$aff_name."&utm_medium=Affiliate"."&title=".rawurlencode($nameurl)."&price=".$aff_price;
							$this->view->setVar($aff_name."AFF_URL",$samsungUrl);
							$affiliateArray[] = array("afpname"=>$aff_pname,"name"=>$aff_name,"url"=>$samsungUrl,"price"=>$aff_price);
							$priceAfiiliateArray["SAMSUNG"]=array($affiliateArray);
							
						} elseif($aff_name == "PAYTM") 
						{  
							
							$pprice=$aff_price;
							$this->view->setVar($aff_name."AFF_PROD_NAME",$aff_pname);	
							$this->view->setVar($aff_name."AFF_PRICE",$aff_price);
							$this->view->setVar($aff_name."OFFER",$offer);
							
							$eventCategory="paytm_mweb_add2cart";
							//$eventaction = $catalogname."_".$categoryname."_".$name;
							//$eventLabel="pdp|".$productid."|".$productname."|".$showPrice;
							
							$sponsrdUrl =  $aff_url;
							if(strpos($sponsrdUrl,"-pdp")!==false)
							{
								$sponsrdUrl = str_replace("-pdp","",$sponsrdUrl);
								$sponsrdUrl = str_replace("https://paytmmall.com","https://paytm.com/shop/p", $sponsrdUrl);
							}else
							{
								$sponsrdUrl = str_replace("https://paytmmall.com","", $sponsrdUrl);
							}
							$sponsrdUrl = rawurlencode($sponsrdUrl);
							 
							$sponsrdUrl = $sponsrdUrl."utm_campaign=".$eventCategory."&utm_source=gadgetsnow&utm_medium=Affiliate&price=".$sprice."&title=".rawurlencode($aff_pname);
							$sponsrdUrl = $GLOBALS['general']['PAYTM_INTERMEDIATE_URL']."?url=".$sponsrdUrl;
							
							$this->view->setVar($aff_name."AFF_URL",$sponsrdUrl);
							$affiliateArray = array("afpname"=>$aff_pname,"name"=>$aff_name,"url"=>$sponsrdUrl,"price"=>$aff_price);
							$priceAfiiliateArray["PAYTM"]=$affiliateArray;
							$priceArray[$aff_price]="PAYTM";
							
						} 
					}
				 }
				 	$this->view->setVar("affiliateExists",$affiliateExists);
				 	// sort the array on key which has prices of all affiliates
					ksort($priceArray,SORT_NUMERIC);
					// Get the first element which is the affiliate with lowest price
					$showFirst=reset($priceArray);
					$this->view->setVar("showFirst",$showFirst);
					
					$this->view->setVar("priceAfiiliateArray",$priceAfiiliateArray);
					 $this->view->setVar("priceArray",$priceArray);
					$this->view->setVar("affiliates",$affiliateList);
				}
				
				$internalMem="";$model="";
				if($jsonResult->productFeatureMap->STANDARD_FEATURE->MEMORY)
				{
					$standard=$jsonResult->productFeatureMap->STANDARD_FEATURE;
					foreach($standard as $feature=>$fmap)
					{
						if($feature == "GENERAL FEATURES")
						{
							foreach($fmap as $key=>$value)
							{
								if($key == "Model")
								{
									$model=$value;
								}
							}
						}
					}
					$memoryFeatures=$jsonResult->productFeatureMap->STANDARD_FEATURE->MEMORY;
					foreach($memoryFeatures as $mem=>$val)
					{ 
						if(strpos("Internal Memory",$mem) !== false)
						{
							$internalMem = $val;
						}
					}
				}
				if(isset($jsonResult->facility))
				{
				
					$invType = $jsonResult->facility->invoiceType;
					$facilityId=$jsonResult->facility->facilityId;
					$product->setFacilityId($facilityId);
					if($invType == "BuyAndSell")
					{
						$product->setFacilityName("Times Internet Ltd");
					}else
					{
						$product->setFacilityName($jsonResult->facility->facilityName);
					}
				
				}
				if(isset($jsonResult->longDescription)) {
					$product->setLongDescription($jsonResult->longDescription);
				}
				
				if(isset($jsonResult->productBrandFilterSEOURL)) {
					$product->setProductBrandUrl(str_replace("http://shopping.indiatimes.com","http://".$FRONTEND_URL,$jsonResult->productBrandFilterSEOURL));
				}	
				$product->setCategory($jsonResult->category->productCategoryId);
				
				$currentCategoryId=$jsonResult->category->productCategoryId;
				
				$product->setCategoryName($jsonResult->category->categoryName);
				$product->setShipmentSla(isset($jsonResult->shipmentSla)?$jsonResult->shipmentSla:'');
				$product->setMinVariantProductSla(isset($jsonResult->slaObj->minVariantProductSla)?$jsonResult->slaObj->minVariantProductSla:"7");
				$product->setSameDayDeliverySLA($jsonResult->slaObj->sameDayDeliverySLA);
				$product->setCatalog($jsonResult->catalog->prodCatalogId);
			    $product->setCatalogName($jsonResult->catalog->catalogName);
				//$product->setCatalog("10001");
                //$product->setCatalogName("Mobiles");
				$product->setProductId($jsonResult->productId);
				if ($jsonResult->isVirtual=='Y') {
					$product->setIsVirtual(true);
					$product->setIsActual(false);
					$product->setSizechart($jsonResult->sizeContent);
					$url = $serverUrl."/rest/product/getJSForVariant";
					$virtualProd = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
					if(isset($virtualProd['code']) && $virtualProd['code']==200 && isset($virtualProd['data'])){
						$product->setVirtualProdJs($virtualProd['data']);
					}
				}else {
					$product->setIsVirtual(false);
					$product->setIsActual(true);
				}
				
				$getJsonUrl = $oldServerUrl."/control/getJsonUrl?productIds=".$jsonResult->productId;
				$jsonUrl=json_decode(Utils::getProtectedApiResponse($getJsonUrl, null, null, null, $authInput)['data'],true)['product'];
				$productUrl=  $jsonUrl[$product->getProductId()];
				$product->setProductUrl($productUrl);
				$this->view->setVar("canonicalUrl","https://shop.gadgetsnow.com".$productUrl);
				if(isset($jsonResult->productOfferObj->offerText) && $jsonResult->productOfferObj->offerText!='NO_OFFER'){
					//$this->view->setVar("offerText",$jsonResult->productOfferObj->offerText);
					$product->setOfferText($jsonResult->productOfferObj->offerText);
				}
				
				$mrp=0;$offerPrice=0;$discountedPrice=0;$avPrice=0;
				foreach($jsonResult->priceObjList as $obj){
					if($obj->productPriceTypeId==='AVERAGE_COST'){
						$avPrice=$obj->price;
					}else if($obj->productPriceTypeId==='DEFAULT_PRICE'){
						$offerPrice=$obj->price;
						//$this->view->setVar("offerPrice",$offerPrice);
						$product->setOfferPrice($offerPrice);
					}else if($obj->productPriceTypeId==='DISCOUNTED_PRICE'){
						$discountedPrice=$obj->price;
						//$this->view->setVar("discountedPrice",$discountedPrice);
						$product->setDiscountedPrice($discountedPrice);
					}else if($obj->productPriceTypeId==='LIST_PRICE'){
						$mrp=$obj->price;
						//$this->view->setVar("mrp",$mrp);
						$product->setMrp($mrp);
					}else if($obj->productPriceTypeId==='SHIPPING_PRICE'){
						//$this->view->setVar("shippingPrice",$obj->price);
						$product->setShippingPrice($obj->price);
					}else if($obj->productPriceTypeId==='MRP')
					{
						$this->view->setVar("mrpPrice",$obj->price);
					}
				}
				$price=($avPrice>0?$avPrice:($offerPrice>0?$offerPrice:$mrp));
				//$this->view->setVar("price",$price);
				$product->setPrice($price);
				$discount=0;
				if(isset($discountedPrice) && $discountedPrice>0){
					$discount=$mrp-$discountedPrice;
				}else if(isset($offerPrice) && $offerPrice>0){
					$discount=$mrp-$offerPrice;
				}
				
				$discountPerc = round($discount*100/$mrp);
				/* $this->view->setVars(array("discount" => $discount,
											"discountPerc" => $discountPerc)); */
				$product->setDiscountPercent($discountPerc);
				$product->setDiscountAmount($discount);
				$product->setAutoGc($jsonResult->isAutoGCOK);
				
				// For SEO
				if($currentCategoryId == '10043') // if refurbished
				{
					$this->view->setVar('metaTitle','Buy '.$brandname.' '.$model.' Refurbished Mobile ('.$internalMem.') at Low Prices in India | shop.gadgetsnow.com');
					$this->view->setVar('metaDesc',' Shop '.$pname.' mobile phone at Low Prices in India. Also check other used mobile phones for sale at shop.gadgetsnow.com');
					
				}else 
				{
					$this->view->setVar('metaTitle',''.$brandname.' '.$model.' '.$internalMem.' '.$price.' : Shop '.$pname.' Mobile Online at Shop.GN');
					$this->view->setVar('metaDesc','Shop '.$brandname.' '.$model.' '.$internalMem.' online at best price in India. Check full specification of '.$pname.' mobile at Shop.Gadgetsnow.com.');
				}
					
			
				if(isset($jsonResult->productFeatureMap->SELECTABLE_FEATURE)){
					$product->setSelectableFeatures($jsonResult->productFeatureMap->SELECTABLE_FEATURE);
				}
				
				
				if(isset($jsonResult->productFeatureMap->DISTINGUISHING_FEAT->HIGHLIGHTS)){
					$keyFeatures = $jsonResult->productFeatureMap->DISTINGUISHING_FEAT->HIGHLIGHTS;
					//$this->view->setVar("keyFeatures",$keyFeatures);
					$product->setKeyFeatures($keyFeatures);
				}
				
				if(isset($jsonResult->productFeatureMap->STANDARD_FEATURE)){
					$standardFeatures = $jsonResult->productFeatureMap->STANDARD_FEATURE;
					//$this->view->setVar("features",$standardFeatures);
					$product->setStandardFeatures($standardFeatures);				
				}
				$comingSoon=false;
				if(isset($jsonResult->introductionDate) && (strtotime($jsonResult->introductionDate)>=time()) && ((strtotime($jsonResult->introductionDate)-time())/(24 * 60 * 60 * 1000)<31)){
					$comingSoon=true;
				}
				
				//$this->view->setVar("comingSoon",$comingSoon);
				$product->setIsComingSoon($comingSoon);
				if(isset($jsonResult->additionalImage)){
					$additionalImage = $jsonResult->additionalImage;
					//$this->view->setVar("additionalImage",$additionalImage);
					$product->setAdditionalImage($additionalImage);
				}
				
				if(isset($jsonResult->similarProductIds) && sizeof($jsonResult->similarProductIds)>0){
					$colorUrl = $oldServerUrl."/control/getJsonUrl?productIds=".implode($jsonResult->similarProductIds, "&productIds=");
	    			$similarProductIds=json_decode(Utils::getProtectedApiResponse($colorUrl, null, null, null, $authInput)['data'],true)['product'];


					//$this->view->setVar("similarProducts",$similarProductIds);
					$product->setSimilarProductIds($similarProductIds);
					
					$similarProductImg = $staticImgUrl.str_replace("/original/","/small/",$jsonResult->originalImageUrl);
					if(isset($jsonResult->additionalImage) && isset($jsonResult->additionalImage[0])) 
						{ 
						$similarProductImg = $staticImgUrl.str_replace("/original/","/small/",$jsonResult->additionalImage[0]);
					}

					//$this->view->setVar("similarProductsImg", $additionalImage);
					$product->setSimilarProductImg($similarProductImg);
				}
				// ******************* Promotional product
				if(isset($jsonResult->promotionalProductIds) && sizeof($jsonResult->promotionalProductIds)>0){
					$pPID = $jsonResult->promotionalProductIds[0];
					// Fetch product details if its not same as the product
					if($pPID != $productId)
					{
						$inventoryFlagPromo=false;
						$data  = "{'productId':'".$pPID."'}";
						$url = $serverUrl."/rest/inventory/checkProductEnableToBuy";
						$invResultPromo = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
						$product->setPromotionalProductIds($pPID);
						
						if(isset($invResultPromo['data'])){
							$invJsonPromo = json_decode($invResultPromo['data']);
							if(isset($invJsonPromo->responseMap->status) && $invJsonPromo->responseMap->status==='success' && $invJsonPromo->responseMap->flag=="true"){
								$inventoryFlagPromo=true;
							}
						}
						if($inventoryFlagPromo) 
						{
							$this->view->setVar("showPromoProduct","sgn");
						}  // if promo product instock
						else 
						{
							$this->view->setVar("showPromoProduct","amazon");
						}
						$dataPromo = "{'productId':'" . $pPID . "'}";
						$productBasicInfoUrl =$serverUrl."/rest/product/getProductBasicInfo/";
						$resultPromo = Utils::getProtectedApiResponse($productBasicInfoUrl, CURLOPT_POST, null, $dataPromo, null);
						if(isset($resultPromo)){
							$jsonResultPromo = json_decode($resultPromo['data']);
							if(isset($jsonResultPromo->productId))
							{
								$productPromo = new Product();
								$productPromo->setProductId($jsonResultPromo->productId);
								$productPromo->setCategory($jsonResultPromo->category->productCategoryId);
								$productPromo->setProductName($jsonResultPromo->productName);
								//$pname1 = $jsonResultPromo->productName;
								$productPromo->setImageUrl(str_replace("/original/","/large/",$staticImgUrl.$jsonResultPromo->originalImageUrl));
								$productPromo->setProductImageUrl(str_replace("/original/","/medium/",$staticImgUrl.$jsonResultPromo->originalImageUrl));
								
								if(isset($jsonResultPromo->productFeatureMap->DISTINGUISHING_FEAT->HIGHLIGHTS)){
									$keyFeatures = $jsonResultPromo->productFeatureMap->DISTINGUISHING_FEAT->HIGHLIGHTS;
									//$this->view->setVar("keyFeatures",$keyFeatures);
									$productPromo->setKeyFeatures($keyFeatures);
								}
								
								
								$mrpP=0;$offerPriceP=0;$discountedPriceP=0;$avPriceP=0;
								foreach($jsonResultPromo->priceObjList as $obj){
									if($jsonResultPromo->productPriceTypeId==='AVERAGE_COST'){
										$avPriceP=$jsonResultPromo->price;
									}else if($jsonResultPromo->productPriceTypeId==='DEFAULT_PRICE'){
										$offerPriceP=$jsonResultPromo->price;
										$productPromo->setOfferPrice($offerPriceP);
									}else if($jsonResultPromo->productPriceTypeId==='DISCOUNTED_PRICE'){
										$discountedPriceP=$jsonResultPromo->price;
										$productPromo->setDiscountedPrice($discountedPriceP);
									}else if($jsonResultPromo->productPriceTypeId==='LIST_PRICE'){
										$mrpP=$jsonResultPromo->price;
										$productPromo->setMrp($mrpP);
									}else if($jsonResultPromo->productPriceTypeId==='SHIPPING_PRICE'){
										$productPromo->setShippingPrice($jsonResultPromo->price);
									}
								}
								$priceP=($avPriceP>0?$avPriceP:($offerPriceP>0?$offerPriceP:$mrpP));
								$productPromo->setPrice($priceP);
								$discountP=0;
								if(isset($discountedPriceP) && $discountedPriceP>0){
									$discountP=$mrpP-$discountedPriceP;
								}else if(isset($offerPriceP) && $offerPriceP>0){
									$discountP=$mrpP-$offerPriceP;
								}
								
								$discountPercP = round($discountP*100/$mrpP);
								
								$productPromo->setDiscountPercent($discountPercP);
								$productPromo->setDiscountAmount($discountP);
								$productPromo->setAutoGc($item['isAutoGCOK']);
								if(isset($jsonResultPromo->affiliateProducts))
									{
									$affiliateProductsinPromo = $jsonResultPromo->affiliateProducts;
									$affiliateList="";
									$priceArray = array();
									$promoToBeshown= $GLOBALS['general']['FEATURED_WIDGET_PRODUCT'];
									foreach($affiliateProductsinPromo as $affp){
										$aff_name= $affp->AFFILIATE_NAME;
										if($affiliateList != "")
											$affiliateList = $affiliateList.','.$aff_name;
										else
											$affiliateList = $aff_name;
										$aff_pname=$affp->AFFILIATE_PRODUCT_NAME;
										$aff_url=$affp->AFFILIATE_PRODUCT_URL;
										$aff_price = $affp->PRICE;
										if(strcasecmp($aff_name,$promoToBeshown) == 0)
										{
											$productPromo->setAmazonPrice($aff_price);
											$productPromo->setAmazonProductName($aff_pname);
											$this->view->setVar("showPromoProduct",$promoToBeshown);
											if($aff_name =="AMAZON")
											{
												$amazonUrlPromo = $aff_url;
												$amazonUrlPromo = str_replace("https://www.amazon.in","",$amazonUrlPromo);
												$amazonUrlPromo = str_replace("http://www.amazon.in","",$amazonUrlPromo);
												$amazonUrlPromo =$amazonUrlPromo."&tag=timofind-21";
												$amazonUrlPromo = rawurlencode($amazonUrlPromo)."&utm_campaign=mweb_gnshop_featuredwidget-21"."&title=".$amazonPromoPName."&price=".$amazonPromoPrie."&utm_source=mweb_gnshop&utm_medium=Affiliate";
												$amazonUrlPromo = "http://m.gadgetsnow.com/affiliate_amazon.cms?url=".$amazonUrlPromo;
												$productPromo->setAmazonUrl($amazonUrlPromo);
											}elseif($aff_name == "TATACLIQ")
											{
												$tatacliqIntermediateurl=$GLOBALS['general']['TATA_CLIQ_URL'];
												$tatacliqUrl = str_replace("https://www.tatacliq.com","",$aff_url);
												$nameurl = str_replace(" ","_",$aff_pname);
												$tatacliqUrl = rawurlencode($tatacliqUrl)."&tag=tatacliq_mweb_gnshop_featuredwidget&utm_source=mweb_gnshop&utm_medium=Affiliate&title=".rawurlencode($nameurl)."&price=".$aff_price;
												$tatacliqUrl=$tatacliqIntermediateurl."?url=".$tatacliqUrl;
												$productPromo->setAmazonUrl($tatacliqUrl);
											}elseif($aff_name == "SAMSUNG")
											{
												$samsungUrl = $aff_url;
												$nameurl = str_replace(" ","_",$aff_pname);
												$intermediateUrl = "https://m.shop.gadgetsnow.com/affiliate.cms?url=";
												$samsungUrl = $intermediateUrl.rawurlencode($samsungUrl)."&tag=samsung_mweb_gnshop_featuredwidget&afname=".$aff_name."&utm_medium=Affiliate"."&title=".rawurlencode($nameurl)."&price=".$aff_price;
												$productPromo->setAmazonUrl($samsungUrl);
											}
										}
									}
								}
								if(null == $productPromo->getAmazonProductName())
								{  
									$this->view->setVar("showPromoProduct","sgn");
								}
								// Get promo product URL
								$getJsonUrl = $oldServerUrl."/control/getJsonUrl?productIds=".$pPID;
								$jsonUrlPromo=json_decode(Utils::getProtectedApiResponse($getJsonUrl, null, null, null, $authInput)['data'],true)['product'];
								$promoProductUrl=  $jsonUrlPromo[$pPID];
								$productPromo->setProductUrl($promoProductUrl);
								$this->view->setVar("promotionalProduct",$productPromo);
							}
						}
					

					}// if productId != pPid
				}
			}
			$this->view->setVar("product",$product);
			$title = $product->getProductName();
		// comment}
		
		$this->view->setVar("staticUrl",$staticImgUrl);
		$this->view->setVar("title",$title);
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		
		$categoryApiUrl = $serverUrl."/rest/catalog/breadcrumb?id=".$currentCategoryId;
		$resultBreadcrumb = Utils::getProtectedApiResponse($categoryApiUrl, null, null, null, null);
		if(isset($resultBreadcrumb))
		{
			$temp = $resultBreadcrumb['data'];
			$res = json_decode($resultBreadcrumb['data'],true);
			$res[$pname]=$productUrl;
			
			
			$this->view->setVar("breadcrumbJson",$res);
			//print_r($resultBreadcrumb['data']);
				
		}
		
		// inventory check call For main product on PDP
		$data  = "{'productId':'".$productId."'}";
		$url = $serverUrl."/rest/inventory/checkProductEnableToBuy";
		$invResult = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
		$inventoryFlag=false; 
		if(isset($invResult['data'])){
			$invJson = json_decode($invResult['data']);
			if(isset($invJson->responseMap->status) && $invJson->responseMap->status==='success' && $invJson->responseMap->flag=="true"){
				/* **SGN-582 show amazon for specific facility ID products */
				$facilityIdBlock = $GLOBALS['general']['FACILITY_BLOCK_FOR_BUY'];
				if($product->getFacilityId() == $facilityIdBlock)
				{
					$showAmazonProduct = true;
					$inventoryFlag=false;
				}else 
				{
					$showAmazonProduct = false;
					$inventoryFlag=true;
					// If product is in stock for SGN then show SGN product first
					$showFirst="SGN"; 
				}
				// Product is instock at SGN then show SGN buy now first
				$this->view->setvar("showFirst",$showFirst);
			}
		} 
		$primarycategoryId  = $jsonResult->category->productCategoryId;
		$product->setInventoryFlag($inventoryFlag);
		$this->view->setVar("showAmazonProduct",$showAmazonProduct);
	    // Get amazon Products
	    $amazonProductsUrl = $serverUrl."/rest/product/getAffiliateRecommendation";
	    if(isset($jsonResult->similarProductIds) && sizeof($jsonResult->similarProductIds)>0)
	    { 
	    	$pidsForSeeAlso = implode("','", $jsonResult->similarProductIds);
	    	$pidsForSeeAlso = "'".$pidsForSeeAlso."','".$productId."'";
	    }else 
	    {
	    	$pidsForSeeAlso ="'".$productId."'";
	    }
	    //$data = "{'productId':[" . $pidsForSeeAlso . "]}";
	    $data = "{'productId':[" . $pidsForSeeAlso . "],brandName:'".$brandname."','categoryId':'".$primarycategoryId."'}";
	    $affiliateProductsJson =  Utils::getProtectedApiResponse($amazonProductsUrl, CURLOPT_POST, null, $data, null);
	    if(isset($affiliateProductsJson['data']))
	    {
	    	$affiliateProducts=json_decode($affiliateProductsJson['data'],true);
	    	$this->view->setVar("affiliateRecoProds",$affiliateProducts);
	    }
	   
	    $categoryName = $jsonResult->category->categoryName;
	    // Amazon sponsored products
	  	    $amazonSponsoredUrl =$GLOBALS['general']['AMAZON_SPONSORED_API'];
		    $amazonSponsoredUrl=$amazonSponsoredUrl."?format=json&category=".$primarycategoryId."&app=wap&placeholder=sponsored";
		    $affiliateSponsoredProductsJson =  Utils::getProtectedApiResponse($amazonSponsoredUrl, CURLOPT_GET, null, null, null);
		    if(isset($affiliateSponsoredProductsJson['data']))
		    {
		    	$affiliateSponsoredProducts=json_decode($affiliateSponsoredProductsJson['data'],true);
		    	$this->view->setVar("affiliateSponsoredProducts",$affiliateSponsoredProducts['product']);
		    }
		    //Paytm sponsored
		     $paytmSponsoredUrl =$GLOBALS['general']['PAYTM_SPONSORED_API'];
		    $paytmSponsoredProductsResult =  Utils::getProtectedApiResponse($paytmSponsoredUrl, CURLOPT_GET, null, null, null);
		    if(isset($paytmSponsoredProductsResult['data']))
		    {
		    	$paytmSponsoredProductsJson=json_decode($paytmSponsoredProductsResult['data'],true);
		    	$this->view->setVar("paytmSponsoredProductsJson",$paytmSponsoredProductsJson['product']);
		    } 
    	 $amazonsponsored = $affiliateSponsoredProducts['product'];
	    $paytmsponsored=$paytmSponsoredProductsJson['product'];
	    $doneShuffling = true;
	    $sponsoredArray = array();
	    $amazonIdx = 0;
	    $paytmIdx = 0;
	    $paytmSize=sizeof($paytmsponsored);
	    $amazonSize=sizeof($amazonsponsored);
	    $temp = 0;
	    $alternate = "paytm";
	    	error_log("size-------".sizeof($paytmsponsored));
	    // Show Paytm and Amazon Products alternatively
	    while($doneShuffling)
	    {
	    	if(isset($amazonsponsored[$amazonIdx]) && $alternate == "amazon")
	    	{
	    		error_log("Amazon size----".$amazonIdx);
	    		print_r("<br>setting amazon--".$amazonIdx);echo "<BR>";
	    		array_push($sponsoredArray,$amazonsponsored[$amazonIdx]);
	    		$amazonIdx=$amazonIdx+1;
	    		$alternate="paytm";
	    	}
	    	if(isset($paytmsponsored[$paytmIdx]) && $alternate == "paytm")
	    	{
	    		error_log("Paytm size----".$paytmIdx);
	    		print_r("<BR>setting paytm---".$paytmIdx);echo "<BR>";
	    		array_push($sponsoredArray,$paytmsponsored[$paytmIdx]);
	    		$paytmIdx=$paytmIdx+1;
	    		
	    		$alternate = "amazon";
	    
	    	}
	    	if($amazonIdx == $amazonSize )
	    		$alternate="paytm";
	    	elseif($paytmIdx == $paytmSize)
	    		$alternate="amazon";
	    	
	    	$temp=$temp+1;
	    	// condition to break the loop
	    	if($paytmIdx === $paytmSize && $amazonIdx === $amazonSize)
	    		$doneShuffling=false;
	    	if($temp == 10) break;
	    } 
	    $this->view->setVar("affiliateSponsoredProducts",$sponsoredArray);
	    //Amazon Popular
	    $samsungPopularUrl=$GLOBALS['general']['AMAZON_SPONSORED_API'];
	    $samsungPopularResult=file_get_contents($samsungPopularUrl."?format=json&category=".$primarycategoryId."&app=wap&placeholder=sponsored");
	    $samsungPopularProducts = json_decode($samsungPopularResult,true);
	    $this->view->setVar("samsungPopularProducts",$samsungPopularProducts);
	    
	    //Paytm offers Popular
	    $paytmOffersAPI=$GLOBALS['general']['PAYTM_OFFERS_API_URL'];
	    $paytmOffersResult=file_get_contents($paytmOffersAPI);
	    $paytmOffersResultJson = json_decode($paytmOffersResult,true);
	    
	    $this->view->setVar("paytmOffersResultJson",$paytmOffersResultJson['product']);
	    
	    
	   
	    // comment amazon popular categories
	    /*$amazonPopularCategoryUrl=$GLOBALS['general']['AMAZON_CATEGORY_API'];
	    $amazonPopularCategoryResult=file_get_contents($amazonPopularCategoryUrl);
	    $amazonPopularCategoryJson = json_decode($amazonPopularCategoryResult,true);
	    if(isset($amazonPopularCategoryJson))
	    	$this->view->setVar("amazonPopularCategoryJson",$amazonPopularCategoryJson['amazon']); */
	    
	    // Comment Amazon More
	    /*$keyForMore="";
	    $amazonMoreUrl=$GLOBALS['general']['AMAZON_SPONSORED_API'];
	    if(in_array($primarycategoryId,$categoryIdNonmobile))
	    {
	    	$amazonMoreResultUrl=$amazonMoreUrl."?format=json&category=10021&app=wap&placeholder=sponsored";
	    	$amazonMoreProducts = json_decode($amazonMoreResult,true);
	    }else 
	    {
		    $randomCategoryIndex = rand(0,18);
		    $randomCategory = $categoryIdNonmobile[$randomCategoryIndex];
	    	$amazonMoreResultUrl=$amazonMoreUrl."?format=json&category=".$randomCategory."&app=wap&placeholder=sponsored";
	    }
		$amazonMoreResult=file_get_contents($amazonMoreResultUrl);
		$amazonMoreProducts = json_decode($amazonMoreResult,true);
		    if(isset($amazonMoreProducts['product']))
		    {
			    $this->view->setVar("samsungMoreProducts",$amazonMoreProducts);
		    } */
	    
	    // Samsung Sponsored
	    
		  $samsungSponsUrl=$GLOBALS['general']['SAMSUNG_SPONSORED_URL'];
	  //  $samsungSponsResult=file_get_contents($samsungSponsUrl);
	    //$samsungSponsProducts = json_decode($samsungSponsResult,true);
        //$this->view->setVar("samsungSponsProducts",$samsungSponsProducts);
		//$this->view->setVar("inventoryFlag",$inventoryFlag);
		
		$isPresentInWishlist=false;
		if(!isset($_SESSION)) session_start();
		if ($product->getIsActual()==true && isset($_SESSION['userLogin'])){
			// is already in wishlist check call
			$url = $serverUrl."/rest/sc/isPresentInwishlist";
			$data  = "{'productId':'".$productId."','partyId':'".$_SESSION['userLogin']->getPartyId()."','shoppingListType':'SLT_WISH_LIST'}"; 
			$wishResult = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
			if(isset($wishResult['data'])){
				$wishJson = json_decode($wishResult['data']);
				if(isset($wishJson->status) && $wishJson->status==='success' && $wishJson->SuccessMsg==='yes') {
					$isPresentInWishlist=true;
				}
			}
		 }
		$this->view->setVar("isPresentInWishlist",$isPresentInWishlist);
		
		$reviewUrl=$serverUrl."/rest/product/getProductReviews";
		$revData = "{'ProductId':'".$productId."'}";
		$revResult = Utils::getProtectedApiResponse($reviewUrl, CURLOPT_POST, null, $revData, null);
		if(isset($revResult))
		{
			$revJson = json_decode($revResult['data'],true);
			$reviews = $revJson['responseMap']['Reviews'];
			$this->view->setVar("reviews",$reviews);
			 
			
		}
		
		$queAnsUrl=$serverUrl."/rest/product/getProductQuesAns";
		$revData = "{'productId':'".$productId."'}";
		$queAnsresult = Utils::getProtectedApiResponse($queAnsUrl, CURLOPT_POST, null, $revData, null);
		$securityTokenQue =	Utils::getSecuritytoken();
		error_log("  securityTokenQue  ".$securityTokenQue , 0);
		
		$_SESSION['securityTokenQue']=$securityTokenQue;
		$this->view->setVar("clientTokenQue",$securityTokenQue);
		
		       $secTokenArray = [];
		
		if(isset($queAnsresult))
		{
			$queAnsJson = json_decode($queAnsresult['data'],true);
			$queAnsList = $queAnsJson['responseMap']['QuesAns'];
			$this->view->setVar("queAnsList",$queAnsList);
			foreach($queAnsList as $value)
			{
				$securityToken =	Utils::getSecuritytoken();
				error_log("securityToken".$value['productQuestionId']." :: ".$securityToken , 0);
				$_SESSION["securityToken".$value['productQuestionId']]=$securityToken;
				$secTokenArray[$value['productQuestionId']]=$securityToken;
			}
			$this->view->setVar("secTokenArray",$secTokenArray);
		}
		
		/* $shopclueUrl =$GLOBALS['general']['SHOPCLUE_API'];
		$shopclueProductJson =  Utils::getProtectedApiResponse($shopclueUrl, CURLOPT_GET, null, null, null);
		if(isset($shopclueProductJson['data']))
		{
			$shopclueProductJsonResult=json_decode($shopclueProductJson['data'],true);
			$this->view->setVar("shopclueProductJsonResult",$shopclueProductJsonResult['product']);
		} */
		
		//Promoted Amazon banner
    	$amzBanneApirUrl=$GLOBALS['general']['AMAZON_BANNER_API'];
    	$amzBannerRes =  file_get_contents($amzBanneApirUrl);
    	if(isset($amzBannerRes)) {
    		$amzresult = json_decode($amzBannerRes,true);
    		foreach($amzresult['promoted'] as $value)
    		{
    			$number = $value['number'];
    			$active = $value['active'];
    			$promoAfName = $value['fname'];
    			$promoStartDate = $value['startdate'];
    			$promoEndDate = $value['enddate'];
    			$promoTagName= $value['tagname'];
    			$promoEventcat= $value['Eventcat'];
    			$promoEventaction= $value['Eventaction'];
    			$promoEventlabel= $value['Eventlabel'];
    			$promoTaglabel= $value['taglabel'];
		    	$promoImgpath = $value['mbannername'];
		    	$promoBannerurl = $value['mbannerurl'];
		    	$promoBannertext = $value['bannertext'];
		    	
		    	$this->view->setVar("affiliateBannerActive".$number,$active);
		    	$this->view->setVar("promoAfName".$number,$promoAfName);
		    	$this->view->setVar("promoTagName".$number,$promoTagName);
		    	$this->view->setVar("promoEventcat".$number,$promoEventcat);
		    	
		    	$promoEventaction=$promoEventaction."_pdp_mweb_promoted";
		    	
		    	$this->view->setVar("promoEventaction".$number,$promoEventaction);
		    	$this->view->setVar("promoEventlabel".$number,$promoEventlabel);
		    	$this->view->setVar("promoImgpath".$number,$promoImgpath);
		    	
		    	$this->view->setVar("promoBannertext".$number,$promoBannertext);
		    	$promointermediateUrl =  $value['intermediateUrl'];
		    	if($promoAfName === "amazon") // For amazon and tatacliq intermediate URL is of m.gadgetsnow.com
		    	{
		    		$promonofollowtag="rel=nofollow";
		    		$promointermediateUrl = str_replace("https://www.amazon.in","", $promoBannerurl);
		    		if(strpos($promointermediateUrl,"?") == false)
		    		{
		    			$promointermediateUrl =$promointermediateUrl."?tag=timofind-21";
		    		}else
		    		{
		    			$promointermediateUrl =$promointermediateUrl."&tag=timofind-21";
		    		}
		    		$promointermediateUrl = rawurlencode($promointermediateUrl);
		    		$promointermediateUrl = $promointermediateUrl."&utm_campaign=mweb_gnshop_promowidget-21&utm_source=mweb_gnshop&utm_medium=Affiliate&price=0&title=".rawurlencode($promoBannertext);
		    		$promointermediateUrl = "http://m.gadgetsnow.com/affiliate_amazon.cms?url=".$promointermediateUrl;

		    		$promoBannerurl=$promointermediateUrl;
		    	}elseif($promoAfName == "tatacliq")
		    	{
		    		$tatacliqUrl = str_replace("https://www.tatacliq.com","",$promoBannerurl);
		    		$tatacliqUrl = $tatacliqUrl;
		    		$nameurl = str_replace(" ","_",$promoBannertext);
		    		$tatacliqUrl = rawurlencode($tatacliqUrl)."&tag=mweb_gnshop_promoted&utm_source=mweb_gnshop&utm_medium=Affiliate&title=".rawurlencode($nameurl)."&price=".$sprice;
		    		$tatacliqUrl=$tatacliqInternediateurl."?url=".$tatacliqUrl;
		    		$promoBannerurl=$tatacliqUrl;
		    	}elseif($promoAfName == "paytmmall") {
					$promoTagName="mweb_gnshop_promoted_pdp";
					
					$paytmBannerUrl = $promoBannerurl;
					$paytmBannerUrl = str_replace("-pdp","", $paytmBannerUrl);
					if(strpos($paytmBannerUrl,"-pdp")!==false)
					{
						$paytmBannerUrl = str_replace("-pdp","",$paytmBannerUrl);
						$paytmBannerUrl = str_replace("https://paytmmall.com","https://paytm.com/shop/p", $paytmBannerUrl);
					}else
					{
						$paytmBannerUrl = str_replace("https://paytmmall.com","", $paytmBannerUrl);
					}
					$paytmBannerUrl = rawurlencode($paytmBannerUrl);
					 
					$paytmBannerUrl = $paytmBannerUrl."&utm_campaign=".$promoTagName."&utm_source=gadgetsnow&utm_medium=Affiliate&price=0&title=".rawurlencode($promoBannertext);
					$paytmBannerUrl = $GLOBALS['general']['PAYTM_INTERMEDIATE_URL']."?url=".$paytmBannerUrl;
					
					$promoBannerurl=$paytmBannerUrl;
					 
				}else {
		    		$promoTagName=$promoTagName."_pdp_mweb_promoted";
		    		$samsungUrl = $promoBannerurl;
		    		$nameurl = str_replace(" ","_",$promoBannertext);
		    		//$intermediateUrl = "https://m.shop.gadgetsnow.com/affiliate.cms?url=";
		    		//$samsungUrl = $intermediateUrl.rawurlencode($samsungUrl)."&tag=".$promoTagName."&afname=".$promoAfName."&utm_medium=Affiliate"."&title=".rawurlencode($nameurl)."&price=";
		    		$promoBannerurl=$samsungUrl;
		    		 
		    	}// case where banner is of gnshop to be handled.
		    	
		    	$this->view->setVar("promoBannerurl".$number,$promoBannerurl);
		    	$this->view->setVar("promonofollowtag".$number,$promonofollowtag);
    	
	    	}
	    	
    	}
		
    	$jsondealUrl = $GLOBALS['general']['KNOCKOUT_DEALS_API_URL'];
    	$this->view->setVar('dealproductId','');
    	$jsonResult= Utils::getProtectedApiResponse($jsondealUrl, null, null, null,null);
    	if($jsonResult['code'] == '200')
    	{
    		$result=json_decode($jsonResult['data'],true);
    		$dealPId=$result['flash-sale']['productId'];
    		
    		if($result['flash-sale']['productId'] == $productId)
    		{
    			$this->view->setVar('dealproductId',$result['flash-sale']['productId']);
    			$this->view->setVar('dealStartdate',$result['flash-sale']['StartDate']);
    			$this->view->setVar('endDate',$result['flash-sale']['EndDate']);
    			$this->view->setVar('dealType','flash-sale');
    			
    		}
    		if($result['today-deal']['productId'] == $productId)
    		{
	    		$this->view->setVar('dealproductId',$result['today-deal']['productId']);
	    		$this->view->setVar('dealStartdate',$result['today-deal']['StartDate']);
	    		$this->view->setVar('endDate',$result['today-deal']['EndDate']);
	    		$this->view->setVar('dealType','today-deal');
    			
    		}
    		// If product is set for BID
    	}
    		if($isBidPid)
    			$this->view->setVar('dealType','Bid');
		
    }// Ends If product exists
		 if(!isset($product) || $product->getProductId() == ""){
		 	header("HTTP/1.0 404 Internal Server Error");
		 }
		 
    }
	
	public static function ampStealJSON($pPId)
	{
		$authInput = $GLOBALS['general']['AUTH_INPUT'];
		$staticImgUrl = $GLOBALS['general']['STATIC_IMG_URL'];
		$oldServerUrl = $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		$inventoryFlagPromo=false;
		$data  = "{'productId':'".$pPId."'}";
		$productJson="";
		$url = $serverUrl."/rest/inventory/checkProductEnableToBuy";
		$invResultPromo = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
		
		if(isset($invResultPromo['data'])){
			$invJsonPromo = json_decode($invResultPromo['data']);
			if(isset($invJsonPromo->responseMap->status) && $invJsonPromo->responseMap->status==='success' && $invJsonPromo->responseMap->flag=="true"){
				$inventoryFlagPromo=true;
			}
		}
		if($inventoryFlagPromo)
		{
			//$this->view->setVar("showPromoProduct","sgn");
		}  // if promo product instock
		else
		{
			//$this->view->setVar("showPromoProduct","amazon");
		}
		$dataPromo = "{'productId':'" . $pPId. "'}";
		$productBasicInfoUrl =$serverUrl."/rest/product/getProductBasicInfo/";
		$resultPromo = Utils::getProtectedApiResponse($productBasicInfoUrl, CURLOPT_POST, null, $dataPromo, null);
		if(isset($resultPromo)){
			$jsonResultPromo = json_decode($resultPromo['data']);
			if(isset($jsonResultPromo->productId))
			{
				$productPromo = new Product();
				$productPromo->setProductId($jsonResultPromo->productId);
				$productPromo->setCategory($jsonResultPromo->category->productCategoryId);
				$productPromo->setProductName($jsonResultPromo->productName);
				$amazonPromoPName = $jsonResultPromo->productName;
				if(isset($jsonResultPromo->originalImageUrl))
				{
					$productPromo->setImageUrl(str_replace("/original/","/large/",$staticImgUrl.$jsonResultPromo->originalImageUrl));
					$productPromo->setProductImageUrl(str_replace("/original/","/medium/",$staticImgUrl.$jsonResultPromo->originalImageUrl));
				}
				if(isset($jsonResultPromo->productFeatureMap->DISTINGUISHING_FEAT->HIGHLIGHTS)){
					$keyFeatures = $jsonResultPromo->productFeatureMap->DISTINGUISHING_FEAT->HIGHLIGHTS;
					//$this->view->setVar("keyFeatures",$keyFeatures);
					$productPromo->setKeyFeatures($keyFeatures);
				}
		
		
				$mrpP=0;$offerPriceP=0;$discountedPriceP=0;$avPriceP=0;
				foreach($jsonResultPromo->priceObjList as $obj){
					if($obj->productPriceTypeId==='AVERAGE_COST'){
						$avPriceP=$obj->price;
					}else if($obj->productPriceTypeId==='DEFAULT_PRICE'){
						$offerPriceP=$obj->price;
						$productPromo->setOfferPrice($offerPriceP);
					}else if($obj->productPriceTypeId==='DISCOUNTED_PRICE'){
						$discountedPriceP=$obj->price;
						$productPromo->setDiscountedPrice($discountedPriceP);
					}else if($obj->productPriceTypeId==='LIST_PRICE'){
						$mrpP=$obj->price;
						$productPromo->setMrp($mrpP);
					}else if($obj->productPriceTypeId==='SHIPPING_PRICE'){
						$productPromo->setShippingPrice($obj->price);
					}
				}
				$priceP=($avPriceP>0?$avPriceP:($offerPriceP>0?$offerPriceP:$mrpP));
				$productPromo->setPrice($priceP);
				$discountP=0;
				if(isset($discountedPriceP) && $discountedPriceP>0){
					$discountP=$mrpP-$discountedPriceP;
				}else if(isset($offerPriceP) && $offerPriceP>0){
					$discountP=$mrpP-$offerPriceP;
				}
		
				$discountPercP = round($discountP*100/$mrpP);
		
				$productPromo->setDiscountPercent($discountPercP);
				$productPromo->setDiscountAmount($discountP);
				//$productPromo->setAutoGc($item['isAutoGCOK']);
				if(isset($jsonResultPromo->affiliateProducts))
				{
					$affiliateProductsinPromo = $jsonResultPromo->affiliateProducts;
					$affiliateList="";
					$priceArray = array();
					$promoToBeshown= $GLOBALS['general']['FEATURED_WIDGET_PRODUCT'];
					foreach($affiliateProductsinPromo as $affp){
						$aff_name= $affp->AFFILIATE_NAME;
						if($affiliateList != "")
							$affiliateList = $affiliateList.','.$aff_name;
						else
							$affiliateList = $aff_name;
						$aff_pname=$affp->AFFILIATE_PRODUCT_NAME;
						$aff_url=$affp->AFFILIATE_PRODUCT_URL;
						$aff_price = $affp->PRICE;
						if(strcasecmp($aff_name,$promoToBeshown) == 0)
						{
							$productPromo->setAmazonPrice($aff_price);
							$amazonPromoPrie=$aff_price;
							$productPromo->setAmazonProductName($aff_pname);
						//	$this->view->setVar("showPromoProduct",$promoToBeshown);
							if($aff_name =="AMAZON")
							{
								$amazonUrlPromo = $aff_url;
								$amazonUrlPromo = str_replace("https://www.amazon.in","",$amazonUrlPromo);
								$amazonUrlPromo = str_replace("http://www.amazon.in","",$amazonUrlPromo);
								$amazonUrlPromo = $amazonUrlPromo."&tag=timofind-21";
								$amazonUrlPromo = rawurlencode($amazonUrlPromo)."&utm_campaign=mweb_gnshop_featuredwidget-21"."&title=".$amazonPromoPName."&price=".$amazonPromoPrie."&utm_source=mweb_gnshop&utm_medium=Affiliate";
								$amazonUrlPromo = "http://m.gadgetsnow.com/affiliate_amazon.cms?url=".$amazonUrlPromo;
								$productPromo->setAmazonUrl($amazonUrlPromo);
							}elseif($aff_name == "TATACLIQ")
							{
								$tatacliqIntermediateurl=$GLOBALS['general']['TATA_CLIQ_URL'];
								$tatacliqUrl = str_replace("https://www.tatacliq.com","",$aff_url);
								$nameurl = str_replace(" ","_",$aff_pname);
								$tatacliqUrl = rawurlencode($tatacliqUrl)."&tag=tatacliq_mweb_gnshop_featuredwidget&utm_source=mweb_gnshop&utm_medium=Affiliate&title=".rawurlencode($nameurl)."&price=".$aff_price;
								$tatacliqUrl=$tatacliqIntermediateurl."?url=".$tatacliqUrl;
								$productPromo->setAmazonUrl($tatacliqUrl);
							}elseif($aff_name == "SAMSUNG")
							{
								$samsungUrl = $aff_url;
								$nameurl = str_replace(" ","_",$aff_pname);
								$intermediateUrl = "https://m.shop.gadgetsnow.com/affiliate.cms?url=";
								$samsungUrl = $intermediateUrl.rawurlencode($samsungUrl)."&tag=samsung_mweb_gnshop_featuredwidget&afname=".$aff_name."&utm_medium=Affiliate"."&title=".rawurlencode($nameurl)."&price=".$aff_price;
								$productPromo->setAmazonUrl($samsungUrl);
							}
						}
					}
				}
				if(null == $productPromo->getAmazonProductName())
				{ 
				//$this->view->setVar("showPromoProduct","sgn");
				}
				// Get promo product URL
				$getJsonUrl = $oldServerUrl."/control/getJsonUrl?productIds=".$pPId;
				$jsonUrlPromo=json_decode(Utils::getProtectedApiResponse($getJsonUrl, null, null, null, $authInput)['data'],true)['product'];
				$promoProductUrl=  $jsonUrlPromo[$pPId];
				$productPromo->setProductUrl($promoProductUrl);
				$yousave="";$mrpPromo="";$offerPriceP="";
				if($productPromo->getAmazonPrice()!=null) {
					$offerPriceP=$productPromo->getAmazonPrice();
				} else 
				{
					 if($productPromo->getAutoGc() && $productPromo->getDiscountedPrice()>0) {
					 	    $offerPriceP= number_format($productPromo->getDiscountedPrice());
					 } else if($productPromo->getOfferPrice()>0) 
					 	{
					 			$offerPriceP = number_format($productPromo->getOfferPrice());
					 	}
						if($productPromo->getDiscountAmount()>0) {
								$mrpPromo =  number_format($productPromo->getMrp());
							}
						 if($productPromo->getAutoGc() && $productPromo->getDiscountAmount()>0) {
						 	$yousave = number_format($productPromo->getDiscountAmount());
						 } 
				} 
				
				$promoToBeshown= $GLOBALS['general']['FEATURED_WIDGET_PRODUCT'];
				$widgetName ="Featured Gadget";
				if(null == $productPromo->getAmazonProductName())
				{
					$promoToBeshown="sgn";
					$widgetName ="Steal Deal";
				}
				$productJson = array ("items"=>
						array(
								array
								(
										"widgetName"=>$widgetName,
										"isActive"=>true,
										"promoOrSgn"=>$promoToBeshown,
										"pname"=>$productPromo->getProductName(),
										"offerPrice"=>$offerPriceP,
										"you_save"=>$yousave,
										"mrpPrice"=>$mrpPromo,
										"urlAction"=>$promoProductUrl,
										"save"=> ($yousave==""?false:true),
										"productId"=>$productPromo->getProductId(),
										"keyfeatures"=>array(
												"display"=>"5 inch Display + Stylish Design",
												"andorid"=>"Android 7.0, Nougat",
												"processor"=>"MT6580m 64-bit Quad-core 1.3GHz Processor",
												"camera"=>"5MP Rear | 2MP Front Camera",
												"ram"=>"1GB RAM | 16GB ROM | Expandable Upto 32GB",
												"battery"=>"2350 mAh Battery",
												"warranty"=>"1 Year Manufacturer Warranty"
										),
								),
						)
				);
			}
		}
			return $productJson;
			
	}

	public static function ampPdpPriceJson($productId)
	{
		$response = array();
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		$data = "{'productId':['" . $productId . "']}";
		$productInfoUrl = $serverUrl."/rest/product/getConfinedProductInfo/";
		$result = Utils::getProtectedApiResponse($productInfoUrl, CURLOPT_POST, null, $data, null);
		if(isset($result)){
	
			$jsonResult = json_decode($result['data'],true);
			//print_r($jsonResult);die;
			//$this->view->setVar("all", $jsonResult);
			if(isset($jsonResult[$productId])){
	
				$product = $jsonResult[$productId];
				$showAmazonProduct=false;
				$listPrice=0;$defaultPrice=0;$discountedPrice=0;$shipPrice=0;$avPrice=0;
				$offerPrice=0;$mrp=0;$offerText=false;$emi=0;
	
				if(isset($product['productOfferObj']['offerText']) && $product['productOfferObj']['offerText'] != "NO_OFFER")
				{
					$offerText=true;
				}
				foreach($product['priceObjList'] as $obj)
				{
					if($obj['productPriceTypeId']==='AVERAGE_COST'){
						$avPrice=$obj->price;
					}elseif($obj['productPriceTypeId']==='DEFAULT_PRICE'){
						$defaultPrice=$obj['price'];
					}else if($obj['productPriceTypeId']==='DISCOUNTED_PRICE'){
						$discountedPrice=$obj['price'];
					}else if($obj['productPriceTypeId']==='LIST_PRICE'){
						$listPrice=$obj['price'];
					}else if($obj['productPriceTypeId']==='SHIPPING_PRICE'){
						$shipPrice=$obj['price'];
					}
				}
				// calculate discount price and percentage
				$discountAmount =0;
				if(isset($discountedPrice) && $discountedPrice>0){
					$discountAmount=$listPrice-$discountedPrice;
				}else if(isset($defaultPrice) && $defaultPrice>0){
					$discountAmount=$listPrice-$defaultPrice;
				}
	
				$discountPercentage = round($discountAmount*100/$listPrice);
				// Offer price
				if($product['isAutoGCOK'] && $discountedPrice>0)
				{
					$offerPrice=$discountedPrice;
					$mrp=$listPrice;
				}else
				{
					$offerPrice = $defaultPrice;
					$mrp=$defaultPrice;
				}
				$price=($avPrice>0?$avPrice:($offerPrice>0?$offerPrice:$mrp));
				if(($discountedPrice>=3000) || ($discountedPrice==0 && $price>=3000)) {
					$emi = ($discountedPrice>0)?number_format(round($discountedPrice/12)):number_format(round(($price+$shipPrice)/12));
				}
	
				// Check inventory
				$data  = "{'productId':'".$productId."'}";
				$url = $serverUrl."/rest/inventory/checkProductEnableToBuy";
				$invResult = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
				$inventoryFlag=false;
				if(isset($invResult['data'])){
					$invJson = json_decode($invResult['data']);
					if(isset($invJson->responseMap->status) && $invJson->responseMap->status==='success' && $invJson->responseMap->flag=="true")
					{
						$showAmazonProduct = false;
						$inventoryFlag=true;
					}
				}// Ends Inventory result
			}
		}
		$a = array ("items"=>
				array(
						array
						(
								"offer_price"=>$offerPrice,
								"mrp"=>$listPrice,
								"stock"=>$inventoryFlag,
								"isAmz"=>false,
								"you_save"=>$discountAmount,
								"you_save_percent"=>$discountPercentage.'%',
								"emi_starting"=>$emi,
								"emi"=>true,
								"save"=>true,
								"offer_text"=>$offerText,
								"Instock"=>$inventoryFlag
						),
				)
		);
		return $a;
	}// Ends AmpPDPJson

	public static function ampPDPPromotedJson($position)
	{
		$amzBanneApirUrl=$GLOBALS['general']['AMAZON_BANNER_API'];
		$amzBannerRes =  file_get_contents($amzBanneApirUrl);
		$response="";
		if(isset($amzBannerRes)) {
			$amzresult = json_decode($amzBannerRes,true);
			foreach($amzresult['promoted'] as $value)
			{
				$number = $value['number'];
				$active = $value['active'];
				if($position == $number)
				{
					$promoAfName = $value['fname'];
					$promoStartDate = $value['startdate'];
					$promoEndDate = $value['enddate'];
					$promoTagName= $value['tagname'];
					$promoEventcat= $value['Eventcat'];
					$promoEventaction= $value['Eventaction'];
					$promoEventlabel= $value['Eventlabel'];
					$promoTaglabel= $value['taglabel'];
					$promoImgpath = $value['mbannername'];
					$promoBannerurl = $value['mbannerurl'];
					$promoBannertext = $value['bannertext'];
					$promoEventaction=$promoEventaction."_pdp_mweb_promoted";
					
					$promointermediateUrl =  $value['intermediateUrl'];
					if($promoAfName === "amazon") // For amazon and tatacliq intermediate URL is of m.gadgetsnow.com
					{
						$promonofollowtag="rel=nofollow";
						$promointermediateUrl = str_replace("https://www.amazon.in","", $promoBannerurl);
						$promointermediateUrl = $promointermediateUrl."&tag=timofind-21";
						$promointermediateUrl = rawurlencode($promointermediateUrl);
						$promointermediateUrl = $promointermediateUrl."&utm_campaign=mweb_gnshop_promowidget-21&utm_source=mweb_gnshop&utm_medium=Affiliate&price=0&title=".rawurlencode($promoBannertext);
						$promointermediateUrl = "http://m.gadgetsnow.com/affiliate_amazon.cms?url=".$promointermediateUrl;
					
						$promoBannerurl=$promointermediateUrl;
					}elseif($promoAfName == "tatacliq")
					{
						$tatacliqUrl = str_replace("https://www.tatacliq.com","",$promoBannerurl);
						$tatacliqUrl = $tatacliqUrl;
						$nameurl = str_replace(" ","_",$promoBannertext);
						$tatacliqUrl = rawurlencode($tatacliqUrl)."&tag=mweb_gnshop_promoted&utm_source=mweb_gnshop&utm_medium=Affiliate&title=".rawurlencode($nameurl)."&price=".$sprice;
						$tatacliqUrl=$tatacliqInternediateurl."?url=".$tatacliqUrl;
						$promoBannerurl=$tatacliqUrl;
					}else {
						$promoTagName=$promoTagName."_pdp_mweb";
						$samsungUrl = $promoBannerurl;
						$nameurl = str_replace(" ","_",$promoBannertext);
						$intermediateUrl = "https://m.shop.gadgetsnow.com/affiliate.cms?url=";
						$samsungUrl = $intermediateUrl.rawurlencode($samsungUrl)."&tag=".$promoTagName."&afname=".$promoTagName."&utm_medium=Affiliate"."&title=".rawurlencode($nameurl)."&price=";
						$promoBannerurl=$samsungUrl;
						 
					}// case where banner is of gnshop to be handled.
					
					$response = array ("items"=>
					array(
						array
						(
								"af_name"=>$promoAfName,
								"event_action"=>$promoEventaction,
								"event_label"=>$promoEventlabel,
								"event_cat"=>$promoAfName,
								"tag_label"=>$promoTaglabel,
								"image_path"=>$promoImgpath,
								"banner_url"=>$promoBannerurl,
								"banner_text"=>$promoBannertext
						),
				)
		);
				}// Ends if number == pos
			} //for ends
		}// if promoted ends
		return $response;
	}
	/**
	 * JSON for Sponsored : In store section
	 * @return multitype:multitype:
	 */
	public static function ampPDPSponsoredJson()
	{
		$amazonSponsoredUrl =$GLOBALS['general']['AMAZON_SPONSORED_API'];
		$amazonSponsoredUrl=$amazonSponsoredUrl."?format=json";
		$affiliateSponsoredProductsJson =  Utils::getProtectedApiResponse($amazonSponsoredUrl, null, null, null, null);
		if(isset($affiliateSponsoredProductsJson['data']))
		{
			$items= array();
			$affiliateSponsoredProducts=json_decode($affiliateSponsoredProductsJson['data'],true);
			foreach($affiliateSponsoredProducts['product'] as $affSProd )
			{
				$name = $affSProd['title'];
				$sponsrdUrl =$affSProd['url'];
				$sprice =$affSProd['price'];
				$imageUrl =$affSProd['imageUrl'];
				
				$affiliate="amazon";
				$event="add2cart_mweb_amazon_sponsor";
				$sponsrdUrl = str_replace("https://www.amazon.in","",$sponsrdUrl);
				$sponsrdUrl = str_replace("http://www.amazon.in","",$sponsrdUrl);
				if(strpos($sponsrdUrl,"?") == false)
				{
					$sponsrdUrl=$sponsrdUrl."?";
				}else
				{
					$sponsrdUrl=$sponsrdUrl."&";
				}
				$sponsrdUrl = $sponsrdUrl."SubscriptionId=AKIAJX7PDW7DEP2LLJLA&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B01ABNWQI4";
				$sponsrdUrl = $sponsrdUrl."&tag=timofind-21";
				$sponsrdUrl = rawurlencode($sponsrdUrl)."&utm_campaign=mweb_gnshop_spons-21&utm_source=mweb_gnshop&utm_medium=Affiliate&title=".rawurlencode($name)."&price=".$sprice;
				$sponsrdUrl="http://m.gadgetsnow.com/affiliate_amazon.cms?url=".$sponsrdUrl;
				$it = array("name"=>$name,
							"sponsoredUrl"=>$sponsrdUrl,
							"price"=>number_format($sprice),
							"imageUrl"=>$imageUrl);
				array_push($items,$it);
				
			} //Ends foreach
			$response = array("items"=>$items);
		}// Ends if
		return $response;
	}



public static function checkampDeliveryLocation(){
	$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
	$pinnumber = strip_tags(html_entity_decode($_POST['pinNumber']));
	$pID=strip_tags(html_entity_decode($_POST['productId']));
	$url = $serverUrl.'/rest/product/checkPdPDeliveryLocation?pinNumber='.$pinnumber.'&productId='.$pID;
	$resp = Utils::getProtectedApiResponse($url, null, null, null, null);
	$jsonResult = json_decode($resp['data']);
	/* if(isset($jsonResult->status) && $jsonResult->status == "SUCCESS"){
		setcookie("PINCODE", $_GET ['pinNumber'], time () + 60 * 60 * 24 * 30 * 12, "/" );
	} */
	$items= array();
	$it = array("status"=>"SUCCESS",
			"deliverydate"=>"Express Delivery on or before" );
	array_push($items,$it);
	$response = array("items"=>$items);
	return $response;
}

}
?>