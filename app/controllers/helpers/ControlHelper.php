<?php
// include __DIR__ . "/../../config/Utils.php";

class ControlHelper {
	
	public static function mtkeywordsearch($thisObj){
		$thisObj->view->setVar("leafTitle","mtkeyword");
		$searchStr = $_GET['SEARCH_STRING'];
		$searchStr1 = urldecode($searchStr);
		$searchStr1 = strip_tags($searchStr1);
		$thisObj->view->setVar("searchStr",$searchStr1);
		//SGN:733 Noindex, No follow - Search Pages in WAP (SEO Changes)
		$thisObj->view->setVar ( "setmeta", "nofollow, noindex" );
		$url = $thisObj->backendOldServerUrl."/control/mtkeywordsearchJson?SEARCH_STRING=".urlencode($searchStr1)."&catalog=all";
		$authInput = $GLOBALS['general']['AUTH_INPUT'];
		$result = Utils::getProtectedApiResponse($url, null, null, null, $authInput);
		$products = [];
		if(isset($result['data'])){
			$jsonResult = json_decode($result['data']);
	
			if(isset($jsonResult->catalogCount)){
				$catalogCount = $jsonResult->catalogCount;
				$thisObj->view->setVar("catalogCount", $catalogCount);
			}
		
			if(isset($jsonResult->productCount)){
				$productCount = $jsonResult->productCount;
				$thisObj->view->setVar("totalProductCount", $productCount);
			}
			$customCategories = $jsonResult->customCategories;
			$thisObj->view->setVar("customCategories", $customCategories);
			foreach($customCategories as $extn){
				if(isset($extn->products[0])){
					$products = array_merge($products,$extn->products);
				}
			}
			if(isset($jsonResult->refineSearch)){
				$refineSearch = $jsonResult->refineSearch;
				$thisObj->view->setVar("refineSearch", $refineSearch);
			}
		}
	
		$paramStr=null;$productUrlArr=null;
		if(isset($products)){
			$extns = $products;
			foreach($extns as $extn){
				$paramStr .= "productIds=".$extn->PRODUCT_ID."&";
			}
			$productsUrl = $thisObj->backendOldServerUrl."/control/getJsonUrl?".$paramStr;
			$urlResult = Utils::getProtectedApiResponse($productsUrl, null, null, null, $authInput);
			//echo "urlResult: ".$productsUrl."<br/>".$urlResult['data'];die;
			if(isset($urlResult['data'])){
				$productUrlArr = json_decode($urlResult['data'],true);
				$thisObj->view->setVar("productUrlArr", $productUrlArr['product']);
				$thisObj->view->setVar("productImageUrlArr", Utils::secureUrls($productUrlArr['product_Image']));
				//print_r($productUrlArr['product_Image']);
			}
		}
	}
	
	public static function getPinPointSearchResult($thisObj)
	{
		$thisObj->view->setVar("leafTitle","pinPoint");
		$thisObj->view->setVar("catalogName", "TEST");
		$searchStr = $_GET['SEARCH_STRING'];
		$filter = $_GET['filter'];
		$authInput = $GLOBALS['general']['AUTH_INPUT'];
	
		$thisObj->view->setVar("searchString",$searchStr);
		$url = $thisObj->backendOldServerUrl."/control/pinpointsearchJson?SEARCH_STRING=".urlencode($searchStr)."&filter=".$filter."&out_of_stock=0";
		
		if(isset($_GET['start']))
		{
			$nextPage =$_GET['start'];
			$url=$url."&start=".$nextPage;
		}
		if(isset($_POST['queryParam']))
		{
			$sortType =$_POST['queryParam'];
			$sortType = urldecode($sortType);
			$url=$url.$sortType;
		}
		else if(isset($_GET['sort']))
		{
			$sortType =$_GET['sort'];
			$url=$url."&sort=".$sortType;
		}
		

		$result = Utils::getProtectedApiResponse($url, null, null, null, null);
		if(isset($result)){
			$jsonResult = json_decode($result['data']);


			if(isset($jsonResult->catalogName)){
				$catalogName = $jsonResult->catalogName;
				$thisObj->view->setVar("catalogName", $catalogName);
			}
			
			if(isset($jsonResult->productCount)){
				$productCount = $jsonResult->productCount;
				$thisObj->view->setVar("totalProductCount", $productCount);
			}
			if(isset($jsonResult->products) /*&& $jsonResult->response->numFound > 0*/){
				$products = $jsonResult->products;
				$thisObj->view->setVar("products", $products);
				
				$paramStr=null;$productUrlArr=null;
				if(isset($products)){
					$extns = $products;
					foreach($extns as $extn){
						$paramStr .= "productIds=".$extn->PRODUCT_ID."&";
					}
					$productsUrl = $thisObj->backendOldServerUrl."/control/getJsonUrl?".$paramStr;
					$urlResult = Utils::getProtectedApiResponse($productsUrl, null, null, null, $authInput);
					//echo "urlResult: ".$productsUrl."<br/>".$urlResult['data'];die;
					if(isset($urlResult['data'])){
						$productUrlArr = json_decode($urlResult['data'],true);
						$thisObj->view->setVar("productUrlArr", $productUrlArr['product']);
						$thisObj->view->setVar("productImageUrlArr", Utils::secureUrls($productUrlArr['product_Image']));
						//print_r($productUrlArr['product_Image']);
					}
				}
			}
			if(isset($jsonResult->sortBy) /*&& $jsonResult->response->numFound > 0*/){
				$sortBy = $jsonResult->sortBy;
				$thisObj->view->setVar("sortBy", $sortBy);
			}
		
		if(isset($jsonResult->page)){
			$page = $jsonResult->page;
			foreach ($page as $pageObj)
			{
		
				if (isset($pageObj->pageName) && "Next"===$pageObj->pageName) {
					$thisObj->view->setVar ("nextPage", $pageObj->pageUrl );
				}
			}
		
			//$this->view->setVar("numFound", $jsonResult->response->numFound);
		}
		//print_r($jsonResult);
		//die;
		}
	}
	
	public static function createBulkBuy($thisObj)
	{ 
		$redirect="/bulkOrders";
		$cntctName = $_POST['contactName'];
		$cntctNo = $_POST['contactNumber'];
		$captcharesponse = $_POST['g-recaptcha-response'];
		$captchaVerify = ControlHelper::verifyRecaptcha($captcharesponse);
		if($captchaVerify)
		{
			$emailId = $_POST['email'];
			$remarks='';
			$company='';
			$productInterested='';
			$qty=0;
			if(isset($_POST['remarks']))
				$remarks = $_POST['remarks'];
			if(isset($_POST['company']))
				$company = $_POST['company'];
			if(isset($_POST['productinterested']))
				$productInterested = $_POST['productinterested'];
			if(isset($_POST['quantity']))
				$qty = $_POST['quantity'];
			
			$res="";
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$url = $serverUrl."/rest/order/bulkOrder";
			$data = '{"contactName":"'.$cntctName.'","cntctNumber":"'.$cntctNo.'","emailAddress":"'.$emailId.'","remarks":"'.$remarks.'","company":"'.$company.'","productInterested":"'.$productInterested.'","quantity":"'.$qty.'"}';
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
			
			if(isset($result)){
				$resultJson = json_decode($result['data'],true);
				$status = $resultJson['status']; 
				if($status === 'success')
				{
					$response = ControlHelper::sendOTP($cntctNo,"bulkbuy");
				    if($response == "success")
					{
						$res = 'OTPSENT';
					}else
					{
						$res = 'OTPNOTSENT';
					}
				}else {
						$res = 'Error while creating bulk buy';
				}
			}
		}else 
		{
			$res = 'Error: Captcha not validated.';
		}
		return $res;
		//header ( "location:" . $redirect );
	}
	public static function verifyRecaptcha($captcha)
	{
		$url = "https://www.google.com/recaptcha/api/siteverify";
		$secret = "6LcOmDsUAAAAAARj1XQVtX8CuyX1167xzes4wyXs";
		$USER_AGENT = "Mozilla/5.0";
		$data = '{"secret":"'.$secret.'","response":"'.$captcha.'"}';
		
		$postdata = http_build_query(
				array(
						'secret' => '6LcOmDsUAAAAAARj1XQVtX8CuyX1167xzes4wyXs',
						'response' => $captcha
				)
		);
		
		$opts = array('http' =>
				array(
						'method'  => 'POST',
						'header'  => "Content-Type:  application/x-www-form-urlencoded",
						'content' => $postdata,
						'timeout' => 60
				)
		);
		 
		$context  = stream_context_create($opts);
		$result = file_get_contents($url, false, $context, -1, 40000);
		$resJson = json_decode($result,true);
		$response = $resJson['success'];
		return $response;
	}
	public static function createSellwithus($thisObj)
	{
		$redirect="/sellWithUs";
		$cntctName = $_POST['contactName'];
		$cntctNo = $_POST['contactNumber'];
		$emailId = $_POST['email'];
		$location =$_POST['cityname'];
		$natureBusiness = $_POST['natureBusiness'];
		$categoryToSell= $_POST['category'];
		$remarks='';$company='';
		$captcharesponse = $_POST['g-recaptcha-response'];
		$captchaVerify = ControlHelper::verifyRecaptcha($captcharesponse);
		if($captchaVerify)
		{
			if(isset($_POST['remarks']))
				$remarks = $_POST['remarks'];
			if(isset($_POST['company']))
				$company = $_POST['company'];
			
			$presentlySelling = 'N';
			$sellerName='';
			$ecommerePlatform='';
			if(isset($_POST['ecommerce']))
			{
				$presentlySelling = $_POST['ecommerce'];
			}
			if(isset($_POST['EcommercePlatform']))
			{
				$ecommerePlatform = $_POST['EcommercePlatform'];
			}
			if(isset($_POST['sellername']))
			{
				$sellerName = $_POST['sellername'];
			}
			$res="";
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$url = $serverUrl."/rest/party/saveSellwithus";
			$data = '{"sellerName":"'.$sellerName.'","ecommerePlatform":"'.$ecommerePlatform.'","presentlySelling":"'.$presentlySelling.'","contactName":"'.$cntctName.'","cntctNumber":"'.$cntctNo.'","emailAddress":"'.$emailId.'","remarks":"'.$remarks.'","company":"'.$company.'","nature":"'.$natureBusiness.'","categoriesToSell":"'.$categoryToSell.'","location":"'.$location.'"}';
			$result = Utils::getProtectedApiResponse ($url, CURLOPT_POST, null, $data, null );
		
			if(isset($result)){
				$resultJson = json_decode($result['data'],true);
				$status = $resultJson['status']; 
				if($status === 'success')
				{
					$response = ControlHelper::sendOTP($cntctNo,"sellwithus");
					if($response == "success")
					{
						$res = 'OTPSENT';
					}else
					{
						$res = 'OTPNOTSENT';
					}
				}else {
					$res='Error while creating sell with us';
				}
			}
		}else 
		{
			$res = 'Error: Captcha not validated.';
		}
		return $res;
		//header ( "location:" . $redirect );
	}
	
	public static function sendOTP($mobileNo,$otpFor)
	{
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$url = $serverUrl . '/rest/pmt/cod/getOTP';
		//$mobileNo = $_POST['mobileNo'];
		$otp="";
		$data='{"mobileNo":"'.$mobileNo.'","otp_type":"COD_OTP"}';
		
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		
		if(isset($result))
		{
			$resultJson = json_decode ( $result ['data'] );
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				$otp=$resultJson->codOtp;
				$_SESSION['OTP_'.$otpFor]= $otp;
			}
			return array("status"=>"success","otp"=>$otp);
		}else {
			return array("status"=>"error");
		}
		
	}
	public static function verifyOtp($this)
	{
		$otpentered = $_POST['otp'];
		$otpFor = $_POST['otpFor'];
		$cntctNumber = $_POST['mobileno'];
		$otp=$_SESSION['OTP_'.$otpFor];
		$otpVerified = array("status"=>"error");
		
		if(isset($_SESSION['OTP'.$otpFor]))
		{
			$otpCount = $_SESSION['OTP_'.$otpFor];
		}
		
		if($otp == $otpentered)
		{
			$otpVerified=array("status"=>"success");
		}elseif ($otpentered == 'NA')
		{
			$otpVerified = array("status"=>"error");
		}
		return $otpVerified;
	}
	public static function validateOTP($this)
	{
		$otpCount=0;
		
		$otpentered = $_POST['otp'];
		$otpFor = $_POST['otpFor'];
		$cntctNumber = $_POST['mobile'];
		$otp=$_SESSION['OTP_'.$otpFor];
		$otpVerified = 'N';
		
		if(isset($_SESSION['OTP'.$otpFor]))
		{
			$otpCount = $_SESSION['OTP'.$otpFor];
		}
		
		if($otp == $otpentered)
		{
			$otpVerified='Y';
		}elseif ($otpentered == 'NA')
		{
			$otpVerified = 'N';
		}else {
			if($otpCount == 5)
			{
				return "expired";
			}else
			{
				$otpCount = $otpCount+1;
				$_SESSION['OTP'.$otpFor]=$otpCount;
				return "error";
			}
		}
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		if($otpFor == "bulkbuy")
			$url = $serverUrl . '/rest/order/updatebulkOrder';
		elseif ($otpFor == "sellwithus")
			$url = $serverUrl . '/rest/party/updateSellwithus';
		$data='{"cntctNumber":"'.$cntctNumber.'","otpverified":"'.$otpVerified.'"}';
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		
		if(isset($result))
		{
			$resultJson = json_decode ( $result ['data'] );
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				return "success";
			}
		}
		return "success";
	}
	
	public static function submitReview($this)
	{
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$userLoginId="ANONYMOUS";
		$redirect = isset ( $_SERVER ['HTTP_REFERER'] ) ? $_SERVER ['HTTP_REFERER'] : "/";
		$frontUrl = $GLOBALS['general']['FRONT_END_SERVER_URL'];
		if(  (strpos($redirect , "p_") !== false) && (strpos($redirect , "/control/")==false))
		{
			$redirect = str_replace($frontUrl,$frontUrl."/control",$redirect);
		}
		$reviewtitle = $_POST['reviewTitle'];
		$casedescription=$_POST['productReview'];
		$productRating=$_POST['productRating'];
		$productTitle=$_POST['product_title'];
		$productId=$_POST['productId'];
		//print_r($_SESSION ['userLogin']);die;
		if (isset($_SESSION ['userLogin']) && (($_SESSION ['userLogin']->getUserName()!='Guest')))
		{
			$userLoginId =  $_SESSION ['userLogin']->getUserLoginId();
			error_log($userLoginId);
			
			if( isset($casedescription) && (strlen(trim($casedescription)) > 7))
			{
			$data = "{'productId':'".$productId."','userLoginId':'".$userLoginId."','productRating':'".$productRating."','productTitle':'".$productTitle."','RnRType':'RRB','ratingDesc':'".$casedescription."','ratingTitle':'".$reviewtitle."'}";
			$url = $serverUrl."/rest/product/submitProductReviews";
			
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
			if(isset($result))
			{
				$resultJson = json_decode ( $result ['data'] );
				if(isset($resultJson->status) && $resultJson->status=='success')
				{
					print_r("sucess".$referer);
					$_SESSION['successMsg']="Reviews and ratings successfully saved.";
				}
			}
			}
			else
			{
			$_SESSION['error']="Please eneter a valid review";
			}
		}else 
		{
			$_SESSION['lastRegisterPage'] =$referer;
			$redirect = "/control/login";
		} 
		header("location:".$redirect);
		
	}
	public static function redirectAmazon($this)
	{
		
	}
	public static function submitRating($this)
	{

		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$userLoginId=$_POST['userLoginId'];
		$productRating=$_POST['productRating'];
		$productId=$_POST['productId'];
		
		if (isset ( $_SESSION ['userLogin'] ) && (($_SESSION ['userLogin']->getUserName()!='Guest')))
		{
			$userLoginId =  $_SESSION ['userLogin']->getUserLoginId();
		}/* else
		{
			$_SESSION['lastRegisterPage'] =$referer;
			header("location:"."/control/login");
		} */
		$data = "{'productId':'".$productId."','userLoginId':'".$userLoginId."','productRating':'".$productRating."','RnRType':'RRA'}";
		$url = $serverUrl."/rest/product/submitProductReviews";
		
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		
		if(isset($result))
		{
			$resultJson = json_decode ( $result ['data'] );
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				print_r("sucess".$referer);
			}
		}
	}
	
	public static function bid2win($thisobj)
	{
		$thisobj->view->setVar('leafTitle','bid2win');
		$queryParam="";
		$productId="";
		$urlProducts= $GLOBALS['general']['BACKEND_SERVER_URL']."/rest/bidding/productsForBidding";
		$jsonProductResult= Utils::getProtectedApiResponse($urlProducts, null, null, null,null);
		if($jsonProductResult['code'] == 200)
		{
			$result=json_decode($jsonResult['data'],true);
			$productId = $result['PRODUCT_ID'];
			
		}
		$jsonUrl = "https://shop.gadgetsnow.com/api/mobile-store-json.php";
		$usrPwd = "admin:gad@687";
		$solrUrl = "http://solr.shop.gadgetsnow.com:8282/solr";//$GLOBALS['general']['SOLR_SERVER_URL'];
		$jsonResult= Utils::getProtectedApiResponse($jsonUrl, null, null, null,null);
		
		
		if($jsonResult['code'] == '200'){
			$result=json_decode($jsonResult['data'],true);
			$thisobj->view->setVar('bannerName',$result['BannerName']);
			$productArr = $result['productIdArr'];
			$thisobj->view->setVar('productArr',$productArr);
			$queryParam = $queryParam."PRODUCT_ID%3A".$productId;
			$thisobj->view->setVar("dealproductId","G13168");
			$thisobj->view->setVar("pageProductId","G13168");
			$thisobj->view->setVar("dealType","Bid");
			$solrurl= $solrUrl."/mt_core/select?q=".$queryParam."&fq=OUT_OF_STOCK:0&wt=json&rt=market&start=0&rows=100";
			$productFromSolr = Utils::getApiResponse($solrurl);
			if(isset($productFromSolr)){
				$productFromSolrResult = json_decode($productFromSolr['data']);
				if(isset($productFromSolrResult->response->docs)){
					$products = $productFromSolrResult->response->docs;
					$thisobj->view->setVar("products",$products);
				}
			}
		}	
	}
	
	public static function sendContactusMail()
	{
		
		$captcharesponse = $_POST['g-recaptcha-response'];
		$captchaVerify = ControlHelper::verifyRecaptcha($captcharesponse);print_r("test---".$captchaVerify);
		if(true)
		{
			$queryType= $_POST['queryType'];
			$orderId="NA";
			if(isset($_POST['orderId']))
				$orderId=$_POST['orderId'];
			$issueType="NA";
			if(isset($_POST['issueType']))
				$issueType=$_POST['issueType'];
			$issueSubType="";
			if(isset($_POST['issueSubType']))
				$issueSubType=$_POST['issueSubType'];
			$issueSubType1="";
			if(isset($_POST['issueSubType1']))
			$issueSubType1=$_POST['issueSubType1'];
			
			$contactnumber=$_POST['contactNumber'];
			$emailId=$_POST['emailId'];
			$firstName=$_POST['firstName'];
			$lastName=$_POST['lastName'];
			$message=$_POST['message'];
			
			$data='{"userId":"'.$emailId.'","orderId":"'.$orderId.'","queryType":"'.$queryType.'","issueType":"'.$issueType.'","issueSubType":"'.$issueSubType.'","issueSubType1":"'.$issueSubType1.'","firstName":"'.$firstName.'","lastName":"'.$lastName.'","emailId":"'.$emailId.'","contactNumber":"'.$contactnumber.'","message":"'.$message.'"}';
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$url=$serverUrl."/rest/contactusmail"; 
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
			print_r($result);
			if(isset($result) && $result['code'] == '200')
			{
				print_r($result['code']);
				$_SESSION['successMsg']='Mail sent to concerned person.';
				header ( "location:" . "/contactUs" );
			}
		}
	
	}
}
?>
