<?php
// include __DIR__ . "/../../config/Utils.php";

class ProductHelper {
	
	public static function getLowestPriceAffiliate($productlatest)
	{
		$staticImgUrl = $GLOBALS['general']['STATIC_IMG_URL'];
		$afproddetails=null;
		$productLate=json_decode($productlatest);
		$latestProdPrice=0;
		$prodImageUrl = str_replace("/original/","/small/",$staticImgUrl.$productLate->originalImageUrl);
		$mrpP=0;$default=0;$discountedPriceP=0;$avPriceP=0;
		foreach($productLate->priceObjList as $obj){
			if($obj->productPriceTypeId==='AVERAGE_COST'){
				$avPriceP=$obj->price;
			}else if($obj->productPriceTypeId==='DEFAULT_PRICE'){
				$default=$obj->price;
			}else if($obj->productPriceTypeId==='DISCOUNTED_PRICE'){
				$discountedPriceP=$obj->price;
			}else if($obj->productPriceTypeId==='LIST_PRICE'){
				$mrpP=$obj->price;
			} 
		}
		if($discountedPriceP > 0)
			$latestProdPrice = $discountedPriceP;
		else $latestProdPrice=$default;
		if(isset($productLate->affiliateProducts))
		{

			$affiliateExists=true;
			$affiliateProducts = $productLate->affiliateProducts;
			$affiliateList="";$totalaffiliate=0;
			$priceArray = array();
			$dummy = false;
			$dummy = $affiliateProducts[0]->AFFILIATE_NAME;
			$priceAfiiliateArray=array();
				
			foreach($affiliateProducts as $affp)
			{
				$affiliateArray = array();
				$aff_name= $affp->AFFILIATE_NAME;
				if($affiliateList != "")
					$affiliateList = $affiliateList.','.$aff_name;
				else
					$affiliateList = $aff_name;
				$aff_pname=$affp->AFFILIATE_PRODUCT_NAME;
				$aff_url=$affp->AFFILIATE_PRODUCT_URL;
				$aff_price = $affp->PRICE;
				$offer="";
				$totalaffiliate=$totalaffiliate+1;
				if(isset($affp->OFFER) && $affp->OFFER != "0")
				{
					$offer=$affp->OFFER;
				}
				
				$affiliateParamArray=array("productname"=>$aff_pname,"price"=>$aff_price,"producturl"=>$aff_url,"pagename"=>"pdp","widget"=>"buyat");
				$affiliateLandingurl=AffiliateUrlCreator::createAffiliateUrl($aff_name,$affiliateParamArray);
				
				$affIndex = strtoupper($aff_name);
				$priceAfiiliateArray[$affIndex]=$affiliateArray;
				$priceArray[$aff_price]=$affIndex;
				
				 
			}
			 
			// sort the array on key which has prices of all affiliates
			ksort($priceArray,SORT_NUMERIC);
			// Get the first element which is the affiliate with lowest price
			 
			$showFirst=reset($priceArray);
			if(isset($priceAfiiliateArray[$showFirst]))
			{
				$afproddetails = $priceAfiiliateArray[$showFirst];
			}
			
		}  
		// If no affiliate is set then set SGN product
		if(!isset($afproddetails['productname']) || $afproddetails['productname'] == "")
		{
			$afproddetails['afname']="SGN";
			$afproddetails['productname']=$productLate->productName;
			$afproddetails['url']="";
			$afproddetails['image']=$prodImageUrl;
			$afproddetails['price']=$latestProdPrice;
		}
		return (array (
				"afname" => $afproddetails['afname'],
				"productname" => $afproddetails['productname'],
				"url" => $afproddetails['url'],
				"price" => $afproddetails['price'],
				"imageurl"=>$afproddetails['image']
		));
	}
	
	public static function addInCartFromPDP(){
		
		$queryString = "";// $_SERVER['QUERY_STRING'];
		$productId=$_GET['add_product_id'];
		$utmSource=isset($_GET['utm_source'])?$_GET['utm_source']:"";
		$utmMedium=isset($_GET['utm_medium'])?$_GET['utm_medium']:"";
		$utmCampaign=isset($_GET['utm_campaign'])?$_GET['utm_campaign']:"";
		$productName="";
		if(isset($_GET['productName']))
			$productName=$_GET['productName'];
		$catalogId=$_GET['catalogId'];
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		if($productId!=""){
			if(!isset($_SESSION)) session_start();
			if(isset($_SESSION['userLogin'])){
				$partyId=$_SESSION['userLogin']->getPartyId();//"3604637";
					
				// add to wishlist
				$data  = "{'partyId':'".$partyId."','productId':'".$productId."','quantity':'1','shoppingListType':'SLT_SPEC_PURP'}";
				$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
				$url = $serverUrl."/rest/sc/addItemtoShoppinglist";
				$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
				if(isset($result['data'])){
					$jsonResult = json_decode($result['data']);
					if(isset($jsonResult->status) && $jsonResult->status === "success"){
						if(!isset($_SESSION)) session_start();
						$_SESSION['successMsg']=$jsonResult->SuccessMsg;
						if(isset($jsonResult->code) && $jsonResult->code == "41")
						{
							$_SESSION['successMsg']="Congratulations!. The product ".$productName." has been successfully added to your shopping cart.";
						}
						if(!isset($_SESSION)) session_start();
						unset($_SESSION["gcValues"]);
					}else{
						if(!isset($_SESSION)) session_start();
						$_SESSION['error']='Error: '.$jsonResult->ErrorMsg;
						if(isset($jsonResult->code) && $jsonResult->code == "40")
						{
							$_SESSION['error']="Hey! Looks like the product ".$productName." is already in your cart. Should you wish to purchase the same product, may we request you to increase the quantity?";
						}
					
					}
				}
			} else 
			{
				// inventory check call
				$url = $serverUrl."/rest/inventory/checkProductEnableToBuy";
				$data  = "{'productId':'".$productId."'}";
				$invResult = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
				$inventoryFlag=false;
				if(isset($invResult['data'])){
					$invJson = json_decode($invResult['data']);
					if(isset($invJson->responseMap->status) && $invJson->responseMap->status==='success' && $invJson->responseMap->flag=="true"){
						$inventoryFlag=true;
					}
				}
				if($inventoryFlag)
				{
					$isExistInCart=false;
					$userCart=new ShoppingCart();
					$shoppingCart=(isset($_SESSION['shoppingCart'])?$_SESSION['shoppingCart']:$userCart);//print_r($shoppingCart);
					$shoppingList=$shoppingCart->getShoppingList();
					$shoppingCartItems=$shoppingList->getShoppingListItems();
					$counter=sizeof($shoppingCartItems);
					if($counter>0){
						foreach ($shoppingCartItems as $productObj){
							$sci=new ShoppingListItem();
							if (get_class($productObj)=='stdClass') {
								Utils::Cast($sci, $productObj);
							}else {
								$sci=$productObj;
							}
							if($sci->getProductId()==$productId){
								$isExistInCart=true;
							}
						}
					}
					if(!$isExistInCart){
						if(!isset($_SESSION)) session_start();
						unset($_SESSION["gcValues"]);
						$shoppingCartItem=new ShoppingListItem();
						$shoppingCartItem->setProductId($productId);
						$shoppingCartItem->setProdCatalogId($catalogId);
						$shoppingCartItem->setQuantity(1);
						$shoppingCartItems[$counter]=$shoppingCartItem;
						$shoppingList->setShoppingListItems($shoppingCartItems);
						
						$_SESSION['shoppingCart']=$shoppingCart;
						$_SESSION['successMsg']="Congratulations!. The product ".$productName." has been successfully added to your shopping cart.";
					}else{
						$_SESSION['error']="Hey! Looks like the product ".$productName." is already in your cart. Should you wish to purchase the same product, may we request you to increase the quantity?";
					}
				}else 
				{
					$_SESSION['error']="The product ".$productName." is currently out of stock.";
				}
			}
		}
		if(isset($utmSource) && $utmSource != "" && isset($utmMedium) && $utmMedium != "" && isset($utmCampaign) && $utmCampaign != "") 
		{
			$queryString = "?utm_source=".$utmSource."&utm_medium=".$utmMedium."&utm_campaign=".$utmCampaign;
		}
		$redirect = "/control/showcart".$queryString;
		header('location:'.htmlentities($redirect));
	}
	
	public static function addInWishlistFromPDP(){
		$productId="";
		if(isset($_GET['add_product_id']))
			$productId=$_GET['add_product_id'];
		if(!isset($productId) || $productId==""){if(isset($_SESSION['pId'])){$productId = $_SESSION['pId'];}}
		
		if(!isset($_SESSION)) session_start();
		if(isset($_SESSION['userLogin']) && $_SESSION ['userLogin']->getUserName()!='Guest'){
			$partyId=$_SESSION['userLogin']->getPartyId();//"3604637";
	
			// add to wishlist
			$data  = "{'partyId':'".$partyId."','productId':'".$productId."','quantity':'1','shoppingListType':'SLT_WISH_LIST'}";
			$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
			$url = $serverUrl."/rest/sc/addItemtoShoppinglist";
			$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
			if(isset($result['data'])){
				$jsonResult = json_decode($result['data']);
				if(isset($jsonResult->status) && $jsonResult->status === "success"){
					if(!isset($_SESSION)) session_start();
					//$_SESSION['successMsg']=$jsonResult->SuccessMsg;
				}else{
					if(!isset($_SESSION)) session_start();
					//$_SESSION['error']='Error: '.$jsonResult->ErrorMsg;
				}
			}
			
			//to direct the page to PDP after add to wishlist
			if(isset($_SESSION['lastRegisterPage'])){
				$redirect = $_SESSION['lastRegisterPage'];
			} else {
				$redirect = $_SERVER['HTTP_REFERER'];
			}
			
			header('location:'.htmlentities($redirect));
		} else {
			$_SESSION['pId'] = $_GET['add_product_id'];
			$_SESSION['fromPDP'] = 'pdp';
			$_SESSION['lastRegisterPage']=$_SERVER['HTTP_REFERER'];
			
			$redirect = "/control/login";
			header('location:'.htmlentities($redirect));
		}
	}
	
	public static function isInWishlist(){
		$productId=$_GET['productId'];
		if(!isset($_SESSION)) session_start();
		if(isset($_SESSION['userLogin'])){
			$partyId=$_SESSION['userLogin']->getPartyId();//"3604637";
	
			// is exist in wishlist
			$data  = "{'partyId':'".$partyId."','productId':'".$productId."','shoppingListType':'SLT_WISH_LIST'}";
			$url = $serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
			$url = $serverUrl."/rest/sc/isPresentInwishlist";
			$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
			print_r($result['data']);
		}else {
			print_r('{"SuccessMsg": "no"}');
		}
	}
	
	public static function notifyMeForProduct(){
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		$url = $serverUrl.'/rest/product/sendOOSNotificationRegMail';
		$data = "{'userId':'".$_GET['notifyinput']."','productId':'".$_GET['productId']."','productUrl':'".$_GET['productUrl']."'}";		
		//$url = $serverUrl.'/rest/sc/storeInStockNotification?productId='.$_GET['productIdNotify'].'&emailId='.$_GET['notifyinput'].'&productUrl='.$_GET['productUrl'];
		$resp = Utils::getProtectedApiResponse($url, null, null, $data, null);
		print_r($resp['data']);
	}
	
	public static function storeInStockNotification($thisObj)
	{
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		$url = $serverUrl.'/rest/sc/storeInStockNotification?hashCode='.$_GET['hashCode'];
		$resp = Utils::getProtectedApiResponse($url, null, null, null, null);
		print_r($resp);
		if(isset($resp['data'])){
			$jsonResult = json_decode($resp['data']);
			if(isset($jsonResult->productName)){
		        $thisObj->view->setVar("productName",$jsonResult->productName);
			}
		}
		$thisObj->view->setVar("leafTitle",'notify');
		//$thisObj->view->setVar("emailId",$_GET['emailId']);
		
	}
	public static function checkDeliveryLocation(){
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		$url = $serverUrl.'/rest/product/checkPdPDeliveryLocation?pinNumber='.$_GET['pinNumber'].'&productId='.$_GET['productId'];
		$resp = Utils::getProtectedApiResponse($url, null, null, null, null);
		$jsonResult = json_decode($resp['data']);
		if(isset($jsonResult->status) && $jsonResult->status == "SUCCESS"){
			setcookie("PINCODE", $_GET ['pinNumber'], time () + 60 * 60 * 24 * 30 * 12, "/" );
		}	
		print_r($resp['data']);
	}
	
	public static function ampPdpSeeAlso($productId)
	{
		$data  = "{'productId':'".$productId."'}";
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		$productBasicInfoUrl = $serverUrl."/rest/product/getProductBasicInfo/";
		$result = Utils::getProtectedApiResponse($productBasicInfoUrl, CURLOPT_POST, null, $data, null);
		if(isset($result))
		{
			$jsonResult = json_decode($result['data']);
			if(isset($jsonResult->similarProductIds) && sizeof($jsonResult->similarProductIds)>0)
			{ 
				$pidsForSeeAlso = implode("','", $jsonResult->similarProductIds);
				$pidsForSeeAlso = "'".$pidsForSeeAlso."','".$productId."'";
			}else
			{
				$pidsForSeeAlso ="'".$productId."'";
			}
		}
		
		$data = "{'productId':[" . $pidsForSeeAlso . "]}";
		$affiliateProductsJson =  Utils::getProtectedApiResponse($amazonProductsUrl, CURLOPT_POST, null, $data, null);
		 
		if(isset($affiliateProductsJson['data']))
		{
			$affiliateProducts=json_decode($affiliateProductsJson['data'],true);
			$this->view->setVar("affiliateRecoProds",$affiliateProducts);
			foreach ($affiliateRecoProds as $affiliateProd)
			{
				if($maxcounts == 4)
					break;
				$affPName = $affiliateProd['AFFILIATE_PRODUCT_NAME'];
				$price = $affiliateProd['PRICE'];
			
				$landingUrl = $affiliateProd['AFFILIATE_PRODUCT_URL'];
				$landingUrl = str_replace("https://www.amazon.in","",$landingUrl);
				$landingUrl = str_replace("http://www.amazon.in","",$landingUrl);
				if(strpos($landingUrl,"?") == false)
				{
					$landingUrl = $landingUrl."?tag=timofind-21";
				}else 
				{
					$landingUrl = $landingUrl."&tag=timofind-21";
				}
				$landingUrl = rawurlencode($landingUrl)."&utm_source=mweb_gnshop&utm_medium=Affiliate&title=".rawurlencode($affPName);
			}
		}
	}
	
	
	
	public static function submitQuestion($this)
	{
	
		$productQuestion= 'NA';
		$productId='NA';
		$userLoginId='';
		//Skip Security test to fix PDP Cache issue
		$securityTokenresult='PASS';
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$responseCode = 'error';
		$status = 'error';
		$responseMsg = 'Sorry your question cannot be post due to some issue';
		//Skip Security test to fix PDP Cache issue
		
	/*	if (isset( $_POST ['token'] ))
		{
			$clientSecurityToken =$_POST['token'];
			if (isset ( $_SESSION ['securityTokenQue'] ))
			{
				$securityTokenQue =    $_SESSION ['securityTokenQue'] ;
				if($securityTokenQue == $clientSecurityToken)	
				{
					$securityTokenresult = 'PASS';
				}else
				{
				$responseMsg = Sorry! Multiple questions can't be posted at the same time.
				}	
			}
			error_log("-------clientSecurityToken".$clientSecurityToken);
			error_log("-------securityTokenQue".$securityTokenQue);
			error_log("-------------------USERlogin".$userLoginId);
		} */
		error_log("  SecurityToken pass  ".$securityTokenresult , 0);
	
		if (isset( $_POST ['askQuestion'] ))
		{
		       $productQuestion=$_POST['askQuestion'];
		}
		
		if (isset( $_POST ['productId'] ))
		{
		       $productId=$_POST['productId'];
		}
		
		if (isset ( $_SESSION ['userLogin'] ) && (($_SESSION ['userLogin']->getUserName()!='Guest')))
		{
			$userLoginId =  $_SESSION ['userLogin']->getUserLoginId();
		}
		
		if($productQuestion!='NA' && $productId!='NA'  && $securityTokenresult!='FAIL')
		{
		$productQuestion =    htmlentities($productQuestion);	
		$data = "{\"productId\":\"".$productId."\",\"userLoginId\":\"".$userLoginId."\",\"productQuestion\":\"".$productQuestion."\"}";
		$url = $serverUrl."/rest/product/submitProductQue";
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		}
		if(isset($result)  && $securityTokenresult!='FAIL')
		{		
			$resultJson = json_decode ( $result ['data'] );
			error_log(" submitProductQue Response  ".$result ['data'] , 0);
			if(isset($resultJson->status) && $resultJson->status=='success')
			{	
				$responseCode = 'success';
				$status = $resultJson->status;
				$responseMsg = 'Thank you for your question. It will appear here in a while.';
				
				if(isset($_SESSION ['securityTokenQue']))
				{
					unset($_SESSION ['securityTokenQue']);
				}
			}
			else
			{
				$responseCode = 'error';
				$responseMsg = $resultJson->message;
			}
		}
		
		return (array (
				"status" => $status,
				"responseCode" => $responseCode,
				"responseMsg" => $responseMsg,
		));
	}
	
	
	public static function submitAnswer($this)
	{
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$productAnswer= 'NA';
		$productQuestionId='NA';
		$userLoginId='';
		$securityTokenresult='PASS';
		if (isset( $_POST ['answer'] ))
		{
			$productAnswer=$_POST['answer'];
		}
		if (isset( $_POST ['questionId'] ))
		{
			$productQuestionId=$_POST['questionId'];
		}
		$responseCode = 'error';
		$status = 'error';
		$responseMsg = 'Sorry your answer cannot be post due to some issue';

		/*if (isset( $_POST ['token'.$productQuestionId] ))
		{
			$clientSecurityToken =$_POST['token'.$productQuestionId];
			if (isset ( $_SESSION ['securityToken'.$productQuestionId] ))
			{
				$securityTokenQue =    $_SESSION ['securityToken'.$productQuestionId] ;
				error_log("  SecurityToken  ".$securityTokenQue , 0);
				if(round( $securityTokenQue) == round( $clientSecurityToken))
				{
					$securityTokenresult = 'PASS';
				}
				else
				{
				$responseMsg = Sorry! Multiple Answer can't be posted at the same time.
				}
			}
		}
		*/
		error_log("  SecurityToken pass  ".$securityTokenresult , 0);
		
		if (isset ( $_SESSION ['userLogin'] ) && (($_SESSION ['userLogin']->getUserName()!='Guest')))
		{
			$userLoginId =  $_SESSION ['userLogin']->getUserLoginId();
		}
		if($productQuestionId!='NA' && $productAnswer!='NA' && $securityTokenresult!='FAIL' )
		{
			
		$productAnswer =    htmlentities($productAnswer);
		$data = "{\"productQuestionId\":\"".$productQuestionId."\",\"userLoginId\":\"".$userLoginId."\",\"productAnswer\":\"".$productAnswer."\"}";
		$url = $serverUrl."/rest/product/submitProductAns";
		$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		}
				
		if(isset($result) && $securityTokenresult!='FAIL')
		{
			$resultJson = json_decode ( $result ['data'] );
			error_log(" submitProductAns Response  ".$result ['data'] , 0);
			if(isset($resultJson->status) && $resultJson->status=='success')
			{
				$status = $resultJson->status;
				$responseCode = 'success';
				$responseMsg = 'Thank you for your answer. It will appear here in a while.';
				if(isset($_SESSION ['securityToken'.$productQuestionId]))
				{
					unset($_SESSION ['securityToken'.$productQuestionId]);
				}	
			}
			else
			{
				$responseCode = 'error';
				$responseMsg = $resultJson->message;
			}
		}
		return (array (
				"status" => $status,
				"responseCode" => $responseCode,
				"responseMsg" => $responseMsg,
		
		));
	}
	
	
}
?>