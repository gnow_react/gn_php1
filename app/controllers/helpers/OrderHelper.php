<?php
// include __DIR__ . "/../../config/Utils.php";

class OrderHelper {
	
	public static function getOrderDetails($thisObj){
		$thisObj->view->setVar("leafTitle","thankyou");
		
		if (! isset ( $_SESSION )) session_start ();
		if (! isset ( $_SESSION ['userLogin'] )) {
			header("Location:/control/logout");
		} else {
			if(isset($_SESSION['orderId'])){
				$oId=$_SESSION['orderId'];
				unset($_SESSION['orderId'],$oId);
			}
			
			$partyId=$_SESSION ['userLogin']->getPartyId ();
			
			$orderId = isset($_GET['orderId'])?$_GET['orderId']:"";
			
			
			if($orderId!=''){
				$data="{'orderId':'".$orderId."'}";
				$url=$thisObj->backendServerUrl."/rest/order/detail";
				$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
				if(isset($result['data']) && isset($result['code']) && $result['code']==200){
					$order = json_decode($result['data'],true);
					if(!isset($order['responseMap']) && $partyId===$order['partyId']){
						$thisObj->view->setVar("order", $order);
					}else{
					header("Location:/control/logout");
					}
				}else{
					header("Location:/control/logout");
				}
			}
		}
	}
	
	public static function getOrderHistory($thisObj){
		$thisObj->view->setVar("leafTitle","orderhistory");
		$data="";
		if (! isset ( $_SESSION )) session_start ();
		if ((isset ( $_SESSION ['userLogin'] )) &&  $_SESSION ['userLogin']->getUserName()!='Guest') {
			$partyId=$_SESSION ['userLogin']->getPartyId ();
			$orderId = isset($_GET['orderId'])?$_GET['orderId']:"";
			if($orderId!=''){
				$data="{'partyId':'".$partyId."','orderId':'".trim($orderId)."'}";
			}else{
				$data="{'partyId':'".$partyId."','index':'".(isset($_GET['VIEW_INDEX'])?$_GET['VIEW_INDEX']:0)."'}";
			}	
		}else if(isset($_POST['orderId']) && (isset($_POST['orderEmail']) || isset($_POST['contactMobile']))){
			$data="{'orderId':'".trim($_POST['orderId'])."','emailId':'".trim($_POST['orderEmail'])."','mobileNum':'".trim($_POST['contactMobile'])."'}";
		}
		print_r($data);	
		if($data!=""){	
// 			$url="localhost:8090/shopping/rest/order/getOrderHistory";
 			$url=$thisObj->backendServerUrl."/rest/order/getOrderHistory";
			//print_r($url);die;
			$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
			if(isset($result['data']) && isset($result['code']) && $result['code']==200){
				$orderList = json_decode($result['data']);
				if(isset($orderList->status) && $orderList->status === 'success'){
				
				$orders=array();
				$productIds=array();
				foreach ($orderList->orderHistory as $item){
					$order=isset($orders[$item->ORDER_ID])?$orders[$item->ORDER_ID]:array();
					array_push($order, $item);
					$orders[$item->ORDER_ID]=$order;
					if (strpos ( $item->PRODUCT_ID, '-' )) {
						array_push($productIds,strstr ( $item->PRODUCT_ID, '-', true ));
					}else{
						array_push($productIds, $item->PRODUCT_ID);
					}
				}
				if(isset($orderList->ocount)){
					$thisObj->view->setVar("orderCount", $orderList->ocount);
				}
				if(sizeof($productIds)){
					$prods = implode ( "&productIds=", $productIds );
					
					$url = $thisObj->backendOldServerUrl."/control/getJsonUrl?productIds=".$prods;
					$usrPwd = $GLOBALS ['general'] ['AUTH_INPUT'];
					$urlResult = Utils::getProtectedApiResponse($url, null, null, null, $usrPwd);
					if(isset($urlResult['data'])){
						$productUrlArr = json_decode($urlResult['data'],true);
						$thisObj->view->setVar("prodUrlArr", $productUrlArr['product']);
						$thisObj->view->setVar("prodImageUrlArr", Utils::secureUrls($productUrlArr['product_Image']));
					}
				}
				$thisObj->view->setVar("orderList", $orders);
			}else if(isset($orderList->status) && $orderList->status === 'error'){
					$_SESSION['error'] = $orderList->message;
			}
		  }
		}
	}	
	
	public static function getCasedetails($thisObj){
		$thisObj->view->setVar("leafTitle","caseDetail");
		if (! isset ( $_SESSION )) session_start ();
		if (! isset ( $_SESSION ['userLogin'] )) {
			header("Location:/control/login");
		} else if(isset($_GET['custRequestId'])){
			$partyId=$_SESSION ['userLogin']->getPartyId ();
			$custRequestId = $_GET['custRequestId'];
			if($custRequestId!=''){
				$data="{'partyId':'".$partyId."','custRequestId':'".trim($custRequestId)."'}";
			}	
	//		$url="localhost:8090/shopping/rest/order/getCasedetails";
			$url=$thisObj->backendServerUrl."/rest/order/getCasedetails";
			$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
			if(isset($result['data']) && isset($result['code']) && $result['code']==200){
				$case = json_decode($result['data']);
				if(isset($case->caseMap)){
					$thisObj->view->setVar("caseMap", $case->caseMap);
					if(isset($case->caseNotes)){
						$thisObj->view->setVar("caseNotes", $case->caseNotes);
					}
				}
			}
		}
	}
	
	public static function createCaseNote($thisObj){
		if (! isset ( $_SESSION )) session_start ();
		if (! isset ( $_SESSION ['userLogin'] )) {
			header("Location:/control/login");exit();
		} else {
			$redirect="/control/casedetails";
			if(isset($_POST['custRequestId']) && isset($_POST['note'])){
				$partyId=$_SESSION ['userLogin']->getPartyId ();
				$custRequestId = $_POST['custRequestId'];
				$internalNote = $_POST['internalNote'];
				$note = $_POST['note'];
				$remove_character = array("\n", "\r\n", "\n\r", "\r");
				$note = str_replace($remove_character , ' ', $note);
					
				if($custRequestId!=''){
					$data="{'partyId':'".$partyId."','custRequestId':'".trim($custRequestId)."','noteInfo':'".trim($note)."','internalNote':'".$internalNote."'}";
				}
				//$url="localhost:8090/shopping/rest/order/createCustRequestNote";
 				$url=$thisObj->backendServerUrl."/rest/order/createCustRequestNote";
				$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
			}
			
			if(isset($_POST['custRequestId'])){
				$redirect=$redirect."?custRequestId=".$_POST['custRequestId'];
			}
			header("Location:".htmlentities($redirect));exit();
		}
	}
	
	public static function reportIssue($thisObj){
		if (! isset ( $_SESSION )) session_start ();
		
		if (! isset ( $_SESSION ['userLogin'] )) {
			header("Location:/control/login");exit();

		}else if ( $_SESSION ['userLogin']->getUserName()==='Guest'){
			header("Location:/control/login");exit();
		
		}else if(isset($_GET['orderId']) && isset($_GET['caseCategory'])) {
			$caseCategory=$_GET['caseCategory'];
			if($caseCategory=='REQUEST_WEB_CASE'){
				$thisObj->view->setVar("leafTitle","reportCase");
			}else if($caseCategory=='REQUEST_CANCELLATI'){
				$thisObj->view->setVar("leafTitle","reportCancellation");
			}else{
				header("Location:/control/orderhistory");exit();
			}
//			$url="localhost:8090/shopping/rest/order/getOrderItemForReportIssue?orderId=".$_GET['orderId']."&partyId=".$_SESSION ['userLogin']->getPartyId ()."&caseCategory=".$caseCategory;
 			$url=$thisObj->backendServerUrl."/rest/order/getOrderItemForReportIssue?orderId=".$_GET['orderId']."&partyId=".$_SESSION ['userLogin']->getPartyId ()."&caseCategory=".$caseCategory;

			$result = Utils::getProtectedApiResponse($url, null, null, null, null);
			if(isset($result) && isset($result['data']) && isset($result['code']) && $result['code']==200){
				$orderItems = json_decode($result['data']);
				if(isset($orderItems->status) && $orderItems->status=='success'){
					$thisObj->view->setVar("orderItems", $orderItems->orderItems);
					$thisObj->view->setVar("caseType", "REQUEST");
					$thisObj->view->setVar("caseCategory", $caseCategory);
					
				}else{
					header("Location:/control/orderhistory");exit();
				}
			}
		} else {
			header("Location:/control/orderhistory");exit();
		}
	}
	
	public static function createCase($thisObj){
		if (! isset ( $_SESSION )) session_start ();
		if (! isset ( $_SESSION ['userLogin'] )) {
			header("Location:/control/login");exit();
		} else {
			$redirect="/control/orderhistory";
			if(isset($_POST['orderId']) && isset($_POST['salesChannelEnumId']) && isset($_POST['custRequestTypeId']) && isset($_POST['custRequestCategoryId']) 
					&& isset($_POST['orderItemSeqId'])){
				$partyId=$_SESSION ['userLogin']->getPartyId ();
				$userLoginId=$_SESSION ['userLogin']->getUserLoginId ();
				$data="'partyId':'".$partyId."','userLoginId':'".$userLoginId."','orderId':'".$_POST['orderId']."','salesChannelEnumId':'".$_POST['salesChannelEnumId'].
					"','caseCategory':'".$_POST['custRequestCategoryId']."','custRequestTypeId':'".$_POST['custRequestTypeId']."','orderItemSeqId':'".$_POST['orderItemSeqId']."'";
				
				$description = $_POST['description'];
				$remove_character = array("\n", "\r\n", "\n\r", "\r");
				$description = trim(str_replace($remove_character , ' ', $description));
				if($description!=''){
					$data=$data.",'description':'".$description."'";
				}
				if(isset($_POST['cancellationReason'])){
					$data=$data.",'cancellationReason':'".$_POST['cancellationReason']."'";
				}
				if(isset($_POST['custRequestSubtypeId'])){
					$data=$data.",'custRequestSubtypeId':'".$_POST['custRequestSubtypeId']."'";
				}
				
				$data="{".$data."}";
				//$url="localhost:8090/shopping/rest/order/createCustRequest";
				$url=$thisObj->backendServerUrl."/rest/order/createCustRequest";
				$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
				
				if(isset($result) && isset($result['data']) && isset($result['code']) && $result['code']==200){
					$res = json_decode($result['data']);
					$resJson = json_decode($res->response);
					if((isset($resJson->STATUS) && $resJson->STATUS=='error')){
						$_SESSION['error']="Error While cancelling items. Please try Again.";
					}					
					
					if((isset($resJson->STATUS) && $resJson->STATUS =='SUCCESS')){
										if(isset($resJson->code) && $resJson->code == "43")
										{
											if(isset($resJson->successmsg))
											$_SESSION['successMsg']= $resJson->successmsg; //"Your Item(s) ".$productName." have been cancelled successfully.";
										}
					}
					else if(isset($res->response) ) 
					{
					  $jsonResponse =   json_decode($res->response);
					  if(isset($jsonResponse->STATUS) && $jsonResponse->STATUS == "SUCCESS")
					  	{
					  	if(isset($res->response->code) && $res->response->code == "43")
										{
											$_SESSION['successMsg']= "Your Item(s) ".$productName." has been cancelled successfully.";
										}
					  
						}
					}
					
				}else{
					$_SESSION['error']="Error While creating issue. Please try Again.";
				}
			}
				
			if(isset($_POST['orderId'])){
				$redirect=$redirect."?orderId=".$_POST['orderId'];
			}
			header("Location:".htmlentities($redirect));exit();
		}
	}
	
	
		public static function createOrderAttribute($thisObj){
		if (! isset ( $_SESSION )) session_start ();
				echo "Debug  in  createOrderAttribute";
		
		if (! isset ( $_SESSION ['userLogin'] )) {
			header("Location:/control/login");exit();
		} else {
			if(isset($_POST['orderId'])) {
				
				$data="'orderId':'".$_POST['orderId']."','attrName':'IS_TP_DISPLAYED','attrValue':'Y'";
				$data="{".$data."}";
				$url=$thisObj->backendServerUrl."/rest/order/orderAttribute";
			//	file_put_contents("E:/mfrontend_V1.5_TOITECH_20160809/app/logs/PHP_LOG.txt", "\n url===".$url." ===data=== ",FILE_APPEND);
			//	file_put_contents("E:/mfrontend_V1.5_TOITECH_20160809/app/logs/PHP_LOG.txt", $data,FILE_APPEND);
				$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
				if(isset($result) && isset($result['data']) && isset($result['code']) && $result['code']==200){
					$res = json_decode($result['data']);
						
					if((isset($res->response->STATUS) && $res->response->STATUS=='error')||(isset($res->status) && $res->status == "error")){
				
				//	file_put_contents("E:/mfrontend_V1.5_TOITECH_20160809/app/logs/PHP_LOG.txt", "Error While inserting  order attribute ",FILE_APPEND);
				//	file_put_contents("E:/mfrontend_V1.5_TOITECH_20160809/app/logs/PHP_LOG.txt", $data,FILE_APPEND);
					
					}					
					else
					{
				//	file_put_contents("E:/mfrontend_V1.5_TOITECH_20160809/app/logs/PHP_LOG.txt", "inserting  order attribute ",FILE_APPEND);
				//	file_put_contents("E:/mfrontend_V1.5_TOITECH_20160809/app/logs/PHP_LOG.txt", $data,FILE_APPEND);
					}
					
				}else{
				//	file_put_contents("E:/mfrontend_V1.5_TOITECH_20160809/app/logs/PHP_LOG.txt", "Error While inserting  order attribute ",FILE_APPEND);
				//	file_put_contents("E:/mfrontend_V1.5_TOITECH_20160809/app/logs/PHP_LOG.txt", $data,FILE_APPEND);

				}
			}
			
		}
	}
	
	
}
?>
