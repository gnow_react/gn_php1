<?php
// include __DIR__ . "/../../config/Utils.php";
class CartHelper {
	public static function getCart($clearGc){
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$sessionCart=isset($_SESSION['shoppingCart'])?$_SESSION ['shoppingCart']:new ShoppingCart();
		if(isset($_SESSION['userLogin']))
		{
			$userLogin=$_SESSION ['userLogin'];
		
			$sessionCart->getShoppingList()->setPartyId($userLogin->getPartyId ());
		}
		$data=json_encode($sessionCart);
		$resp=null;
		
		 if(isset($clearGc))
		{
			$showcartUrl = $serverUrl . '/rest/sc/getShoppingCart?PARTY_ID=' . $sessionCart->getPartyId() . '&SHOPPING_LIST_TYPE=SLT_SPEC_PURP';
			$resp = Utils::getProtectedApiResponse ( $showcartUrl, null, null, null, null );
		}else {
			$showcartUrl = $serverUrl.'/rest/sc/getValidatedShoppingCart';
			$resp = Utils::getProtectedApiResponse ( $showcartUrl, CURLOPT_POST, null, $data, null );
		} 
	//	print_r($resp);die;
		if (isset ( $resp )) {
			if($resp ['code']!=200){
				throw new Exception();
			}
			$respJson = json_decode ( $resp ['data'] );
			if(isset($respJson->status) && $respJson->status=='success'){
				$decodedCart=json_decode ($resp ['data'],true);
				if($decodedCart ['CART']['shoppingList']['shoppingListItems'][0]['productId']!=null){
					$newCart = $respJson->CART;
					$stdobject_ = (object)$newCart;
						
					$userCart=new ShoppingCart();
					Utils::Cast($userCart, $stdobject_);
					if((isset($userLogin)) && $userLogin->getUserName()=='Guest') { $userCart->setUserLogin($userLogin); }
					if($userCart->getOrderId()!=null && $userCart->getOrderId()!=""){$userCart->setOrderId("");}
					$_SESSION ['shoppingCart']=$userCart;
				}else{
					unset($_SESSION['shoppingCart']);
				}
				if(isset($respJson->code) && $respJson->code===300){
					if(!isset($_SESSION["successMsg"]) && $_SESSION["successMsg"]!="") {
						$_SESSION['error']=$respJson->message;
					}
					unset($_SESSION["gcValues"]);
				}
				 if(isset($clearGc))
				{ 
					unset($_SESSION["gcValues"]);
				} 
			}
		}else{
			throw new Exception();
		}
	}
	
	public static function showcart($thisObj) {
	
		$thisObj->view->setVar("leafTitle",'cartPage');
		$clearGc = $_GET['clearGc'];
		$emptyCart = true;
		$prods=array();
		$prodIds = array ();
		$cart = new ShoppingCart ();
		if (! isset ( $_SESSION ))
			session_start ();
		
		if (isset($_SESSION['userLogin'])) {
			CartHelper::getCart($clearGc);
			if(isset($_SESSION['shoppingCart']) && sizeof($_SESSION ['shoppingCart']->getShoppingList()->getShoppingListItems())>0){
				$_SESSION ['shoppingCart']->setShippingContactMechId(null);
				$_SESSION ['shoppingCart']->setShippingAddress(array());
				foreach ($_SESSION ['shoppingCart']->getShoppingList()->getShoppingListItems() as $item){
					$userCartListItem=new ShoppingListItem();
					if (get_class($item)=='stdClass') {
						Utils::Cast($userCartListItem, $item);
					}else {
						$userCartListItem=$item;
					}
					if($userCartListItem->getProductId()!=null){
						if($userCartListItem->getIsVariant()){
							array_push($prods,$userCartListItem->getVirtualProductId());
						}else{
							array_push($prods,$userCartListItem->getProductId());
						}
					}
				}
			}
		} else if (isset($_SESSION['shoppingCart'])) {
			if(!isset ($_SESSION["gcValues"])) {
				$shoppingCart = $_SESSION ['shoppingCart']; 
				$cartItems = array();
				$shoppingCartItems = $shoppingCart->getShoppingList()->getShoppingListItems();
				if (sizeof ( $shoppingCartItems ) > 0) {
					foreach ( $shoppingCartItems as $key => $productObj ) {
						$item=new ShoppingListItem();
						if (get_class($productObj)=='stdClass') {
							Utils::Cast($item, $productObj);
						}else {
							$item=$productObj;
						}
						if ($item->getProductId () != "") {
							$cartItem = new ShoppingListItem();
							$cartItem->setProductId ( $item->getProductId () );
							$cartItem->setQuantity ( $item->getQuantity () );
							$cartItems [$key] = $cartItem;
							$prodIds [$key] = $item->getProductId ();
						}
					}
				}
				if (sizeof ( $prodIds ) > 0) {
					$productIds = implode ( "','", $prodIds );
					$emptyCart = false;
					$data = "{'productId':['" . $productIds . "']}";
					$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
					$isAutoGcApprove = $GLOBALS ['general'] ['gc.auto.approve'];
					$gcId = $GLOBALS ['general'] ['gc.auto.approve.gcId'];
					$gcMaxValue = $GLOBALS ['general'] ['gc.auto.approve.gcMaxValue'];
					$url = $serverUrl."/rest/product/getConfinedProductInfo";
					$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
					if (isset ( $result )) {
						$resultJson = json_decode ( $result ['data'], true );
						$gcDiscount=0;
						foreach ( $cartItems as $ci ) {
							$pid=$ci->getProductId ();
							$cartItem = $resultJson [$pid];
							$ci->setProductName( $cartItem ['productName'] );
							if (strpos ( $pid, '-' )) {
								$ci->setIsVariant ( true );
								$ci->setVirtualProductId ( strstr ( $pid, '-', true ) );
								$ci->setStandardFeature ( $cartItem ['productFeatureMap'] ['STANDARD_FEATURE'] );
								array_push($prods,strstr ( $pid, '-', true ));
							}else{
								array_push($prods,$pid);
							}
							if (isset ( $cartItem ['productOfferObj'] ['offerText'] ))
								$ci->setOfferText ( $cartItem ['productOfferObj'] ['offerText'] );
							foreach ( $cartItem ['priceObjList'] as $obj ) {
								if ($obj ['productPriceTypeId'] === 'DEFAULT_PRICE') {
								$defaultPrice=$obj ['price'];
									$ci->setBasePrice ( $obj ['price'] );
								} else if ($obj ['productPriceTypeId'] === 'DISCOUNTED_PRICE' && $obj ['price']>0 && $isAutoGcApprove==1) {
									$discountedPrice = $obj ['price'];
									$ci->setDisplayPrice ( $obj ['price'] );
								} else if ($obj ['productPriceTypeId'] === 'SHIPPING_PRICE') {
									$ci->setShippingPrice ( $obj ['price'] );
								} else if ($obj ['productPriceTypeId'] === 'LIST_PRICE') {
									$ci->setListPrice ( $obj ['price'] );
								}
							}
							if($ci->getDisplayPrice()==0 && $ci->getBasePrice()>0){
								$ci->setDisplayPrice ($ci->getBasePrice()+$ci->getShippingPrice());
							}else if($ci->getDisplayPrice()>0){
								$gcItemDiscount=($ci->getBasePrice()+$ci->getShippingPrice())-$ci->getDisplayPrice();
								$gcDiscountPerc=$gcItemDiscount*100/($ci->getBasePrice()+$ci->getShippingPrice());
								$default60 = ($ci->getBasePrice() *$gcMaxValue)/100;
								$discNshipping = $discountedPrice+$ci->getShippingPrice();
								if($discNshipping >= $default60)
								{
								  $newDiscount = $defaultPrice - $discountedPrice;
								}
								if($newDiscount < 0)
								{
								  $ci->setGcDiscount(0);
								}else
								{
								 $ci->setGcDiscount($newDiscount);
								}
								/*if($gcDiscountPerc>$gcMaxValue){
									$ci->setDisplayPrice ($ci->getBasePrice()+$ci->getShippingPrice());
								}else{
									$ci->setGcDiscount($gcItemDiscount);
									$gcDiscount=$gcDiscount+$gcItemDiscount;
								}*/
							}
							if (isset ( $cartItem ['category'] ['productCategoryId'] ))
								$ci->setProductCategoryId ( $cartItem ['category'] ['productCategoryId'] );
							if (isset ( $cartItem ['catalog'] ['prodCatalogId'] ))
								$ci->setProdCatalogId ( $cartItem ['catalog'] ['prodCatalogId'] );
							if(isset($cartItem ['introductionDate']) && (strtotime($cartItem ['introductionDate'])>=time()) && ((strtotime($cartItem ['introductionDate'])-time())/(24 * 60 * 60 * 1000)<31))
								$ci->setComingSoon(true);
						}
						$shoppingList=new ShoppingList();
						$shoppingList->setShoppingListItems ( $cartItems );
						$cart->setShoppingList($shoppingList);
						$cart->setShippingAddress(array());
						$cart->setShippingContactMechId(null);
						$_SESSION ['shoppingCart'] = $cart;//print_r(json_encode($_SESSION ['shoppingCart']));die;
					}
				}
			}else{
				foreach ($_SESSION ['shoppingCart']->getShoppingList()->getShoppingListItems() as $item){
					$userCartListItem=new ShoppingListItem();
					if (get_class($item)=='stdClass') {
						Utils::Cast($userCartListItem, $item);
					}else{
						$userCartListItem=$item;
					}
					if($userCartListItem->getProductId()!=null){
						if($userCartListItem->getIsVariant()){
							array_push($prods,$userCartListItem->getVirtualProductId());
						}else{
							array_push($prods,$userCartListItem->getProductId());
						}
					}
				}
			}
		}
		$gcproductIds="";
		if(isset($prods) && sizeof($prods))
		{
			$gcproductIds = implode ( "','", $prods );
		}elseif(isset($prodIds) && sizeof($prodIds)) 
		{
			$gcproductIds =  implode ( "','", $prodIds );
		}
		$userId = "";
		$userLogin=$_SESSION ['userLogin'];
		if(isset($userLogin))
		{
			$userId = $userLogin->getUserLoginId();
		}
		
		$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
		$gcUrl = $serverUrl."/rest/sc/getGCForCart";
		$data = "{'userId':'".$userId."','productId':['" . $gcproductIds . "']}";
		$gcResult = Utils::getProtectedApiResponse($gcUrl, null, null, $data, null);
		if (isset ($gcResult))
		{
			$gcResultJson = json_decode ( $gcResult ['data'], true );
			$productOffers =  $gcResultJson['GCIDs'];
			$thisObj->view->setVar("productOffers",$productOffers);
		}
		if(sizeof($prods)>0){
			$emptyCart = false;
			$productIds = implode ( "&productIds=", $prods );
			$url = $thisObj->backendOldServerUrl."/control/getJsonUrl?productIds=".$productIds;
			$usrPwd = $GLOBALS ['general'] ['AUTH_INPUT'];
			$urlResult = Utils::getProtectedApiResponse($url, null, null, null, $usrPwd);
			if(isset($urlResult['data'])){
				$productUrlArr = json_decode($urlResult['data'],true);
				$thisObj->view->setVar("cartProdUrlArr", $productUrlArr['product']);
				$thisObj->view->setVar("cartProdImageUrlArr", Utils::secureUrls($productUrlArr['product_Image']));
			}
		}
		
		if(isset($_SESSION['shoppingCart'])){
			$thisObj->view->setVar("cart",$_SESSION ['shoppingCart']);
		}
		$thisObj->view->setVar ("emptyCart",$emptyCart);
	}
	public static function showWishList($thisObj) {
		$thisObj->view->setVar("leafTitle",'cartPage');
		$emptyWishList = true;
		$prodIds = array ();
		$wishList = new ShoppingCart ();
		if (! isset ( $_SESSION ))
			session_start ();
		if (isset ( $_SESSION ['userLogin'] ) && $_SESSION ['userLogin']->getUserName()!='Guest') {
			$prods=array();
			$partyId = $_SESSION ['userLogin']->getPartyId (); // "3604637";
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$showcartUrl = $serverUrl . '/rest/sc/getShoppingCart?PARTY_ID=' . $partyId . '&SHOPPING_LIST_TYPE=SLT_WISH_LIST';
			$resp = Utils::getProtectedApiResponse ( $showcartUrl, null, null, null, null );
			if (isset ( $resp )) {
				$respJson = json_decode ( $resp ['data'] );
					
				if(isset($respJson->status) && $respJson->status=='success'){
					$decodedCart=json_decode ($resp ['data'],true);
					if($decodedCart ['CART']['shoppingList']['shoppingListItems'][0]['productId']!=null){
						$newCart = $respJson->CART;
						$stdobject_ = (object)$newCart;
	
						Utils::Cast($wishList, $stdobject_);
						if(sizeof($wishList->getShoppingList()->getShoppingListItems())>0){
							foreach ($wishList->getShoppingList()->getShoppingListItems() as $item){
								$userCartListItem=new ShoppingListItem();
								if (get_class($item)=='stdClass') {
									Utils::Cast($userCartListItem, $item);
								}else{
									$userCartListItem=$item;
								}
								if($userCartListItem->getProductId()!=null){
									if($userCartListItem->getIsVariant()){
										array_push($prods,$userCartListItem->getVirtualProductId());
									}else{
										array_push($prods,$userCartListItem->getProductId());
									}
								}
							}
							
							if(sizeof($prods)>0){
								$emptyWishList = false;
								$productIds = implode ( "&productIds=", $prods );
								$url = $thisObj->backendOldServerUrl."/control/getJsonUrl?productIds=".$productIds;
								$usrPwd = $GLOBALS ['general'] ['AUTH_INPUT'];
								
								$urlResult = Utils::getProtectedApiResponse($url, null, null, null, $usrPwd);
								if(isset($urlResult['data'])){
									$productUrlArr = json_decode($urlResult['data'],true);
									$thisObj->view->setVar("wishlistProdUrlArr", $productUrlArr['product']);
									$thisObj->view->setVar("wishlistProdImageUrlArr", Utils::secureUrls($productUrlArr['product_Image']));
								}
							}
						}
					}
				}
			}
			
			$thisObj->view->setVars ( array ("wishList" => $wishList,"emptyWishList" => $emptyWishList) );
			
		} 
	
	}
	public static function modifycart() {
		$productId = $_POST ['productid'];
		if (! isset ( $_SESSION ))
			session_start ();
		$cart = $_SESSION ['shoppingCart'];
		$cartAction = $_POST ['cart_action'];
		if($cart != null && $cart->getShoppingList() != null && $cart->getShoppingList()!="")
		{
			if (isset ( $_SESSION ['userLogin'] )) 
			{
				$partyId = $_SESSION ['userLogin']->getPartyId ();
				
					if ($cartAction == 'delete') 
					{
						// delete from cart
						$data = "{'productId':'" . $productId . "','shoppingListId':'" . $cart->getShoppingList()->getShoppingListId() . "','shoppingListType':'SLT_SPEC_PURP'}";
						$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
						$url = $serverUrl . "/rest/sc/itemRemoveFromShoppinglist";
						$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
		
					} else {
						// update to cart
						$data = "{'productId':'" . $productId . "','shoppingListId':'" . $cart->getShoppingList()->getShoppingListId() . "','quantity':'" . $_POST ['quantity'] . "'}";
						$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
						$url = $serverUrl . "/rest/sc/updateCart";
						$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
						
						if(isset($result)){
							$jsonResult=json_decode($result['data']);
							if(isset($jsonResult->status) && $jsonResult->status === "error"){
								if(!isset($_SESSION)) session_start();
								$_SESSION['error']='Error: '.$jsonResult->message;
							}
						}
					}
			} else {
				$shoppingList=$cart->getShoppingList();
				$shoppingCartItems = $shoppingList->getShoppingListItems ();
				
				$cartItems = array ();
				$counter = 0;
				foreach ( $shoppingCartItems as $productObj ) {
					$sci=new ShoppingListItem();
					if (get_class($productObj)=='stdClass') {
						Utils::Cast($sci, $productObj);	
					}else {
						$sci=$productObj;
					}
					if ($sci->getProductId () != $productId) {
						$cartItems [$counter ++] = $sci;
					} elseif ($cartAction != 'delete') {
						$sci->setQuantity ( $_POST ['quantity'] );
						$cartItems [$counter ++] = $sci;
					}
				}
				$shoppingList->setShoppingListItems ( $cartItems );
				$_SESSION ['shoppingCart'] = $cart;
			}
		}
		if(!isset($_SESSION)) session_start();
		//unset($_SESSION["gcValues"]);
		$redirect = "/control/showcart";
		$gcDisType = $cart->getGiftCoupondiscountType();
		if(isset($_SESSION['gcValues']['coupon']) && isset($gcDisType) && $gcDisType == 'PERCENTAGE')
		{
			
			$gcId =  $_SESSION['gcValues']['coupon'];
			CartHelper::getCart(true);
			CartHelper::gcCalculateForCart($gcId);
		}else 
		{  
			header ( 'location:' . $redirect );
		}
		
	}

	public static function modifycartToWishlist(){
		
		$productId = "";
		$productId = $_POST ['productid'];
		if($productId == "" && isset($_SESSION['pId'])){$productId = $_SESSION['pId'];}
		$cart = $_SESSION ['shoppingCart'];
		if(!isset($_SESSION)) session_start();
						
		if(isset($_SESSION['userLogin']) && ($_SESSION['userLogin']->getUserName() != 'Guest')){
			
			$partyId=$_SESSION['userLogin']->getPartyId();
			$data = "{'partyId':'".$partyId."','productId':'" . $productId . "','shoppingListId':'" . $cart->getShoppingList()->getShoppingListId() . "','shoppingListType':'SLT_SPEC_PURP'}";
			$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
			$url = $serverUrl."/rest/sc/moveCartItemToWishlist";
			$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
			if(isset($result['data'])){
				$jsonResult = json_decode($result['data']);
				if(isset($jsonResult->status) && $jsonResult->status === "success"){
					if(!isset($_SESSION)) session_start();
					$_SESSION['successMsg']=$jsonResult->SuccessMsg;
										
				}else{
					if(!isset($_SESSION)) session_start();
					$_SESSION['error']='Error: '.$jsonResult->message;
				}
			}
			header('location:/control/showcart');exit;
		} else {
			$_SESSION['pId'] = $_POST['productid'];
			$redirect = "/control/login";
			header('location:'.$redirect);
		}
	}	
	
	public static function modifywishlist() {
		$productId=$_POST['productid'];
		$productName = $_POST['productName'];
		$shoppingListId=$_POST['cart_id'];
		$shoppingListItemSeqId=$_POST['cart_item_seq_id'];
		$cartAction=$_POST['cart_action'];
		if (! isset ( $_SESSION ))
			session_start ();
		
		if (isset ( $_SESSION ['userLogin'] )) {
			$partyId = $_SESSION ['userLogin']->getPartyId (); // "3604637";
			if ($cartAction == 'delete') {
				// delete from cart
				$data = "{'productId':'" . $productId . "','shoppingListId':'" .$shoppingListId. "','shoppingListType':'SLT_WISH_LIST'}";
				$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
				$url = $serverUrl . "/rest/sc/itemRemoveFromShoppinglist";
				$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
				$redirect = "/control/showcart?mywishlist";
			} else {
				// move to cart
				$data  = "{'partyId':'".$partyId."','shoppingListId':'".$shoppingListId."','shoppingListItemSeqId':'".$shoppingListItemSeqId."'}";
				$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
				$url = $serverUrl."/rest/sc/moveWishListItemToCart";
				$result = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
				if(isset($result)){
					$jsonResult=json_decode($result['data']);
					if(isset($jsonResult->status) && $jsonResult->status === "success"){
						if(!isset($_SESSION)) session_start();
						$_SESSION['successMsg']="Congratulations!. The product ".$productName." has been successfully moved to your shopping cart.";//$jsonResult->SuccessMsg;
						unset($_SESSION["gcValues"]);
					}else{
						if(!isset($_SESSION)) session_start();
						$_SESSION['error']='Error: '.$jsonResult->ErrorMsg;
						if(isset($jsonResult->ErrorMsg) && $jsonResult->ErrorMsg === "Product already in list == SLT_SPEC_PURP")
						{
							$_SESSION['error']='Error: Product already exists in cart.';
						}
					}
				}
				$redirect = "/control/showcart";
			}
			
		}
		header ( 'location:' . $redirect );
	}
	
	public static function inventoryCheck(){
		$inventoryFlag="false";
		if(isset($_GET['productId']) && trim($_GET['productId']!="")){
			$data="{'productId':'".$_GET['productId']."'}";
			$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
			$url = $serverUrl."/rest/inventory/checkProductEnableToBuy";
			$invResult = Utils::getProtectedApiResponse($url, CURLOPT_POST, null, $data, null);
			if(isset($invResult['data'])){
				$invJson = json_decode($invResult['data']);
				if(isset($invJson->responseMap->status) && $invJson->responseMap->status==='success' && $invJson->responseMap->flag=="true"){
					$inventoryFlag="true";
				}
			}
		}
		return array('inStock'=>$inventoryFlag);
	}
	
	public static function gcCalculateForCart($gcId) {
		if(isset($_SESSION['gcValues']) && isset($_SESSION['shoppingCart']))
			{
				unset($_SESSION ['gcValues']);
				$_SESSION ['shoppingCart']->setGiftCouponUsed(false);
			}
		// Get it from POST or as function parameter
		if(isset($_SESSION ['shoppingCart']) && isset($_POST ['gc']) && trim($_POST ['gc'])!="")
		{
			$gcUsed = $_POST['gc'];
		}else 
		{ 
			$gcUsed = $gcId;
		}
		  if(isset($gcUsed)){
			if (! isset ( $_SESSION )) session_start ();
			// apply GC to cart
			$_SESSION ['shoppingCart']->setGiftCoupons(trim($gcUsed));
			$data=json_encode($_SESSION ['shoppingCart']);
			$serverUrl = $GLOBALS ['general'] ['BACKEND_SERVER_URL'];
			$url = $serverUrl . "/rest/gcws/validateGC";
			$result = Utils::getProtectedApiResponse ( $url, CURLOPT_POST, null, $data, null );
			//print_r($url.'---'.$data);print_r($result);die;
			if(isset($result)){
				$gcValidation=json_decode($result['data']);
				if(isset($gcValidation->status) && $gcValidation->status=='success'){
					$gcValues=array();
					$newCart=$gcValidation->cart;
					$stdobject_ = (object)$newCart;
					
					$gcCart=new ShoppingCart();
					Utils::Cast($gcCart, $stdobject_);
					$_SESSION ['shoppingCart']=$gcCart;
					$_SESSION ['shoppingCart']->setGiftCoupons(trim($gcUsed));
					$gcValues['coupon']=$gcUsed;
					$gcValues['status']=$gcValidation->status;
					$_SESSION ['gcValues']=$gcValues;
// 					if(strpos($gcUsed,'SGN'))
// 					{
					 // $_SESSION['successMsg']='Coupon Applied: Your cashback will be credited to Freecharge wallet within 90 days after completion of Order.';
// 					}else {
						$_SESSION['successMsg']='Coupon code has been successfully applied.';
// 					}
				}else{
					$_SESSION ['error']=$gcValidation->message;
				}
			}
		}
		$redirect = "/control/showcart";
		header ( 'location:' . $redirect );
	}
}
?>
