<?php
include __DIR__ . "/../config/Utils.php";
class GadgetsnowController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        //parent::__construct();
        //$this->showSearchTab = "false";
        //$this->backendServerUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
        //$this->backendOldServerUrl = $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
    }
    public function ssoAction()
    {
        
    }
    public function healthcheckAction()
    {
        
    }
    public function relatedpagesAction()
    {
        $this->view->setVar("leafTitle", "relatedPages");
        $productId = $this->dispatcher->getparam("productId");
        $category = $this->dispatcher->getparam("category");
        $categoryObj = Utils::getGadgetCategory($category);
        $this->view->setVar("categoryData", $categoryObj);
		$this->view->setVar("domainPrefix", "https://www.gadgetsnow.com");
		$productUrl = '/'.$categoryObj["seoName"].'/'.$productId;
		$compUrlsuffix = '/compare-'.$category.'/';
		$photoGallerySuffix = $categoryObj["seoName"].'/images/';
		 $this->view->setVar("mainProUrl", "https://gadgetsnow.com"."$productUrl");
		 $this->view->setVar("compUrlsuffix", $compUrlsuffix);
		 $this->view->setVar("photoGallerySuffix", $photoGallerySuffix);
        $categoryparam = $GLOBALS['gncategory'][$category];
        $dataGA = ['category' => 'WAP_GS_NEW'];
        $url = "https://www.gadgetsnow.com/pwafeeds/gnow/mweb/list/gadgets/json?path=/relateProductDetail/&category=" . $categoryObj["id"] . "&productid=" . $productId;
        $data = array(
            "header" => "https://www.gadgetsnow.com/pwafeeds/gnow/mweb/common/header/json?path=/general",
            "footer" => "https://www.gadgetsnow.com/pwafeeds/gnow/web/common/footer/json",
            "relateddata" => $url,
        );
        $headerData = [
            ["title" => "News", "url" => "/latest-news"],
            ["title" => "Compare", "url" => "/compare-mobile-phones"],
            ["title" => "Reviews", "url" => "/reviews"],
            ["title" => "Videos", "url" => "/videos"],
            ["title" => "Top Gadgets", "url" => "/top-gadgets"],
        ];
        $responseData = Utils::multiRequest($data);
        if (!empty($responseData["footer"])) {
            $footerData = $responseData["footer"]["jsonFeed"]['sections'];
        }
        if (!empty($responseData["relateddata"])) {
            $relatedData = $responseData["relateddata"]["jsonFeed"]["data"]["items"];
            $seoDetailData = $responseData["relateddata"]["jsonFeed"]["seoDetail"];
        }else{
            http_response_code(500);exit;
        }
        if (!empty($responseData["header"])) {
            $topSearches = $responseData["header"]["jsonFeed"]['sections']['Trending']['data']['items'];
        }
        $seoData = [
			'title' => $seoDetailData["metaTitle"] ? $seoDetailData["metaTitle"] : "Gadgetsnow",
            'keywords' => $seoDetailData["kws"] ? $seoDetailData["kws"] : "",
            'description' => $seoDetailData["metaDescription"] ? $seoDetailData["metaDescription"] : "",
            'canonicalUrl' => $seoDetailData["metaIndex"] === 'index' ? 'https://www.gadgetsnow.com'.$productUrl.'/alternatives' : `https://www.gadgetsnow.com`,
            'robots' => $seoDetailData["metaIndex"] === 'index' ? 'index, follow' : 'noindex,nofollow',
        ];
        $showFooter=$showHeader=true;
        $this->view->setVar("showFooter", $showFooter);
        $this->view->setVar("showAds", false);
        $this->view->setVar("showSearch", false);
        $this->view->setVar("showBottomNav", true);
        $this->view->setVar("showHeader", $showHeader);
        $this->view->setVar("headerData", $headerData);
        $this->view->setVar("topSearches", $topSearches);
        $this->view->setVar("footerData", $footerData);
        $this->view->setVar("pageData", $relatedData);
        //print_r($relatedData);die;
        $this->view->setVar("seoData", $seoData);
        $this->view->setVar("dataGA", $dataGA);
    }

    public function gadgetshowAction()
    {
        $productId = $this->dispatcher->getparam("productId");
        $category = $this->dispatcher->getparam("category");
        $categoryObj = Utils::getGadgetCategory($category);
        if (empty($categoryObj)) {
            http_response_code(500);exit;
        }
        $excludedNames = $GLOBALS['general']['EXT_URL'];
        $excludedUname=explode(',',$excludedNames);
        $this->view->setVar("isSEOExclude",false);
        if(in_array($productId,$excludedUname))
        {
            $this->view->setVar("isSEOExclude",true);
        }
        $data = array(
            "header" => "https://www.gadgetsnow.com/pwafeeds/gnow/mweb/common/header/json?path=/general",
            "footer" => "https://www.gadgetsnow.com/pwafeeds/gnow/web/common/footer/json",
            "show" => "https://www.gadgetsnow.com/pwafeeds/gnow/mweb/show/gadgets/json?uName=" . $productId . "&category=" . $categoryObj["id"] . "&url=" . $_SERVER['REQUEST_URI'],
            "comparison" => "https://www.gadgetsnow.com/pwafeeds/gnow/mweb/list/compare/json?path=/recent/&category=" . $categoryObj["id"] . "&productid=" . $productId. "&perpage=5",
            "similar" => "https://www.gadgetsnow.com/pwafeeds/gnow/mweb/list/search/json?path=/search/gadgetKey/&category=" . $categoryObj["id"] . "&productid=" . $productId . "&perpage=10",
        );
        $responseData = Utils::multiRequest($data);
        $topSearches = $footerData = $gadgetData = $seoData = $comparisonData = $similarGadgetsData = array();
        $headerData = [
            ["title" => "News", "url" => "/latest-news"],
            ["title" => "Compare", "url" => "/compare-mobile-phones"],
            ["title" => "Reviews", "url" => "/reviews"],
            ["title" => "Videos", "url" => "/videos"],
            ["title" => "Top Gadgets", "url" => "/top-gadgets"],
        ];
        $tabsData = [
            'overview' => [
                "id" => 'overview',
                "active" => true,
                "name" => 'Overview',
            ],
            'specs' => [
                "id" => 'specs',
                "active" => false,
                "name" => 'Specs',
            ],
            'criticReview' => [
                "id" => 'criticReview',
                "active" => false,
                "name" => 'Critic Review',
            ],
            'userReview' => [
                "id" => 'userReview',
                "active" => false,
                "name" => 'User Review',
            ],
        ];
        if (!empty($responseData["footer"])) {
            $footerData = $responseData["footer"]["jsonFeed"]['sections'];
        }
        if (!empty($responseData["show"]) && !isset($responseData["show"]["error"])) {
            $gadgetData = $responseData["show"]["jsonFeed"]["data"]["item"];
            $seoDetailData = $responseData["show"]["jsonFeed"]["seoDetail"];
        }else{
            http_response_code(500);exit;
        }
        if (!empty($responseData["header"])) {
            $topSearches = $responseData["header"]["jsonFeed"]['sections']['Trending']['data']['items'];
        }
        if (!empty($responseData["comparison"])) {
            $comparisonData = $responseData["comparison"]["jsonFeed"]['data']['items'];
        }
        if (!empty($responseData["similar"])) {
            $similarGadgetsData = $responseData["similar"]["jsonFeed"]['data'];
        }
        $productUrl = '/'.$categoryObj["seoName"].'/'.$gadgetData['productUName'];
        $seoData = [
			'title' => $seoDetailData["altTitle"] ? $seoDetailData["altTitle"] : "Gadgetsnow",
            'keywords' => $seoDetailData["kws"] ? $seoDetailData["kws"] : "",
            'amphtml' => 'https://www.gadgetsnow.com/amp'. $productUrl,
            'description' => $seoDetailData["metaDescription"] ? $seoDetailData["metaDescription"] : "",
            'canonicalUrl' => $seoDetailData["metaIndex"] === 'index' ? 'https://www.gadgetsnow.com'.$productUrl : `https://www.gadgetsnow.com`,
            'robots' => $seoDetailData["metaIndex"] === 'index' ? 'index, follow' : 'noindex,nofollow',
        ];
		if ( isset($seoDetailData["atfContent"]) ){
			$seoData['atfContent'] = $seoDetailData["atfContent"];
		}
		if ( isset($seoDetailData["h1Title"]) ){
			$seoData['h1Title'] = $seoDetailData["h1Title"];
		}
		if ( isset($seoDetailData["priceInIndia"]) ){
			$seoData['priceInIndia'] = $seoDetailData["priceInIndia"];
		}
        $dataGA = ['category' => 'WAP_GS_NEW'];
        $showHeader = $showFooter = true;
        if(isset($_GET['frmapp']) && $_GET['frmapp'] === "yes"){
            $showHeader = false; $showFooter = false;
        }
        $categoryList = Utils::getGadgetCategory();
        $this->view->setVar("categoryList", $categoryList);
        $this->view->setVar("showFooter", $showFooter);
        $this->view->setVar("showHeader", $showHeader);
        $this->view->setVar("similarGadgetsData", $similarGadgetsData);
        $this->view->setVar("comparisonData", $comparisonData);
        $this->view->setVar("categoryData", $categoryObj);
        $this->view->setVar("headerData", $headerData);
        $this->view->setVar("topSearches", $topSearches);
        $this->view->setVar("footerData", $footerData);
        $this->view->setVar("gadgetData", $gadgetData);
        $this->view->setVar("seoData", $seoData);
        $this->view->setVar("tabsData", $tabsData);
        $this->view->setVar("domainPrefix", "https://www.gadgetsnow.com");
        $this->view->setVar("dataGA", $dataGA);
        $this->view->setVar("showAds", true);
        $this->view->setVar("showSearch", true);
        $this->view->setVar("showBottomNav", false);
        //echo json_encode($comparisonData,true);die;

    }

	public function gadgetsaffiliateAction()
    {
		$widgettype = $this->dispatcher->getparam("widgettype");
        $productId = $this->dispatcher->getparam("productId");
        $category = $this->dispatcher->getparam("category");
        $categoryObj = Utils::getGadgetCategory($category);
        if (empty($categoryObj)) {
            die('Wrong url');
        }
		$affiliateData = [];
		$affiliateUrl = "https://www.gadgetsnow.com/pwafeeds/gnow/mweb/show/gadgets/json?category=" . $categoryObj["id"]. "&reqType=". $widgettype . "&uName=" . $productId;
        $result = Utils::getProtectedApiResponse($affiliateUrl, null, null, null, null);
		$result = json_decode($result['data']);
        $data = json_decode(json_encode($result->jsonFeed), true);
		if ( !empty($data["affiliate"])){
			$affiliateData = $data["affiliate"]['dataArray'];
		}
		//echo $affiliateUrl;
		//echo "<pre>";print_r($affiliateData);die;
        $dataGA = ['category' => 'WAP_GS_NEW'];
        $this->view->setVar("dataGA", $dataGA);
		$this->view->setVar("widgettype", $widgettype);
       	$this->view->setVar("affiliateData", $affiliateData);

    }
    public function counterAction()
    {
        $counterData = [];
		$counterUrl = "https://www.gadgetsnow.com/pwafeeds/gnow/web/common/countdownTimer/json";
        $result = Utils::getProtectedApiResponse($counterUrl, null, null, null, null);
	    $result = json_decode($result['data']);
        $data = json_decode(json_encode($result->jsonFeed), true);
        $status = $data['data']['status'];
        if(strcasecmp($status,"SUCCESS")==0)
        {   
            $endDate=$data['data']['endDate'];
            $startDate=$data['data']['startDate'];
            $url=$data['data']['url'];
            $title=$data['data']['title'];
            $suffix=$data['data']['suffix'];
            $this->view->setVar("startDate", $startDate);
            $this->view->setVar("endDate", $endDate);
            $this->view->setVar("url", $url);
            $this->view->setVar("title", $title);
            $this->view->setVar("suffix", $suffix);
        }
        
    }
}
