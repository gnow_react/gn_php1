<?php
 
class GadgetfinderController extends ControllerBase {
	
	public function indexAction()
	{
		$this->view->setVar("leafTitle","PhoneFinder");
		
		 
	}
	public function mobileAction()
	{
		$this->view->setVar("leafTitle","MobileFinder");
		$xmlUrl = "https://www.gadgetsnow.com/xml/gl_wdt_filters.cms?target=1&v=3&limit=20&perpage=20&aff_tag=web_gadgetlisting&type=compare&view=keyfeatures&category=mobile";
		
		$xml = Utils::getXMLResponse($xmlUrl);
		$facets = $xml->techgadgetfilter->solrResponse->facets;
		for($i=0;$i<sizeof($facets);$i++)
		{
			$filters = $facets[$i]->filter;
			$this->view->setVar("features",$filters);
			for($j=0;$j<sizeof($filters);$j++)
			{
				$filtername = (string)$filters[$j]->d_name;
				print_r("<BR>-".$filtername);
				$values = $filters[$j]->values->val;
				if(strcasecmp($filtername,"PRICE")==0)
					$this->view->setVar("pricerange",$values);
				elseif(strcasecmp($filtername,"BRAND")==0)
					$this->view->setVar("brandrange",$values);
				for($g=0;$g<sizeof($values);$g++)
				{
					print_r("<BR>----".(string)$values[$g]->d_name);
				}
			}
		} 
	}
	
	public function gadgetsAction()
	{
		$this->view->setVar("leafTitle","MobileFinder");
		if(isset($_GET['type'])) 
		{
			$type=$_GET['type'];
			$xmlUrl = "https://www.gadgetsnow.com/xml/gl_wdt_filters.cms?target=1&v=3&limit=20&perpage=20&aff_tag=web_gadgetlisting&type=compare&view=keyfeatures&category=".$type;
		
			$xml = Utils::getXMLResponse($xmlUrl);
			$facets = $xml->techgadgetfilter->solrResponse->facets;
			$this->view->setVar("features",$facets);
			for($i=0;$i<sizeof($facets);$i++)
			{
			$filters = $facets[$i]->filter;
				for($j=0;$j<sizeof($filters);$j++)
				{
					$filtername = (string)$filters[$j]->d_name;
					print_r("<BR>-".$filtername);
					$values = $filters[$j]->values->val;
					if(strcasecmp($filtername,"PRICE")==0)
						$this->view->setVar("pricerange",$values);
					elseif(strcasecmp($filtername,"BRAND")==0)
						$this->view->setVar("brandrange",$values);
					for($g=0;$g<sizeof($values);$g++)
					{
						print_r("<BR>---dd-".(string)$values[$g]->d_name);
					}
				}
			}  
		}
	}
	public function gadgetlistAction()
	{
		$this->view->setVar("leafTitle","GadgetsFinderList");
	}
}

?>