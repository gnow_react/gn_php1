<?php


class GNController extends \Phalcon\Mvc\Controller
{
	public $backendServerUrl;
	public $backendOldServerUrl;
	function initialize(){
		//parent::__construct();
		$this->showSearchTab = "false";
		$this->backendServerUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		$this->backendOldServerUrl = $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
	}
	public function relatedpagesAction()
	{
	    $this->view->setVar("leafTitle", "relatedPages");
	    $productId = $this->dispatcher->getparam("productId");
	    $category = $this->dispatcher->getparam("category");
	    
	    $footerUrl= "https://gnpwastg.gadgetsnow.com/pwafeeds/gnow/web/common/footer/json";
	    $footerResult = Utils::getProtectedApiResponse($footerUrl, null, null, null, null);//
	    $footerJsonResult = json_decode($footerResult['data']);
	    $tempfoot = json_decode(json_encode($footerJsonResult->jsonFeed), true);
	    $popularMobile = $tempfoot['sections']['Popular Mobiles']['data']['items'];
	    $this->view->setVar("footer",$popularMobile);
	    
	    $headerUrl= "https://gnpwastg.gadgetsnow.com/pwafeeds/gnow/mweb/common/header/json?path=/general";
	    $headerResult = Utils::getProtectedApiResponse($headerUrl, null, null, null, null);
	    $headerJsonResult = json_decode($headerResult['data']);
	    $temphead = json_decode(json_encode($headerJsonResult->jsonFeed), true);
	    $this->view->setVar("header",$temphead['data']['items']);
	    $this->view->setVar("topsearch",$temphead['sections']['Trending']['data']['items']);
	    
	    $url="https://gnpwastg.gadgetsnow.com/pwafeeds/gnow/mweb/list/gadgets/json?path=/relateProductDetail/&category=".$category."&productid=".$productId;
	    $categoryResult = Utils::getProtectedApiResponse($url, null, null, null, null);
	    $categoryJsonResult = json_decode($categoryResult['data']);
	    //$this->view->setVar("pageData",$categoryJsonResult->jsonFeed->data->items);
	    //$temp1=$categoryJsonResult->jsonFeed->data->items;
	    $temp1=json_decode(json_encode($categoryJsonResult->jsonFeed->data->items), true);
	    $this->view->setVar("pageData",$temp1);
	    //print_r($temp1[related]);die;
	    //$related=json_decode($temp1['related']);
	    //for($i=0;$i<sizeof($temp1['related']);$i++)
	    // {
	    //  print_r($temp1['related'][$i]['productName']);echo "<BR>";
	        //}die;
	        //print_r($categoryJsonResult);die;
	}
}
?>   