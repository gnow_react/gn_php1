<?php

include __DIR__ . "/../config/Redirects.php";
include __DIR__ . "/../config/SessionManager.php";

class UserReviewsController extends \Phalcon\Mvc\Controller
{
    public $redirect;

	public function Initialize() {
		$this->redirect = Redirects::$HOME;
		SessionManager::handleSession();
	}
	
	public function indexAction()
	{
		$this->view->setVar('leafTitle','userReviews');
		
		/*$userName = "";
		if(isset($_SESSION['userLogin'])){
			$userLogin = $_SESSION['userLogin'];
			$userName = $userLogin->getUserName();
		}
		
		if(isset($_SESSION['userLogin']) && $userName != "Guest"){
			header('location:/');
		}*/
		
		if(!(session_status() === PHP_SESSION_ACTIVE)){
			session_start();
		}
						
	}
}

