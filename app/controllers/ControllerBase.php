<?php
include __DIR__ . "/../config/Utils.php";
include __DIR__ . "/helpers/ProductHelper.php";
include __DIR__."/../config/AffiliateUrlCreator.php";

class ControllerBase extends \Phalcon\Mvc\Controller
{
	public function onConstruct()
	{
		$pagename = $this->dispatcher->getparam("pagename");
		if(isset($pagename))
			$productId=$this->dispatcher->getparam("pagename");
		$this->view->setVar('pagename',$pagename);
		
		$this->view->setVar('showSeoWidget',false);
		$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
		$oldServerUrl = $GLOBALS['general']['BACKEND_OLD_SERVER_URL'];
		$seoProductBasicInfoUrl = $serverUrl."/rest/product/productsForSeoWidget";
		$data="{'pID':'G10003'}";
		$result = Utils::getProtectedApiResponse($seoProductBasicInfoUrl, CURLOPT_POST, null, $data, null);
		$jsonResponse = json_decode($result['data'],true);
		$this->view->setVar("seoProducts",$jsonResponse);
					
		
		
		ControllerBase::initPromotedBanners($pagename);
		ControllerBase::initSEOWidget($pagename);
		
		// Code to get ATF,Maskhead, BTF
		
		$hfBannerAPIUrl = $GLOBALS['general']['HF_BANNER_API_URL'];

		$hfBannerResult = file_get_contents($hfBannerAPIUrl);
		if(isset($hfBannerResult))
		{
			
			$hfBannerJSON = json_decode($hfBannerResult,true);
			foreach($hfBannerJSON['promoted'] as $value)
			{  
				$active = $value['active'];
				$googlewap = $value['googlewap'];
				$hfbannerType=$value['bannerType'];
				$hfEventaction= $value['Eventaction'];
				$hfEventaction=$hfEventaction."_".strtolower($hfbannerType)."_mweb";
				if($hfbannerType == "ATF")
				{   
					$atfgooglewap = $googlewap;
					$atfintermediateUrl = $value['intermediateUrl'];
					$atfName = $value['fname'];
					$atfStartDate = $value['startdate'];
					$atfEndDate = $value['enddate'];
					$atfTagName= $value['tagname'];
					$atfBannerActive=$active;
					$atfTaglabel= $value['taglabel'];
					$atfImgpath = $value['mbannername'];
					$atfBannerurl = $value['mbannerurl'];
					$atfBannertext = $value['bannertext'];
					
					$atfEventcat= $atfName."|gnshop|mweb|".$pagename."|atf";//$value['Eventcat'];
					$atfEventAction = "click";
					$atfEventlabel= "NA|".$atfBannertext."|NA";
					 
					$affiliateParamArray=array("productname"=>$atfBannertext,"price"=>"NA","producturl"=>$atfBannerurl,"pagename"=>"","widget"=>"atf","tagname"=>$atfTagName);
					$affiliateLandingurl=AffiliateUrlCreator::createAffiliateUrl($atfName,$affiliateParamArray);
					$atfBannerurl=$affiliateLandingurl;
					 
					$atfDataGATrack = $atfEventcat."##".$atfEventAction."##".$atfEventlabel."##0";
					$this->view->setVar("atfBannerActive",$atfBannerActive);
					$this->view->setVar("atfBannerurl",$atfBannerurl);
					$this->view->setVar("atfImgpath",$atfImgpath);
					$this->view->setVar("atfDataGATrack",$atfDataGATrack);

				}elseif($hfbannerType == "BTF")
				{
					$btfgooglewap = $googlewap;
					$btfintermediateUrl = $value['intermediateUrl'];
					$btfName = $value['fname'];
					$btfStartDate = $value['startdate'];
					$btfEndDate = $value['enddate'];
					$btfTagName= $value['tagname'];
					$btfEventcat= $value['Eventcat'];
					$btfBannerActive=$active;
					$btfEventlabel= $value['Eventlabel'];
					$btfTaglabel= $value['taglabel'];
					$btfImgpath = $value['mbannername'];
					$btfBannerurl = trim($value['mbannerurl']);
					$btfBannertext = $value['bannertext'];
					$btfEventAction = $hfEventaction;
					$btfEventcat=$btfEventcat."_mweb_btf";
					
					$btfEventcat= "gnshop|mweb|btf";//$value['Eventcat'];
					$btfEventAction = "click";
					$btfEventlabel= $btfBannertext;
					
					$affiliateParamArray=array("productname"=>$btfBannertext,"price"=>"NA","producturl"=>$btfBannerurl,"pagename"=>"","widget"=>"btf","tagname"=>$btfTagName);
					$affiliateLandingurl=AffiliateUrlCreator::createAffiliateUrl($btfName,$affiliateParamArray);
					$btfBannerurl=$affiliateLandingurl;
					
					 
					$btfDataGATrack = $btfEventcat."##".$btfEventAction."##".$btfEventlabel."##0";
					$this->view->setVar("btfBannerActive",$btfBannerActive);
					$this->view->setVar("btfBannerurl",$btfBannerurl);
					$this->view->setVar("btfImgpath",$btfImgpath);
					$this->view->setVar("btfDataGATrack",$btfDataGATrack);
				}elseif($hfbannerType == "MaskHead")
				{
					$maskheadgooglewap = $googlewap;
					$maskHeadintermediateUrl = $value['intermediateUrl'];
					$maskHeadName = $value['fname'];
					$maskHeadActive = $value['active'];
					$maskHeadStartDate = $value['startdate'];
					$maskHeadEndDate = $value['enddate'];
					$maskHeadTagName= $value['tagname'];
					$maskHeadEventcat= $value['Eventcat'];
					$maskHeadEventaction= $value['Eventaction'];
					$maskHeadEventlabel= $value['Eventlabel'];
					$maskHeadTaglabel= $value['taglabel'];
					$maskHeadImgpath = $value['mbannername'];
					$maskHeadBannerurl = $value['mbannerurl'];
					$maskHeadBannertext = $value['bannertext'];
					$maskHeadEventaction=$maskHeadEventaction;
					$maskHeadEventcat=$maskHeadEventcat."_mweb_maskhead";
					
					$maskHeadEventcat= "gnshop|mweb|maskhead";//$value['Eventcat'];
					$maskHeadEventAction = "click";
					$maskHeadEventlabel= "Maskhead|".$maskHeadBannertext;
					
					$affiliateParamArray=array("productname"=>$maskHeadBannertext,"price"=>"NA","producturl"=>$maskHeadBannerurl,"pagename"=>"","widget"=>"masthead","tagname"=>$maskHeadTagName);
					$affiliateLandingurl=AffiliateUrlCreator::createAffiliateUrl($maskHeadName,$affiliateParamArray);
					$maskHeadBannerurl=$affiliateLandingurl;
					
					 
					$maskHeadDataGATrack = $maskHeadEventcat."##".$maskHeadEventaction."##".$maskHeadEventlabel."##0";
					$this->view->setVar("maskHeadActive",$maskHeadActive);
					$this->view->setVar("maskHeadBannerurl",$maskHeadBannerurl);
					$this->view->setVar("maskHeadImgpath",$maskHeadImgpath);
					$this->view->setVar("maskHeadDataGATrack",$maskHeadDataGATrack);
				}
			}
		}// ATF BTF MASKHead banners
	}
	
	/**
	 * Initialize Trending , Popular, Latest Widgets
	 */
function initSEOWidget($pageName,$categoryId, $catalogId)
{
	$serverUrl = $GLOBALS['general']['BACKEND_SERVER_URL'];
	$latestWidgetUrl = $serverUrl."/rest/catalog/promoLandingWidget?widgetType=LATEST_MOBILE";
	$result = Utils::getProtectedApiResponse($latestWidgetUrl, null, null, null, null);
	$jsonResponse = json_decode($result['data'],true);
	$latestInfoArray = array();
	if(!isset($categoryId)|| $categoryId=='10022'  ) {
	$categoryId = '10021';
		}
		if(!isset($catalogId))
		{
		$catalogId='10002';
		}
	foreach ($jsonResponse as $latestInfo ) 
	{	
		if(isset($latestInfo['product']) && ( isset($latestInfo['CATEGORY_ID']) && $categoryId==$latestInfo['CATEGORY_ID'] ))
		{	
			$product = json_encode($latestInfo['product']);
			$temp  = ProductHelper::getLowestPriceAffiliate($product);
			array_push($latestInfoArray,array("PAGE_NAME" => $latestInfo['PAGE_NAME'],"PAGE_URL"=>$latestInfo['PAGE_URL'],"PRODUCT"=>$temp));
		}
	}
	 
	$this->view->setVar("latestInfoArray",$latestInfoArray);
	
	$popularWidgetUrl = $serverUrl."/rest/catalog/promoLandingWidget?widgetType=POPULAR_MOBILE";
	$resultPopular = Utils::getProtectedApiResponse($popularWidgetUrl, null, null, null, null);
	
	$jsonResponsePopular = json_decode($resultPopular['data'],true);
	$popularInfoArray = array();
	foreach ($jsonResponsePopular as $popularInfo )
	{
		if(isset($popularInfo['product']) && ( isset($popularInfo['CATEGORY_ID']) && $categoryId==$popularInfo['CATEGORY_ID'] ))
		{
			$productPopular = json_encode($popularInfo['product']);
			$temp  = ProductHelper::getLowestPriceAffiliate($productPopular);
			array_push($popularInfoArray,array("PAGE_NAME" => $popularInfo['PAGE_NAME'],"PAGE_URL"=>$popularInfo['PAGE_URL'],"PRODUCT"=>$temp));
		}
	}
	$this->view->setVar("popularInfoArray",$popularInfoArray);
	
	$trendingWidgetUrl = $serverUrl."/rest/catalog/promoLandingWidget?widgetType=TRENDING_URL";
	$resultTrending = Utils::getProtectedApiResponse($trendingWidgetUrl, null, null, null, null);
	$jsonResponseTrending = json_decode($resultTrending['data'],true);
	$trendingInfoArray = array();
		$trendingInfoArray_catalog = array();
	
	
	foreach ($jsonResponseTrending as $trendingInfo )
	{
		if(isset($trendingInfo['product']) && ( isset($trendingInfo['CATALOG_ID']) && $catalogId==$trendingInfo['CATALOG_ID'] ))
		{
			$productTrending = json_encode($trendingInfo['product']);
			$temp  = ProductHelper::getLowestPriceAffiliate($productTrending);
			array_push($trendingInfoArray,array("PAGE_NAME" => $trendingInfo['PAGE_NAME'],"PAGE_URL"=>$trendingInfo['PAGE_URL'],"PRODUCT"=>$temp));
		}
		else if(isset($trendingInfo['CATALOG_ID']) && $catalogId==$trendingInfo['CATALOG_ID'] )
		{
			array_push($trendingInfoArray_catalog,array("PAGE_NAME" => $trendingInfo['PAGE_NAME'],"PAGE_URL"=>$trendingInfo['PAGE_URL']));
		}
	}
	$this->view->setVar("trendingInfoArray",$trendingInfoArray);
	 	$this->view->setVar("trendingInfoArray_catalog",$trendingInfoArray_catalog);
	 
}	
/**
 * 
 */
function initPromotedBanners($pagename)
{
	$amzBanneApirUrl=$GLOBALS['general']['AMAZON_BANNER_API'];
	$amzBannerRes =  file_get_contents($amzBanneApirUrl);
	if(isset($amzBannerRes)) {
		$amzresult = json_decode($amzBannerRes,true);
		foreach($amzresult['promoted'] as $value)
		{
			$number = $value['number'];
			$active = $value['active'];
			$promoAfName = $value['fname'];
			$promoStartDate = $value['startdate'];
			$promoEndDate = $value['enddate'];
			$promoTagName= $value['tagname'];
			$promoEventcat= $value['Eventcat'];
			$promoEventaction= $value['Eventaction'];
			$promoEventlabel= $value['Eventlabel'];
			$promoTaglabel= $value['taglabel'];
			$promoImgpath = $value['mbannername'];
			$promoBannerurl = $value['mbannerurl'];
			$promoBannertext = $value['bannertext'];
			$promonofollowtag="rel=follow";
			
			$promoEventaction="click";//$promoEventaction."_home_mweb_promoted";
			$promoEventlabel="Promoted".$number."|".$promoBannertext;
			$promoEventcat="gnshop|mweb|promoted";
			
			$this->view->setVar("affiliateBannerActive".$number,$active);
			$this->view->setVar("promoAfName".$number,$promoAfName);
			$this->view->setVar("promoTagName".$number,$promoTagName);
			$this->view->setVar("promoEventcat".$number,$promoEventcat);
			
			$this->view->setVar("promoEventaction".$number,$promoEventaction);
			$this->view->setVar("promoEventlabel".$number,$promoEventlabel);
			$this->view->setVar("promoImgpath".$number,$promoImgpath);
	
			$this->view->setVar("promoBannertext".$number,$promoBannertext);

			$affiliateParamArray=array("productname"=>$promoBannertext,"price"=>"NA","producturl"=>$promoBannerurl,"pagename"=>$pagename,"widget"=>"promowidget".$number,"tagname"=>$promoTagName);
			$affiliateLandingurl=AffiliateUrlCreator::createAffiliateUrl($promoAfName,$affiliateParamArray);
			
			 
			$promoBannerurl=$affiliateLandingurl;
			$this->view->setVar("promoBannerurl".$number,$promoBannerurl);
			$this->view->setVar("promonofollowtag".$number,$promonofollowtag);
		}
	}
}
}
