<?php
include __DIR__ . "/../config/Utils.php";
include __DIR__ . "/../config/SessionManager.php";

class GadgetsbrandController extends \Phalcon\Mvc\Controller {
	public function indexAction() { print_r("******************");
		$this->view->setVar('leafTitle', 'shopbybrands' );
		
		$jsonUrl = $GLOBALS ['general'] ['SHOP_TAB_JSON_URL'];
		$usrPwd = "admin:gad@687";
		
		$resultArr = array ();
		if (isset ( $jsonUrl )) {
			$jsonResult = Utils::getProtectedApiResponse ( $jsonUrl, null, null, null, $usrPwd );
			print_r($jsonResult);
			$result = json_decode ( $jsonResult ['data'], true );
			foreach ( $result as $value ) {
				$name = $value ['h1'];
				if ($name == "SHOP BY BRANDS") 
				{
					array_push ( $resultArr,  $value ['item'] );
				} 
			}
		}
		$this->view->setVar('brands',$resultArr);
	}
	
	public function jiofiAction()
	{
		$this->view->setVar('leafTitle', 'jiofi' );
	}
}

