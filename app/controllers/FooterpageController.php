<?php
include __DIR__ . "/../config/SessionManager.php";
class FooterpageController extends \Phalcon\Mvc\Controller
{

	function initialize(){
		SessionManager::handleSession();
	}
	
	public function indexAction()
	{
		echo "inside index";die;
	}
	
	public function paymentInformationAction()
    {
    	$this->view->setVar('leafTitle','paymentInformation');
    }
    
    public function termsofUseAction()
    {
    	$this->view->setVar('leafTitle','termsofUse');
    }
    
    public function helpAction()
    {
    	$this->view->setVar('leafTitle','help');
    }
    
    public function policyAction()
    {
    	$this->view->setVar('leafTitle','policy');
    }
    
    public function returnPolicyAction()
    {
    	$this->view->setVar('leafTitle','returnPolicy');
    }

    public function refundPolicyAction()
    {
    	$this->view->setVar('leafTitle','refundPolicy');
    }
}

?>