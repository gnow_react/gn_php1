<?php 
class MerchantInfo
{
	public $merAppId;
	public $merCountryCode;
	public $merId;
	
	public function setMerAppId($appId)
	{
		$this->merAppId=$appId;
	}
	public function getMerAppId()
	{
		return $this->merAppId;
	}
	public function setMerCountryCode($cCode)
	{
		$this->merCountryCode = $cCode;
	}
	
	public function getMerCountryCode()
	{
		return $this->merCountryCode;
	}
	
	public function getMerId()
	{
		return $this->merId;
	}
	
	public function setMerId($mId)
	{
		$this->merId=$mId;
	}
	
}
?>