<?php class WibmoTransaction
{
	
	public $customerInfo;
	public $transactionInfo;
	public $merchantInfo;
	public $msgHash;
	public function setCustomerInfo($cInfo)
	{
		$this->customerInfo=$cInfo;
	}
	public function getCustomerInfo()
	{
		return $this->customerInfo;
	}
	public function setTransactionInfo($tInfo)
	{
		$this->transactionInfo=$tInfo;
	}
	public function getTransactionInfo()
	{
		return $this->transactionInfo;
	}
	public function setMerchantInfo($merchantInfo)
	{
		$this->merchantInfo=$merchantInfo;
	}
	public function getMerchantInfo()
	{
		return $this->merchantInfo;
	}
	 public function setMsgHash($msgHash)
        {
                $this->msgHash=$msgHash;
        }
        public function getMsgHash()
        {
                return $this->msgHash;
        }

}

?>
