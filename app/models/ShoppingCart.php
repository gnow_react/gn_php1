<?php
class ShoppingCart {

	public $autoSaveListId;
	public $paymentParams;
	public $billingAccountAmt;
	public $billingAccountId;
	public $channel; // default channel enum
	public $defaultItemComment;
	public $defaultItemDeliveryDate;
	public $externalId;
	public $firstAttemptOrderId;
	public $gcFulfillemntId;
	public $gcPaymentId;
	public $giftCartOffer;
	public $giftCouponRefId;
	public $giftCouponType;
	public $giftCouponValue;
	public $giftCoupons;
	public $internalCode;
	public $lastListRestore;
	public $orderAdditionalEmails;
	public $orderId;
	public $orderName;
	public $orderStatusId;
	public $orderStatusString;
	public $orderTermSet;
	public $orderTerms;
	public $orderType; // default orderType
	public $partyId;
	public $poNumber;
	public $productStoreShipMethId;
	public $quoteId;
	public $readOnlyCart;
	public $tempGiftCartOffer;
	public $tempGiftCoupon;
	public $tempGiftCouponValue;
	public $viewCartOnAdd;
	public $walletPaymentId;
	public $workEffortId;
	public $shippingAddress;
	public $allowedZipCodeMode;
	public $amountToCollect;
	public static $generalPropertiesFile;
	public $userLogin;
	
	
	protected $nextItemSeq;
	
	public static $ALL;
	public static $EMPTY_ONLY;
	public static $FILLED_ONLY;
	public static $ZERO;
	public static $generalRounding;
	public static $percentage;
	public static $resource_error;
	public static $rounding;
	public static $scale;
	public static $serviceTaxRate;
	
	public $paymentModes;
	public $includeBins;
	public $excludeBins;
	public $includeBanks;
	public $excludeBanks;
	public $zipCodeType;
	public $gcAllowedRefIds;
	public $paycardTypes;
	public $cartLines;
	public $paymentInfos;//array
	public $currency;
	public $shippingContactMechId;
	public $shoppingList;
	public $autoGiftCouponUsed;
	public $giftCouponUsed;
	public $paymentInfo;
	public $currencyUom;
	public $holdOrder;
	public $orderDate;
	public $cancelBackOrderDate ;
	public $expressCheckoutInfo;
	public $grandTotal;
	public $codCharges;
	public $webSiteId;
	public $giftCoupondiscountType;
	
	public function ShoppingCart() {
		$ALL = 1;
		$EMPTY_ONLY = 2;
		$FILLED_ONLY = 3;
		$ZERO = 0;
		$this->billingAccountAmt = 0;
		$this->channel = "WAP_SALES_CHANNEL";
		$this->webSiteId="WebStore";
		$generalPropertiesFile = "general.properties";
		$generalRounding = 10;
		$this->giftCouponValue = 0;
		$this->nextItemSeq = 1;
		$this->orderTermSet = false;
		$this->orderTerms = array ();
		$this->orderType = "SALES_ORDER";
		$percentage = 1;
		$this->readOnlyCart = false;
		$resource_error = "OrderErrorUiLabels";
		$rounding = 4;
		$scale = 2;
		$serviceTaxRate = 12.36;
		$this->tempGiftCouponValue = 0;
		$this->viewCartOnAdd = false;
		$this->shippingAddress=array();
		$this->allowedZipCodeMode=array();
		$this->amountToCollect= 0;
		$this->userLogin=null;
		$this->paymentModes=array();
		$this->giftCoupondiscountType=null;
		$this->includeBins=array();
		$this->excludeBins=array();
		$this->includeBanks=array();
		$this->excludeBanks=array();
		$this->zipCodeType=array();
		$this->gcAllowedRefIds=array();
		$this->paycardTypes=array();
		$this->cartLines=array();
		$this->paymentInfos=array();
		$this->paymentInfo=array();
		$this->currency="INR";
		$this->shippingContactMechId=null;
		$this->shoppingList=new ShoppingList();
		$this->autoGiftCouponUsed=false;
		$this->giftCouponUsed=false;
		$this->holdOrder=false;
		$this->grandTotal=0;
		$this->orderId='';
		$this->codCharges=0;
		$this->paymentParams=null;
	}
	/* function __construct() {
		self::ShoppingCart();
	} */
	public function getPaymentParams()
	{
		return $this->paymentParams;
	}
	public function setPaymentParams($paymentParams)
	{
	$this->paymentParams = $paymentParams;
	return $this->paymentParams;
	}
	public function getAutoSaveListId() {
		return $this->autoSaveListId;
	}
	public function setAutoSaveListId($autoSaveListId) {
		$this->autoSaveListId = $autoSaveListId;
		return $this;
	}
	public function getBillingAccountAmt() {
		return $this->billingAccountAmt;
	}
	public function setBillingAccountAmt($billingAccountAmt) {
		$this->billingAccountAmt = $billingAccountAmt;
		return $this;
	}
	public function getBillingAccountId() {
		return $this->billingAccountId;
	}
	public function setBillingAccountId($billingAccountId) {
		$this->billingAccountId = $billingAccountId;
		return $this;
	}
	public function getChannel() {
		return $this->channel;
	}
	public function setChannel($channel) {
		$this->channel = $channel;
		return $this;
	}
	public function getDefaultItemComment() {
		return $this->defaultItemComment;
	}
	public function setDefaultItemComment($defaultItemComment) {
		$this->defaultItemComment = $defaultItemComment;
		return $this;
	}
	public function getDefaultItemDeliveryDate() {
		return $this->defaultItemDeliveryDate;
	}
	public function setDefaultItemDeliveryDate($defaultItemDeliveryDate) {
		$this->defaultItemDeliveryDate = $defaultItemDeliveryDate;
		return $this;
	}
	public function getExternalId() {
		return $this->externalId;
	}
	public function setExternalId($externalId) {
		$this->externalId = $externalId;
		return $this;
	}
	public function getFirstAttemptOrderId() {
		return $this->firstAttemptOrderId;
	}
	public function setFirstAttemptOrderId($firstAttemptOrderId) {
		$this->firstAttemptOrderId = $firstAttemptOrderId;
		return $this;
	}
	public function getGcFulfillemntId() {
		return $this->gcFulfillemntId;
	}
	public function setGcFulfillemntId($gcFulfillemntId) {
		$this->gcFulfillemntId = $gcFulfillemntId;
		return $this;
	}
	public function getGcPaymentId() {
		return $this->gcPaymentId;
	}
	public function setGcPaymentId($gcPaymentId) {
		$this->gcPaymentId = $gcPaymentId;
		return $this;
	}
	public function getGiftCartOffer() {
		return $this->giftCartOffer;
	}
	public function setGiftCartOffer($giftCartOffer) {
		$this->giftCartOffer = $giftCartOffer;
		return $this;
	}
	public function getGiftCouponRefId() {
		return $this->giftCouponRefId;
	}
	public function setGiftCouponRefId($giftCouponRefId) {
		$this->giftCouponRefId = $giftCouponRefId;
		return $this;
	}
	public function getGiftCouponType() {
		return $this->giftCouponType;
	}
	public function setGiftCouponType($giftCouponType) {
		$this->giftCouponType = $giftCouponType;
		return $this;
	}
	public function getGiftCouponValue() {
		return $this->giftCouponValue;
	}
	public function setGiftCouponValue($giftCouponValue) {
		$this->giftCouponValue = $giftCouponValue;
		return $this;
	}
	public function getGiftCoupons() {
		return $this->giftCoupons;
	}
	public function setGiftCoupons($giftCoupons) {
		$this->giftCoupons = $giftCoupons;
		return $this;
	}
	public function getInternalCode() {
		return $this->internalCode;
	}
	public function setInternalCode($internalCode) {
		$this->internalCode = $internalCode;
		return $this;
	}
	public function getLastListRestore() {
		return $this->lastListRestore;
	}
	public function setLastListRestore($lastListRestore) {
		$this->lastListRestore = $lastListRestore;
		return $this;
	}
	public function getOrderAdditionalEmails() {
		return $this->orderAdditionalEmails;
	}
	public function setOrderAdditionalEmails($orderAdditionalEmails) {
		$this->orderAdditionalEmails = $orderAdditionalEmails;
		return $this;
	}
	public function getOrderId() {
		return $this->orderId;
	}
	public function setOrderId($orderId) {
		$this->orderId = $orderId;
		return $this;
	}
	public function getOrderName() {
		return $this->orderName;
	}
	public function setOrderName($orderName) {
		$this->orderName = $orderName;
		return $this;
	}
	public function getOrderStatusId() {
		return $this->orderStatusId;
	}
	public function setOrderStatusId($orderStatusId) {
		$this->orderStatusId = $orderStatusId;
		return $this;
	}
	public function getOrderStatusString() {
		return $this->orderStatusString;
	}
	public function setOrderStatusString($orderStatusString) {
		$this->orderStatusString = $orderStatusString;
		return $this;
	}
	public function getOrderTermSet() {
		return $this->orderTermSet;
	}
	public function setOrderTermSet($orderTermSet) {
		$this->orderTermSet = $orderTermSet;
		return $this;
	}
	public function getOrderTerms() {
		return $this->orderTerms;
	}
	public function setOrderTerms($orderTerms) {
		$this->orderTerms = $orderTerms;
		return $this;
	}
	public function getOrderType() {
		return $this->orderType;
	}
	public function setOrderType($orderType) {
		$this->orderType = $orderType;
		return $this;
	}
	public function getPartyId() {
		return $this->partyId;
	}
	public function setPartyId($partyId) {
		$this->partyId = $partyId;
		return $this;
	}
	public function getPoNumber() {
		return $this->poNumber;
	}
	public function setPoNumber($poNumber) {
		$this->poNumber = $poNumber;
		return $this;
	}
	public function getProductStoreShipMethId() {
		return $this->productStoreShipMethId;
	}
	public function setProductStoreShipMethId($productStoreShipMethId) {
		$this->productStoreShipMethId = $productStoreShipMethId;
		return $this;
	}
	public function getQuoteId() {
		return $this->quoteId;
	}
	public function setQuoteId($quoteId) {
		$this->quoteId = $quoteId;
		return $this;
	}
	public function getReadOnlyCart() {
		return $this->readOnlyCart;
	}
	public function setReadOnlyCart($readOnlyCart) {
		$this->readOnlyCart = $readOnlyCart;
		return $this;
	}
	public function getTempGiftCartOffer() {
		return $this->tempGiftCartOffer;
	}
	public function setTempGiftCartOffer($tempGiftCartOffer) {
		$this->tempGiftCartOffer = $tempGiftCartOffer;
		return $this;
	}
	public function getTempGiftCoupon() {
		return $this->tempGiftCoupon;
	}
	public function setTempGiftCoupon($tempGiftCoupon) {
		$this->tempGiftCoupon = $tempGiftCoupon;
		return $this;
	}
	public function getTempGiftCouponValue() {
		return $this->tempGiftCouponValue;
	}
	public function setTempGiftCouponValue($tempGiftCouponValue) {
		$this->tempGiftCouponValue = $tempGiftCouponValue;
		return $this;
	}
	public function getViewCartOnAdd() {
		return $this->viewCartOnAdd;
	}
	public function setViewCartOnAdd($viewCartOnAdd) {
		$this->viewCartOnAdd = $viewCartOnAdd;
		return $this;
	}
	public function getWalletPaymentId() {
		return $this->walletPaymentId;
	}
	public function setWalletPaymentId($walletPaymentId) {
		$this->walletPaymentId = $walletPaymentId;
		return $this;
	}
	public function getWorkEffortId() {
		return $this->workEffortId;
	}
	public function setWorkEffortId($workEffortId) {
		$this->workEffortId = $workEffortId;
		return $this;
	}
	public function getNextItemSeq() {
		return $this->nextItemSeq;
	}
	public function setNextItemSeq($nextItemSeq) {
		$this->nextItemSeq = $nextItemSeq;
		return $this;
	}
	public function getShippingAddress() {
		return $this->shippingAddress;
	}
	public function setShippingAddress($shipAddress) {
		$this->shippingAddress = $shipAddress;
		return $this;
	}
	public function getAllowedZipCodeMode() {
		return $this->allowedZipCodeMode;
	}
	public function setAllowedZipCodeMode($allowedZipCodeMode) {
		$this->allowedZipCodeMode = $allowedZipCodeMode;
		return $this;
	}
	public function getAmountToCollect() {
		return $this->amountToCollect;
	}
	public function setAmountToCollect($amountToCollect) {
		$this->amountToCollect = $amountToCollect;
		return $this;
	}
	public function getUserLogin(){
		return $this->userLogin;
	}
	public function setUserLogin($userLogin){
		$this->userLogin=$userLogin;
		return $this;
	}
	public function getPaymentModes() {
	  return $this->paymentModes;
	}
	public function setPaymentModes($value) {
	  $this->paymentModes = $value;
	  return $this;
	}
	public function getIncludeBins() {
	  return $this->includeBins;
	}
	public function setIncludeBins($value) {
	  $this->includeBins = $value;
	  return $this;
	}
	public function getExcludeBins() {
	  return $this->excludeBins;
	}
	public function setExcludeBins($value) {
	  $this->excludeBins = $value;
	  return $this;
	}
	public function getIncludeBanks() {
	  return $this->includeBanks;
	}
	public function setIncludeBanks($value) {
	  $this->includeBanks = $value;
	  return $this;
	}
	public function getExcludeBanks() {
	  return $this->excludeBanks;
	}
	public function setExcludeBanks($value) {
	  $this->excludeBanks = $value;
	  return $this;
	}
	public function getZipCodeType() {
	  return $this->zipCodeType;
	}
	public function setZipCodeType($value) {
	  $this->zipCodeType = $value;
	  return $this;
	}
	public function getGcAllowedRefIds() {
	  return $this->gcAllowedRefIds;
	}
	public function setGcAllowedRefIds($value) {
	  $this->gcAllowedRefIds = $value;
	  return $this;
	}
	public function getPaycardTypes() {
	  return $this->paycardTypes;
	}
	public function setPaycardTypes($value) {
	  $this->paycardTypes = $value;
	  return $this;
	}
	public function getCartLines() {
	  return $this->cartLines;
	}
	public function setCartLines($value) {
	  $this->cartLines = $value;
	  return $this;
	}
	public function getPaymentInfos() {
	  return $this->paymentInfos;
	}
	public function setPaymentInfos($value) {
	  $this->paymentInfos = $value;
	  return $this;
	}
	public function getCurrency() {
	  return $this->currency;
	}
	public function setCurrency($value) {
	  $this->currency = $value;
	  return $this;
	}
	public function getShippingContactMechId() {
	  return $this->shippingContactMechId;
	}
	public function setShippingContactMechId($value) {
	  $this->shippingContactMechId = $value;
	  return $this;
	}
	public function getShoppingList() {
	  return $this->shoppingList;
	}
	public function setShoppingList($value) {
	  $this->shoppingList = $value;
	  return $this;
	}
	public function getAutoGiftCouponUsed() {
	  return $this->autoGiftCouponUsed;
	}
	public function setAutoGiftCouponUsed($value) {
	  $this->autoGiftCouponUsed = $value;
	  return $this;
	}
	public function getGiftCouponUsed() {
	  return $this->giftCouponUsed;
	}
	public function setGiftCouponUsed($value) {
	  $this->giftCouponUsed = $value;
	  return $this;
	}
	public function getPaymentInfo() {
	  return $this->paymentInfo;
	}
	public function setPaymentInfo($value) {
	  $this->paymentInfo = $value;
	  return $this;
	}
	public function getCurrencyUom() {
	  return $this->currencyUom;
	}
	public function setCurrencyUom($value) {
	  $this->currencyUom = $value;
	  return $this;
	}
	public function getHoldOrder() {
	  return $this->holdOrder;
	}
	public function setHoldOrder($value) {
	  $this->holdOrder = $value;
	  return $this;
	}
	public function getOrderDate() {
	  return $this->orderDate;
	}
	public function setOrderDate($value) {
	  $this->orderDate = $value;
	  return $this;
	}
	public function getCancelBackOrderDate () {
	  return $this->cancelBackOrderDate ;
	}
	public function setCancelBackOrderDate ($value) {
	  $this->cancelBackOrderDate  = $value;
	  return $this;
	}
	public function getExpressCheckoutInfo() {
	  return $this->expressCheckoutInfo;
	}
	public function setExpressCheckoutInfo($value) {
	  $this->expressCheckoutInfo = $value;
	  return $this;
	}
	public function getGrandTotal() {
		return $this->grandTotal;
	}
	public function setGrandTotal($grandTotal) {
		$this->grandTotal = $grandTotal;
		return $this;
	}
	public function setCodCharges($codCharges){
		$this->codCharges = $codCharges;
		return $this;
	}public function getCodCharges(){
		return $this->codCharges;
	}
	public function getWebSiteId() {
	  return $this->webSiteId;
	}
	public function setWebSiteId($webSiteId) {
	  $this->webSiteId = $webSiteId;
	  return $this;
	}
	public function getGiftCoupondiscountType() {
		return $this->giftCoupondiscountType;
	}
	public function setGiftCoupondiscountType($gcdiscountType) {
		$this->giftCoupondiscountType = $gcdiscountType;
		return $this;
	}
}

