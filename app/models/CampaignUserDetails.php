<?php
class CampaignUserDetails{
	
	public $campaignUserId;
	
	public $campaignShowUserPasswd;
	
	public $campaignUserMobile;
	
	public $campaignUserFirstName;
	
	public $campaignUserGender;
	
	public function CampaignUserDetails($campaignUserId, $campaignShowUserPasswd, $campaignUserFirstName ,$campaignUserMobile, $campaignUserGender){
		
		$this->campaignUserId 	  = $campaignUserId;
		$this->campaignShowUserPasswd = $campaignShowUserPasswd;
		$this->campaignUserFirstName = $campaignUserFirstName;
		$this->campaignUserMobile = $campaignUserMobile;
		$this->campaignUserGender = $campaignUserGender;
	}
	
	public function getcampaignUserId(){
		return $this->campaignUserId;
	}
	
	public function getcampaignShowUserPasswd(){
		return $this->campaignShowUserPasswd;
	}	
	
	public function getcampaignUserFirstName(){
		
		return $this->campaignUserFirstName;
	} 
	
	public function getcampaignUserMobile(){
		return $this->campaignUserMobile;
	}
	
	public function getcampaignUserGender(){
		return $this->campaignUserGender;
	}

} 