<?php

class ShoppingList
{
	public $shoppingListId;
	public $shoppingListItems;
	public $partyId;
	
    public function ShoppingList()
    {
    	$this->shoppingListId=null;
    	$this->shoppingListItems=array();
    	$this->partyId="";
    }
    
    public function getShoppingListId() {
      return $this->shoppingListId;
    }
    public function setShoppingListId($shoppingListId) {
      $this->shoppingListId = $shoppingListId;
      return $this;
    }
    
    public function getShoppingListItems() {
      return $this->shoppingListItems;
    }
    public function setShoppingListItems($shoppingListItems) {
      $this->shoppingListItems = $shoppingListItems;
      return $this;
    }
    public function getPartyId() {
      return $this->partyId;
    }
    public function setPartyId($partyId) {
      $this->partyId = $partyId;
      return $this;
    }
}

