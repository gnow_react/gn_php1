<?php 
class TransactionInfo
{
	public $txnAmount;
	public $txnCurrency;
	public $supportedPaymentType;
	public $txnDesc;
	public $merTxnId;
	public $merAppData;
	
	public function setTxnAmount($amnt)
	{
		$this->txnAmount=$amnt;
	}
	
	public function setTxnCurrency($curr)
	{
		$this->txnCurrency=$curr;
	}
	
	public function setTxnDesc($txnDesc)
	{
		$this->txnDesc=$txnDesc;
	}
	public function setMerAppData($mAppData)
	{
		$this->merAppData = $mAppData;
	}
	
	public function setSupportedPaymentType($paymentTypes)
	{
		$this->supportedPaymentType=$paymentTypes;
	}
	
	public function setMerTxnId($TxnId)
	{
		$this->merTxnId=$TxnId;
	}
	
	
	
	public function getTxnAmount()
	{
		return $this->txnAmount;
	}
	public function getTxnCurrency()
	{
		return $this->txnCurrency;
	}
	
	public function getTxnDesc()
	{
		return $this->txnDesc;
	}
	public function getMerAppData()
	{
		return $this->merAppData;
	}
	
	public function getSupportedPaymentType()
	{
		return $this->supportedPaymentType;
	}
	
	public function getMerTxnId()
	{
		return $this->merTxnId;
	}
	
	
	
}

?>
