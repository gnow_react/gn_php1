<?php
class UserLogin {
	public $currentPassword;
	public $enabled;
	public $hasLoggedOut;
	public $isSystem;
	public $partyId;
	public $successiveFailedLogins;
	public $timeStamp;
	public $userLoginId;
	public $userName;
	public $temp;
	public $isPrime;
	public $ssoId;
	public function UserLogin($userLoginId, $partyId, $userName,$temp1,$isPrime,$ssoId){
		$this->userLoginId = $userLoginId;
		$this->partyId = $partyId;
		$this->userName = $userName;
		$this->temp = $temp1;
		$this->isPrime=$isPrime;
		$this->ssoId=$ssoId;
	}
	public function getSsoId() {
		return $this->ssoId;
	}
	public function setSsoId($ssoid) {
		$this->ssoId = $ssoid;
		return $this;
	}
	public function isPrime() {
		return $this->isPrime;
	}
	public function setIsPrime($isprime) {
		$this->isPrime = $isprime;
		return $this;
	}
	public function getCurrentPassword() {
		return $this->currentPassword;
	}
	public function setCurrentPassword($currentPassword) {
		$this->currentPassword = $currentPassword;
		return $this;
	}
	public function getEnabled() {
		return $this->enabled;
	}
	public function setEnabled($enabled) {
		$this->enabled = $enabled;
		return $this;
	}
	public function getHasLoggedOut() {
		return $this->hasLoggedOut;
	}
	public function setHasLoggedOut($hasLoggedOut) {
		$this->hasLoggedOut = $hasLoggedOut;
		return $this;
	}
	public function getIsSystem() {
		return $this->isSystem;
	}
	public function setIsSystem($isSystem) {
		$this->isSystem = $isSystem;
		return $this;
	}
	public function getPartyId() {
		return $this->partyId;
	}
	public function setPartyId($partyId) {
		$this->partyId = $partyId;
		return $this;
	}
	public function getSuccessiveFailedLogins() {
		return $this->successiveFailedLogins;
	}
	public function setSuccessiveFailedLogins($successiveFailedLogins) {
		$this->successiveFailedLogins = $successiveFailedLogins;
		return $this;
	}
	public function getTimeStamp() {
		return $this->timeStamp;
	}
	public function setTimeStamp($timeStamp) {
		$this->timeStamp = $timeStamp;
		return $this;
	}
	public function getUserLoginId() {
		return $this->userLoginId;
	}
	public function setUserLoginId($userLoginId) {
		$this->userLoginId = $userLoginId;
		return $this;
	}
	public function getUserName() {
		return $this->userName;
	}
	public function setUserName($userName) {
		$this->userName = $userName;
		return $this;
	}
	
	public function getTemp() {
		return $this->temp;
	}
	public function setTemp($temp1) 
	{
		$this->temp = $temp1;
		return $this;
	}
	
}

