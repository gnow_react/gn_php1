<?php
class Compare{
	public $category;
	public $categoryName;
	public $recentProductName;
	public $recentProductId;
	public $recentProductCategory;
	public $url;
	public $selectedProduct;
	public $selectedProductName;
	public $error;
	public $dummyFeatureList;
	
	
	public function Compare(){
		$this->category = null;
		$this->categoryName = null;
		$this->recentProductName = null;
		$this->recentProductId = null;
		$this->recentProductCategory = null;
		$this->selectedProduct = array();
		$this->selectedProductName = array();
	}
	
	
	public function setRecentProductCategory($category) {
		$this->recentProductCategory = $category;
		return $this;
	}
	public function getRecentProductCategory() {
		return $this->recentProductCategory;
	}
	public function getCategory() {
		return $this->category;
	}
	public function setCategory($category) {
		$this->category = $category;
		return $this;
	}
	public function getCategoryName() {
		return $this->categoryName;
	}
	public function setCategoryName($categoryName) {
		$this->categoryName = $categoryName;
		return $this;
	}
	
	
	public function getUrl() {
		return $this->url;
	}
	public function setUrl($url) {
		$this->url = $url;
		return $this;
	}
	
	public function getRecentProductId() {
		return $this->recentProductId;
	}
	public function setRecentProductId($recentProductId) {
		$this->recentProductId = $recentProductId;
		return $this;
	}
	public function getRecentProductName() {
		return $this->recentProductName;
	}
	public function setRecentProductName($recentProductName) {
		$this->recentProductName = $recentProductName;
		return $this;
	}
	
	 public function getSelectedProduct() {
      return $this->selectedProduct;
    }
    public function setSelectedProduct($selectedProduct) {
      $this->selectedProduct = $selectedProduct;
      return $this;
    }
	
	public function getSelectedProductName() {
      return $this->selectedProductName;
    }
    public function setSelectedProductName($selectedProductName) {
      $this->selectedProductName = $selectedProductName;
      return $this;
    }
	
	public function getError() {
      return $this->error;
    }
    public function setError($error) {
      $this->error = $error;
      return $this;
    }
	public function getdummyFeatureList() {
      return $this->dummyFeatureList;
    }
    public function setdummyFeatureList($dummyFeatureList) {
      $this->dummyFeatureList = $dummyFeatureList;
      return $this;
    }
	
}