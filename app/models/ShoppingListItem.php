<?php

class ShoppingListItem
{

	public $averageCost;
	public $basePrice;
	public $displayPrice;
	public $discountedPrice;
	public $gcDiscount;
	public $itemDescription;
	public $listPrice;
	public $offerText;
	public $prodCatalogId;
	public $productCategoryId;
	public $prodCatalogName;
	public $productId;
	public $quantity;
	public $shippingPrice;
	public $isVariant;
	public $virtualProductId;
	public $standardFeature;
	public $shoppingListItemSeqId;
	public $codCharges;
	public $comingSoon;
	public $productName;
	public $shoppingList;
	public $categoryName;
	public $presentInWishList;
	public $isInStock;
	
    public function ShoppingListItem()
    {
    	$this->basePrice=0;
    	$this->displayPrice=0;
    	$this->discountedPrice=0;
    	$this->itemDescription="";
    	$this->listPrice=0;
    	$this->offerText="";
    	$this->productId="";
    	$this->quantity=0;
    	$this->shippingPrice=0;
    	$this->isVariant=false;
    	$this->virtualProductId="";
    	$this->gcDiscount=0;
    	$this->codCharges=0;
    	$this->comingSoon=false;
		$this->prodCatalogName="";
		$this->categoryName="";
		$this->presentInWishList=false;
		$this->isInStock=true;
    }
    public function getIsInStock() {
    	return $this->isInStock;
    }
    public function setIsInStock($inStock) {
    	$this->categoryName = $categoryName;
    	return $this;
    }
    public function getCategoryName() {
    	return $this->categoryName;
    }
    public function setCategoryName($categoryName) {
    	$this->categoryName = $categoryName;
    	return $this;
    }
    public function getPresentInWishList() {
    	return $this->presentInWishList;
    }
    public function setPresentInWishList($presentInWishList) {
    	$this->presentInWishList = $presentInWishList;
    	return $this;
    }
    
	public function getAverageCost() {
		return $this->averageCost;
	}
	public function setAverageCost($averageCost) {
		$this->averageCost = $averageCost;
		return $this;
	}
	public function getBasePrice() {
		return $this->basePrice;
	}
	public function setBasePrice($basePrice) {
		$this->basePrice = $basePrice;
		return $this;
	}
	public function getDisplayPrice() {
		return $this->displayPrice;
	}
	public function setDisplayPrice($displayPrice) {
		$this->displayPrice = $displayPrice;
		return $this;
	}
	public function getGcDiscount() {
		return $this->gcDiscount;
	}
	public function setGcDiscount($gcDiscount) {
		$this->gcDiscount = $gcDiscount;
		return $this;
	}
	public function getItemDescription() {
		return $this->itemDescription;
	}
	public function setItemDescription($itemDescription) {
		$this->itemDescription = $itemDescription;
		return $this;
	}
	public function getListPrice() {
		return $this->listPrice;
	}
	public function setListPrice($listPrice) {
		$this->listPrice = $listPrice;
		return $this;
	}
	public function getOfferText() {
		return $this->offerText;
	}
	public function setOfferText($offerText) {
		$this->offerText = $offerText;
		return $this;
	}
	public function getProdCatalogId() {
		return $this->prodCatalogId;
	}
	public function setProdCatalogId($prodCatalogId) {
		$this->prodCatalogId = $prodCatalogId;
		return $this;
	}
	public function getProductCategoryId() {
		return $this->productCategoryId;
	}
	public function setProductCategoryId($productCategoryId) {
		$this->productCategoryId = $productCategoryId;
		return $this;
	}
	public function getProductId() {
		return $this->productId;
	}
	public function setProductId($productId) {
		$this->productId = $productId;
		return $this;
	}
	public function getQuantity() {
		return $this->quantity;
	}
	public function setQuantity($quantity) {
		$this->quantity = $quantity;
		return $this;
	}
	public function getShippingPrice() {
		return $this->shippingPrice;
	}
	public function setShippingPrice($shippingPrice) {
		$this->shippingPrice = $shippingPrice;
		return $this;
	}
	public function getIsVariant(){
		return $this->isVariant;
	}
	public function setIsVariant($isVariant){
		$this->isVariant = $isVariant;
		return $this;
	}
	public function getVirtualProductId(){
		return $this->virtualProductId;
	}
	public function setVirtualProductId($virtualProductId){
		$this->virtualProductId = $virtualProductId;
		return $this;
	}public function getStandardFeature(){
		return $this->standardFeature;
	}
	public function setStandardFeature($standardFeature){
		$this->standardFeature = $standardFeature;
		return $this;
	}
	
	public function setShoppingListItemSeqId($shoppingListItemSeqId){
		$this->shoppingListItemSeqId = $shoppingListItemSeqId;
		return $this;
	}public function getShoppingListItemSeqId(){
		return $this->shoppingListItemSeqId;
	}
	public function setCodCharges($codCharges){
		$this->codCharges = $codCharges;
		return $this;
	}public function getCodCharges(){
		return $this->codCharges;
	}
	
	public function getComingSoon() {
	  return $this->comingSoon;
	}
	public function setComingSoon($value) {
	  $this->comingSoon = $value;
	  return $this;
	}
	public function getProdCatalogName() {
	  return $this->prodCatalogName;
	}
	public function setProdCatalogName($prodCatalogName) {
	  $this->prodCatalogName = $prodCatalogName;
	  return $this;
	}	
	    
	public function getProductName() {
	  return $this->productName;
	}
	public function setProductName($value) {
	  $this->productName = $value;
	  return $this;
	}
	public function getShoppingList() {
	  return $this->shoppingList;
	}
	public function setShoppingList($value) {
	  $this->shoppingList = $value;
	  return $this;
	}
	public function getdiscountedPrice() {
		return $this->discountedPrice;
	}
	public function setDiscountedPrice($discountedPrice) {
		$this->discountedPrice = $discountedPrice;
		return $this;
	}
}

