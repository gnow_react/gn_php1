<?php
class Product {

	public $additionalImage;
	public $brandName;
	public $facilityName;
	public $facilityId;
	public $invoiceType;
	public $brandObj;
	public $brandUrl;
	public $catalog;
	public $catalogName;
	public $category;
	public $categoryName;
	public $checkDeliveryLocationInfo;
	public $description;
	public $discountAmount;
	public $discountPercent;
	public $discountedPrice;
	public $expressShipmentSla;
	public $imageUrl;
	public $introductionDate;
	public $inventoryFlag;
	public $isActual;
	public $isComingSoon;
	public $isVariant;
	public $isVirtual;
	public $keyFeatures;
	public $longDescription;
	public $minVariantProductSla;
	public $mrp;
	public $offerPrice;
	public $offerText;
	public $originalImageUrl;
	public $price;
	public $priceObjList;
	public $primaryProductCategoryId;
	public $productBrandUrl;
	public $productFeatureMap;
	public $productId;
	public $productImageUrl;
	public $productName;
	public $productOfferObj;
	public $productTypeId;
	public $productUrl;
	public $productVariantObj;
	public $recoJson;
	public $requireInventory;
	public $salesDiscontinuationDate;
	public $sameDayDeliverySLA;
	public $shipmentSla;
	public $shippingPrice;
	public $similarProductIds;
	public $promotionalProductIds;
	public $similarProductImg;
	public $selectableFeatures;
	public $slaObj;
	public $standardFeatures;
	public $virtualProdJs;
	public $sizechart;
	public $autoGc;
	public $amazonUrl;
	public $amazonPrice;
	public $amazonProductName;
	public $manufacturerAddress;
	public $importerAddress;
	public $isBidPid;
	public $tooGudAffiliate;
		
	public function Product() {
		$additionalImage = "";
		$brandName = "";
		$brandObj = "";
		$catalog = "";
		$catalogName = "";
		$category = "";
		$checkDeliveryLocationInfo = "";
		$description = "";
		$discountAmount = 0;
		$discountPercent = 0;
		$discountedPrice = 0;
		$expressShipmentSla = "";
		$imageUrl = ""; 
		$introductionDate = "";
		$inventoryFlag = "";
		$isActual = "";
		$isComingSoon = "";
		$isVariant = false;
		$isVirtual = false;
		$keyFeatures = null;
		$longDescription = "";
		$mrp = 0;
		$offerPrice = 0;
		$offerText = "";
		$originalImageUrl = "";
		$price = 0;
		$priceObjList = null;
		$primaryProductCategoryId = "";
		$productBrandUrl = "";
		$productFeatureMap = null;
		$productId = "";
		$productImageUrl = "";
		$productName = "";
		$productOfferObj = null;
		$productTypeId = "";
		$productUrl = "";
		$productVariantObj = null;
		$recoJson = null;
		$requireInventory = true;
		$salesDiscontinuationDate = null;
		$shipmentSla = 7;
		$shippingPrice = 0;
		$similarProductIds = null;
		$promotionalProductIds=null;
		$selectableFeatures = null;
		$slaObj = null;
		$standardFeatures = null;
		$virtualProdJs = null;
		$sizechart="";
		$facilityName="";
		$invoiceType="";
		$autoGc=false;
		$amazonUrl = "";
		$amazonPrice="";
		$amazonProductName="";
		$manufacturerAddress="";
		$importerAddress="";
		$facilityId="";
		$isBidPid=false;
		$tooGudAffiliate=null;
	}
	public function getTooGudAffiliate()
	{
		return $this->tooGudAffiliate;
	}
	public function setTooGudAffiliate($toogud)
	{
		$this->tooGudAffiliate=$toogud;
	}
	public function getImporterAddress()
	{
		return $this->importerAddress;
	}
	public function getManufacturerAddress()
	{
		return $this->manufacturerAddress;
	}
	
	public function setImporterAddress($iadd)
	{
		$this->importerAddress=$iadd;
		return $this;
	}
	public function setManufacturerAddress($madd)
	{
		$this->manufacturerAddress=$madd;
		return $this;
	}
	public function getAdditionalImage() {
		return $this->additionalImage;
	}
	public function setAdditionalImage($additionalImage) {
		$this->additionalImage = $additionalImage;
		return $this;
	}
	public function getBrandName() {
		return $this->brandName;
	}
	public function setBrandName($brandName) {
		$this->brandName = $brandName;
		return $this;
	}
	public function getBrandObj() {
		return $this->brandObj;
	}
	public function setBrandObj($brandObj) {
		$this->brandObj = $brandObj;
		return $this;
	}
	public function getBrandUrl() {
		return $this->brandUrl;
	}
	public function setBrandUrl($brandUrl) {
		$this->brandUrl = $brandUrl;
		return $this;
	}
	public function getCatalog() {
		return $this->catalog;
	}
	public function setCatalog($catalog) {
		$this->catalog = $catalog;
		return $this;
	}
	public function getCatalogName() {
		return $this->catalogName;
	}
	public function setCatalogName($catalogName) {
		$this->catalogName = $catalogName;
		return $this;
	}
	public function getCategory() {
		return $this->category;
	}
	public function setCategory($category) {
		$this->category = $category;
		return $this;
	}
	public function getCategoryName() {
		return $this->categoryName;
	}
	public function setCategoryName($categoryName) {
		$this->categoryName = $categoryName;
		return $this;
	}
	public function getCheckDeliveryLocationInfo() {
		return $this->checkDeliveryLocationInfo;
	}
	public function setCheckDeliveryLocationInfo($checkDeliveryLocationInfo) {
		$this->checkDeliveryLocationInfo = $checkDeliveryLocationInfo;
		return $this;
	}
	public function getDescription() {
		return $this->description;
	}
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}
	public function getDiscountAmount() {
		return $this->discountAmount;
	}
	public function setDiscountAmount($discountAmount) {
		$this->discountAmount = $discountAmount;
		return $this;
	}
	public function getDiscountPercent() {
		return $this->discountPercent;
	}
	public function setDiscountPercent($discountPercent) {
		$this->discountPercent = $discountPercent;
		return $this;
	}
	public function getDiscountedPrice() {
		return $this->discountedPrice;
	}
	public function setDiscountedPrice($discountedPrice) {
		$this->discountedPrice = $discountedPrice;
		return $this;
	}
	public function getExpressShipmentSla() {
		return $this->expressShipmentSla;
	}
	public function setExpressShipmentSla($expressShipmentSla) {
		$this->expressShipmentSla = $expressShipmentSla;
		return $this;
	}
	public function getImageUrl() {
		return $this->imageUrl;
	}
	public function setImageUrl($imageUrl) {
		$this->imageUrl = $imageUrl;
		return $this;
	}
	public function getIntroductionDate() {
		return $this->introductionDate;
	}
	public function setIntroductionDate($introductionDate) {
		$this->introductionDate = $introductionDate;
		return $this;
	}
	public function getInventoryFlag() {
		return $this->inventoryFlag;
	}
	public function setInventoryFlag($inventoryFlag) {
		$this->inventoryFlag = $inventoryFlag;
		return $this;
	}
	public function getIsActual() {
		return $this->isActual;
	}
	public function setIsActual($isActual) {
		$this->isActual = $isActual;
		return $this;
	}
	public function getIsComingSoon() {
		return $this->isComingSoon;
	}
	public function setIsComingSoon($isComingSoon) {
		$this->isComingSoon = $isComingSoon;
		return $this;
	}
	public function getIsVariant() {
		return $this->isVariant;
	}
	public function setIsVariant($isVariant) {
		$this->isVariant = $isVariant;
		return $this;
	}
	public function getIsVirtual() {
		return $this->isVirtual;
	}
	public function setIsVirtual($isVirtual) {
		$this->isVirtual = $isVirtual;
		return $this;
	}
	public function getKeyFeatures() {
		return $this->keyFeatures;
	}
	public function setKeyFeatures($keyFeatures) {
		$this->keyFeatures = $keyFeatures;
		return $this;
	}
	public function getLongDescription() {
		return $this->longDescription;
	}
	public function setLongDescription($longDescription) {
		$this->longDescription = $longDescription;
		return $this;
	}
	public function getMinVariantProductSla() {
		return $this->minVariantProductSla;
	}
	public function setMinVariantProductSla($minVariantProductSla) {
		$this->minVariantProductSla = $minVariantProductSla;
		return $this;
	}
	public function getMrp() {
		return $this->mrp;
	}
	public function setMrp($mrp) {
		$this->mrp = $mrp;
		return $this;
	}
	public function getOfferPrice() {
		return $this->offerPrice;
	}
	public function setOfferPrice($offerPrice) {
		$this->offerPrice = $offerPrice;
		return $this;
	}
	public function getOfferText() {
		return $this->offerText;
	}
	public function setOfferText($offerText) {
		$this->offerText = $offerText;
		return $this;
	}
	public function getOriginalImageUrl() {
		return $this->originalImageUrl;
	}
	public function setOriginalImageUrl($originalImageUrl) {
		$this->originalImageUrl = $originalImageUrl;
		return $this;
	}
	public function getPrice() {
		return $this->price;
	}
	public function setPrice($price) {
		$this->price = $price;
		return $this;
	}
	public function getPriceObjList() {
		return $this->priceObjList;
	}
	public function setPriceObjList($priceObjList) {
		$this->priceObjList = $priceObjList;
		return $this;
	}
	public function getPrimaryProductCategoryId() {
		return $this->primaryProductCategoryId;
	}
	public function setPrimaryProductCategoryId($primaryProductCategoryId) {
		$this->primaryProductCategoryId = $primaryProductCategoryId;
		return $this;
	}
	public function getProductBrandUrl() {
		return $this->productBrandUrl;
	}
	public function setProductBrandUrl($productBrandUrl) {
		$this->productBrandUrl = $productBrandUrl;
		return $this;
	}
	public function getProductFeatureMap() {
		return $this->productFeatureMap;
	}
	public function setProductFeatureMap($productFeatureMap) {
		$this->productFeatureMap = $productFeatureMap;
		return $this;
	}
	public function getProductId() {
		return $this->productId;
	}
	public function setProductId($productId) {
		$this->productId = $productId;
		return $this;
	}
	public function getProductImageUrl() {
		return $this->productImageUrl;
	}
	public function setProductImageUrl($productImageUrl) {
		$this->productImageUrl = $productImageUrl;
		return $this;
	}
	public function getProductName() {
		return $this->productName;
	}
	public function setProductName($productName) {
		$this->productName = $productName;
		return $this;
	}
	public function getProductOfferObj() {
		return $this->productOfferObj;
	}
	public function setProductOfferObj($productOfferObj) {
		$this->productOfferObj = $productOfferObj;
		return $this;
	}
	public function getProductTypeId() {
		return $this->productTypeId;
	}
	public function setProductTypeId($productTypeId) {
		$this->productTypeId = $productTypeId;
		return $this;
	}
	public function getProductUrl() {
		return $this->productUrl;
	}
	public function setProductUrl($productUrl) {
		$this->productUrl = $productUrl;
		return $this;
	}
	public function getProductVariantObj() {
		return $this->productVariantObj;
	}
	public function setProductVariantObj($productVariantObj) {
		$this->productVariantObj = $productVariantObj;
		return $this;
	}
	public function getRecoJson() {
		return $this->recoJson;
	}
	public function setRecoJson($recoJson) {
		$this->recoJson = $recoJson;
		return $this;
	}
	public function getRequireInventory() {
		return $this->requireInventory;
	}
	public function setRequireInventory($requireInventory) {
		$this->requireInventory = $requireInventory;
		return $this;
	}
	public function getSalesDiscontinuationDate() {
		return $this->salesDiscontinuationDate;
	}
	public function setSalesDiscontinuationDate($salesDiscontinuationDate) {
		$this->salesDiscontinuationDate = $salesDiscontinuationDate;
		return $this;
	}
	public function getSameDayDeliverySLA() {
		return $this->sameDayDeliverySLA;
	}
	public function setSameDayDeliverySLA($sameDayDeliverySLA) {
		$this->sameDayDeliverySLA = $sameDayDeliverySLA;
		return $this;
	}
	public function getShipmentSla() {
		return $this->shipmentSla;
	}
	public function setShipmentSla($shipmentSla) {
		$this->shipmentSla = $shipmentSla;
		return $this;
	}
	public function getShippingPrice() {
		return $this->shippingPrice;
	}
	public function setShippingPrice($shippingPrice) {
		$this->shippingPrice = $shippingPrice;
		return $this;
	}
	public function getSimilarProductIds() {
		return $this->similarProductIds;
	}
	public function setSimilarProductIds($similarProductIds) {
		$this->similarProductIds = $similarProductIds;
		return $this;
	}
	public function getPromotionalProductIds() {
		return $this->promotionalProductIds;
	}
	public function setPromotionalProductIds($promotionalProductIds) {
		$this->promotionalProductIds = $promotionalProductIds;
		return $this;
	}
	public function getSimilarProductImg() {
		return $this->similarProductImg;
	}
	public function setSimilarProductImg($similarProductImg) {
		$this->similarProductImg = $similarProductImg;
		return $this;
	}
	public function getSelectableFeatures() {
		return $this->selectableFeatures;
	}
	public function setSelectableFeatures($selectableFeatures) {
		$this->selectableFeatures = $selectableFeatures;
		return $this;
	}
	public function getSlaObj() {
		return $this->slaObj;
	}
	public function setSlaObj($slaObj) {
		$this->slaObj = $slaObj;
		return $this;
	}
	public function getStandardFeatures() {
		return $this->standardFeatures;
	}
	public function setStandardFeatures($standardFeatures) {
		$this->standardFeatures = $standardFeatures;
		return $this;
	}
	public function getVirtualProdJs() {
		return $this->virtualProdJs;
	}
	public function setVirtualProdJs($virtualProdJs) {
		$this->virtualProdJs = $virtualProdJs;
		return $this;
	}
	public function getSizechart() {
		return $this->sizechart;
	}
	public function setSizechart($sizechart) {
		$this->sizechart = $sizechart;
		return $this;
	}
	public function getAutoGc() {
		return $this->autoGc;
	}
	public function setAutoGc($autogc) {
		$this->autoGc = $autogc;
		return $this;
	}

	public function getFacilityName() {
		return $this->facilityName;
	}
	public function setFacilityName($fac) {
		$this->facilityName = $fac;
		return $this;
	}
	
	public function getFacilityId() {
		return $this->facilityId;
	}
	public function setFacilityId($fac) {
		$this->facilityId = $fac;
		return $this;
	}
	
	public function getInvoiceType() {
		return $this->invoiceType;
	}
	public function setInvoiceType($invoicetype) {
		$this->invoiceType = $invoicetype;
		return $this;
	}
	
	public function getAmazonUrl() {
		return $this->amazonUrl;
	}
	public function setAmazonUrl($aUrl) {
		$this->amazonUrl = $aUrl;
		return $this;
	}
	
	public function getAmazonPrice() {
		return $this->amazonPrice;
	}
	public function setAmazonPrice($aPrice) {
		$this->amazonPrice = $aPrice;
		return $this;
	}
	
	public function getAmazonProductName() {
		return $this->amazonProductName;
	}
	public function setAmazonProductName($aPname) {
		$this->amazonProductName = $aPname;
		return $this;
	}
	public function isBidPid() {
		return $this->isBidPid;
	}
	public function setBidPid($flag) {
		$this->isBidPid = $flag;
		return $this;
	}
}

