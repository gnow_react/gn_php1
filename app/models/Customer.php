<?php
class Customer {
	public $partyId;
	public $userLoginId;
	
	public function Customer($userLoginId, $partyId){
		$this->userLoginId = $userLoginId;
		$this->partyId = $partyId;
	}
	public function getPartyId() {
		return $this->partyId;
	}
	public function setPartyId($partyId) {
		$this->partyId = $partyId;
		return $this;
	}
	public function getUserLoginId() {
		return $this->userLoginId;
	}
	public function setUserLoginId($userLoginId) {
		$this->userLoginId = $userLoginId;
		return $this;
	}
}

