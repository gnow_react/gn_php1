<?php
class Person{
	public $userLoginId;
	public $firstName;
	public $lastName;
	public $gender;
	public $partyId;
	public $contactMobile;
	public function Person($userLoginId,$firstName,$lastName,$partyId){
		$this->userLoginId = $userLoginId;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->partyId = $partyId;
	}
	
	public function getUserLoginId() {
		return $this->userLoginId;
	}
	public function setUserLoginId($userLoginId) {
		$this->userLoginId = $userLoginId;
		return $this;
	}
	public function getFirstName(){
		return $this->firstName;
	}
	public function setFirstName($firstName){
		$this->firstName = $firstName;
		return $this;
	}
	public function getLastName(){
		return $this->lastName;
	}
	public function setLastName($lastName){
		$this->lastName = $lastName;
		return $this;
	}
	public function getGender(){
		return $this->gender;
	}
	public function setGender($gender){
		$this->gender = $gender;
		return $this;
	}
	public function setPartyId($partyId){
		$this->partyId = $partyId;
		return $this;
	}
	public function getPartyId(){
		return $this->partyId;
	}
}