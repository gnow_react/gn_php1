$(document).ready(function(){
	applyLazyLoad();
	var isPrimeUser = false;
	if(isPrimeUser!=null){
		$("*[data-slot=256682]").remove();
	}else{
		$("body").removeClass("adoff").addClass("adon");
	}
	$("#toi-carousel").owlCarousel({
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			navigation : false, // Show next and prev buttons
			pagination: true,
			slideSpeed : 300,
			paginationSpeed : 400,
			autoWidth:true,
			singleItem:true
	});
	
		 $(".toi-owl-carousel").owlCarousel({
		pagination: false,
		 scrollPerPage : false,
		// navigation : true, // Show next and prev buttons
		//navigationText : ["&lsaquo;","&rsaquo;"],
		//lazyLoad : true,
		addClassActive: true,
		afterMove: moved,
		 itemsCustom:[[300,1.2],[400,1.3],[600,2.2],[750,2.3],[850,3.3]]
	 });
	
	  function moved(_this) {
		//var _this = this;
		//var owl = $(_this).data('owlCarousel');
		//$(".toi-owl-carousel").each(function(idx, value){
			var _cls = $(_this).attr("data-count");
			var _newClass = $(".nbtevent"+_cls);		
			_Ctnevent(_newClass);
			
		//});
			
	}
	function _Ctnevent(_findclass){
		var _n_array = _findclass.selector.split('.')		
		var _newClassName = _n_array[1];
		$("."+_newClassName).each(function(indx, obj){
				if($(obj).find(".owl-item").hasClass("active")){
					//setTimeout(function(){
						var _class = $(obj).find(".active");
						var _nclass = $(_class[0]).find(".colombiaad");
						var _id = $(_nclass).attr("id");
						if(typeof _id != 'undefined'){
							colombia.notifyColombiaAd(_id);							
							ctnAdcheck();
						}	
					//},0.5);
					
				}
			});
	}
	

	$("body").on("click",".hamburger-menu",function(e){
		e.preventDefault();
		if($(".navigation").hasClass("mobile-top-list-out")){
			$(".overlay").removeClass("display-none").addClass("humbig");
			$(".navigation").removeClass("mobile-top-list-out").addClass("mobile-top-list-in");
			$("body").addClass("overflowhidden");
		}
	});
	$("body").on("click",".humbig",function(e){
		e.preventDefault();
			$(".overlay").addClass("display-none").removeClass("humbig");
			$(".navigation").removeClass("mobile-top-list-in").addClass("mobile-top-list-out");
			$("body").removeClass("overflowhidden");
	});
	$("body").on("click",".click-tab",function(e){
		e.preventDefault();
		_tab = $(this).attr("data-tab");
		_bt = _tab.split("##");
		_closest = $(this).closest(".closest");
		$(_closest).find(".click-tab").removeClass("btn-color");
		$("."+_bt[1]).css("height","0");
		$(this).addClass("btn-color");
		$("#"+_bt[0]).css("height","auto");
		applyLazyLoad();
	});
	
	$("body").on("click",".gdgt_nav a",function(e){
		e.preventDefault();
		_tab = $(this).attr("data-tab-c");
		$(".gdgt_nav a").removeClass("active");
		$(".widgets-tab").css("height","0");
		$(this).addClass("active");
		$("#"+_tab).css("height","auto");
	});
	
	$(".check-carousel").each(function(){
		_this = this;
		var _carousel_list_count = $(_this).find(".newcarousel li").length; 
		var _list_width = $(_this).find(".newcarousel").attr("data-width-list");
		$(_this).find(".newcarousel").css("width",_carousel_list_count*_list_width);
	});
	$("body").on("click",".searchOpen",function(){
		if (window.history && history.pushState) history.pushState('', document.title, window.location.pathname); 
	});
	$(window).on('hashchange', function() {
		var hash = location.hash;
		if(hash == "#search"){
			$(".searchpage").addClass("searchon");
			$("#autocomplete_query").val('');
		}else{
			$(".searchpage").removeClass("searchon");
		}
	 }); 
	$(window).on('orientationchange', function(event) {
        console.log(orientation);
    });
});



function isScrolledIntoView(elem){
	//console.log(elem);
	if($(elem).offset() != undefined){
		var docViewTop = $(window).scrollTop();
		var docViewBottom = docViewTop + $(window).height();			
		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();			
		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));		
	}else{
		return false;
	}
}
function applyLazyLoad(){
			$(window).scroll(function(){displayImages();});
			$(window).resize(function(){displayImages();}); 
			displayImages();   
	function displayImages(){    
		$("img.lazy").each(function(indx,elem){
		var _src='img/defaultImage.jpg';
		var _data_original='img/defaultImage.jpg'; 
		if($(elem).attr("src") != undefined){_src=$(elem).attr("src")}
		if($(elem).attr("data-original") != undefined){_data_original=$(elem).attr("data-original")}       		
		if(isScrolledIntoView(elem) && $(elem).attr("src")!=$(elem).attr("data-original") && $(elem).hasClass("lazy")){
				$(elem).attr("src",$(elem).attr("data-original"));
				$(elem).removeClass("lazy");
				$(elem).error(function(){
					$(elem).attr("src",'img/defaultImage.jpg');
				})
			}
		});    	
	}	 
}