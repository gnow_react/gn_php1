﻿var flg=false;
var cardflg;
var emailValidation = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
var emailreg = /^[a-zA-Z0-9_\+-]+(\.[a-zA-Z0-9_\+-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.([A-Za-z]{2,4})$/;
var emailregIND = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
var alphachar= /^[a-z ]+$/i;
var userName= /^[-a-z0-9_ @.]{1,52}$/i;
var numOnly = /^[2-9][0-9]{1,3}$/i;
var cityname= /^[a-z() ]*$/i;
var alphanum = /^[a-z][-a-z0-9_ ]+$/i;
var upi = /([a-zA-Z0-9])*@[a-zA-Z]/i;
var alphanumnospace = /^[a-z][-a-z0-9_]+$/i;
var giftcard = /^[0-9a-zA-Z]+$/i; // alphanumber with no space -  gift card
var phone = /^[1-9][0-9-.]{7,15}$/i;
var phoneIND = /^[7-9]\d{9}$/i;
//var phoneIND = /^[1-9][0-9]{9}$/i;
var intnum=/^[0-9]+$/i;
var pincodeIND=/^[\d]{6}$/i;
var pincode=/^[\d]{6}$/i;
var dt=/(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/;
var zeros=/^0*$/;
var htmltag=/^[^<>%]+$/i;
var cvv1=/^[0-9]{3}$/i;
var cvv2=/^[0-9]{4}$/i;
var cardTypeOnPayment;
function DOM(id){return document.getElementById(id)}
function trim(str){return str.replace(/^\s+|\s+$/g,"");}
function checkLen(obj){return (trim(obj.value)).length;}
function onError(id,msg){$(".onError").removeClass("onError");jQuery('#'+id).val('').focus().addClass("onError");showErrorMsg(id,msg);return false;}
function onError2(errorField,id,msg){jQuery('#'+id).focus().addClass("onError");showErrorMsg(id,msg);return false;}

function showTooltipErrorMsg(id,msg){
		//$("#"+id).next(".errorMsg").addClass("tooltip").html(msg).show('slow');
		$('.tooltip').remove();
		$('<span>'+msg+'</span>').attr('class','tooltip').insertAfter("#"+id).show('slow');	
	}
function showErrorMsg(id,msg){
	    $(".errorMsg").html("&nbsp;");	  
		if($("#"+id+"_error").length>0){$("#"+id+"_error").addClass("errorMsg").html(msg).show(); }
			else{$("#"+id).parent().next(".message").addClass("errorMsg").html(msg).show(); }
	}	
function validate(obj, required,typ, msg){
	//var alert($(obj).attr('id'));
	var inputField = obj.getAttribute('id');
	var err=0;
	var errorField = '';
	var val=obj.value;
	var typVal=typ;
	typ= (typ.indexOf('cmpcheck~') != -1)? 'cmpcheck':typ;
	typ= (typ.indexOf('cardexpiry~') != -1)? 'cardexpiry':typ;
	switch(typ)
	{
		case 'blank':
		    var _placeholder_text="";
		    if($(obj).attr('placeholder') != 'undefined' ){
		    	_placeholder_text=$(obj).attr('placeholder');
		    }
			if(trim(val) == '' && required == 'true'){err=1;}
			if(trim(val) == _placeholder_text  && required == 'true'){err=1;}
			break;
		case 'chkexp':
			var monthid = $(obj).attr('id');
			monthid=monthid.replace('Year','Month');
			var cardDate=val+$('#'+monthid).val();
			cardDate=parseInt(cardDate);
			var cdate=new Date();
		    var cmon=("0" + (cdate.getMonth() + 1)).slice(-2);
		    var cyear=cdate.getFullYear();
		    var cmonthyear=cyear+cmon;
		    cmonthyear=parseInt(cmonthyear);
			if(cardDate < cmonthyear)
				{err=1;}
			break;
		case 'checklen':
			if(trim(val) == '' && required == 'true')
				err=1;
			else if(checkLen(obj) < 2 && trim(val) != '')
				err=1;
			break;
		case 'alphachar':
			if(trim(val) == '' && required == 'true')
				err=1;
			else if(alphachar.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'cityname':
			if(trim(val) == '' && required == 'true')
				err=1;
			else if(cityname.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;	
		case 'userName':
			if(trim(val) == '' || trim(val) == 'Your Username' && required == 'true')
				err=1;
			else if(userName.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'numOnly':
			if(trim(val) == '' && required == 'true' || zeros.test(trim(val)) == true && required == 'true')
				err=1;
			else if(numOnly.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'alphanum':
			if(trim(val) == '' && required == 'true' || zeros.test(trim(val)) == true && required == 'true')
				err=1;
			else if(alphanum.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'upi':
			if(trim(val) == '' && required == 'true' || zeros.test(trim(val)) == true && required == 'true')
				err=1;
			else if(upi.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'giftcard':
			if(trim(val) == '' && required == 'true' || zeros.test(trim(val)) == true && required == 'true')
				err=1;
			else if(giftcard.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'phone':
			if( trim(val) == '' && required == 'true')
				err=1;
			else if(phone.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'cvv1':
			if( trim(val) == '' && required == 'true')
				err=1;
			else if(cvv1.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'cvv2':
			if( trim(val) == '' && required == 'true')
				err=1;
			else if(cvv2.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'phoneIND':
			if( trim(val) == '' && required == 'true' || zeros.test(trim(val)) == true && required == 'true')
				err=1;
			else if(phoneIND.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;

		case 'intnum':
			if( trim(val) == '' && required == 'true' || zeros.test(trim(val)) == true && required == 'true')
				err=1;
			else if(intnum.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
			
		case 'pincodeIND':
			if(trim(val) == ''  && required == 'true' || zeros.test(trim(val)) == true && required == 'true')
				err=1;
			else if(pincode.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'pincode':
			if(trim(val) == ''  && required == 'true' || zeros.test(trim(val)) == true && required == 'true')
				err=1;
			else if(pincode.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'zero':
			if(trim(val) == '0'  && required == 'true' || trim(val) == ''  && required == 'true')
				err=1;
			break;
		case 'htmltag':
			if(trim(val) == '' && required == 'true' || zeros.test(trim(val)) == true && required == 'true')
				err=1;
			else if(htmltag.test(trim(val)) == false && trim(val) != '')
				err=1;
			break;
		case 'email':
			if(emailreg.test(trim(val)) == false  && required == true || emailreg.test(trim(val)) == false   && required == 'true')
				err=1;
			else if(trim(val) !='' && emailreg.test(trim(val)) == false)
				err=1;
			break;
		case 'date':
			if(dt.test(trim(val))== false)
				err=1;
			break;
		case 'cardexpiry':
			var d = new Date();
			var curr_month = parseInt(d.getMonth()+1);
			var curr_year = parseInt(String(d.getFullYear()).substring(2,4));
			var mnyrarr=typVal.split('~');
			var selMonth= parseInt(DOM(mnyrarr[1]).value);
			var selYear=parseInt(DOM(mnyrarr[2]).value);
			if(curr_year > selYear || (curr_year == selYear && selMonth < curr_month))
				err=1;
			break;
		case 'cmpcheck':
			var temp1= typVal.split('~');
			if(DOM(temp1[1]).checked == false || DOM(temp1[1]).checked == '')
				err=1;
			break;
		case 'password':
			if(trim(val).length < 6 || trim(val).length > 14)
				err=1;
			break;
		case 'confField':
			var elmID = jQuery('#'+inputField).attr('id');
			var preElemSplit = elmID.split('_');
			preElemSplit=preElemSplit[1];
			if(jQuery('#'+preElemSplit).val() != trim(val))
				err=1;
			break;
		case 'checkChecked':
			errorField= obj.getAttribute('name');
			var chk = document.getElementsByName(errorField);
			var k =0;
			for (j=0; j<chk.length; j++)
			{if(chk[j].checked){k++;}}
			if(k==0){err=2;}else{err=3;}
			break;
		default:
			break;
	}

	if(err == 1){return onError(inputField,msg);}
	else if(err == 2){return onError2(errorField,inputField,msg);}
	else if(err == 3){jQuery('#'+errorField+'_error').removeClass('errorMsg').html("&nbsp;");return true;}
	else{jQuery('#'+inputField+'_error').removeClass('errorMsg').hide();jQuery('#'+inputField).removeClass('onError');return true;}
}

function checkCardNum(obj,required,field_type){	
	cardflg=true;
	var curflg = true;
	var val=obj.value;
	var cardno=val;
	var mastcrd= /^[5][1-5][0-9]{14}$/i;
	
	if(trim(val) == ''  && required == 'true')
	{
		if(field_type=="ccNumber" || field_type=="dcNumber" || field_type=="ccNumberemi")
		{
			curflg = onError(field_type,'Please Enter Card Number');
		}
		else
		{
			curflg = onError(field_type,'Please Enter CVV Number');
		}
		return false;
	}
	if(field_type=="ccNumber" || field_type=="dcNumber" || field_type=="ccNumberemi")
	{
		 cardTypeOnPayment=getCardType(val);
	}
    $("#cardType").attr("value",cardTypeOnPayment);
	var selectedcard=cardTypeOnPayment;	
	if(field_type=='ccCVV' || field_type=='dcCVV' || field_type=='ccCVVemi')
		{
		selectedcard=selectedcard+"CVV";
		}
	var baseval=isValidCreditCard(cardno);
	switch(selectedcard){
		case 'VISA':
				if(baseval==0){
						//curflg = onError(field_type,'Please Enter a 16 Digit valid Visa Card Number');
					curflg = onError(field_type,'Please Enter a valid Card Number');
				}
				break;
		case 'VISACVV':
			if(!cvv1.test(trim(cardno)))
				curflg = onError(field_type,'Please Enter a 3 numeric value for CVV Number!');
			break;
		case 'MC':
			if(baseval==0)
					//curflg=onError(field_type,'Please Enter a 16 Digit valid Master Card Number');
				curflg = onError(field_type,'Please Enter a valid Card Number');
				break;
		case 'MCCVV':
			if(!cvv1.test(trim(cardno)))
					curflg=onError(field_type,'Please Enter a 3 numeric value for CVV Number!');
				break;
		case 'AMEX':
				if((trim(cardno)).length !=15 || (cardno.indexOf('37')!=0 && cardno.indexOf('34')!=0) )
				// curflg=onError(field_type,'Please Enter a 15 Digit valid American Express Card Number');
					curflg = onError(field_type,'Please Enter a valid Card Number');
				break;
		case 'AMEXCVV':
			if(!cvv2.test(trim(cardno)))
				curflg=onError(field_type,'Please Enter a 4 numeric value for CVV Number!');
			break;
		case 'MAEST':
				if(baseval==0){
						//curflg = onError(field_type,'Please Enter a 16 Digit valid Visa Card Number');
					curflg = onError(field_type,'Please Enter a valid Card Number');
				}
				break;			
		case '':
			curflg = onError(field_type,'Please Enter a valid Card Number');
			break;
	}
	if(curflg==true && ifEmiAvailable()==false){	
		curflg = onError(field_type,'EMI option is not available on this card.');		
	}if(field_type=="ccNumber" || field_type=="dcNumber" || field_type=="ccNumberemi"){
		if(curflg==true && (checkExcludedBin(cardno)==true || checkIncludedBin(cardno)==false || checkCardType(cardno)==false)){	
			curflg = onError(field_type,'Offer is not available on this card.');		
		}
	}
	
return curflg;

}

function checkExcludedBin(_card_num){	
		var _extracted_card_num=_card_num.substring(0,6);
		var _valid_arr=eval(checkoutPayment.exBinList);		
		for(n=0;n<_valid_arr.length;n++){
			if(_extracted_card_num.search(_valid_arr[n]) != -1){			
				return true;
			}		
		}return false;
}
function checkIncludedBin(_card_num){
		var _extracted_card_num=_card_num.substring(0,6);
		var _valid_arr=eval(checkoutPayment.inBinList);
		if(_valid_arr.length>0){
			for(n=0;n<_valid_arr.length;n++){
				if(_extracted_card_num.search(_valid_arr[n]) != -1){			
					return true;
				}		
			}return false;
		}else return true;
}

function checkCardType(_card_num){	
	var cardType = getCardType(_card_num);
	var _valid_arr=eval(checkoutPayment.payCardTypes);
	if(_valid_arr.length>0){
		for(n=0;n<_valid_arr.length;n++){
			if(cardType.search(_valid_arr[n]) != -1){			
				return true;
			}		
		}return false;
	}else return true;
}

function checkRadio(obj,required,radiotype,errmsg){
	var radioButtons="";
	var curflg = true;
	var radio=radiotype.split("-",2);
	$('#'+radio[1]+'_error').addClass('errorMsg').html("");
	radioButtons = $("input:radio[name='"+radio[1]+"']:checked").val();
	if(radioButtons ==undefined || radioButtons =='undefined'){$('#'+radio[1]+'_error').addClass('errorMsg').show().html(errmsg); curflg=false;}
return curflg;
}

function evalForm(obj){
	if (obj === undefined) {
		return true;
	}
	$(obj).find(".onError").removeClass("onError");
	$(obj).find(".errorMsg").removeClass("errorMsg").html("&nbsp");
	var flg= true;
	var cnt=0;
	for(i=0;i<obj.elements.length;i++)
	{
		var rel_data=obj.elements[i].getAttribute('validate');
		var rel_id=obj.elements[i].getAttribute('id');
		if(rel_data != null && !obj.elements[i].getAttribute('disabled'))
		{
			var val_data=(rel_data).substring(1,rel_data.length-1);
			var temp=val_data.split(',');
			var j=1;
			if(temp[3]){
				if(temp[3].indexOf('customCardNo') !=-1){
					flg = checkCardNum(obj.elements[i],temp[0],temp[4]);
					if(flg == false || flg == 'false'){
						break;
					}
				}
			}
			if(temp[1].indexOf('radio') !=-1){
				flg = checkRadio(obj,temp[0],temp[1],temp[2]);
			}
			for(var m=0;m<((temp.length-1)/2);m++){
				if(cnt==0){
					flg=validate(obj.elements[i],temp[0],temp[j],temp[j+1]);
					cnt=1;
				}else{
					if(flg == true){
						flg=validate(obj.elements[i],temp[0],temp[j],temp[j+1]);
					}else{
						flg = false;
					}
				}
				j=j+2;
			}
			if(flg == false || flg == 'false'){
				break;
			}
		}
	}
	return flg;
}

//script for NewsLetter emailId validation
function errorMessage(id)
{
		if($('#'+id).val()=="" || $('#'+id).val()=="Type Your Email")
		{
			$('#'+id+'_errorMsg').text('Please enter your Email Id');
			$('#'+id).addClass('onError').focus();
			return false;
		}
		else if(!emailValidation.test($('#'+id).val()))
		{
			$('#'+id+'_errorMsg').text('Invalid email address.Please use a valid email address');
			$('#'+id).addClass('onError').focus();
			return false;
		}
		else
		{
			var SubmitURL = $('#'+id).parents('form').attr('action');
			var formid = $('#'+id).parents('form').attr('id');
			$('#overeffect').show().css('height', $(document).height()+'px');
			$('#updating').show();
			var mailid = (formid == 'newsReqForm')?$('#newsEmail').val():$('#offeremail').val()
			$.getJSON(SubmitURL+'?contactListId='+$('#contactListId').val()+'&email='+mailid, function(data) {
				  var items = [];
				  var value = null ;
				  $.each(data, function(key, val) {
					  if(key == '_EVENT_MESSAGE_'){
						  value = val;
					  }	else if(key == '_ERROR_MESSAGE_LIST_')	{
						  value = val;
					  }else if(key == '_ERROR_MESSAGE_')	{
						  value = val;
					  }
				  });
				  if (value == 'Success') {
				  	if (formid == 'newsReqForm') {
				  		$('span.newsform').html('<span style="color:#fff"><b style="font-size:1.4em; color:#ffcc00">Thanks for Signing Up!</b><br />You will surely like us.</span>');
				  	}
				  	else {
				  		$('span.dealnewsform').html('<span><b style="font-size:1.4em; color:#ffcc00">Thanks for Signing Up!</b><br />You will surely like us.</span>');
				  	}
				  }else {
				  	if (formid == 'newsReqForm') {
				  		$('#newsEmail_errorMsg').html(value);
				  	}
				  	else {
				  		$('#dealnewsEmail_errorMsg').html(value);
				  	}
				  }
				  $('#updating, #overeffect').hide();
				});
			return false;
		}
}


/*---------  Dynamically change the CVV maxlength on detecting the card type ------------*/
		$("#ccNumber,#ccNumberemi,#dcNumber").blur(function(){
			cardTypeOnPayment=getCardType(this.value);
			var _length = 4;
			switch(cardTypeOnPayment){
			case 'VISA':
			    _length = 3;				
				break;
			case 'MC':
			    _length = 3;
				break;
			case 'AMEX':
				  _length = 4;
			default:	
				 _length = 4;
				break;	
			}
			$(".ccCVV").attr("maxlength",_length);
			$(".ccCVV").attr("value","");
		});



/*------------ get  credit/debit card type  ------------
//Logic involves identifying the card type based on its first digit
function getCardType(_val){	
	var _first_digit=_val.substr(0,1);
	var _card_type;
	switch(_first_digit){
	case '4':
		_card_type="VISA"; 
		break;
	case '5':
		_card_type="MC";
		break;
	case '3':
		_card_type="AMEX";
		break;
	default:
		_card_type="";
		break;
	}
	return _card_type;
	
}*/

/*-------------- eithr of two phone no validation---------------*/
$(document).ready(function(){
	$('.mobNum').blur(function(){
		var newval = "no";
		if($('#contactMobile').val().split(' ').join('')==""){
			if($('#contactLandline').val().split(' ').join('')==""){;
				$('#phonevalidate').val("");
			}else{
				newval=$('#contactLandline').val().split(' ').join('');
				$('#phonevalidate').val(newval);
			}
		}else{
			newval=$('#contactMobile').val().split(' ').join('');
			$('#phonevalidate').val(newval);
		}
	});  

/*
	$('.bmobNum').blur(function(){
	var newval = "no"
	if($('#bcontactMobile').val().split(' ').join('')==""){
		if($('#bcontactLandline').val().split(' ').join('')==""){
			$('#bphonevalidate').val("");
		}else{
			$('#bphonevalidate').val(newval);
		}
	}else{
		$('#bphonevalidate').val(newval);
	}
	}); 	
*/	
	
});
